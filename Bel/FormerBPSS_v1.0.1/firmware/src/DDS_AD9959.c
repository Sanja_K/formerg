/*
 * File:   DDS_AD9959.c
 * Author: Alexandr
 *
 * Created on September 20, 2017, 4:20 PM
 */
#include <xc.h>
#include "system_cfg.h"
#include "DDS_AD9959.h"
#include "synt_AD9959.h"

unsigned char   DDS1SWPstat, DDS2SWPstat;

uint8_t  Init_AD9959(int DDSn)
{
    uint8_t status;
      
    status = AD9959_begin(DDSn);
	
    return status;
}
void Stop_AD9959(int DDSn)
{
     AD9959_reset(DDSn);
}

 
  
// int  Set_param_AD9959_1RCH (uint64_t F_DDS) 
// {
//    int status;
////    AD9959_set_Phase(CH2,90);   
//    status = 0;
//    AD9959_set_Frequency(CH1,F_DDS); 
////     AD9959_set_Frequency(CH3,F_DDS);    
// 
//     return status;
// }
//  int  Set_param_AD9959_0RCH (uint64_t F_DDS) 
// {	int status;
//    status = 0;
//        
////    AD9959_set_Phase(CH2,90);
//    AD9959_set_Frequency(CH0,F_DDS);  
// //   AD9959_set_Frequency(CH2,F_DDS);      
//     return status;
// }
//   
  
// uint8_t  Set_param_AD9959_3RCH (uint64_t F_DDS) 
// {	uint8_t error_flg;
//    error_flg = 0;
//    
////    AD9959_set_Phase(CH2,90);   
//    
////    AD9959_set_Frequency(CH1,F_DDS); 
//     AD9959_set_Frequency(CH3,F_DDS);    
// 
//     return error_flg;
// }
//  uint8_t  Set_param_AD9959_2RCH (uint64_t F_DDS) 
// {	uint8_t error_flg;
//    error_flg = 0;
//        
////    AD9959_set_Phase(CH2,90);
// //   AD9959_set_Frequency(CH0,F_DDS);  
//    AD9959_set_Frequency(CH2,F_DDS);      
//     return error_flg;
// }  
  
  
// uint8_t Set_param_AD9959_1QPSK (uint64_t F_DDS1,uint64_t F_man)
// {	uint8_t Status;
    // Status = 0;

    
    // AD9959_set_Phase(CH2,90);   
    // AD9959_sweep_Phase(CH1,180,1);   
    // AD9959_sweep_Phase(CH2,270,1);           
    // AD9959_set_Frequency(CH12,F_DDS1);     // задаем частоту на два канала
     // return Status;
// }

// uint8_t Set_param_AD9959_2QPSK (uint64_t F_DDS2,uint64_t F_man)
// {	uint8_t Status;
    // Status = 0;

    // AD9959_set_Phase(CH2,90);
    
    
    // AD9959_sweep_Phase(CH1,180,1);   
    // AD9959_sweep_Phase(CH2,270,1);    

    // AD9959_set_Frequency(CH12,F_DDS2);     // задаем частоту на два канала
     // return Status;
// }
//-----------------------------------------------

uint8_t Set_param_AD9959_GPS_L1 (void)
{   uint8_t Status;  Status = 0;
    uint64_t F_DDS_start,  F_DDS_stop,  dF_scan;

    
    F_DDS_start = FDDS_start_GPS_L1;   
    F_DDS_stop = FDDS_stop_GPS_L1;    
    dF_scan = dFscan_GPS_L1;
    
 //   AD9959_set_Phase(CH2,90);       
    AD9959_set_Frequency  (DDS_CS1,CH2,F_DDS_start);     // задаем два канала
    AD9959_sweep_Frequency(DDS_CS1,CH2,F_DDS_stop,1);    
    AD9959_sweep_Rates    (DDS_CS1,CH2,dF_scan,10,dF_scan,10);       
     return Status;
}

uint8_t Set_param_AD9959_Glonass_L1 (void)
{   uint8_t Status; Status = 0;
    uint64_t F_DDS_start,  F_DDS_stop,  dF_scan;
    
    F_DDS_start = FDDS_start_GL_L1;    
    F_DDS_stop = FDDS_stop_GL_L1;    
    dF_scan = dFscan_GL_L1;
    
//    AD9959_set_Phase(CH3,90);        
    AD9959_set_Frequency   (DDS_CS2,CH0,F_DDS_start);     // задаем два канала
    AD9959_sweep_Frequency (DDS_CS2,CH0,F_DDS_stop,1);    
    AD9959_sweep_Rates     (DDS_CS2,CH0,dF_scan,10,dF_scan,10);   

     return Status;
}
//---------------------------------------------------
uint8_t Set_param_AD9959_Beidou_L1 (void)
{   uint8_t Status;  Status = 0;
    uint64_t F_DDS_start,  F_DDS_stop,  dF_scan;

    
    F_DDS_start = FDDS_start_BD_L1;   
    F_DDS_stop = FDDS_stop_BD_L1;    
    dF_scan = dFscan_BD_L1;
    
 //   AD9959_set_Phase(CH2,90);       
    AD9959_set_Frequency  (DDS_CS2,CH1,F_DDS_start);     // задаем два канала
    AD9959_sweep_Frequency(DDS_CS2,CH1,F_DDS_stop,1);    
    AD9959_sweep_Rates    (DDS_CS2,CH1,dF_scan,10,dF_scan,10);       
     return Status;
}

//---------------------------------------------------
uint8_t Set_param_AD9959_Galileo_L1 (void)
{   uint8_t Status;  Status = 0;
    uint64_t F_DDS_start,  F_DDS_stop,  dF_scan;

    
    F_DDS_start = FDDS_start_GLO_L1;   
    F_DDS_stop = FDDS_stop_GLO_L1;    
    dF_scan = dFscan_GLO_L1;
    
 //   AD9959_set_Phase(CH2,90);       
    AD9959_set_Frequency   (DDS_CS2,CH1,F_DDS_start);     // задаем два канала
    AD9959_sweep_Frequency (DDS_CS2,CH1,F_DDS_stop,1);    
    AD9959_sweep_Rates     (DDS_CS2,CH1,dF_scan,10,dF_scan,10);       
    return Status;
}
//*******************L2**************************
uint8_t Set_param_AD9959_GPS_L2 (void)
{   uint8_t Status;  Status = 0;
    uint64_t F_DDS_start,  F_DDS_stop,  dF_scan;

    
    F_DDS_start = FDDS_start_GPS_L2;   
    F_DDS_stop = FDDS_stop_GPS_L2;    
    dF_scan = dFscan_GPS_L2;
    
 //   AD9959_set_Phase(CH2,90);       
    AD9959_set_Frequency  (DDS_CS1,CH0,F_DDS_start);     //1
    AD9959_sweep_Frequency(DDS_CS1,CH0,F_DDS_stop,1);    
    AD9959_sweep_Rates    (DDS_CS1,CH0,dF_scan,10,dF_scan,10);       
     return Status;
}
//----------------------------------------------------
  //  AD9959_set_Phase(DDS1,CH3,90);
 //   AD9959_IOUpdate();     
  //  AD9959_set_Frequency(DDS1,CH03,F_DDS); 

uint8_t Set_param_AD9959_Glonass_L2 (void)
{    uint8_t Status; Status = 0;
    uint64_t F_DDS_start,  F_DDS_stop,  dF_scan;
    
    F_DDS_start = FDDS_start_GL_L2;    
    F_DDS_stop = FDDS_stop_GL_L2;    
    dF_scan = dFscan_GL_L2;
    
 //   AD9959_set_Phase(CH3,90);        
    AD9959_set_Frequency  (DDS_CS1,CH1,F_DDS_start);     // 2
    AD9959_sweep_Frequency(DDS_CS1,CH1,F_DDS_stop,1);    
    AD9959_sweep_Rates    (DDS_CS1,CH1,dF_scan,10,dF_scan,10);    
     return Status;
}
//---------------------------------------------------
uint8_t Set_param_AD9959_Beidou_L2 (void)
{   uint8_t Status;  Status = 0;
    uint64_t F_DDS_start,  F_DDS_stop,  dF_scan;

    
    F_DDS_start = FDDS_start_BD_L2;   
    F_DDS_stop = FDDS_stop_BD_L2;    
    dF_scan = dFscan_BD_L2;
    
 //   AD9959_set_Phase(CH2,90);       
    AD9959_set_Frequency  (DDS_CS1,CH3,F_DDS_start);     //1
    AD9959_sweep_Frequency(DDS_CS1,CH3,F_DDS_stop,1);    
    AD9959_sweep_Rates    (DDS_CS1,CH3,dF_scan,10,dF_scan,10);       
     return Status;
}
//---------------------------------------------------
uint8_t Set_param_AD9959_Galileo_L2 (void)
{   uint8_t Status;  Status = 0;
    uint64_t F_DDS_start,  F_DDS_stop,  dF_scan;

    
    F_DDS_start = FDDS_start_GLO_L2;   
    F_DDS_stop = FDDS_stop_GLO_L2;    
    dF_scan = dFscan_GLO_L2;
    
 //   AD9959_set_Phase(CH2,90);       
    AD9959_set_Frequency  (DDS_CS1,CH3,F_DDS_start);     //1
    AD9959_sweep_Frequency(DDS_CS1,CH3,F_DDS_stop,1);    
    AD9959_sweep_Rates    (DDS_CS1,CH3,dF_scan,10,dF_scan,10);       
     return Status;
}
//---------------------------------------------------
uint8_t Set_param_AD9959_GPS_Fclk (void)
{   uint8_t Status;  Status = 0;
    uint64_t F_DDS_start;

    
    F_DDS_start = F0DDS_GPS_Fclk;   

 //   AD9959_set_Phase(CH2,90);       
    AD9959_set_Frequency  (DDS_CS2,CH3,F_DDS_start);     //1
      
     return Status;
}
//---------------------------------------------------
uint8_t Set_param_AD9959_Glonass_Fclk (void)
{   uint8_t Status;  Status = 0;
    uint64_t F_DDS_start;

    
    F_DDS_start = F0DDS_GL_Fclk;   

 //   AD9959_set_Phase(CH2,90);       
    AD9959_set_Frequency  (DDS_CS2,CH2,F_DDS_start);     //1
      
     return Status;
}