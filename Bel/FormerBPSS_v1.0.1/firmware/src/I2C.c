/*
 * File:   I2C.c
 * Author: Alexandr
 *
 * Created on 27 сентября 2017 г., 14:08
 */


#include "xc.h"
#include "I2C.h"
#include "system_cfg.h" 

I2C_DATA_def i2c_data;

uint8_t I2CStartBusy(void)
{

    while( i2c_data.Status != I2C_STATUS_SUCCESS);  
    
    return MCHP_SUCCESS;
}


void MyI2CCallback(uintptr_t context)
{
    // This function will be called when the transfer completes. Note
    // that this functioin executes in the context of the I2C interrupt.
    i2c_data.Status = I2C_STATUS_SUCCESS;
    while(I2C1_IsBusy( ));
}

void I2CInit(void)
{
    i2c_data.Status = I2C_STATUS_IDLE;
    I2C1_CallbackRegister(MyI2CCallback, NULL);
    
}