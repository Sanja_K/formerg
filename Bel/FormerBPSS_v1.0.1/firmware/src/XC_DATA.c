/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/
#include <xc.h>         /* XC8 General Include File */
#include "system_cfg.h"
#include "XC_DATA.h"


uint8_t SetGPSL1, SetGLL1, SetGPSL2, SetGLL2,SetBDL1,SetBDL2,SetGLOL1,SetGLOL2;


void Init_XC(void)
{
    
    //---------- XC DATA ------------
//    TRISCbits.TRISC1 = 0;      
//    LATCbits.LATC1 = 0; //XC_v_o_DATA_0
//
//    TRISCbits.TRISC2 = 0;    
//    LATCbits.LATC2 = 0;//XC_v_o_CLK_0
//    
//    TRISCbits.TRISC3 = 0; 
//    TRISCbits.TRISC4 = 0;     
//    LATCbits.LATC3 = 0;//XC1_v_o_LE_0
//    LATCbits.LATC4 = 0;//XC2_v_o_LE_0    
//    
//    TRISGbits.TRISG13 = 0; // modulation on off
//    LATGbits.LATG13 = 0;
//    
//    TRISGbits.TRISG12 = 0; // modulation on off
//    LATGbits.LATG12 = 0;    
//    
//    
//    LATGbits.LATG14 = 0;       
//    TRISGbits.TRISG14 = 0; //  status
//
//    LATAbits.LATA6 = 1;     
//    TRISAbits.TRISA6 = 0; //  status rw
//    
//    LATAbits.LATA7 = 0;      
//    TRISAbits.TRISA7 = 0; //  status_strob

}
uint8_t  XC_LD_DATA( void)
{
    uint8_t LD_data;

    XC_ADR_status_0;
    XC_RW_status_1;
    XC_strob_status_0;
    delay_us (1);    
    XC_strob_status_1;
    delay_us (1);     
    LD_data = data_reg_XC_status;
    XC_strob_status_0;    

    return LD_data;
}





static void  XC_v_o_DATA( unsigned int ena )
{
    if(ena == 1)
        XC_v_o_DATA_1
    else
        XC_v_o_DATA_0;   
}    
static void  XC_writeReg1( unsigned int reg )
{ unsigned int i=0;
    XC_v_o_CLK_0;

    delay_us( 10 );

    for( i=0; i<16; i++ ) {
        XC_v_o_DATA( (reg>>15)&0x01 );
        delay_us( 10 );
        XC_v_o_CLK_1;
        reg = reg << 1;
        delay_us( 10 );
       XC_v_o_CLK_0;
    }
    delay_us( 10 );

} 
static void  XC_writeReg2( unsigned int reg )
{ unsigned int i=0;
    XC_v_o_CLK_0;

    delay_us( 10 );

    for( i=0; i<4; i++ ) {
        XC_v_o_DATA( (reg>>3)&0x01 ); // starshim vpered
        delay_us( 10 );
        XC_v_o_CLK_1;
        reg = reg << 1;
        delay_us( 10 );
       XC_v_o_CLK_0;
    }
    delay_us( 10 );

} 
void Init_XC_GPSGL (void)
{  
     unsigned int XCx_dt;
     
     
     XCx_dt = (SetGPSL1<<2)|(SetGLL1<<3)|(SetGPSL2<<1)|(SetGLL2); 
     XC1_v_o_LE_1;
     XC_writeReg2(XCx_dt);

     XC1_v_o_LE_0;
    
}
void Init_XC_BDGLO (void)
{  
     unsigned int XCx_dt;
     
     
     XCx_dt = (SetBDL1<<2)|(SetGLOL1<<3)|(SetBDL2<<1)|(SetGLOL2); 
     XC1_v_o_LE_1;
     XC_writeReg2(XCx_dt);

     XC1_v_o_LE_0;
    
}
uint8_t Init_XC_freq (unsigned char DDSx, uint64_t F_man)
{  
     unsigned int XCx_dt;
     if (F_man > 1*MHz)
     { return 1; } // 
     
     XCx_dt = 65536 - (25000000/F_man); 
     
     XC2_v_o_LE_1; 

     XC_writeReg1(XCx_dt);

     XC2_v_o_LE_0;
     return 0;
}

uint8_t Init_XC_ph (unsigned char DDSx, uint64_t F_man)
{  
     unsigned int XCx_dt;
     if (F_man > 20*MHz)
     { return 1; } //
     
     XCx_dt = 65536 - (25000000/(2*F_man)); 
     
     XC2_v_o_LE_1; 

     XC_writeReg1(XCx_dt);

      XC2_v_o_LE_0;
     return 0;
}