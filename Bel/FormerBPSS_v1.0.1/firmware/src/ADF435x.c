//#include "system.h"
#include "ADF435x.h"
//#include "synt_AD4350.h"
//#include "synt_AD4351.h"
#include "synt_AD4351.h"

//#include "modulator.h"
#include "system_cfg.h"


struct ADF4351_initDef ADF4351_1_Init;
struct ADF4351_initDef ADF4351_2_Init;
struct ADF4351_initDef ADF4351_3_Init;

struct ADF4351_initDef *pADF4351_Init; 


uint8_t ADF4351_Init_num (int ADF4351_LE_num)
{
    
    if (ADF4351_LE_num == ADF4351_LE1)
    {
        pADF4351_Init = &ADF4351_1_Init; 
        
    }  
    else if (ADF4351_LE_num == ADF4351_LE2)
    {    
        pADF4351_Init = &ADF4351_2_Init;
    }        
    else if (ADF4351_LE_num == ADF4351_LE3)
    {
        pADF4351_Init = &ADF4351_3_Init;
    }  
    else
    {
       return MCHP_DEVICE_NOT_FOUND;
    }       
    
    return MCHP_SUCCESS;    
}

uint8_t  ADF4351_setFreq_num (int ADF4351_LE_num, uint64_t AD4351_Freq_Hz)
{
	uint8_t Status;
    Status = ADF4351_Init_num(ADF4351_LE_num);  
    pADF4351_Init->AD4351_Freq_Hz = AD4351_Freq_Hz;
    Status = ADF4351_setFreq(pADF4351_Init);
    
    return Status;
}
uint8_t  ADF4351_PWUP_num (int ADF4351_LE_num)
{
	uint8_t Status;
    Status = ADF4351_Init_num(ADF4351_LE_num);  
    Status = ADF4351_PWUP(pADF4351_Init);
    
    return Status;
}

 uint8_t ADF4351_update_REG_num(int ADF4351_LE_num)
{
	uint8_t Status;
	Status = ADF4351_Init_num(ADF4351_LE_num); // poluch adres struktury registrov
	
    ADF4351_update_REG(pADF4351_Init);             
    
    return Status;
 }
 
 
uint8_t Set_RFoff_ADFApp (int ADF4351_LE_num)
{
    uint8_t Status;
	Status = ADF4351_Init_num(ADF4351_LE_num); // poluch adres struktury registrov
	
    ADF4351_PWDWN (pADF4351_Init);   
    ADF4351_update_REG(pADF4351_Init);     
    return Status;
}


uint8_t Init_ADFApp (int ADF4351_LE_num, uint64_t freq_MHz)
{
    struct ADF4351_initDef ADF4351x_Init = {1,0,0,0,4095,enum_ADF_MUX_OUT_digitalLockDetect,enum_ADF_lockDetectFunction_digitalLockDetect,enum_ADF_outputPower_2dBm,enum_ADF_outputPower_disabled,0};
  
    unsigned long int CHSPACE = 200000; // 200  
    uint64_t  AD4351_Freq_Hz = freq_MHz *MHz;//40*MHz;// 35 MHz to 4400 MHz          
    uint8_t status;
		
	status = ADF4351_Init_num(ADF4351_LE_num);	

	*pADF4351_Init = ADF4351x_Init;
		
	if (ADF4351_LE_num == ADF4351_LE1)
	{
        pADF4351_Init->ADF4351_LE_num = ADF4351_LE1;
	 // uint64_t  AD4351_Freq_Hz = 160*MHz;//160*MHz; // 54 MHz to 6800 MHz   
		pADF4351_Init->reference_freq_Hz =  25*MHz;
		pADF4351_Init->phaseDet_freq_Hz  =  25*MHz;

		pADF4351_Init->freq_modulusValue = pADF4351_Init->phaseDet_freq_Hz/CHSPACE; 
	  //  ADF4351_1_Init.enum_ADF_outputPower_RFOUT_A =  enum_ADF_outputPower_m1dBm;
	  //  ADF4351_1_Init.enum_ADF_outputPower_RFOUT_B =  enum_ADF_outputPower_m1dBm;
		pADF4351_Init->enum_ADF_outputPower_RFOUT_A =  enum_ADF_outputPower_2dBm;
		pADF4351_Init->enum_ADF_outputPower_RFOUT_B =  enum_ADF_outputPower_disabled;  
		pADF4351_Init->enum_ADF_MUX_OUT =  enum_ADF_MUX_OUT_R_dividerOutput;//enum_ADF_MUX_OUT_R_dividerOutput;// enum_ADF_MUX_OUT_digitalLockDetect; 		
        
	}
	else if  (ADF4351_LE_num == ADF4351_LE2)
	{
		pADF4351_Init->ADF4351_LE_num = ADF4351_LE2;
	 // uint64_t  AD4351_Freq_Hz = 160*MHz;//160*MHz; // 54 MHz to 6800 MHz   
		pADF4351_Init->reference_freq_Hz =  25*MHz;
		pADF4351_Init->phaseDet_freq_Hz  =  25*MHz;

		pADF4351_Init->freq_modulusValue = pADF4351_Init->phaseDet_freq_Hz/CHSPACE; 
		pADF4351_Init->enum_ADF_outputPower_RFOUT_A =  enum_ADF_outputPower_2dBm;
		pADF4351_Init->enum_ADF_outputPower_RFOUT_B =  enum_ADF_outputPower_disabled;  
		pADF4351_Init->enum_ADF_MUX_OUT =  enum_ADF_MUX_OUT_R_dividerOutput;//enum_ADF_MUX_OUT_R_dividerOutput;// enum_ADF_MUX_OUT_digitalLockDetect; 		
		
		
	}
	else if  (ADF4351_LE_num == ADF4351_LE3)
	{
	    pADF4351_Init->ADF4351_LE_num = ADF4351_LE3; 
		pADF4351_Init->reference_freq_Hz =  25*MHz;
		pADF4351_Init->phaseDet_freq_Hz  =  25*MHz;

		pADF4351_Init->freq_modulusValue = pADF4351_Init->phaseDet_freq_Hz/CHSPACE; 
		pADF4351_Init->enum_ADF_outputPower_RFOUT_A =  enum_ADF_outputPower_2dBm;
		pADF4351_Init->enum_ADF_outputPower_RFOUT_B =  enum_ADF_outputPower_disabled;  
		pADF4351_Init->enum_ADF_MUX_OUT =  enum_ADF_MUX_OUT_R_dividerOutput;//enum_ADF_MUX_OUT_R_dividerOutput;// enum_ADF_MUX_OUT_digitalLockDetect; 		

	}
	else
	{
	   return MCHP_DEVICE_NOT_FOUND;
	}
	status = ADF4351_initialize(pADF4351_Init);            
  //ADF4351_v_o_RFOUT1_EN;                
	if (status != MCHP_SUCCESS)
	{
		return MCHP_CMD_FAILURE;
	}   
	
	status = ADF4351_setFreq_num(ADF4351_LE_num, AD4351_Freq_Hz);  

	if (status != MCHP_SUCCESS)
	{
		return MCHP_CMD_FAILURE;
	}        
//	
	ADF4351_PWUP_num (ADF4351_LE_num);
//	delay_ms(1);
//	//  ADF4351_PWDWN ();
	ADF4351_update_REG_num(ADF4351_LE_num);  
        
        return MCHP_SUCCESS;
}



