/*
 * File:   UART.c
 * Author: Alexandr Kazeka
 *
 * Created on September 20, 2017, 4:20 PM
 */
#include "system_cfg.h"        /* System funct/params, like osc/peripheral config */
#include "ADF435x.h" 
#include "synt_AD4351.h"        /* System funct/params, like osc/peripheral config */

#define CHSPACE1   1000000 // 1 ���

   // 1 - lock, 0 - not, -1 - �� ��������������


//int ADF4351_LE_num = ADF4351_LE1;


uint8_t  ADF4351_check_lockDetect( void )
{   char px;

    if (ADF4351_v_i_LD == 1)
        px = 0; 
    else
        px = 1;
  
    return  px;
}

uint8_t  ADF4351_check_MUX_OUT( void )
{  char px;

//    if (ADF4351_v_i_MUX_OUT == 1)
//     px = 0; 
//    else
     px = 1;
    return  px;
}

static void  ADF4351_v_o_DATA (uint32_t ena)
{
    if(ena == 1)
        ADF4351_v_o_DATA_1;
    else
        ADF4351_v_o_DATA_0;   
}    


uint8_t  ADF4351_v_writeReg (int ADF4351_LE_num, uint32_t reg)
{
    unsigned int i=0;
    ADF4351_v_o_CLK_0;
//    ADF4351_v_o_CE_1;     
    //-----------------------------------------------
    if (ADF4351_LE_num == ADF4351_LE1)
    {
        ADF4351_v_o_LE1_0;
    }
    else if (ADF4351_LE_num == ADF4351_LE2)
    {
        ADF4351_v_o_LE2_0;
    }
    else if (ADF4351_LE_num == ADF4351_LE3)
    {
        ADF4351_v_o_LE3_0;
    }
    else
    {
        return MCHP_DEVICE_NOT_FOUND;
    }
	//-----------------------------------------------------
    delay_us( 10 );

    for( i=0; i<32; i++ ) 
    {
        ADF4351_v_o_DATA( (reg>>31)&1 );
        delay_us( 10 );
        ADF4351_v_o_CLK_1;
        reg = reg << 1;
        delay_us( 10 );
        ADF4351_v_o_CLK_0;
    }
    delay_us( 10 );

    ADF4351_v_o_LE1_1;
    ADF4351_v_o_LE2_1;
    ADF4351_v_o_LE3_1;  
//    ADF4351_v_o_CE_0;
    
    return MCHP_SUCCESS;     
} 
  
uint8_t  ADF4351_initialize(struct ADF4351_initDef *pADF4351_Init)//
{

   if( pADF4351_Init->reference_freq_Hz == 0)
   {   
       return MCHP_FAILURE;
   }//error

   if( pADF4351_Init->phaseDet_freq_Hz == 0)  
   {   
		pADF4351_Init->phaseDet_freq_Hz = pADF4351_Init->reference_freq_Hz;
   }
    // ADF4351_v_o_RFOUT_nEN();
    ADF4351_v_o_LE1_1;  
    ADF4351_v_o_LE2_1; 
    ADF4351_v_o_LE3_1;  
    ADF4351_v_o_CLK_0;
    ADF4351_v_o_DATA(0);

    // 
//  ADF4351_v_o_CE_1;
     
    // reset reg

//    pADF4351_Init->ADF4351_RegNum.register5 =  0x000400005; // 140 MHz
//    pADF4351_Init->ADF4351_RegNum.register4 =  0x000C28024; // 0x000C28024; // 0x000C28124
//    pADF4351_Init->ADF4351_RegNum.register3 =  0x0000004B3;
//    pADF4351_Init->ADF4351_RegNum.register2 =  0x018014EC2;
//    pADF4351_Init->ADF4351_RegNum.register1 =  0x008008011;
//    pADF4351_Init->ADF4351_RegNum.register0 =  0x000E00000;
    
    pADF4351_Init->ADF4351_RegNum.register5 =  0x000580005; // 140 MHz
    pADF4351_Init->ADF4351_RegNum.register4 =  0x0009C803C; // 0x000C28024; // 0x000C28124
    pADF4351_Init->ADF4351_RegNum.register3 =  0x0000004B3;
    pADF4351_Init->ADF4351_RegNum.register2 =  0x00C004E42;
    pADF4351_Init->ADF4351_RegNum.register1 =  0x008008011;
    pADF4351_Init->ADF4351_RegNum.register0 =  0x000430000;    
  
        
    // 0 = Enables the fractional-N mode; 1 = Enables the integer-N mode; The LDF bit must also be set to the appropriate mode.
    // ��������� �������� �������� FRAC1 = 0; FRAC2 = 0

    if(!pADF4351_Init->mode_INT_nFRAC)
    {  pADF4351_Init->ADF4351_RegNum.Reg0.FRAC = 0;}   
 
    // MUX_OUT  
    pADF4351_Init->ADF4351_RegNum.Reg2.MUXOUT = ( (unsigned)pADF4351_Init->enum_ADF_MUX_OUT>>0 )&0x7;
    // R divider
    unsigned  Rdiv = pADF4351_Init->reference_freq_Hz / pADF4351_Init->phaseDet_freq_Hz; // � �����������
    if( Rdiv<1 || Rdiv>1023 )
	{	return  MCHP_FAILURE;}
	
    pADF4351_Init->ADF4351_RegNum.Reg2.REFCNT = Rdiv;
   
    if (pADF4351_Init->mode_INT_nFRAC)
	{
        pADF4351_Init->ADF4351_RegNum.Reg2.LDF = 1; // LD MODE 0 = FRACTIONAL-N; 1 = INTEGER-N (2.9ns) ���� mode_INT_nFRAC = true 1 = INTEGER-N
        pADF4351_Init->ADF4351_RegNum.Reg2.LDP = 0; //
    }
    else
    {
        pADF4351_Init->ADF4351_RegNum.Reg2.LDF = 0; // 
        pADF4351_Init->ADF4351_RegNum.Reg2.LDP = 1; //        
    }    
    
  

    pADF4351_Init->ADF4351_RegNum.Reg1.MOD = pADF4351_Init->freq_modulusValue;
            
            // Lock detect function
    pADF4351_Init->ADF4351_RegNum.Reg5.LDPM = pADF4351_Init->enum_ADF_lockDetectFunction;

    pADF4351_Init->ADF4351_RegNum.Reg2.PWRDN = 1;
    delay_ms(10);
    
    // output power ch A
    ADF4351_setPowerLevel(pADF4351_Init,enum_ADF_RFOUT_A);
   //  output power ch B
    ADF4351_setPowerLevel(pADF4351_Init,enum_ADF_RFOUT_B);  

    
//    pADF4351_Init->ADF4351_RegNum.register5 =  0x000580005; // 140 MHz
//    pADF4351_Init->ADF4351_RegNum.register4 =  0x0009C803C; // 0x000C28024; // 0x000C28124
//    pADF4351_Init->ADF4351_RegNum.register3 =  0x0000004B3;
//    pADF4351_Init->ADF4351_RegNum.register2 =  0x00C004E42;
//    pADF4351_Init->ADF4351_RegNum.register1 =  0x008008011;
//    pADF4351_Init->ADF4351_RegNum.register0 =  0x000430000;  
    
    
//    pADF4351_Init->ADF4351_RegNum.register5 =  0x000580005; // 140 MHz
//    pADF4351_Init->ADF4351_RegNum.register4 =  0x0008C802C; // 0x000C28024; // 0x000C28124
//    pADF4351_Init->ADF4351_RegNum.register3 =  0x0000004B3;
//    pADF4351_Init->ADF4351_RegNum.register2 =  0x00C004E42;
//    pADF4351_Init->ADF4351_RegNum.register1 =  0x008008011;
//    pADF4351_Init->ADF4351_RegNum.register0 =  0x000460000;    
    
    return  MCHP_SUCCESS;
} 
  
  
  
uint8_t  ADF4351_setFreq(struct ADF4351_initDef *pADF4351_Init)
{
  uint64_t quot,rem; // �������, ������� 
  
  uint64_t fVCO;
  uint64_t freq_Hz;
  uint64_t fVCO_min = 2.2 * 1000000000LL; // ��� ������� VCO 2.2GHz to 4.4GHz
  uint64_t Result;
  uint32_t FRAC, MOD;
  uint16_t INT;
  unsigned divA = 0;
  
  freq_Hz = pADF4351_Init->AD4351_Freq_Hz;
  fVCO = freq_Hz;
  
 if ((freq_Hz < 35*MHz)&&( 4400*MHz < freq_Hz))      
 {
     return  MCHP_FAILURE;
 } // �������� ������� ��� ���������
  // ������������ �������� RF = divA

    while( fVCO < fVCO_min ) 
	{
        fVCO = fVCO * 2;
        if( ++divA == 6 ) 
		{break;}
    }
    if( fVCO<fVCO_min ) 
	{	fVCO = fVCO_min;}
  // ���������� ��������
    pADF4351_Init->ADF4351_RegNum.Reg4.RFDIV = divA;
  // ������������ �������

    LLDIV (fVCO,pADF4351_Init->phaseDet_freq_Hz,&quot,&rem);  
 
    INT = (uint16_t)quot;
    
    if ((INT>23)&&(INT<32767))
    {   
		pADF4351_Init->ADF4351_RegNum.Reg1.PRESC = 0; // PRESCALER 0 = 4/5  �� 23 .. 32767
	}
    else if ((INT>75)&&(INT<65535))
	{  
		pADF4351_Init->ADF4351_RegNum.Reg1.PRESC = 1; //1 = 8/9    1= 8/9  �� 75...65535
	}
    else  
	{	return  MCHP_FAILURE;}
    
    pADF4351_Init->ADF4351_RegNum.Reg0.INT = INT; //  = INT ADF
    
    Result = rem; // �������
    
    MOD = pADF4351_Init->freq_modulusValue; //����� ������� �� ������

    // ��������� ����� � ������� ����� FRAC1 = MOD1 * rem / InitData.phaseDet_freq_Hz

    Result = Result*MOD; // �������� ���������� FRAC �����
    
     
    FRAC = Result/pADF4351_Init->phaseDet_freq_Hz; // �������� ����� �����
    pADF4351_Init->ADF4351_RegNum.Reg0.FRAC = FRAC;

    
   delay_ms(1); 
  return MCHP_SUCCESS;
  
  }
  
uint8_t  ADF4351_disable( void )
{
    // ������ ��������� ����������� � ��������� ���������
    ADF4351_v_o_CLK_0;
    ADF4351_v_o_DATA(0);
    ADF4351_v_o_LE1_1;
    ADF4351_v_o_LE2_1;
    ADF4351_v_o_LE3_1;     
//    ADF4351_v_o_LE_0();
//    ADF4351_v_o_RFOUT_nEN();

    // ��������� ����������
//    ADF4351_v_o_CE_0;

    return  MCHP_SUCCESS;
}

uint8_t  ADF4351_PWDWN(struct ADF4351_initDef *pADF4351_Init)
{
        pADF4351_Init->ADF4351_RegNum.Reg2.PWRDN = 1;
        return MCHP_SUCCESS;
}
uint8_t  ADF4351_PWUP(struct ADF4351_initDef *pADF4351_Init)
{
        pADF4351_Init->ADF4351_RegNum.Reg2.PWRDN = 0;
        return MCHP_SUCCESS;
}

uint8_t  ADF4351_setCH(struct ADF4351_initDef *pADF4351_Init, uint8_t CH)
{
    switch (CH) {
        case RFCH1:   // RF1  Z12  Z21
            // 00
        //  ADF4351_Reg->Reg4.RFAEN = 0;
            pADF4351_Init->ADF4351_RegNum.Reg4.RFBEN = 1;
        break;    
        case RFCH2:   
            // 10     //RF3  Z13  Z22
        //  ADF4351_Reg->Reg4.RFAEN = 0;
            pADF4351_Init->ADF4351_RegNum.Reg4.RFBEN = 1;
   
        break;      
        case RFCH3:   
            // 11    //RF4   Z14  Z23  
            pADF4351_Init->ADF4351_RegNum.Reg4.RFAEN = 1;
            pADF4351_Init->ADF4351_RegNum.Reg4.RFBEN = 0;
   
        break;           
        case RFCH4:   //RF2   Z15  Z24
            // 01    
            pADF4351_Init->ADF4351_RegNum.Reg4.RFAEN = 1;
            pADF4351_Init->ADF4351_RegNum.Reg4.RFBEN = 0;
        break;
		default:
			return MCHP_FAILURE; 
	     break;	
    }
    return MCHP_SUCCESS;    
}


uint8_t  ADF4351_setPowerLevel(struct ADF4351_initDef *pADF4351_Init,enum_ADF_RFOUT RFOUT)
{
    // out Power A
    if( (RFOUT == enum_ADF_RFOUT_A) || (RFOUT==enum_ADF_RFOUT_both) ) 
	{
        if( pADF4351_Init->enum_ADF_outputPower_RFOUT_A==enum_ADF_outputPower_disabled ) 
		{
            pADF4351_Init->ADF4351_RegNum.Reg4.RFAEN = 0;
            pADF4351_Init->ADF4351_RegNum.Reg4.RFAPWR = 0;
        } else 
		{
            pADF4351_Init->ADF4351_RegNum.Reg4.RFAEN = 1;
            pADF4351_Init->ADF4351_RegNum.Reg4.RFAPWR = (unsigned)pADF4351_Init->enum_ADF_outputPower_RFOUT_A & 3;
        }
    }

    // out Power B
    if( RFOUT==enum_ADF_RFOUT_B || RFOUT==enum_ADF_RFOUT_both ) 
	{
        if( pADF4351_Init->enum_ADF_outputPower_RFOUT_B==enum_ADF_outputPower_disabled ) 
		{
            pADF4351_Init->ADF4351_RegNum.Reg4.RFBEN = 0;
            pADF4351_Init->ADF4351_RegNum.Reg4.RFBPWR = 0;
        } else 
        {
            pADF4351_Init->ADF4351_RegNum.Reg4.RFBEN = 1;
            pADF4351_Init->ADF4351_RegNum.Reg4.RFBPWR = (unsigned)pADF4351_Init->enum_ADF_outputPower_RFOUT_B & 3;
        }
    }
//  rf_Synt_ADF4351_v_writeReg( Reg->register6 );
    return MCHP_SUCCESS;
}

uint8_t  ADF4351_update_REG( struct ADF4351_initDef *pADF4351_Init)
 {
    pADF4351_Init->ADF4351_LE_num = ADF4351_LE2;
    
    //pADF4351_Init->ADF4351_LE_num = ADF4351_LE1;    
//    pADF4351_Init->ADF4351_RegNum.register5 =  0x000580005; // 140 MHz
//    pADF4351_Init->ADF4351_RegNum.register4 =  0x0008C802C; // 0x000C28024; // 0x000C28124
//    pADF4351_Init->ADF4351_RegNum.register3 =  0x0000004B3;
//    pADF4351_Init->ADF4351_RegNum.register2 =  0x00C004E42;
//    pADF4351_Init->ADF4351_RegNum.register1 =  0x008008011;
//    pADF4351_Init->ADF4351_RegNum.register0 =  0x000460000;
    
    pADF4351_Init->ADF4351_RegNum.register5 =  0x000580005; // 140 MHz
    pADF4351_Init->ADF4351_RegNum.register4 =  0x0009C803C; // 0x000C28024; // 0x000C28124
    pADF4351_Init->ADF4351_RegNum.register3 =  0x0000004B3;
    pADF4351_Init->ADF4351_RegNum.register2 =  0x00C004E42;
    pADF4351_Init->ADF4351_RegNum.register1 =  0x008008011;
    pADF4351_Init->ADF4351_RegNum.register0 =  0x000430000;  

    
    ADF4351_v_writeReg(pADF4351_Init->ADF4351_LE_num, pADF4351_Init->ADF4351_RegNum.register5);
    ADF4351_v_writeReg(pADF4351_Init->ADF4351_LE_num, pADF4351_Init->ADF4351_RegNum.register4);
    ADF4351_v_writeReg(pADF4351_Init->ADF4351_LE_num, pADF4351_Init->ADF4351_RegNum.register3);
    ADF4351_v_writeReg(pADF4351_Init->ADF4351_LE_num, pADF4351_Init->ADF4351_RegNum.register2);
    ADF4351_v_writeReg(pADF4351_Init->ADF4351_LE_num, pADF4351_Init->ADF4351_RegNum.register1);  
    ADF4351_v_writeReg(pADF4351_Init->ADF4351_LE_num, pADF4351_Init->ADF4351_RegNum.register0); 
    
    return  MCHP_SUCCESS;   
     
 }