/******************************************************************************/
/*  Level #define Macros                                                */
/******************************************************************************/

// 

#ifndef XC_ADF4351_H
#define XC_ADF4351_H

	#include <xc.h> 
	#include <stdint.h>         /* For uint8_t definition */
	#include <stdbool.h>        /* For true/false definition */
	#include <stdlib.h>        /* For true/false definition */
	#include "definitions.h" 

   
//rf_Synt_ADF4351_initDef  InitData;
#define ADF4351_LE1  1
#define ADF4351_LE2  2  
#define ADF4351_LE3  3
  
//--------------------------------------------------  

#define ADF4351_v_o_DATA_0 SDATA_ADF_Clear()
#define ADF4351_v_o_DATA_1 SDATA_ADF_Set() 
  
#define ADF4351_v_o_CLK_0  SCLK_ADF_Clear()
#define ADF4351_v_o_CLK_1  SCLK_ADF_Set() 
  
//--------------------------------------------------  
 
#define ADF4351_v_o_LE1_0 LE_ADF1_Clear() 
#define ADF4351_v_o_LE1_1 LE_ADF1_Set()   
  
#define ADF4351_v_o_LE2_0 LE_ADF2_Clear()
#define ADF4351_v_o_LE2_1 LE_ADF2_Set() 

#define ADF4351_v_o_LE3_0 LE_ADF3_Clear()
#define ADF4351_v_o_LE3_1 LE_ADF3_Set()  
//----------------------------------------------------  
//#define ADF4351_v_o_RFOUT1_nEN LATDbits.LATD4 = 0;
//#define ADF4351_v_o_RFOUT1_EN  LATDbits.LATD4 = 1;  
//  
// #define ADF4351_v_o_RFOUT2_nEN  LATBbits.LATB10 = 0;
// #define ADF4351_v_o_RFOUT2_EN   LATBbits.LATB10 = 1;

//#define ADF4351_v_o_CE_0 LATAbits.LATA2 = 0;//test
//#define ADF4351_v_o_CE_1 LATAbits.LATA2 = 1;   
  
#define ADF4351_v_i_LD  PORTGbits.RG2 //lockDetect  
#define ADF4351_v_i_LD2 PORTGbits.RG3 //lockDetect     
  
//#define ADF4351_v_i_MUX_OUT PORTBbits.RB8// MUX_OUT  

struct  ADF4351_Reg_typeDef{
	//  Register 0 (Address: 000, Default:  HEX)
	union {
		uint32_t   register0;
		struct {
			unsigned  ADDR_0      :3;  // Register address bits - Устанавливается при загрузке
            unsigned  FRAC        :12; //FRACTIONAL VALUE (FRAC) 0 - 4095
			unsigned  INT         :16; // 0 .... 65535 Integer Division Value. Sets integer part (N-divider) of the feedback divider factor. All integer values from 16 to 65,535 are allowed for integer mode. Integer values from 19 to 4,091 are allowed for fractional mode
			unsigned  RES1        :1;  // RESERVED 0
		} __attribute__((packed))Reg0;
	};
	//  Register 1 (Address: 001, Default:  HEX)
	union {
		uint32_t   register1;
		struct {
			unsigned   ADDR_1      :3;  // Register address bits - Устанавливается при загрузке
			unsigned   MOD         :12; // INTERPOLATOR MODULUS (MOD) 2 - 4095
 			unsigned   PHASE       :12; // PHASE VALUE (PHASE) 0 - 4095; 1-(RECOMMENDED)
 			unsigned   PRESC       :1; //  PRESCALER 4/5 8/9            
			unsigned   RES1        :4; // RESERVED 0000
		} __attribute__((packed))Reg1;
	};
	// Register 2 (Address: 010, Default:  HEX)
	union {
		uint32_t   register2;
		struct {
			unsigned   ADDR_2     :3;  // Register address bits - Устанавливается при загрузке
			unsigned   CNTRES     :1;  // COUNTER RESET 0 = DISABLED; 1 = ENABLED;
            unsigned   CPTS       :1;//CP THREE-STATE 0 = DISABLED; 1 = ENABLED;
            unsigned   PWRDN      :1;//POWER DOWN 0 = DISABLED; 1 = ENABLED;
            unsigned   PD         :1;//PD POLARITY 0 = NEGATIVE; 1 = POSITIVE;            
            unsigned   LDP        :1;//LDP 0 = 10ns; 1 = 6ns;    
            unsigned   LDF        :1;//LDF 0 = FRAC-N; 1 = INT-N;  
            unsigned  CPCR        :4;// CHARGE PUMP CURRENT  0 - 15 (0.31- 5mA)
            unsigned  DBF         :1; //DOUBLE BUFF 0 = DISABLED; 1 = ENABLED;
            unsigned  REFCNT      :10; //R DIVIDER (R) 1-1023
            unsigned  REFDIV      :1; //REFERENCE DIVIDE BY 2     0 = DISABLED; 1 = ENABLED;      
            unsigned  REFDB       :1; //REFERENCE DOUBLER      0 = DISABLED; 1 = ENABLED;            
            unsigned   MUXOUT     :3;  // OUTPUT
             /* 0 - THREE-STATE OUTPUT
             * 1 - DVdd
             * 2 - DGnd
             * 3 - R DIVIDER OUTPUT
             * 4 - N DIVIDER OUTPUT
             * 5 - ANALOG LOCKDETECT
             * 6 - DIGITAL LOCKDETECT
             * 7 - RESERVED
             */
            unsigned   LNMOD       :2;  // LOW NOISE MODE      0 - LOWNOISEMODE 3-LOWSPURMODE
            unsigned   RES1        :1; // RESERVED 0000
		} __attribute__((packed))Reg2;
	};
	// Register 3 (Address: 011, Default:  HEX)
	union {
		uint32_t   register3;
		struct {
			unsigned   ADDR_3       :3;  // Register address bits - Устанавливается при загрузке
			unsigned   CLKD         :12; // CLOCK DIVIDER VALUE 0 - 4095
			unsigned   CLKDM        :2; // CLOCK DIVIDER MODE  0-CLOCK DIVIDER OFF; 1-FAST-LOCK ENABLE; 2-RESYNC ENABLE; 
            unsigned   RES1         :1; // RESERVED
            unsigned   CSR          :1; // CYCLE SLIP REDUCTION  0 = DISABLED; 1 = ENABLED; 
            unsigned   RES2         :13; // RESERVED           
		} __attribute__((packed))Reg3;
	};
	// Table 8. Register 4 (Address: 100, Default:  HEX)
	union {
		uint32_t   register4;
		struct {
			unsigned   ADDR_4    :3;  // Register address bits - Устанавливается при загрузке
			unsigned   RFAPWR    :2;  // OUTPUT POWER 0 = –4dBm; 1 = –1dBm; 2 = +2dBm; 3 = +5dBm
            unsigned   RFAEN     :1;  // RF 0 = DISABLED; 1 = ENABLED
            unsigned   RFBPWR    :2;  // AUXRF OUTPUT POWER 0 = –4dBm; 1 = –1dBm; 2 = +2dBm; 3 = +5dBm
            unsigned   RFBEN     :1;  // AUXILIARY OUT 0 = DISABLED; 1 = ENABLED
            unsigned   AOS       :1;  // AUX OUTPUT SELECT 0 = DISABLED; 1 = ENABLED  
            unsigned   MTLD      :1;  // MUTE TILL LOCK DETECT  0 = DISABLED; 1 = ENABLED         
            unsigned   VCOPWR    :1;  // VCO POWER-DOWN   0 = POWERED UP; 1 = POWERED DOWN
            
            unsigned   BNDCLKDIV    :8;  //BAND SELECT CLOCK DIVIDER (R) 1 - 255
            unsigned   RFDIV    :3;//RF DIVIDER SELECT RF DIVIDER SELECT   ÷1; ÷2; ÷4; ÷8; ÷16; ÷32; ÷64
            unsigned   FBSEL    :1;             //FEEDBACK SELECT 0 = DIVIDED; 1 = FUNDAMENTAL
            unsigned   RES1         :8; // RESERVED 
		} __attribute__((packed))Reg4;
	};
	//  Register 5 (Address: 101, Default:  HEX)
 	union {
		uint32_t   register5;
		struct {
			unsigned   ADDR_5   :3;  // Register address bits
            unsigned   RES1     :16;  // RESERVED   
             unsigned   RES2    :2;  // RESERVED  11  
             unsigned   RES3    :1;  // RESERVED  0               
             unsigned   LDPM    :2;  //LOCK DETECT PIN OPERATION 0- LOW; 1-DIGITAL LOCK DETECT; 2-LOW; 3-HIGH
             unsigned   RES4    :8;  // RESERVED               
		} __attribute__((packed))Reg5;
	};   
};

  typedef enum {
    RFAP   = 1,     // RFA+
    RFAM   = 2,     // RFA-
    RFBP   = 3,     // RFB+
    RFBM   = 4,     // RFB-            
            
  } CH_SEL_ADF435x;

  typedef enum {
    RFCH4   = 1,     // RFA+
    RFCH3   = 2,     // RFA-
    RFCH2   = 3,     // RFB+
    RFCH1   = 4,     // RFB-            
            
  } CH_SEL_ADF435x_2;

  typedef enum {
    RF_Z15   = 1,     // RFA+
    RF_Z14    = 2,     // RFA-
    RF_Z13    = 3,     // RFB+
    RF_Z12    = 4,     // RFB-            
            
  } CH_SEL_ADF4351;

  typedef enum {
    RF_Z24   = 1,     // RFA+
    RF_Z23    = 2,     // RFA-
    RF_Z22    = 3,     // RFB+
    RF_Z21    = 4,     // RFB-            
            
  } CH_SEL_ADF4355;  
  
    // 
typedef enum ADF_RFOUT_typedef{
    enum_ADF_RFOUT_A    = 1,
    enum_ADF_RFOUT_B    = 2,
    enum_ADF_RFOUT_both = 3
} enum_ADF_RFOUT;   
         /* 0 - THREE-STATE OUTPUT
         * 1 - DVdd
         * 2 - SDGnd
         * 3 - R DIVIDER OUTPUT
         * 4 - N DIVIDER OUTPUT
         * 5 - ANALOG LOCKDETECT
         * 6 - DIGITAL LOCKDETECT
         * 7 - RESERVED
         */

//  MUX_OUT_typeDef
typedef enum ADF_MUX_OUT_typeDef{
    enum_ADF_MUX_OUT_threeStateOutput  = 0,
    enum_ADF_MUX_OUT_D_VDD             = 1,
    enum_ADF_MUX_OUT_D_GND             = 2,  
    enum_ADF_MUX_OUT_R_dividerOutput   = 3,
    enum_ADF_MUX_OUT_N_dividerOutput   = 4,
    enum_ADF_MUX_OUT_analogLockDetect  = 5,
    enum_ADF_MUX_OUT_digitalLockDetect = 6 // +
} enum_ADF_MUX_OUT_typeDef;
//  lockDetectFunction_typeDef
 typedef enum ADF_lockDetectFunction_typeDef{
    enum_ADF_lockDetectFunction_low = 0,
    enum_ADF_lockDetectFunction_digitalLockDetect = 1,//+
    enum_ADF_lockDetectFunction_analogLockDetect = 2,
    enum_ADF_lockDetectFunction_high = 3
} enum_ADF_lockDetectFunction_typeDef;   
//  outputPower typedef

 typedef enum ADF_outputPower_typeDef{
    enum_ADF_outputPower_m4dBm    = 0, // -4 dBm
    enum_ADF_outputPower_m1dBm    = 1, // -1 dBm
    enum_ADF_outputPower_2dBm     = 2, // +2 dBm
    enum_ADF_outputPower_5dBm     = 3, // +5 dBm
    enum_ADF_outputPower_disabled = 4  //
} enum_ADF_outputPower_typeDef;   


struct ADF4351_initDef{
    //-----
    int ADF4351_LE_num;    
    bool mode_INT_nFRAC;// = false;
    uint64_t reference_freq_Hz;//= 0;
	uint64_t phaseDet_freq_Hz;// = 0;
    uint64_t freq_modulusValue;// CHSPACE;//freq_modulusValue= REFIN/fRES;
    enum_ADF_MUX_OUT_typeDef             enum_ADF_MUX_OUT;// = enum_MUX_OUT_D_GND;    
	enum_ADF_lockDetectFunction_typeDef  enum_ADF_lockDetectFunction;// = enum_lockDetectFunction_digitalLockDetect;    
    enum_ADF_outputPower_typeDef         enum_ADF_outputPower_RFOUT_A;// = enum_outputPower_5dBm;
	enum_ADF_outputPower_typeDef         enum_ADF_outputPower_RFOUT_B;// = enum_outputPower_5dBm;    
    uint64_t AD4351_Freq_Hz;   
    struct  ADF4351_Reg_typeDef ADF4351_RegNum;

  };
  
//   void  v_o_CLK(  bool x ) = 0;   // Serial Clock Input. The data is latched into the 32-bit shift register on the rising edge of the CLK line.
//	 void  ADF4351_v_o_DATA( uint32_t ena );   // Serial Data Input. The serial data is loaded MSB first. The 3 LSBs identify the register address
//   void  v_o_LE(   bool x ) = 0;   // Load Enable Input. When LE goes high the data stored in the shift register is loaded into the appropriate latches
//   void  v_o_CE(   bool x )     {}; // Chip Enable. A logic-low powers the part down and the charge pump becomes high impedance.
//	 void  v_o_RFOUT_EN( bool ena ); // RF Output Enable. A logic-low disables the RF outputs

uint8_t  ADF4351_check_lockDetect( void ); // Lock Detect Output. Logic-high when locked, and logic-low when unlocked. See register description for more details (Table 9).
uint8_t  ADF4351_check_MUX_OUT( void ) ;


uint8_t  ADF4351_v_writeReg(int ADF4351_LE_num, uint32_t reg);
     

uint8_t  ADF4351_initialize(struct ADF4351_initDef *pADF4351_Init);
uint8_t  ADF4351_disable( void );
     
 
uint8_t  ADF4351_setFreq(struct ADF4351_initDef *pADF4351_Init);


    // 0=A, 1=B
uint8_t  ADF4351_setPowerLevel(struct ADF4351_initDef *pADF4351_Init,enum_ADF_RFOUT RFOUT );
uint8_t  ADF4351_PWUP(struct ADF4351_initDef *pADF4351_Init);
uint8_t  ADF4351_PWDWN(struct ADF4351_initDef *pADF4351_Init);

uint8_t  ADF4351_setCH(struct ADF4351_initDef *pADF4351_Init, uint8_t CH);
uint8_t  ADF4351_update_REG( struct ADF4351_initDef *pADF4351_Init);

#endif    