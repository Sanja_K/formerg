/*
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef ReceiveCMD_H
#define	ReceiveCMD_H

#include <xc.h> // include processor files - each processor file is guarded.  
//
//extern uint8_t   addr_Dt;
//extern uint8_t   cmd_Dt;
//extern uint8_t   cnt_inf_Dt;
//extern uint8_t   inf_Dt[255];
//
//extern uint8_t   err_CRC;

extern uint8_t   data_RX_Urt[255];
extern uint8_t   data_TX_Urt[255];
extern  unsigned int cntrTMR2;
typedef enum
{
    URT_STATUS_IN_PROGRESS,
    URT_STATUS_SUCCESS,
    URT_STATUS_ERROR,
    URT_STATUS_IDLE,

} URT_TRANSFER_STATUS;


typedef struct
{
    /* The application's current state */
   URT_TRANSFER_STATUS      Status;
   uint8_t                  URT_NUM;          

    /* TODO: Define any additional data used by the application. */

} URT_DATA_def;




void InitReceiveDtCMD(void);
void ReceiveDtUrt(uint8_t bBf_Rx);
void RsNewRxDtUrt(void);
void TransmitDtUrt (int CMD, unsigned char *infDt, int lenInfDt);
#endif	/* ReceiveCMD_H */

