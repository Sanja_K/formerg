#ifndef _EEPROM_H    /* Guard against multiple inclusion */
#define _EEPROM_H


#include <xc.h>
#include <I2C.h>


#define EE_ADDR             0b01010001


#define EE_READ_MASK        0b00000001
#define EE_WRITE_MASK       0b00000000

#define EE_WRDI             0b00000100
#define EE_WREN             0b00000110
#define EE_RDSR             0b00000101
#define EE_WRSR             0b00000001

/* sizes in bytes*/
#define EE_PAGE_SIZE        128
#define EE_SIZE             64000 // kbytes

void eeprom_init(void);

void eeprom_write_byte(uint16_t addr, uint8_t byte);
uint8_t eeprom_write_array(uint16_t addr, uint8_t *buf, uint16_t size);

uint8_t eeprom_read_byte(uint16_t addr);
uint8_t eeprom_read_array(uint16_t addr, uint8_t *buf, uint16_t size);


#endif /* _EEPROM_H */

