/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */
#include <xc.h>
#include <stddef.h>
#include <stdbool.h> 
#include <stdlib.h>  
#include <stdint.h>
#include "definitions.h" 
#include "system_cfg.h" 
/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

#define READ_CORE_TIMER()                 _CP0_GET_COUNT()          // Read the MIPS Core Timer
 
 
uint8_t addrL, addrO;
 
 

void delay_us(uint32_t microseconds)
{
    uint32_t time;
    
    time = READ_CORE_TIMER(); // Read Core Timer    
    time += (SYS_CLK_FREQ / 2 / 1000000) * microseconds; // calc the Stop Time    
    while ((int32_t)(time - READ_CORE_TIMER()) > 0){};    
}

void delay_ms(uint32_t ms)
{
    delay_us(ms * 1000);
}
/* *****************************************************************************
 End of File
 */
int CalcCRC(int dtSum, int dt) //
{
	dtSum = dtSum + dt;
	dtSum = dtSum % 255;

	return dtSum;
}

// 
  void LLDIV (uint64_t numer, uint64_t denom,  uint64_t *quot, uint64_t *rem)
{
    uint64_t a,b,b1;
    uint64_t q,r;//  
    a= numer;
    b = denom;
    b1 = b;
    while (b1<=a) b1*=2;
     q = 0;
     r = a;
     while (b1!=b) {
      b1/=2; q*=2;
      if (r>=b1) { r-=b1; q++; }
     }
     *rem = r;
     *quot = q;
}
long Nod(long a, long b)
{
    while (a && b)
        if (a >= b)
           a -= b;
        else
           b -= a;
    return a | b;
}

void AddrInit(void)
{ 
//    addrL = addrF1;
//    addrO = addrO1;   
//    LED1_ON;
//    delay_ms(300);
//    LED1_OFF;
//    delay_ms(300);
//    delay_ms(300);        

}
