#ifndef _FORMER_SIGNAL_H    /* Guard against multiple inclusion */
#define _FORMER_SIGNAL_H

/* Convert frequency from kHz to FTW */
#define KHZ_TO_FTW(freq)    (((uint64_t)(freq)) * 4294 + \
                            ((((uint64_t)(freq)) * 1981) >> 11))

/* Convert frequency from Hz to FTW */
#define HZ_TO_FTW(freq)     (((uint64_t)(freq)) * 4 + \
                            ((((uint64_t)(freq)) * 604) >> 11))

/* Convert frequency from kHz to Hz */
#define KHZ_TO_HZ(freq)     ((freq) * 1000)

/* 200 ns per one frequency */
#define DIGITAL_RAMP_RATE   0x00320032
#define LFM_POINT_TIME      200 //in ns
#define RAMP_RATE           0x00010001  //0x000F000F AD9914

#define CS_DDS3  1
#define CS_DDS4  2
#define CS_DDS5  3


uint8_t former9914_init(int CS_DDS);
uint8_t ad9914_deinit (int CS_DDS);
//void repeat_init_former(void);


void set9914_mode_lfm(int CS_DDS, uint64_t freq, uint64_t dev, uint32_t speed, uint64_t Freq_ref);
void set9914_mode_freq(int CS_DDS, uint64_t freq, uint64_t Freq_ref);
void set9914_mode_amp(int CS_DDS, uint32_t set_amp);



#endif /* _FORMER_SIGNAL_H */

/* *****************************************************************************
 End of File
 */
