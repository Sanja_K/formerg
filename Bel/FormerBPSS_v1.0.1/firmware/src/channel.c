/*
 * File:   timers.c
 * Author: Alexandr
 *
 * Created on September 20, 2017, 4:53 PM
 */


#include "xc.h"
#include "system_cfg.h"
#include "definitions.h" 
#include "user.h"
#include "executeCMD.h"
#include "channel.h"

char swtest;


//--------QPC6044--------------------
#define SW1RF1_V1_1 SWDDS1_V1_Set()
#define SW1RF1_V1_0 SWDDS1_V1_Clear()

#define SW1RF1_V2_1 SWDDS1_V2_Set()
#define SW1RF1_V2_0 SWDDS1_V2_Clear()
//--------------------------------------
void SW1DA3_RF1(char sw)//1
{
    if (sw==1)
    {
        SW1RF1_V1_1;  //   RFC => RF1 test
        SW1RF1_V2_1; //   
    }
    else if (sw==2)//
    {
        SW1RF1_V1_0;  //   RFC => RF2 
        SW1RF1_V2_0; //    
    }
    else if (sw==3)//
    {
        SW1RF1_V1_1;  //   RFC => RF3 
        SW1RF1_V2_0; //    
    }
    else if (sw==4)//
    {
        SW1RF1_V1_0;  //   RFC => RF4 
        SW1RF1_V2_1; //   
    } 
}


//--------QPC6044--------------------
#define SW2RF1_V1_1 SWRF1_V1_Set()
#define SW2RF1_V1_0 SWRF1_V1_Clear()

#define SW2RF1_V2_1 SWRF1_V2_Set()
#define SW2RF1_V2_0 SWRF1_V2_Clear()
//--------------------------------------
void SW2DA16_RF1(char sw)//1
{
    if (sw==1)
    {
        SW2RF1_V1_1;  //   RFC => RF1 test
        SW2RF1_V2_1; //   
    }
    else if (sw==2)//
    {
        SW2RF1_V1_0;  //   RFC => RF2 
        SW2RF1_V2_0; //    
    }
    else if (sw==3)//
    {
        SW2RF1_V1_1;  //   RFC => RF3 
        SW2RF1_V2_0; //    
    }
    else if (sw==4)//
    {
        SW2RF1_V1_0;  //   RFC => RF4 
        SW2RF1_V2_1; //   
    } 
}

//----------- SN74LVC2G04DBVR ------------
#define SW3RF2_1 RF2_CTRL_Set()
#define SW3RF2_0 RF2_CTRL_Clear()

void SW3DA39_RF2(char sw)// lit1 DA15 SWIF1_V1
{
   if (sw==1)
   {
       SW3RF2_1;  /// RF1 
   }
   else 
   {
       SW3RF2_0;// RF2 RF test
   }
}

#define SW4RF3_1 RF3_DIR_Set()
#define SW4RF3_0 RF3_DIR_Clear()

void SW3DA45_RF3(char sw)// lit1 DA15 SWIF1_V1
{
   if (sw==1)
   {
       SW4RF3_1;  /// RF1 
   }
   else 
   {
       SW4RF3_0;// RF2 RF test
   }
}
