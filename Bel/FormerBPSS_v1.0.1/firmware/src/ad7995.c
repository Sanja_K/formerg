/*
 * File:   ad7995.c
 * Author: Alexandr
 *
 * Created on 27 � 2017 г., 13:58
 */


#include "xc.h"
#include "system_cfg.h"
#include "I2C.h"
#include "ad7995.h"



uint16_t fun_Read7995(uint8_t channel,uint8_t addr)
{
    uint16_t res;
//    uint8_t msb, lsb;
    uint8_t rxdti2c[2];    
    
    if(channel > 4)
    {
        return 0;
    }
    
    uint8_t cfg = 1 << (channel + 4);
    
    I2C1_Write(addr, &cfg, 1);     
    delay_ms(5);   
    I2C1_Read(addr, rxdti2c,2);
    delay_ms(1);    

//    StartI2C();
//    IdleI2C();
//    if (addr == AD7995_ADDR)
//    {
//        WriteI2C(AD7995_ADDR | ADWRITE_MASK);    
//    }else
//    {
//        WriteI2C(AD7995_ADDR2 | ADWRITE_MASK); 
//    }  
    
//    IdleI2C();
//    WriteI2C(cfg);
//    IdleI2C();
//    StopI2C();

    
    //
//   StartI2C();
//    IdleI2C();
//    if (addr == AD7995_ADDR)
//    {    
//        WriteI2C(AD7995_ADDR | ADREAD_MASK);
//    }else
//    {
//        WriteI2C(AD7995_ADDR2 | ADREAD_MASK);
//    }
//    IdleI2C();
//    msb = ReadI2C();
//    AckI2C();
//    IdleI2C();
//    lsb = ReadI2C();
//    NotAckI2C();
//    IdleI2C();
//    StopI2C();
//    IdleI2C();
//    delay_ms(1);
    
    res = ((uint16_t)(((rxdti2c[0] & 0x0F) << 8) | rxdti2c[1])) >> 2;
    
    return res;
}
