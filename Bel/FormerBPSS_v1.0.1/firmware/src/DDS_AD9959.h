/* 
 * File:   DDS_AD9959.h
 * Author: Alexandr
 *
 * Created on 28 сентября 2017 г., 9:32
 */

#ifndef DDS_AD9959_H
#define	DDS_AD9959_H
#include "synt_AD9959.h"
//-------------------------------------------

//#define deltaFg           (70*MHz) // смещение частоты гетеродина от сигнала 
//#define dtScan            (125*MHz) // dtScan  = 8*10^-9
#define dtScan            (12500*kHz) // dtScan  = 8*10^-8
#define Fs                (500*MHz) 
//******************************L2************************************
#define Fget_ADF1          (1285)// MHz
#define Fman_L2 (1*kHz)
//----------------------------GPS L2----------------------------------
#define FDDS_devGPS_L2           (1*MHz)
#define F0DDS_GPS_L2             (Fget_ADF1*MHz - 1227600*kHz)  //

#define FDDS_start_GPS_L2 (F0DDS_GPS_L2 - FDDS_devGPS_L2)//125
#define FDDS_stop_GPS_L2  (F0DDS_GPS_L2 + FDDS_devGPS_L2)

#define dFscan_GPS_L2  Fman_L2*(FDDS_stop_GPS_L2-FDDS_start_GPS_L2)/dtScan
//----------------------------GL L2------------------------------------
#define FDDS_devGL_L2            (4*MHz)
#define F0DDS_GL_L2              (Fget_ADF1*MHz - 1245000*kHz)  /// 

#define FDDS_start_GL_L2 (F0DDS_GL_L2 - FDDS_devGL_L2)//150
#define FDDS_stop_GL_L2  (F0DDS_GL_L2 + FDDS_devGL_L2)

#define dFscan_GL_L2  Fman_L2*(FDDS_stop_GL_L2-FDDS_start_GL_L2)/dtScan
//----------------------------BD L2----------------------------------
#define FDDS_devBD_L2           (1*MHz)
#define F0DDS_BD_L2             (Fget_ADF1*MHz - 1261000*kHz)  //

#define FDDS_start_BD_L2 (F0DDS_BD_L2 - FDDS_devBD_L2)//125
#define FDDS_stop_BD_L2  (F0DDS_BD_L2 + FDDS_devBD_L2)

#define dFscan_BD_L2  Fman_L2*(FDDS_stop_BD_L2-FDDS_start_BD_L2)/dtScan
//---------------------------GLO L2------------------------------
#define FDDS_devGLO_L2           (1*MHz)
#define F0DDS_GLO_L2             (Fget_ADF1*MHz - 1261000*kHz)  //

#define FDDS_start_GLO_L2 (F0DDS_GLO_L2 - FDDS_devGLO_L2)//125
#define FDDS_stop_GLO_L2  (F0DDS_GLO_L2 + FDDS_devGLO_L2)

#define dFscan_GLO_L2  Fman_L2*(FDDS_stop_GLO_L2-FDDS_start_GLO_L2)/dtScan

//************************L1*************************************
//-----------------------GPS L1----------------------------------
#define Fget_ADF2         (1675)//   
#define Fman_L1 (1*kHz)//

#define FDDS_devGPS_L1           (500*kHz)
#define F0DDS_GPS_L1             (Fget_ADF2*MHz - 1575420*kHz)  //
#define FDDS_start_GPS_L1 (F0DDS_GPS_L1 - FDDS_devGPS_L1)
#define FDDS_stop_GPS_L1  (F0DDS_GPS_L1 + FDDS_devGPS_L1)

#define dFscan_GPS_L1  Fman_L1*(FDDS_stop_GPS_L1-FDDS_start_GPS_L1)/dtScan

//-----------------------GL L1------------------------------------
#define FDDS_devGL_L1            (4*MHz)
#define F0DDS_GL_L1              (Fget_ADF2*MHz - 1602000*kHz)   //
#define FDDS_start_GL_L1 (F0DDS_GL_L1 - FDDS_devGL_L1)
#define FDDS_stop_GL_L1  (F0DDS_GL_L1 + FDDS_devGL_L1)

#define dFscan_GL_L1  Fman_L1*(FDDS_stop_GL_L1-FDDS_start_GL_L1)/dtScan

//---------------------BD L1--------------------------------------

#define FDDS_devBD_L1           (16*MHz)
#define F0DDS_BD_L1             (Fget_ADF2*MHz - 1575420*kHz)  // 99580
#define FDDS_start_BD_L1 (F0DDS_BD_L1 - FDDS_devBD_L1)
#define FDDS_stop_BD_L1  (F0DDS_BD_L1 + FDDS_devBD_L1)

#define dFscan_BD_L1  Fman_L1*(FDDS_stop_BD_L1-FDDS_start_BD_L1)/dtScan
//--------------------------------------------------------------------------------

//---------------------GLO L1--------------------------------------

#define FDDS_devGLO_L1           (16*MHz)
#define F0DDS_GLO_L1             (Fget_ADF2*MHz - 1575420*kHz)  // 99580
#define FDDS_start_GLO_L1 (F0DDS_GLO_L1 - FDDS_devGLO_L1)
#define FDDS_stop_GLO_L1  (F0DDS_GLO_L1 + FDDS_devGLO_L1)

#define dFscan_GLO_L1  Fman_L1*(FDDS_stop_GLO_L1-FDDS_start_GLO_L1)/dtScan
//--------------------------------------------------------------------------------
#define F0DDS_GPS_Fclk			(2046*kHz)
#define F0DDS_GL_Fclk		    (1022*kHz)
//------------------------------------------------------------
uint8_t  Init_AD9959(int DDSn);

char  Set_param_AD9959_1 (uint64_t F_DDS_start, uint64_t F_DDS_stop, uint64_t dF_scan);

char  Set_param_AD9959_2 (uint64_t F_DDS_start, uint64_t F_DDS_stop, uint64_t dF_scan);


 char  Set_param_AD9959_0RCH (uint64_t F_DDS); 
 char  Set_param_AD9959_1RCH (uint64_t F_DDS) ;
 char  Set_param_AD9959_2RCH (uint64_t F_DDS); 
 char  Set_param_AD9959_3RCH (uint64_t F_DDS) ; 
 
 
 // uint8_t Set_param_AD9959_1QPSK (uint64_t F_DDS1,uint64_t F_dev);
 // uint8_t Set_param_AD9959_2QPSK (uint64_t F_DDS2,uint64_t F_dev);
 
 uint8_t Set_param_AD9959_GPS_L1 (void);
 uint8_t Set_param_AD9959_Glonass_L1 (void);
 uint8_t Set_param_AD9959_Beidou_L1 (void); 
 uint8_t Set_param_AD9959_Galileo_L1 (void); 
 uint8_t Set_param_AD9959_GPS_L2 (void);
 uint8_t Set_param_AD9959_Glonass_L2 (void);
 uint8_t Set_param_AD9959_Beidou_L2 (void);
 uint8_t Set_param_AD9959_Galileo_L2 (void);
 uint8_t Set_param_AD9959_GPS_Fclk (void);
 uint8_t Set_param_AD9959_Glonass_Fclk (void);
void Stop_AD9959(int DDSn);
  
  
#endif	/* DDS_AD9959_H */  
  