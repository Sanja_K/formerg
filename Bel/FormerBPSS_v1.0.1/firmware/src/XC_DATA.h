/* 
 * File:   XC_DATA.h
 * Author: Alexandr
 *
 * Created on 25 октября 2017 г., 10:07
 */

#ifndef XC_DATA_H
#define	XC_DATA_H

#ifdef	__cplusplus
extern "C" {
#endif

#define XC_v_o_DATA_0 SDI_XC_Clear();//+
#define XC_v_o_DATA_1 SDI_XC_Set(); 
  
#define XC_v_o_CLK_0 SCLK_XC_Clear() ;//+
#define XC_v_o_CLK_1 SCLK_XC_Set(); 
  
#define XC1_v_o_LE_0 LE_XC1_Clear();//XC1+
#define XC1_v_o_LE_1 LE_XC1_Set();

#define XC2_v_o_LE_0  LE_XC2_Clear();//XC2+
#define XC2_v_o_LE_1  LE_XC2_Set();
    
#define GNSS_ON_XC 		ADR_XC1_Set();// on/ off+
#define GNSS_OFF_XC 	ADR_XC1_Clear();     
    //---------------------------------------------------------------------
 //------modulation-----------------   
#define DDS1Mod_XC_0 	ADR_XC1_Clear()// mod off+
#define DDS1Mod_XC_1 	ADR_XC1_Set(); // mod on    +
    
//------------------------------------------------------------------------

#define data_reg_XC_status (PORTE&0x0FF);
    
#define XC_strob_status_1 STR_ADR_XC_Set();
#define XC_strob_status_0 STR_ADR_XC_Clear();
    
#define XC_RW_status_1 RnW_XC_Set();
#define XC_RW_status_0 RnW_XC_Clear();  
    
#define XC_ADR_status_1 ADR_XC0_Set();
#define XC_ADR_status_0 ADR_XC0_Clear();    

//extern uint8_t SetGPSL1, SetGLL1, SetGPSL2, SetGLL2;
//-------------------------------------------------------------------------    
/*  
#define DDS1P3_XC_0 LATEbits.LATE4 = 0;
#define DDS1P3_XC_1 LATEbits.LATE4 = 1; 
*/  

/*
#define DDS1P4_XC_0 LATEbits.LATE3 = 0;
#define DDS1P4_XC_1 LATEbits.LATE3 = 1; 


#define DDS2P5_XC_0 LATEbits.LATE2 = 0;
#define DDS2P5_XC_1 LATEbits.LATE2 = 1;
*/    

 void Init_XC(void);    
 uint8_t Init_XC_freq (unsigned char DDSx, uint64_t F_man);   
 uint8_t Init_XC_ph (unsigned char DDSx, uint64_t F_man);
 void Init_XC_GPSGL (void);
 void Init_XC_BDGLO (void);
 uint8_t  XC_LD_DATA( void);
    
#ifdef	__cplusplus
}
#endif

#endif	/* XC_DATA_H */

