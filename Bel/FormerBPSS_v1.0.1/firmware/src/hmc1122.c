#include "hmc1122.h"
#include "definitions.h" 

#define CLK_hmc_1     CLK_ATT_Set()
#define CLK_hmc_0     CLK_ATT_Clear() 

#define DATA_hmc_1    DATA_ATT_Set()
#define DATA_hmc_0    DATA_ATT_Clear()

#define LE1_hmc_1     LE_ATT1_Set()
#define LE1_hmc_0     LE_ATT1_Clear()

// #define LE2_hmc_1     LE_ATT2_Set() 
// #define LE2_hmc_0     LE_ATT2_Clear() 

#define LE1_hmc_seq()   {LE1_hmc_1; Nop(); Nop(); Nop(); LE1_hmc_0;}
//#define LE2_hmc_seq()   {LE2_hmc_1; Nop(); Nop(); Nop(); LE2_hmc_0;}

    
static void hmc1122_write(unsigned char val)
{
    unsigned char i;
    val <<= 2;
    
    for(i = 0; i < 6; i++)
    {
        if(val & 0x80)
            DATA_hmc_1;
        else
            DATA_hmc_0;
        
        CLK_hmc_1;
        val <<= 1;
        CLK_hmc_0;
    }
    
    DATA_hmc_0;
}

 void hmc1122_set_gain(unsigned char gain)
 {
     // gain <<= 1;
 
      if(gain > 63)
        gain = 0;   
    hmc1122_write(gain);

 }
 
 void hmc1122_set_gain_amp1(unsigned char gain) // 
{
    LE1_hmc_seq();
    hmc1122_set_gain(gain);
    LE1_hmc_seq();
}
 
 // void hmc1122_set_gain_amp2(unsigned char gain) // 
// {
    // LE2_hmc_seq();
    // hmc1122_set_gain(gain);
    // LE2_hmc_seq();
// }