/* 
 * File:   executeCMD.h
 * Author: Alexandr
 *
 * Created on 12 11 2020 г., 11:36
 */

#ifndef EXECUTECMD_H
#define	EXECUTECMD_H

typedef struct LITERA_DATA_CMD_def {
	uint8_t addr;
	uint8_t cmd;
	uint8_t cntDt;// dlina pack data
	uint8_t cntSrc;  // kolich istochnikov  
 	uint8_t err_CRC;    
} LITERA_dt_cmd_def; //

typedef struct  LITERA_PARAM_def {
	uint32_t  freq; //  // kHz
	uint8_t   mod;// 
    uint32_t  dev; //  // kHz
    uint32_t  manip; // // kHz
    uint16_t  timerad; // 
}LITERA_param_def;



typedef struct NAVIG_PARAM_def {
    
	uint8_t GPS_L1; //    
	uint8_t GPS_L2; //   
	uint8_t GL_L1; //    
	uint8_t GL_L2; // 
	uint8_t BD_L1; //    
	uint8_t BD_L2; // 
	uint8_t GLO_L1; //   
	uint8_t GLO_L2; //
	uint8_t GPS_Fclk; //   
	uint8_t GL_Fclk; //    
    
	uint8_t GNSS_GAIN_L1; // L1 
	uint8_t GNSS_GAIN_L2; // L2
    
} NAVIG_param_def; // 



extern LITERA_param_def    LITERA_param;
extern LITERA_dt_cmd_def   LITERA_cmd_dt;
extern NAVIG_param_def     NAVIG_dt;
//------------former 1 ------------------------------------ 


//------------------CH2-----------------------------------    
#define   Freq_dds2_F1      1500     //MHz   LMX4 form 1
#define   Freq_dds2_F1_Hz   (3500*MHz)     //Hz   LMX4 form 1
#define   fDDS2_CH2_F1      490000   //kHz   DDS2 form 1
#define   Freq_get21_F1     1700     //MHz   LMX3 form 1
#define   Freq_get22_F1     1960     //MHz   LMX4 form 1
        
//------------former 2 ------------------------------------  
//------------------CH2-----------------------------------   
#define   fADF2_CH2_F1   (3500000) // kHz
#define   fADF2_CH2_F2   (3500000)  
#define   fADF2_CH2_F3   (2500000) 
#define   fADF2_CH2_F4   (2900000) 
#define   fADF2_CH2_F5   (2900000) 
#define   fADF2_CH2_F6   (3100000) 
#define   fADF2_CH2_F7   (3300000) 

//------------------CH1-----------------------------------   
#define   fADF1_CH1_F1   (3500000) // kHz
#define   fADF1_CH1_F2   (3500000)  
#define   fADF1_CH1_F3   (2500000) 
#define   fADF1_CH1_F4   (2600000) 
#define   fADF1_CH1_F5   (2700000) 
#define   fADF1_CH1_F6   (2800000) 
#define   fADF1_CH1_F7   (2900000) 

#define   fDDS1_CH1_F2      200000LL   //kHz   DDS1  CH1   form2 200000LL
    
#define   Freq_get11_F2     2180    //MHz   LMX 1 form2 MHz
#define   Freq_get12_F2     2795//2950    //MHz   LMX 1
#define   Freq_get122_F2    2180 

#define   Freq_get13_F2     1980    //MHz    LMX 2 form2 1960
#define   Freq_get14_F2     2595    //MHz    LMX 2 form2  
#define   Freq_get144_F2    1980    //1960
//-----------------CH2------------------------------------
#define   fDDS2_CH2_F2      260000LL   //kHz   DDS2 form 2   260000LL 

#define   Freq_get21_F2     3200    //MHz    LMX 3 form 2
#define   Freq_get22_F2     3460    //MHz    LMX 4 form 2   

uint8_t exeCMD4(void);
uint8_t exeCMD24(uint8_t CHx);
uint8_t exeCMD28(void);
uint8_t exeCMD30(void);
uint8_t exeCMD31(uint8_t Cmdx, uint8_t rfCh);// test switch
uint8_t exeCMD34(void);
uint8_t exeCMD40(uint8_t status);
uint8_t exeCMD41(uint8_t *IDparamP, uint8_t DtCnt);
uint8_t exeCMD42(void);
uint8_t exeCMD43(void);
uint8_t AnlRxDtUrt(void);


uint8_t setRF_2500_6000(void);
//uint8_t setRF_500_2500(void);


//unsigned char calcGain( unsigned short int strt_Freq_MHz, unsigned short int stp_Freq_MHz,int gainK, unsigned char gainN);
uint8_t getGainK(uint32_t Freq);
void getGainK_gnss(uint8_t *GainL1, uint8_t *GainL2);






#endif


