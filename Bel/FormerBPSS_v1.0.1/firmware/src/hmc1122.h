/* 
 * File:   hmc1122.h
 * Author: Alexandr
 *
 * Created on 18 октября 2017 г., 11:38
 */

#ifndef HMC1122_H
#define	HMC1122_H

#ifdef	__cplusplus
extern "C" {
#endif
#include <xc.h>
    
  typedef enum {
    hD0   = 62,     // 0.5 111110
    hD1   = 61,     // 1   111101
    hD2   = 59,     // 2    111011
    hD3   = 55,     // 4     110111
    hD4   = 47,    // 8    101111
    hD5   = 31     // 16    011111
  } ATT_HMC1122;    
    

 void hmc1122_set_gain_amp1(unsigned char gain) ;
 //void hmc1122_set_gain_amp2(unsigned char gain); 
#ifdef	__cplusplus
}
#endif

#endif	/* HMC1122_H */

