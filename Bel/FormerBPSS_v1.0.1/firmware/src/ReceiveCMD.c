/*
 * File:   ReceiveCMD.c
 * Author: KAZEKA Alexandr
 *
 * Created on 14.12.2020 г., 14:08
 * 
 * 
 */


#include "xc.h"
#include "ReceiveCMD.h"
#include "system_cfg.h" 
#include "user.h"
#include "executeCMD.h"

uint8_t   data_RX_Urt[255];
uint8_t   data_TX_Urt[255];

uint8_t   addr_Dt;
uint8_t   cmd_Dt;
uint8_t   cnt_inf_Dt;
uint8_t   inf_Dt[255];
uint8_t   err_CRC;
uint32_t  sumUrt,cntTMR4;
uint8_t   ki_Urt;
uint8_t   dtUrtReg;


enum RxState_def {
	rx_addr,
    rx_inf, 
    rx_crc
}RxUrtState; 

unsigned int cntTMR4;
unsigned int cntrTMR2 = 0;

URT_DATA_def    urt_data_rx,urt_data_tx;

void TMR4_TimerInterruptHandler(uint32_t intCause, uintptr_t context)
{
//    bToggleLED = true;
    cntTMR4++;
    if (cntTMR4 > 500)
	{   
       RsNewRxDtUrt();
       delay_ms(1);         
    }    
}

void TMR2_TimerInterruptHandler(uint32_t intCause, uintptr_t context)
{
//    bToggleLED = true;
    cntrTMR2++;
    if (cntrTMR2 > 5000)
	{  
        cntrTMR2 = 0;
    }    
}


void UART2RX_InterruptHandler( uintptr_t context)
{
   // bToggleLED = true;
    TMR4 = 0x0; 
    TMR4_Start();    
    TMR4_InterruptEnable();
    ReceiveDtUrt(dtUrtReg);    
    UART2_Read(&dtUrtReg, 1);// next data
}
void UART2TX_InterruptHandler( uintptr_t context)
{
   // bToggleLED = true;
        while (UART2_WriteIsBusy() == true);
        while (!U2STAbits.TRMT == 1)
        {Nop();}
         RsNewRxDtUrt();
    AD485Rx_ON;     
}

void RsNewRxDtUrt(void)
{  
	ki_Urt = 0;
	sumUrt = 0;
    TMR4_InterruptDisable();    
    TMR4_Stop();
    TMR4 = 0x0;
	cntTMR4 = 0;
	RxUrtState = rx_addr;
    //UART2_ReadAbort();
	AD485Rx_ON;
	UART2_Read(&dtUrtReg,1);//dlyz nachala peredachi
}

void InitReceiveDtCMD(void)
{
    TMR4_CallbackRegister( TMR4_TimerInterruptHandler, (uintptr_t)NULL ); 
    TMR2_CallbackRegister( TMR2_TimerInterruptHandler, (uintptr_t)NULL );     
    UART2_ReadCallbackRegister( UART2RX_InterruptHandler, (uintptr_t)NULL );
    UART2_WriteCallbackRegister( UART2TX_InterruptHandler, (uintptr_t)NULL );    
    urt_data_rx.Status = URT_STATUS_IDLE;
    urt_data_tx.Status = URT_STATUS_IDLE;   
    TMR2 = 0x0; 
    TMR2_Start();    
    TMR2_InterruptEnable();
    RsNewRxDtUrt();
}

void ReceiveDtUrt(uint8_t bBf_Rx)
{
    switch (RxUrtState) {
		case rx_addr:
			data_RX_Urt[ki_Urt] = bBf_Rx;
			sumUrt = CalcCRC(sumUrt, bBf_Rx); 
			ki_Urt++;     
            if (ki_Urt == 3)//
            {
                addr_Dt = data_RX_Urt[0]; 
                cmd_Dt = data_RX_Urt[1]; 
                cnt_inf_Dt = data_RX_Urt[2]; 
                if (cnt_inf_Dt == 0)
                    RxUrtState = rx_crc;
                else
                    RxUrtState = rx_inf; 
            }
	    break;
		case rx_inf:
			data_RX_Urt[ki_Urt] = bBf_Rx;
			sumUrt = CalcCRC(sumUrt, bBf_Rx);
            ki_Urt++;            
			if (ki_Urt == cnt_inf_Dt + 3) 
			{
				RxUrtState = rx_crc; // rx crc
			}
		break;
		case rx_crc:
            data_RX_Urt[ki_Urt] = bBf_Rx;      
            ki_Urt++;
            err_CRC = 1;
            if (sumUrt == bBf_Rx) // CRC
            { err_CRC = 0;}

                app_task.state = app_task_AnlRxCMD;
               
                LITERA_cmd_dt.addr  = addr_Dt;
                LITERA_cmd_dt.cmd   = cmd_Dt; // 
                LITERA_cmd_dt.cntDt = cnt_inf_Dt; //                
                LITERA_cmd_dt.err_CRC = err_CRC;
            //inf_tx_Dt[0] = 0x40;
                         
        //    TransmitDtUrt (cmd_Dt, inf_tx_Dt, 1);

		break;
    }
}

    
void TransmitDtUrt (int CMD, unsigned char *infDt, int lenInfDt)
{
     int i, len_DT;
     
      //---------------- Send DATA ----------------       
        len_DT = 0;
        
        if (addrL == addrF1)
        {
            data_TX_Urt [0] = addrO1; //        
        }
        else if (addrL == addrF3)
        {
            data_TX_Urt [0] = addrO3; //
        }
        else if (addrL == addrF4)
        {
            data_TX_Urt [0] = addrO4; //
        }

        len_DT++;
        
        data_TX_Urt [1] = CMD; // 
        len_DT++;
        
        data_TX_Urt [2] = lenInfDt; // 
        len_DT++; 
        
        for (i = 0; i< lenInfDt; i++)
        {
           data_TX_Urt [len_DT] = infDt [i];
           len_DT++;
        }

        sumUrt = 0;// 
        for (i = 0; i < len_DT ;i++ )
        {        
         sumUrt = CalcCRC(sumUrt, data_TX_Urt [i]);    
        }
        
        data_TX_Urt [len_DT] = sumUrt;
        len_DT++;
        AD485Tx_ON; 
       // UART2_Write(&buffer2[0],sizeof(buffer2));
		UART2_Write(data_TX_Urt,len_DT); // rabotaet tolko vne preryvaniy
      //  Send_UART_485P(data_TX_Urt,len_DT); 
}