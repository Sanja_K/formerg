
#include "ad9914.h"
#include "former_signal.h"
#include "system_cfg.h"
#include "executeCMD.h"


uint8_t former9914_select_dds(int CS_DDS)
{
    switch(CS_DDS) {
    case CS_DDS3: ad9914_select_dds_3 (); break;
    case CS_DDS4: ad9914_select_dds_4 (); break;
    case CS_DDS5: ad9914_select_dds_5(); break;
    default: return MCHP_DDS_ERROR; break;
    }    
 return MCHP_SUCCESS;
}
uint8_t former9914_unselect_dds(int CS_DDS)
{
    switch(CS_DDS) {
    case CS_DDS3: ad9914_unselect_dds_3(); break;
    case CS_DDS4: ad9914_unselect_dds_4(); break;
    case CS_DDS5: ad9914_unselect_dds_5(); break;
    default: return MCHP_DDS_ERROR; break;
    }    
 return MCHP_SUCCESS;
}

uint8_t former9914_power_on(int CS_DDS)
{
    switch(CS_DDS) {
    case CS_DDS3: ad9914_pow_on_3(); break;
    case CS_DDS4: ad9914_pow_on_4(); break;
    case CS_DDS5: ad9914_pow_on_5(); break;
    default: return MCHP_DDS_ERROR; break;
    }    
 return MCHP_SUCCESS;
}

uint8_t former9914_reset(int CS_DDS)
{
    switch(CS_DDS) {
    case CS_DDS3: ad9914_reset_3(); break;
    case CS_DDS4: ad9914_reset_4(); break;
    case CS_DDS5: ad9914_reset_5(); break;
    default: return MCHP_DDS_ERROR; break;
    }    
 return MCHP_SUCCESS;
}

uint8_t former9914_init(int CS_DDS)
{
   // uint8_t status;
    former9914_unselect_dds(CS_DDS);
    Nop();
    former9914_power_on(CS_DDS);

    Nop();
    former9914_reset(CS_DDS);    
    delay_us (2);
    
    former9914_select_dds(CS_DDS); 
    delay_us (10);    
    ad9914_spi_3wire_mode();
    delay_us (1);
    ad9914_init_dac_cal();
    ad9914_set_profile_mode();
    former9914_unselect_dds(CS_DDS);  
    delay_us (300);    
 //   DDS9914_1_Select = 1;
 //   ad9910_enable_amp_scale();
    return MCHP_SUCCESS;
}


uint8_t  ad9914_deinit (int CS_DDS)
{
    switch(CS_DDS) {
    case CS_DDS3: ad9914_pow_off_3 (); break;
    case CS_DDS4: ad9914_pow_off_4 (); break;
    case CS_DDS5: ad9914_pow_off_5(); break;
    default: return MCHP_DDS_ERROR; break;
    }    
return MCHP_SUCCESS;

}


static uint32_t convert_hz_to_ftw(uint64_t freq_abs, uint64_t ad9914_sys_freq)
{
    return (uint32_t)((freq_abs * (1ULL << 32)) / ad9914_sys_freq);
}

//-------------------------------------------------------------
void set9914_mode_freq(int CS_DDS,uint64_t  freq, uint64_t Freq_ref)
{
    uint32_t freq_lo;    
    //freq  = freq*kHz; // kHz to Hz;  
    freq_lo = convert_hz_to_ftw(freq*kHz, Freq_ref*kHz);
    former9914_select_dds(CS_DDS);       
    ad9914_set_freq(ad9914_reg.PROF_F[0], freq_lo);   
    former9914_unselect_dds(CS_DDS); 
}


//
void set9914_mode_lfm(int CS_DDS, uint64_t freq, uint64_t dev, uint32_t speed,uint64_t Freq_ref )
{
    uint32_t freq_ftw;
    uint32_t dev_ftw;
    freq = freq*kHz;
    dev = dev*kHz;
   
    freq_ftw = convert_hz_to_ftw(freq, Freq_ref*kHz);    
    dev_ftw = convert_hz_to_ftw(dev, Freq_ref*kHz);
    
    uint32_t freq_lo = freq_ftw - dev_ftw;
    uint32_t freq_hi = freq_ftw + dev_ftw;
    uint32_t step_size = (2ULL * dev * speed * 24) / Freq_ref;
    uint32_t ramp_rate = RAMP_RATE;
    
    if(step_size == 0)
    {
        step_size = 1;
    }
    step_size = convert_hz_to_ftw(step_size, Freq_ref*kHz);

    former9914_select_dds(CS_DDS);

    ad9914_write_reg(ad9914_reg.DRAMP_LO, &freq_lo, REG_DWORD);
    ad9914_write_reg(ad9914_reg.DRAMP_HI, &freq_hi, REG_DWORD);
    ad9914_write_reg(ad9914_reg.DRAMP_RIS, &step_size, REG_DWORD);
    ad9914_write_reg(ad9914_reg.DRAMP_FAL, &step_size, REG_DWORD);
    ad9914_write_reg(ad9914_reg.DRAMP_RATE, &ramp_rate, REG_DWORD);
    
    
    ad9914_set_lfm_mode();  
    former9914_unselect_dds(CS_DDS);
}

void set9914_mode_amp(int CS_DDS, uint32_t set_amp)
{
//  int flgPOW;
//    freq  = freq*kHz; // kHz to Hz;  
    set_amp = set_amp << 16;
//    delay_us(10);
//    flgPOW = 0;
    former9914_select_dds(CS_DDS);
    ad9914_set_amp(ad9914_reg.PROF_PA[0], set_amp);  
    former9914_unselect_dds(CS_DDS);
}


