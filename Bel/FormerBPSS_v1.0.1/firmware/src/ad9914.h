#ifndef _AD9914_H    /* Guard against multiple inclusion */
#define _AD9914_H

#include <xc.h>
#include <stdint.h>
#include "definitions.h" 

#define REG_BYTE        1
#define REG_WORD        2
#define REG_DWORD       4

#define READ_MASK       0x80

#define FREQ_290        0x1536202E
#define FREQ_400        0x1D41D41D
#define FREQ_512        0x369D0369
#define FREQ_700        0x33333333
#define FREQ_860        0x3EE721A5
#define FREQ_1000       0x49249249
#define FREQ_1215       0x58DE5AB2 

#define ADDAC_ON        0x00010008
#define ADDAC_OFF       0x00010048
#define ADSPI_3WIRE_OSK 0x0001030A
#define ADSPI_3WIRE     0x0001000A
#define PROF_MODE       0x00800900
#define PARALL_MODE     0x00400900
#define ADLFM_MODE      0x008E2900
#define DAC_CAL_EN      0x01052120
#define DAC_CAL_DIS     0x00052120

/********************************* VARIABLES **********************************/
struct ad9914
{
    unsigned char CFR1;
    unsigned char CFR2;
    unsigned char CFR3;
    unsigned char CFR4;
    unsigned char DRAMP_LO;
    unsigned char DRAMP_HI;
    unsigned char DRAMP_RIS;
    unsigned char DRAMP_FAL;
    unsigned char DRAMP_RATE;
    unsigned char PROF_F[8];
    unsigned char PROF_PA[8];
    unsigned char PROF_P1_FTW[4];
};

extern const struct ad9914 ad9914_reg;

/********************************* FUNCTIONS **********************************/
/* 
 * IO_UPDATE    LATD1
 * DDS_EXT_PD   LATD2
 * M_RESET      LATD11
 * CS_DDS       LATD9 
 */


#define SCLK_0               SCLK_DDS_Clear()  
#define SCLK_1               SCLK_DDS_Set()

#define v_o_DATA_0           SDO_DDS_Clear() //SDO
#define v_o_DATA_1           SDO_DDS_Set()
  
#define v_i_DATA             SDIO_DDS_Get()   /// SDI





#define ad9914_select_dds_5()     nCSDDS_9914_Clear()//DDS2_CH2_L2
#define ad9914_unselect_dds_5()   nCSDDS_9914_Set()

#define ad9914_select_dds_4()     nCSDDS_9914_Clear()//DDS1_CH1_L2
#define ad9914_unselect_dds_4()   nCSDDS_9914_Set()

#define ad9914_select_dds_3()     nCSDDS_9914_Clear()//DDS3_CH3_L1
#define ad9914_unselect_dds_3()   nCSDDS_9914_Set()

//#define ad9914_reset_1()          {LATDbits.LATD6 = 1; LATDbits.LATD6 = 0;}   //DDS1 master reset

#define ad9914_reset_4()          {M_RST_9914_Set(); M_RST_9914_Clear();}   //DDS1 
#define ad9914_reset_3()          {M_RST_9914_Set(); M_RST_9914_Clear();}
#define ad9914_reset_5()          {M_RST_9914_Set(); M_RST_9914_Clear();}


//#define ad9914_on()             LATDbits.LATD4 = 0//DDS1
//#define ad9914_off()            LATDbits.LATD4 = 1//DDS1

#define ad9914_io_update_set()  I_O_UPDATE_Set()
#define ad9914_io_update_clr()  I_O_UPDATE_Clear()

//#define ad9914_pow_on_1()         LATDbits.LATD7 = 0;//LATCbits.LATC2 = 1
//#define ad9914_pow_off_1()        LATDbits.LATD7 = 1;//LATCbits.LATC2 = 0

#define ad9914_pow_on_4()         PWR_DOWN_9914_Clear()//
#define ad9914_pow_off_4()        PWR_DOWN_9914_Set()//

#define ad9914_pow_on_5()         PWR_DOWN_9914_Clear()//
#define ad9914_pow_off_5()        PWR_DOWN_9914_Set()//

#define ad9914_pow_on_3()         PWR_DOWN_9914_Clear()//
#define ad9914_pow_off_3()        PWR_DOWN_9914_Set()//
/* 
 * Reg's read/write functions
 */

void ad9914_write_reg(uint8_t reg, void *buf, uint32_t reg_size);
void ad9914_read_reg(uint8_t reg, void *buf, uint32_t reg_size);

/* 
 * Control functions 
 */
void ad9914_spi_3wire_mode(void);
void ad9914_init_dac_cal(void);
void ad9914_set_profile_mode(void);
void ad9914_set_lfm_mode(void);
void ad9914_set_freq(uint8_t reg_addr, uint32_t freq_curr);
void ad9914_set_phase(uint8_t reg_addr, uint32_t phase_curr);
void ad9914_set_dds_st1(void);
void ad9914_dac_on(void);
void ad9914_dac_off(void);
//void SetCMD_FPGA(uint16_t setCmd);
void ad9914_spi_3wire_osk_mode(void);
void ad9914_set_amp(uint8_t reg_addr, uint32_t amp);

#endif /* _AD9914_H */

//struct ad9914_Reg_Def 
//{
//	
//	union {
//		unsigned   CFR1;
//		struct {
////-----------------------------7:0-----------------------------------
//			unsigned  LSBf           :1;  // LSB first
//			unsigned  SDIOi          :1;  // SDIO input only
//            unsigned  Open1          :1;  // OPEN
//			unsigned  EPWDn          :1;  // External power-down control
//			unsigned  Open2          :1;  // OPEN
//			unsigned  REFCLKPWDn     :1;  // REFCLK input power-down
//			unsigned  DACPWDn        :1;  // DAC power-down          
//			unsigned  DPWDn          :1;  // Digital power-down             
////-----------------------------15:8------------------------------------------            
//			unsigned  OSKEn           :1; // OSK Enable
//			unsigned  ExtOSKEn        :1; // External OSK Enable
//            unsigned  Open3           :1; // OPEN
//			unsigned  ClPhA           :1; // Clear phase accumulator
//			unsigned  CDRampA         :1; // Clear digital ramp accumulator
//			unsigned  AClPhA          :1; // Autoclear phase accumulator
//			unsigned  AClDRampA       :1; // Autoclear digital ramp accumulator         
//			unsigned  LLRRIOU         :1; // Load LRR @ I/O update                   
////-----------------------------23:16------------------------------------------           
//			unsigned  EnSinO          :1; // Enable sine output
//			unsigned  PPStrEn         :1; // Parallel port streaming enable 
//            unsigned  Open4           :6; // OPEN			
////-----------------------------31:24------------------------------------------           
//            unsigned  VCOCalEn        :1; // VCO cal enable
//			unsigned  Open5           :7; // OPEN
//		} __attribute__((packed))CFR1Reg;
//	};
//	union {
//		unsigned   CFR2;
//		struct {
////-----------------------------7:0-----------------------------------
//			unsigned  Open1           :8; // OPEN         
////-----------------------------15:8------------------------------------------            
//			unsigned  Open2           :1; // OPEN
//			unsigned  Rrvd            :1; // Reserved
//            unsigned  SYNC_CLKinv     :1; // SYNC_CLK invert
//			unsigned  SYNC_CLKen      :1; // SYNC_CLK enable
//			unsigned  Open3           :1; // OPEN
//			unsigned  DRGOOEn         :1; // DRG over output enable
//            unsigned  FreqJmpEn       :1; // Frequency jump enable
//            unsigned  MchLatEn        :1; // Matched latency enable
////-----------------------------23:16------------------------------------------           
//			unsigned  PrgMdlEn        :1; // Program modulus enable
//			unsigned  DRnDL           :1; // Digital ramp no-dwell low
//            unsigned  DRnDH           :1; // Digital ramp no-dwell high
//			unsigned  DREn            :1; // Digital ramp enable
//			unsigned  DRD             :2; // Digital ramp destination
//            unsigned  PllDPEn         :1; // Parallel data port enable
//			unsigned  PflMEn          :1; // Profile mode enable
////-----------------------------31:24------------------------------------------           
//            unsigned  Open4           :8; //  OPEN
//
//		} __attribute__((packed))CFR2Reg;
//	};    
//	union {
//		unsigned   CFR3;
//		struct {
////-----------------------------7:0-----------------------------------
//			unsigned  MinLDW          :2; // Minimum LDW
//			unsigned  LDEn            :1; // Lock detect enable
//            unsigned  Icp             :3; // Icp  
//            unsigned  MIcpSel         :1; // Manual Icp selection  
//            unsigned  Open1           :1; // OPEN  
////-----------------------------15:8------------------------------------------            
//			unsigned  FBDiv           :8  // Feedback Divider N             
////-----------------------------23:16------------------------------------------           
//			unsigned  DClkEd          :1; // Double clock edge
//			unsigned  PLLInDivEn      :1; // PLL input divider enable
//            unsigned  PLLEn           :1; // PLL enable
//            unsigned  DblEn           :1; // Doubler enable
//            unsigned  InDiv           :2; // Input divider
//            unsigned  InDivRes        :1; // Input divider reset
//            unsigned  Open2           :1; // OPEN
////-----------------------------31:24------------------------------------------           
//            unsigned  Open3           :8; // Open
//		
//		} __attribute__((packed))CFR3Reg;
//	};    
//	union {
//		unsigned   CFR4;
//		struct {
////-----------------------------7:23-----------------------------------
//			unsigned  RRDVS           :24; // Requires register default value settings (0x20)     
////-----------------------------31:24------------------------------------------           
//			unsigned  DAC_CALEn       :1; // DAC CAL enable
//            unsigned  DAC_CALClkPD    :1; // DAC CAL clock power-down
//            unsigned  ADivPD          :1; // Auxiliary divider power-down
//            unsigned  Open            :5; // OPEN
//
//		} __attribute__((packed))CFR4Reg;
//	};      
//	union {
//		unsigned   DRLLR;
//		struct {
//
//			unsigned  DRLL            :32; // Digital ramp lower limit    
//
//		} __attribute__((packed))DRLLRReg;
//	};       
//
//	union {
//		unsigned   DRULR;
//		struct {
//
//			unsigned  DRUL            :32; // Digital ramp upper limit      
//          
//		} __attribute__((packed))DRULRReg;
//	};  
//    union {
//        unsigned short   RDRSSR;
//        struct {
//
//			unsigned  RDRSS           :32; // Rising digital ramp increment step size     
///
//        } __attribute__((packed))RDRSSRReg;
//    };    
//    union {
//        unsigned   FDRSSR;
//        struct {
//
//			unsigned  FDRSS           :32; // Falling digital ramp increment step size     
//            
//        } __attribute__((packed))FDRSSRReg;
//    };
//    union {
//        unsigned   DRRR;
//        struct {
////-----------------------------15:0-----------------------------------
//			unsigned  DRRP            :16; // Digital ramp positive slope rate     
////-----------------------------231:16------------------------------------------           
//			unsigned  DRRN            :16; // Digital ramp negative slope rate
//                  
//        } __attribute__((packed))DRRRReg;
//    }; 
//    union {        
//        unsigned long long   LFJP;
//        struct {
//       
//            unsigned    LFJP          :32; // Lower frequency jump point
//        
//        } __attribute__((packed))LFJPReg;
//    };    
//    union {        
//        unsigned long long   UFJP;
//        struct {
//       
//            unsigned   UFJP           :32; // Upper frequency jump point
//        
//        } __attribute__((packed))UFJPReg;
//    };        
//    
//};