/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef XC_channel_H
#define	XC_channel_H

#include <xc.h> // include processor files - each processor file is guarded.  

#define SWRF1 1
#define SWRF2 2
#define SWRF3 3
#define SWRF4 4

void SW1DA3_RF1(char sw);
void SW2DA16_RF1(char sw);
void SW3DA39_RF2(char sw);
void SW3DA45_RF3(char sw);

#endif    //XC_channel_H