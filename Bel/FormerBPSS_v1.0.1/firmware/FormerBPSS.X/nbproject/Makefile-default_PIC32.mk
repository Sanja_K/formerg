#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default_PIC32.mk)" "nbproject/Makefile-local-default_PIC32.mk"
include nbproject/Makefile-local-default_PIC32.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default_PIC32
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/FormerBPSS.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/FormerBPSS.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/config/default_PIC32/peripheral/clk/plib_clk.c ../src/config/default_PIC32/peripheral/evic/plib_evic.c ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c ../src/config/default_PIC32/peripheral/tmr/plib_tmr4.c ../src/config/default_PIC32/peripheral/tmr/plib_tmr2.c ../src/config/default_PIC32/peripheral/uart/plib_uart2.c ../src/config/default_PIC32/stdio/xc32_monitor.c ../src/config/default_PIC32/initialization.c ../src/config/default_PIC32/interrupts.c ../src/config/default_PIC32/exceptions.c ../src/main.c ../src/user.c ../src/system_cfg.c ../src/ADF435x.c ../src/DDS_AD9959.c ../src/LMX2572.c ../src/ad9914.c ../src/channel.c ../src/executeCMD.c ../src/former_signal.c ../src/softwareSPI.c ../src/synt_AD4351.c ../src/synt_AD9959.c ../src/ad7995.c ../src/XC_DATA.c ../src/eepromParam.c ../src/hmc1122.c ../src/adl5240.c ../src/I2C.c ../src/eeprom.c ../src/ReceiveCMD.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1471492474/plib_clk.o ${OBJECTDIR}/_ext/1628442791/plib_evic.o ${OBJECTDIR}/_ext/1628496619/plib_gpio.o ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o ${OBJECTDIR}/_ext/1628899562/plib_uart2.o ${OBJECTDIR}/_ext/279352370/xc32_monitor.o ${OBJECTDIR}/_ext/1177533048/initialization.o ${OBJECTDIR}/_ext/1177533048/interrupts.o ${OBJECTDIR}/_ext/1177533048/exceptions.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1360937237/user.o ${OBJECTDIR}/_ext/1360937237/system_cfg.o ${OBJECTDIR}/_ext/1360937237/ADF435x.o ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o ${OBJECTDIR}/_ext/1360937237/LMX2572.o ${OBJECTDIR}/_ext/1360937237/ad9914.o ${OBJECTDIR}/_ext/1360937237/channel.o ${OBJECTDIR}/_ext/1360937237/executeCMD.o ${OBJECTDIR}/_ext/1360937237/former_signal.o ${OBJECTDIR}/_ext/1360937237/softwareSPI.o ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o ${OBJECTDIR}/_ext/1360937237/ad7995.o ${OBJECTDIR}/_ext/1360937237/XC_DATA.o ${OBJECTDIR}/_ext/1360937237/eepromParam.o ${OBJECTDIR}/_ext/1360937237/hmc1122.o ${OBJECTDIR}/_ext/1360937237/adl5240.o ${OBJECTDIR}/_ext/1360937237/I2C.o ${OBJECTDIR}/_ext/1360937237/eeprom.o ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1471492474/plib_clk.o.d ${OBJECTDIR}/_ext/1628442791/plib_evic.o.d ${OBJECTDIR}/_ext/1628496619/plib_gpio.o.d ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o.d ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o.d ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o.d ${OBJECTDIR}/_ext/1628899562/plib_uart2.o.d ${OBJECTDIR}/_ext/279352370/xc32_monitor.o.d ${OBJECTDIR}/_ext/1177533048/initialization.o.d ${OBJECTDIR}/_ext/1177533048/interrupts.o.d ${OBJECTDIR}/_ext/1177533048/exceptions.o.d ${OBJECTDIR}/_ext/1360937237/main.o.d ${OBJECTDIR}/_ext/1360937237/user.o.d ${OBJECTDIR}/_ext/1360937237/system_cfg.o.d ${OBJECTDIR}/_ext/1360937237/ADF435x.o.d ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o.d ${OBJECTDIR}/_ext/1360937237/LMX2572.o.d ${OBJECTDIR}/_ext/1360937237/ad9914.o.d ${OBJECTDIR}/_ext/1360937237/channel.o.d ${OBJECTDIR}/_ext/1360937237/executeCMD.o.d ${OBJECTDIR}/_ext/1360937237/former_signal.o.d ${OBJECTDIR}/_ext/1360937237/softwareSPI.o.d ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o.d ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o.d ${OBJECTDIR}/_ext/1360937237/ad7995.o.d ${OBJECTDIR}/_ext/1360937237/XC_DATA.o.d ${OBJECTDIR}/_ext/1360937237/eepromParam.o.d ${OBJECTDIR}/_ext/1360937237/hmc1122.o.d ${OBJECTDIR}/_ext/1360937237/adl5240.o.d ${OBJECTDIR}/_ext/1360937237/I2C.o.d ${OBJECTDIR}/_ext/1360937237/eeprom.o.d ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1471492474/plib_clk.o ${OBJECTDIR}/_ext/1628442791/plib_evic.o ${OBJECTDIR}/_ext/1628496619/plib_gpio.o ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o ${OBJECTDIR}/_ext/1628899562/plib_uart2.o ${OBJECTDIR}/_ext/279352370/xc32_monitor.o ${OBJECTDIR}/_ext/1177533048/initialization.o ${OBJECTDIR}/_ext/1177533048/interrupts.o ${OBJECTDIR}/_ext/1177533048/exceptions.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1360937237/user.o ${OBJECTDIR}/_ext/1360937237/system_cfg.o ${OBJECTDIR}/_ext/1360937237/ADF435x.o ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o ${OBJECTDIR}/_ext/1360937237/LMX2572.o ${OBJECTDIR}/_ext/1360937237/ad9914.o ${OBJECTDIR}/_ext/1360937237/channel.o ${OBJECTDIR}/_ext/1360937237/executeCMD.o ${OBJECTDIR}/_ext/1360937237/former_signal.o ${OBJECTDIR}/_ext/1360937237/softwareSPI.o ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o ${OBJECTDIR}/_ext/1360937237/ad7995.o ${OBJECTDIR}/_ext/1360937237/XC_DATA.o ${OBJECTDIR}/_ext/1360937237/eepromParam.o ${OBJECTDIR}/_ext/1360937237/hmc1122.o ${OBJECTDIR}/_ext/1360937237/adl5240.o ${OBJECTDIR}/_ext/1360937237/I2C.o ${OBJECTDIR}/_ext/1360937237/eeprom.o ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o

# Source Files
SOURCEFILES=../src/config/default_PIC32/peripheral/clk/plib_clk.c ../src/config/default_PIC32/peripheral/evic/plib_evic.c ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c ../src/config/default_PIC32/peripheral/tmr/plib_tmr4.c ../src/config/default_PIC32/peripheral/tmr/plib_tmr2.c ../src/config/default_PIC32/peripheral/uart/plib_uart2.c ../src/config/default_PIC32/stdio/xc32_monitor.c ../src/config/default_PIC32/initialization.c ../src/config/default_PIC32/interrupts.c ../src/config/default_PIC32/exceptions.c ../src/main.c ../src/user.c ../src/system_cfg.c ../src/ADF435x.c ../src/DDS_AD9959.c ../src/LMX2572.c ../src/ad9914.c ../src/channel.c ../src/executeCMD.c ../src/former_signal.c ../src/softwareSPI.c ../src/synt_AD4351.c ../src/synt_AD9959.c ../src/ad7995.c ../src/XC_DATA.c ../src/eepromParam.c ../src/hmc1122.c ../src/adl5240.c ../src/I2C.c ../src/eeprom.c ../src/ReceiveCMD.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default_PIC32.mk dist/${CND_CONF}/${IMAGE_TYPE}/FormerBPSS.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX795F512L
MP_LINKER_FILE_OPTION=,--script="..\src\config\default_PIC32\p32MX795F512L.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1471492474/plib_clk.o: ../src/config/default_PIC32/peripheral/clk/plib_clk.c  .generated_files/d918959263c1c06a103b577d8e2285a574f1997b.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1471492474" 
	@${RM} ${OBJECTDIR}/_ext/1471492474/plib_clk.o.d 
	@${RM} ${OBJECTDIR}/_ext/1471492474/plib_clk.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1471492474/plib_clk.o.d" -o ${OBJECTDIR}/_ext/1471492474/plib_clk.o ../src/config/default_PIC32/peripheral/clk/plib_clk.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1628442791/plib_evic.o: ../src/config/default_PIC32/peripheral/evic/plib_evic.c  .generated_files/c53cedd1d6f7adde2fa3d508239779ef238533b.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1628442791" 
	@${RM} ${OBJECTDIR}/_ext/1628442791/plib_evic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1628442791/plib_evic.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1628442791/plib_evic.o.d" -o ${OBJECTDIR}/_ext/1628442791/plib_evic.o ../src/config/default_PIC32/peripheral/evic/plib_evic.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1628496619/plib_gpio.o: ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c  .generated_files/d53d8f63c0c9e02773f324bd6fb8bc6fb156883d.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1628496619" 
	@${RM} ${OBJECTDIR}/_ext/1628496619/plib_gpio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1628496619/plib_gpio.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1628496619/plib_gpio.o.d" -o ${OBJECTDIR}/_ext/1628496619/plib_gpio.o ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o: ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c  .generated_files/598547bae6b9514ef000a2322618d93498ccb029.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/997135251" 
	@${RM} ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o.d 
	@${RM} ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o.d" -o ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1471476099/plib_tmr4.o: ../src/config/default_PIC32/peripheral/tmr/plib_tmr4.c  .generated_files/e668ae2fb229d3ab8a5cbaf6668a031ecbc2f368.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1471476099" 
	@${RM} ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o.d 
	@${RM} ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1471476099/plib_tmr4.o.d" -o ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o ../src/config/default_PIC32/peripheral/tmr/plib_tmr4.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1471476099/plib_tmr2.o: ../src/config/default_PIC32/peripheral/tmr/plib_tmr2.c  .generated_files/434c7094df74fd4a18d36e607fb00f5fdb2f02b9.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1471476099" 
	@${RM} ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o.d 
	@${RM} ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1471476099/plib_tmr2.o.d" -o ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o ../src/config/default_PIC32/peripheral/tmr/plib_tmr2.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1628899562/plib_uart2.o: ../src/config/default_PIC32/peripheral/uart/plib_uart2.c  .generated_files/3ef6ea2754d51601b3770145b4ac2f1b6c8dfc77.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1628899562" 
	@${RM} ${OBJECTDIR}/_ext/1628899562/plib_uart2.o.d 
	@${RM} ${OBJECTDIR}/_ext/1628899562/plib_uart2.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1628899562/plib_uart2.o.d" -o ${OBJECTDIR}/_ext/1628899562/plib_uart2.o ../src/config/default_PIC32/peripheral/uart/plib_uart2.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/279352370/xc32_monitor.o: ../src/config/default_PIC32/stdio/xc32_monitor.c  .generated_files/cba018207b38657375da8128d47495ce049a4a7a.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/279352370" 
	@${RM} ${OBJECTDIR}/_ext/279352370/xc32_monitor.o.d 
	@${RM} ${OBJECTDIR}/_ext/279352370/xc32_monitor.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/279352370/xc32_monitor.o.d" -o ${OBJECTDIR}/_ext/279352370/xc32_monitor.o ../src/config/default_PIC32/stdio/xc32_monitor.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1177533048/initialization.o: ../src/config/default_PIC32/initialization.c  .generated_files/acba61c44c6917d4760fc3e01160942bce275367.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/initialization.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/initialization.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1177533048/initialization.o.d" -o ${OBJECTDIR}/_ext/1177533048/initialization.o ../src/config/default_PIC32/initialization.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1177533048/interrupts.o: ../src/config/default_PIC32/interrupts.c  .generated_files/f5e99ae7791f547815f30f6e8627c3805c4c2b4f.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/interrupts.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1177533048/interrupts.o.d" -o ${OBJECTDIR}/_ext/1177533048/interrupts.o ../src/config/default_PIC32/interrupts.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1177533048/exceptions.o: ../src/config/default_PIC32/exceptions.c  .generated_files/48faf6dd25ff997e4370344bfd78c0858a3414a0.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1177533048/exceptions.o.d" -o ${OBJECTDIR}/_ext/1177533048/exceptions.o ../src/config/default_PIC32/exceptions.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  .generated_files/c66691d1cf29d884c2de0fd0f48f7a76cf2c862.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/user.o: ../src/user.c  .generated_files/af6a90dffc7d6111d07288615b4b4ac44a2cacc4.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/user.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/user.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/user.o.d" -o ${OBJECTDIR}/_ext/1360937237/user.o ../src/user.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/system_cfg.o: ../src/system_cfg.c  .generated_files/6fe123b6bfa7a5f1487a9a8c881b159b38489071.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/system_cfg.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/system_cfg.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/system_cfg.o.d" -o ${OBJECTDIR}/_ext/1360937237/system_cfg.o ../src/system_cfg.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/ADF435x.o: ../src/ADF435x.c  .generated_files/68a204c5bae96970aead74b1a370ca7a8d976f8f.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ADF435x.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ADF435x.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ADF435x.o.d" -o ${OBJECTDIR}/_ext/1360937237/ADF435x.o ../src/ADF435x.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o: ../src/DDS_AD9959.c  .generated_files/60c8fb7f194378ebd3ae39a275cdd949d7080f06.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o.d" -o ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o ../src/DDS_AD9959.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/LMX2572.o: ../src/LMX2572.c  .generated_files/2472bdaa5a46253d60f082dac3d8c02d073eb361.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/LMX2572.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/LMX2572.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/LMX2572.o.d" -o ${OBJECTDIR}/_ext/1360937237/LMX2572.o ../src/LMX2572.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/ad9914.o: ../src/ad9914.c  .generated_files/e5dc2a913d50435fbcd58927596c378e78a1ed2a.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad9914.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad9914.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ad9914.o.d" -o ${OBJECTDIR}/_ext/1360937237/ad9914.o ../src/ad9914.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/channel.o: ../src/channel.c  .generated_files/4bfe1dce6f49313f8fe93a54f65a211bad3f940a.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/channel.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/channel.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/channel.o.d" -o ${OBJECTDIR}/_ext/1360937237/channel.o ../src/channel.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/executeCMD.o: ../src/executeCMD.c  .generated_files/ae2b6150f717f198fa5796af6985a0a50168676b.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/executeCMD.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/executeCMD.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/executeCMD.o.d" -o ${OBJECTDIR}/_ext/1360937237/executeCMD.o ../src/executeCMD.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/former_signal.o: ../src/former_signal.c  .generated_files/ef38b519ebb56b13b672c2c1945b36469b4a8092.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/former_signal.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/former_signal.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/former_signal.o.d" -o ${OBJECTDIR}/_ext/1360937237/former_signal.o ../src/former_signal.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/softwareSPI.o: ../src/softwareSPI.c  .generated_files/ae20cce4173ae5ed23f7de363f4efa7dfa960024.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/softwareSPI.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/softwareSPI.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/softwareSPI.o.d" -o ${OBJECTDIR}/_ext/1360937237/softwareSPI.o ../src/softwareSPI.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/synt_AD4351.o: ../src/synt_AD4351.c  .generated_files/d912164d9897e2720faed2d268a30bed344f5412.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/synt_AD4351.o.d" -o ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o ../src/synt_AD4351.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/synt_AD9959.o: ../src/synt_AD9959.c  .generated_files/c4a8e8e4ae40496f0e635371579a40faa0344b73.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/synt_AD9959.o.d" -o ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o ../src/synt_AD9959.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/ad7995.o: ../src/ad7995.c  .generated_files/91b5f3c78b54440070e01e68d87361efb297b579.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad7995.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad7995.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ad7995.o.d" -o ${OBJECTDIR}/_ext/1360937237/ad7995.o ../src/ad7995.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/XC_DATA.o: ../src/XC_DATA.c  .generated_files/46cb0a8bb5fd021faff4d5469dae69f957f6027f.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/XC_DATA.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/XC_DATA.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/XC_DATA.o.d" -o ${OBJECTDIR}/_ext/1360937237/XC_DATA.o ../src/XC_DATA.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/eepromParam.o: ../src/eepromParam.c  .generated_files/61ec5ab6051e5eccc59ac50dab148a2567e30edb.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eepromParam.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eepromParam.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/eepromParam.o.d" -o ${OBJECTDIR}/_ext/1360937237/eepromParam.o ../src/eepromParam.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/hmc1122.o: ../src/hmc1122.c  .generated_files/a9455b403894194317ce3535ab4665d62676e6ba.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/hmc1122.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/hmc1122.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/hmc1122.o.d" -o ${OBJECTDIR}/_ext/1360937237/hmc1122.o ../src/hmc1122.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/adl5240.o: ../src/adl5240.c  .generated_files/955799ddfa4984c457297390ca2e653e0e2cd971.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/adl5240.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/adl5240.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/adl5240.o.d" -o ${OBJECTDIR}/_ext/1360937237/adl5240.o ../src/adl5240.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/I2C.o: ../src/I2C.c  .generated_files/67542f5ce5a2d16da14ac5c6636a4f7ead410fc9.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/I2C.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/I2C.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/I2C.o.d" -o ${OBJECTDIR}/_ext/1360937237/I2C.o ../src/I2C.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/eeprom.o: ../src/eeprom.c  .generated_files/1da18b01d8165cfd50b1d23732dc03d8565f81b8.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eeprom.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eeprom.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/eeprom.o.d" -o ${OBJECTDIR}/_ext/1360937237/eeprom.o ../src/eeprom.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o: ../src/ReceiveCMD.c  .generated_files/9b1a65ef9f14b9110f35ba809f6d41029251c1d7.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o.d" -o ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o ../src/ReceiveCMD.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
else
${OBJECTDIR}/_ext/1471492474/plib_clk.o: ../src/config/default_PIC32/peripheral/clk/plib_clk.c  .generated_files/15230f874a61a7facc64ce374455019f7fb6949.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1471492474" 
	@${RM} ${OBJECTDIR}/_ext/1471492474/plib_clk.o.d 
	@${RM} ${OBJECTDIR}/_ext/1471492474/plib_clk.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1471492474/plib_clk.o.d" -o ${OBJECTDIR}/_ext/1471492474/plib_clk.o ../src/config/default_PIC32/peripheral/clk/plib_clk.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1628442791/plib_evic.o: ../src/config/default_PIC32/peripheral/evic/plib_evic.c  .generated_files/5b9ff1e7fc34a04b726f4db4884e5b7afca96d47.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1628442791" 
	@${RM} ${OBJECTDIR}/_ext/1628442791/plib_evic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1628442791/plib_evic.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1628442791/plib_evic.o.d" -o ${OBJECTDIR}/_ext/1628442791/plib_evic.o ../src/config/default_PIC32/peripheral/evic/plib_evic.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1628496619/plib_gpio.o: ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c  .generated_files/c1b27b671104af352e0776ebd037d0e86fb13029.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1628496619" 
	@${RM} ${OBJECTDIR}/_ext/1628496619/plib_gpio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1628496619/plib_gpio.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1628496619/plib_gpio.o.d" -o ${OBJECTDIR}/_ext/1628496619/plib_gpio.o ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o: ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c  .generated_files/439ad31d5c931da52e1b320389f20e471e84fafb.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/997135251" 
	@${RM} ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o.d 
	@${RM} ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o.d" -o ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1471476099/plib_tmr4.o: ../src/config/default_PIC32/peripheral/tmr/plib_tmr4.c  .generated_files/2f2f7dd06d606d99120fa0860d087ea2b94a7cb.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1471476099" 
	@${RM} ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o.d 
	@${RM} ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1471476099/plib_tmr4.o.d" -o ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o ../src/config/default_PIC32/peripheral/tmr/plib_tmr4.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1471476099/plib_tmr2.o: ../src/config/default_PIC32/peripheral/tmr/plib_tmr2.c  .generated_files/ec789035d9bdae2d2a51e60777ecfc3bb5c36301.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1471476099" 
	@${RM} ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o.d 
	@${RM} ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1471476099/plib_tmr2.o.d" -o ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o ../src/config/default_PIC32/peripheral/tmr/plib_tmr2.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1628899562/plib_uart2.o: ../src/config/default_PIC32/peripheral/uart/plib_uart2.c  .generated_files/5fc599ba5bfe36c85f86d3dd4f48b942d295fe82.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1628899562" 
	@${RM} ${OBJECTDIR}/_ext/1628899562/plib_uart2.o.d 
	@${RM} ${OBJECTDIR}/_ext/1628899562/plib_uart2.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1628899562/plib_uart2.o.d" -o ${OBJECTDIR}/_ext/1628899562/plib_uart2.o ../src/config/default_PIC32/peripheral/uart/plib_uart2.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/279352370/xc32_monitor.o: ../src/config/default_PIC32/stdio/xc32_monitor.c  .generated_files/f89ebd4d3216c191485cda14039b3e1e3db7b74b.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/279352370" 
	@${RM} ${OBJECTDIR}/_ext/279352370/xc32_monitor.o.d 
	@${RM} ${OBJECTDIR}/_ext/279352370/xc32_monitor.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/279352370/xc32_monitor.o.d" -o ${OBJECTDIR}/_ext/279352370/xc32_monitor.o ../src/config/default_PIC32/stdio/xc32_monitor.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1177533048/initialization.o: ../src/config/default_PIC32/initialization.c  .generated_files/13c5421842a64615d9284fe6a9e85aeb77514e58.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/initialization.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/initialization.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1177533048/initialization.o.d" -o ${OBJECTDIR}/_ext/1177533048/initialization.o ../src/config/default_PIC32/initialization.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1177533048/interrupts.o: ../src/config/default_PIC32/interrupts.c  .generated_files/ef09c4e0d2831740840d463b492bd4f0d00b9fe8.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/interrupts.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1177533048/interrupts.o.d" -o ${OBJECTDIR}/_ext/1177533048/interrupts.o ../src/config/default_PIC32/interrupts.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1177533048/exceptions.o: ../src/config/default_PIC32/exceptions.c  .generated_files/49addd2269a6947a81689ad743928973955ba149.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1177533048/exceptions.o.d" -o ${OBJECTDIR}/_ext/1177533048/exceptions.o ../src/config/default_PIC32/exceptions.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  .generated_files/f6d27a74346f6091333d96fb687c9b544ff5d5f8.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/user.o: ../src/user.c  .generated_files/6ba834324753e7b63318b55a5580899807c01fa3.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/user.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/user.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/user.o.d" -o ${OBJECTDIR}/_ext/1360937237/user.o ../src/user.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/system_cfg.o: ../src/system_cfg.c  .generated_files/633c5ce6d368116dc05378743045b027a4bcf12a.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/system_cfg.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/system_cfg.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/system_cfg.o.d" -o ${OBJECTDIR}/_ext/1360937237/system_cfg.o ../src/system_cfg.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/ADF435x.o: ../src/ADF435x.c  .generated_files/f465d2225b12b876f7c0c54f1f6599555b5cf3a5.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ADF435x.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ADF435x.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ADF435x.o.d" -o ${OBJECTDIR}/_ext/1360937237/ADF435x.o ../src/ADF435x.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o: ../src/DDS_AD9959.c  .generated_files/d054af87fab1537a0ce94051aee7a1d112e51e54.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o.d" -o ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o ../src/DDS_AD9959.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/LMX2572.o: ../src/LMX2572.c  .generated_files/be8790a2eeb1256697a50f3d614f34d929d2980e.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/LMX2572.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/LMX2572.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/LMX2572.o.d" -o ${OBJECTDIR}/_ext/1360937237/LMX2572.o ../src/LMX2572.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/ad9914.o: ../src/ad9914.c  .generated_files/c2750773d22365534358caa83f60b7584d5ffd30.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad9914.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad9914.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ad9914.o.d" -o ${OBJECTDIR}/_ext/1360937237/ad9914.o ../src/ad9914.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/channel.o: ../src/channel.c  .generated_files/68592e4762e8c542a1660c7d9890318248460c9d.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/channel.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/channel.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/channel.o.d" -o ${OBJECTDIR}/_ext/1360937237/channel.o ../src/channel.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/executeCMD.o: ../src/executeCMD.c  .generated_files/e9d76022c2efc1490e5d72879eaf5463ee83dd55.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/executeCMD.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/executeCMD.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/executeCMD.o.d" -o ${OBJECTDIR}/_ext/1360937237/executeCMD.o ../src/executeCMD.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/former_signal.o: ../src/former_signal.c  .generated_files/3caf65df7f7f7d9e2b4addbea01cce422c43c6d6.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/former_signal.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/former_signal.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/former_signal.o.d" -o ${OBJECTDIR}/_ext/1360937237/former_signal.o ../src/former_signal.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/softwareSPI.o: ../src/softwareSPI.c  .generated_files/61365c1af59b5f99e3e43fdd424989ad66aba09.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/softwareSPI.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/softwareSPI.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/softwareSPI.o.d" -o ${OBJECTDIR}/_ext/1360937237/softwareSPI.o ../src/softwareSPI.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/synt_AD4351.o: ../src/synt_AD4351.c  .generated_files/17e24dcf1c5876bee4c8efba3259a5354d3b65bb.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/synt_AD4351.o.d" -o ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o ../src/synt_AD4351.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/synt_AD9959.o: ../src/synt_AD9959.c  .generated_files/c7fa28477ce8058eefb3896ed6c561b9a13d3025.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/synt_AD9959.o.d" -o ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o ../src/synt_AD9959.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/ad7995.o: ../src/ad7995.c  .generated_files/b2a6a131be005141432de5afb72ea05a62c8439e.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad7995.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad7995.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ad7995.o.d" -o ${OBJECTDIR}/_ext/1360937237/ad7995.o ../src/ad7995.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/XC_DATA.o: ../src/XC_DATA.c  .generated_files/4858d03cfb6361e317d8e3136f028aed0b1ed4c5.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/XC_DATA.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/XC_DATA.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/XC_DATA.o.d" -o ${OBJECTDIR}/_ext/1360937237/XC_DATA.o ../src/XC_DATA.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/eepromParam.o: ../src/eepromParam.c  .generated_files/35c6c8e9c4c7dcc361bdd8d3a8058d5be573ca27.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eepromParam.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eepromParam.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/eepromParam.o.d" -o ${OBJECTDIR}/_ext/1360937237/eepromParam.o ../src/eepromParam.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/hmc1122.o: ../src/hmc1122.c  .generated_files/541c22003fdabe2c542aaaf9e45b2a4ae0a25825.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/hmc1122.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/hmc1122.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/hmc1122.o.d" -o ${OBJECTDIR}/_ext/1360937237/hmc1122.o ../src/hmc1122.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/adl5240.o: ../src/adl5240.c  .generated_files/1c15fb11c36a5b56dd7a95edab8f6f59f567537b.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/adl5240.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/adl5240.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/adl5240.o.d" -o ${OBJECTDIR}/_ext/1360937237/adl5240.o ../src/adl5240.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/I2C.o: ../src/I2C.c  .generated_files/696ff05d3ab062aa5aba77bd90eb6e0656f369c0.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/I2C.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/I2C.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/I2C.o.d" -o ${OBJECTDIR}/_ext/1360937237/I2C.o ../src/I2C.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/eeprom.o: ../src/eeprom.c  .generated_files/7c3c2602863c9438d75c2a57dec0abc7a9f7019d.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eeprom.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eeprom.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/eeprom.o.d" -o ${OBJECTDIR}/_ext/1360937237/eeprom.o ../src/eeprom.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o: ../src/ReceiveCMD.c  .generated_files/bbd53e03de46a914dfa386750d4d19a470d59f04.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o.d" -o ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o ../src/ReceiveCMD.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/FormerBPSS.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../src/config/default_PIC32/p32MX795F512L.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g -mdebugger -D__MPLAB_DEBUGGER_ICD4=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/FormerBPSS.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   -mreserve=data@0x0:0x1FC -mreserve=boot@0x1FC02000:0x1FC02FEF -mreserve=boot@0x1FC02000:0x1FC024FF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=__MPLAB_DEBUGGER_ICD4=1,--defsym=_min_heap_size=512,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -mdfp="${DFP_DIR}"
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/FormerBPSS.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../src/config/default_PIC32/p32MX795F512L.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/FormerBPSS.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=512,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -mdfp="${DFP_DIR}"
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/FormerBPSS.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default_PIC32
	${RM} -r dist/default_PIC32

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
