//**********************************************************************
// � 2013 Radar DB.
// FileName:        main.c
// Dependencies:    Header (.h) files if applicable, see below
// Processor:       dsPIC33FJ128GP708A
// Compiler:        MPLAB� C30 v3.00 or higher
// IDE:             MPLAB� IDE v8.83 or later

// REVISION HISTORY:
//     Version      1.3.120
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//     Author                  Date     Comments on this revision
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//     Oleg Shabrov           --/--/-- First release of source file
//     Aleksey Martinovich    26/07/14 First release of source file
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#include <math.h>
#include <xc.h>
#include <sys/attribs.h>
#include <stddef.h>
#include <string.h>
#include "Declaration.h"
#include "plib.h"
#include "common.h"
#include "ad9914.h"
#include "adf4351.h"
#include "former_signal.h"
#include "eeprom.h"
#include "periphery.h"
#include "LMX2592.h"
#include "adl5240.h"

// <editor-fold defaultstate="collapsed" desc="Configuration bits">
// DEVCFG3
// USERID = No Setting
#pragma config FSRSSEL = PRIORITY_7     // SRS Select (SRS Priority 7)
#pragma config FMIIEN = OFF             // Ethernet RMII/MII Enable (RMII Enabled)
#pragma config FETHIO = OFF             // Ethernet I/O Pin Select (Alternate Ethernet I/O)
#pragma config FCANIO = OFF             // CAN I/O Pin Select (Alternate CAN I/O)
#pragma config FUSBIDIO = OFF           // USB USID Selection (Controlled by Port Function)
#pragma config FVBUSONIO = OFF          // USB VBUS ON Selection (Controlled by Port Function)

// DEVCFG2
#pragma config FPLLIDIV = DIV_2         // PLL Input Divider (2x Divider)
#pragma config FPLLMUL = MUL_20         // PLL Multiplier (20x Multiplier)
#pragma config UPLLIDIV = DIV_1         // USB PLL Input Divider (1x Divider)
#pragma config UPLLEN = OFF             // USB PLL Enable (Disabled and Bypassed)
#pragma config FPLLODIV = DIV_1         // System PLL Output Clock Divider (PLL Divide by 1)

// DEVCFG1
#pragma config FNOSC = PRIPLL           // Oscillator Selection Bits (Primary Osc w/PLL (XT+,HS+,EC+PLL))
#pragma config FSOSCEN = OFF            // Secondary Oscillator Enable (Disabled)
#pragma config IESO = OFF               // Internal/External Switch Over (Disabled)
#pragma config POSCMOD = XT             // Primary Oscillator Configuration (XT osc mode)
#pragma config OSCIOFNC = OFF           // CLKO Output Signal Active on the OSCO Pin (Disabled)
#pragma config FPBDIV = DIV_1           // Peripheral Clock Divisor (Pb_Clk is Sys_Clk/1)
#pragma config FCKSM = CSDCMD           // Clock Switching and Monitor Selection (Clock Switch Disable, FSCM Disabled)
#pragma config WDTPS = PS1024           // Watchdog Timer Postscaler (1:1024)
#pragma config FWDTEN = ON              // Watchdog Timer Enable (WDT Enabled)

// DEVCFG0
#pragma config DEBUG = OFF              // Background Debugger Enable (Debugger is disabled)
#pragma config ICESEL = ICS_PGx1        // ICE/ICD Comm Channel Select (ICE EMUC1/EMUD1 onion omegapins shared with PGC1/PGD1)
#pragma config PWP = OFF                // Program Flash Write Protect (Disable)
#pragma config BWP = OFF                // Boot Flash Write Protect bit (Protection Disabled)
#pragma config CP = OFF                 // Code Protect (Protection Disabled)
// </editor-fold>

// <editor-fold defaultstate="collapsed" desc="Global variables">
static struct flags flg;
static struct lit_parameters param;
static unsigned int lit_amp[LIT_NUM];

static unsigned char buf_uart[256];
static unsigned int read_buf;
static unsigned int data_len, number_buf;

/* 
 * ���������� ��� �������� ���������� ���������� ������� 
 */
static unsigned char iri_num;              // ���������� ���
static unsigned char modulation_code[4];   // ��� ��������� �������
static unsigned char deviation_code[4];    // ��� �������� �������
static unsigned char manipulation_code[4]; // ��� ����������� �������
static unsigned char duration_code[4];     // ��� ������������

static unsigned long long int freq_abs[4];
static unsigned long long int freq_hop_start_abs;

static unsigned int curr_modulation = 0;
static unsigned int duration_step_counter;
static unsigned int cur_iri_step;
static unsigned int cur_fnm_step;
static unsigned int suppression_mode = FIXED_FREQ;

/* 
 * Tables for generating signals 
 */
static const unsigned int noise_table[256] =
{
    23, 20, 21, 35, 27, 18, 29, 29, 28, 25, 28, 24, 32, 17, 19, 23, 25, 19, 33, 26, 
    25, 25, 32, 18, 21, 25, 21, 14, 24, 46, 24, 24, 29, 19, 25, 24, 25, 17, 42, 32, 
    35, 8,  21, 27, 47, 28, 15, 32, 29, 17, 17, 16, 26, 31, 27, 35, 21, 34, 22, 21, 
    21, 19, 20, 31, 50, 19, 29, 22, 28, 34, 13, 24, 24, 27, 6,  25, 28, 26, 26, 27, 
    25, 4,  44, 30, 14, 27, 36, 30, 17, 31, 15, 28, 21, 18, 18, 26, 31, 22, 31, 16, 
    24, 31, 16, 0,  34, 2,  30, 20, 27, 33, 35, 25, 20, 37, 36, 3,  19, 22, 15, 26, 
    30, 25, 34, 24, 13, 26, 20, 26, 31, 37, 19, 32, 29, 29, 27, 19, 41, 22, 28, 36, 
    32, 15, 28, 21, 30, 36, 39, 32, 22, 23, 20, 20, 26, 23, 16, 18, 43, 34, 25, 16, 
    30, 20, 27, 17, 26, 31, 35, 38, 23, 28, 22, 11, 25, 29, 34, 21, 21, 23, 18, 23, 
    19, 26, 33, 17, 33, 30, 10, 23, 26, 24, 22, 38, 32, 33, 13, 30, 29, 25, 1,  33, 
    45, 16, 24, 24, 48, 9,  37, 31, 24, 22, 31, 27, 18, 28, 39, 28, 22, 30, 30, 21, 
    19, 40, 29, 49, 26, 20, 27, 12, 27, 24, 22, 15, 14, 18, 20, 23, 7,  33, 30, 23, 
    27, 23, 11, 28, 29, 26, 29, 23, 14, 23, 22, 12, 20, 28, 22, 5
};

/* All parameters are in Hz!!! */
static const unsigned long int fnm_df_table[19] =
{
    1750,
    2470,
    3500,
    4200,
    5000,
    7070,
    10000,
    15800,
    25000,
    35350,
    50000,
    70000,
    100000,
    150000,
    250000,
    350000,
    500000,
    700000,
    1000000
};

static const unsigned long int fnm_step_table[19] =
{
    70,
    98,
    140,
    168,
    200,
    282,
    400,
    632,
    1000,
    1414,
    2000,
    2800,
    4000,
    6000,
    10000,
    14000,
    20000,
    28000,
    40000
};

/* Start frequency for FSK */
static const unsigned long int fsk_df_table[12] =
{
    2000,
    4000,
    6000,
    8000,
    10000,
    12000,
    14000,
    16000,
    18000,
    20000,
    22000,
    24000
};

static const unsigned int fsk_step_table[3][12] = {
    /* FKS2 */
    {
        4000,
        8000,
        12000,
        16000,
        20000,
        24000,
        28000,
        32000,
        36000,
        40000,
        44000,
        48000
    },
    /* FSK4 */
    {
        4000  / 3,
        8000  / 3,
        12000 / 3,
        16000 / 3,
        20000 / 3,
        24000 / 3,
        28000 / 3,
        32000 / 3,
        36000 / 3,
        40000 / 3,
        44000 / 3,
        48000 / 3
    },
    /* FSK8 */
    {
        4000  / 7,
        8000  / 7,
        12000 / 7,
        16000 / 7,
        20000 / 7,
        24000 / 7,
        28000 / 7,
        32000 / 7,
        36000 / 7,
        40000 / 7,
        44000 / 7,
        48000 / 7
    }
};

static const unsigned int fsk_man_table[6] =
{
    0xFE7A,
    0xFCF4,
    0xF9E8,
    0xF3D0,
    0xE7A0,
    0xCF40
};

static const unsigned int psk_man_table[9] =
{
    0xFFFB,
    0xFFF3,
    0xFFE6,
    0xFF82,
    0xFF05,
    0xFE0B,
    0xFC17,
    0xF82F,
    0xD8EF
};

// </editor-fold>

/* 
 * version Major.Minor.Patch
 * Major - change of this field show, that the changes in the functional are very
 * significat (perhaps even compatibility with data and protocols was lost). 
 * Minor - change of this field show, that new functional was added.
 * Patch - change of this field show, that some bugs was fixed. 
 */
static const char *version = "ver. 1.2.0, build: " __DATE__ ", " __TIME__;

int  __attribute__((section("Main"))) main (void)
{  
    while(!FPGA_INIT);
    
    struct button_s led_button = button_init((unsigned int*)&BUTTON_PORT, 
        BUTTON_PIN_NUM, (unsigned int*)&LED_LAT, LED_PIN_NUM);
    
    // ������������� ������
    flg.radiation_on = 0;
    flg.busy = 0;
    flg.cmd_eo_time = 1;
    
    // ������������� �������������
    system_init();
    uart1_init();
    uart2_init();
    timer2_init();
    timer3_init();
    timer4_init();
    int1_init();
    port_init();
    Nop();
    irq_init();
    rf_channel(CHANNEL4);
    if_channel(CHANNEL1);    
    spi1_init();
    spi2_init(); 
    eeprom_init();
    formet_lit_init(&param);
    former_init(); 
 
    if (RCONbits.SWR == 0)  // Soft reset flag
    {
        delay_ms(100);
        clr_wdt();
        delay_ms(100);
        clr_wdt();
        delay_ms(100);
        clr_wdt();
        delay_ms(100);
        clr_wdt();
        delay_ms(100);
        clr_wdt();
        delay_ms(100);
        clr_wdt();
        delay_ms(100);
        clr_wdt();
        delay_ms(100);
        clr_wdt();
        delay_ms(100);
        clr_wdt();
        delay_ms(100);
        clr_wdt();
		
		// starting critical sequence
		SYSKEY = 0x00000000; //write invalid key to force lock
		SYSKEY = 0xAA996655; //write key1 to SYSKEY
		SYSKEY = 0x556699AA; //write key2 to SYSKEY
		// OSCCON is now unlocked
        RSWRSTbits.SWRST = 1;   // Enable soft reset  
		unsigned int dummy;		// read RSWRST register
		dummy = RSWRST;
		while(1);
    }
    
    
    adl5240_set_gain_amp1(63);
    hmc1122_set_att(63);
    LATAbits.LATA1 = 1; 
    
    for(;;)
    {
        LATFbits.LATF12 = 1;  //test led
        LATFbits.LATF13 = 1;  //test led
        clr_wdt();
        fun_uart_main(&param);
        fun_button_handler(&led_button, param.central_freq);
    }
    return 0;
}

int fun_uart_main(struct lit_parameters *param)
{
    unsigned int answer;//, central_freq = param->central_freq;
    if(read_buf == 1)
    {
        if((buf_uart[POS_ADR] == param->addr_rx) && (number_buf == data_len + 3))
        {
            switch(buf_uart[POS_CID])
            {
                case CMD_STATUS:
                {
                    u2_transmit_ch(param->addr_tx);
                    u2_transmit_ch(CMD_STATUS_OK);

                    debug_msg(buf_uart, buf_uart[POS_LEN] + 3);
                    break;
                }
                case CMD_RADIATION_OFF:
                {
                    T2CONbits.TON = 0;
                    fnm_freq_switch_off();
                    signal_off();
                    suppression_mode = FIXED_FREQ;
                    flg.radiation_on = 0;
                    read_buf_init();
                    u2_transmit_ch(param->addr_tx);
                    u2_transmit_ch(CMD_STATUS_OK);
                    
                    debug_msg(buf_uart, buf_uart[POS_LEN] + 3);
                    break;
                }
                case CMD_TX_FREQ_DATA:
                {
                    suppression_mode = FIXED_FREQ;
                    cur_iri_step = 0;
                    cur_fnm_step = 0;
                    duration_step_counter = 0;

                    repeat_init_former();
                    answer = fun_param(param);
                                                          
                    u2_transmit_ch(param->addr_tx);
                    if(answer == 1)
                    {
                        u2_transmit_ch(CMD_ERROR);
                        u2_transmit_ch(0x03);
                    }
                    else
                    {
                        u2_transmit_ch(CMD_STATUS_OK);
                        flg.radiation_on = 1;
                    }

                    debug_msg(buf_uart, buf_uart[POS_LEN] + 3);
                    break;
                }
                case CMD_MODE_FRF:
                {
                    suppression_mode = FIXED_FREQ;
                    cur_iri_step = 0;
                    cur_fnm_step = 0;
                    duration_step_counter = 0;

                    u2_transmit_ch(param->addr_tx);
                    u2_transmit_ch(CMD_STATUS_OK);

                    debug_msg(buf_uart, buf_uart[POS_LEN] + 3);
                    break;
                }
                case CMD_MODE_FHSS:
                {
                    cur_iri_step = 0;
                    cur_fnm_step = 0;
                    duration_step_counter = 0;
                    flg.radiation_on = 1;

                    repeat_init_former();

                    freq_hop_start_abs = ((unsigned int) buf_uart[POS_F_L_FHSS]) + 
                            (((unsigned int) buf_uart[POS_F_M_FHSS]) << 8) + 
                            (((unsigned int)buf_uart[POS_F_H_FHSS]) << 16);
                    
                    modulation_code[0] = buf_uart[POS_MOD_FHSS];
                    deviation_code[0] = buf_uart[POS_DEV_FHSS];
                    manipulation_code[0] = buf_uart[POS_MAN_FHSS];
                    duration_code[0] = buf_uart[POS_RES_FHSS];
                    
                    fun_frh_init(modulation_code[0], manipulation_code[0]);
                    
                    read_buf_init();
                    suppression_mode = FREQ_HOPING;

                    u2_transmit_ch(param->addr_tx);
                    u2_transmit_ch(CMD_STATUS_OK);

                    debug_msg(buf_uart, buf_uart[POS_LEN] + 3);
                    break;
                }
                
                case CMD_GET_VERSION:
                {
                    size_t len = strlen(version) + 1;
                    buf_uart[POS_ADR] = param->addr_tx;
                    buf_uart[POS_CID] = CMD_GET_VERSION;
                    buf_uart[POS_LEN] = len;
                    memcpy(&buf_uart[3], version, len);
                    
                    u2_transmit_arr(buf_uart, buf_uart[POS_LEN] + 3);
                    
                    debug_msg(buf_uart, buf_uart[POS_LEN] + 3);
                    break;
                }
                
                case CMD_SET_AMP_VAL:
                {
                    unsigned int val = (((unsigned int)buf_uart[POS_DATA + 1]) << 24) | 
                                       (((unsigned int)buf_uart[POS_DATA + 2]) << 16) |
                                       (((unsigned int)buf_uart[POS_DATA + 3]) << 8)  |
                                       (((unsigned int)buf_uart[POS_DATA + 4]));
                    
                    if(!set_amp_val(buf_uart[POS_DATA] - 5, val))
                    {
                        lit_amp[buf_uart[POS_DATA] - 5] = val;
                        set_amp(val);
                        u2_transmit_ch(param->addr_tx);
                        u2_transmit_ch(CMD_STATUS_OK);
                    }
                    else
                    {
                        u2_transmit_ch(param->addr_tx);
                        u2_transmit_ch(CMD_ERROR);// ������
                    }
                    break;
                }
                
                case CMD_GET_AMP_VAL:
                {
                    unsigned int val = get_amp_val(buf_uart[POS_DATA] - 5);
                    buf_uart[POS_ADR] = param->addr_tx;
                    buf_uart[POS_CID] = CMD_GET_AMP_VAL;
                    buf_uart[POS_LEN] = 0x04;
                    buf_uart[POS_DATA] = val >> 24;
                    buf_uart[POS_DATA + 1] = val >> 16;
                    buf_uart[POS_DATA + 2] = val >> 8;
                    buf_uart[POS_DATA + 3] = val;
                    
                    u2_transmit_arr(buf_uart, buf_uart[POS_LEN] + 3);
                    break;
                }
                
                default:
                {
                    u2_transmit_ch(param->addr_tx);
                    u2_transmit_ch(CMD_ERROR);// ������
                    u2_transmit_ch(0x02);// ����������� �������

                    debug_msg(buf_uart, buf_uart[POS_LEN] + 3);
                    break;
                }
            }
            flg.cmd_eo_time = 1;
        }
        read_buf = 0;
    }
    return 0;
}

int fun_param(struct lit_parameters *param)
{
    unsigned int i;
    unsigned long long int freq_fnm_start, freq_fnm_curr, freq_fsk_arr[8];
    unsigned char uchCurNoise;
    unsigned int freq_is_valid;
    int ret = 0;
    unsigned int ftw[8];

    T2CONbits.TON = 0;
    fnm_freq_switch_off();
    radiation_off();

    if(suppression_mode == FIXED_FREQ)
    {
        iri_num = buf_uart[POS_NUM];
        for (i=0;i<iri_num;i++)
        {
            freq_abs[i] = ((unsigned int) buf_uart[POS_F_L + i*PAR_NUM]) + 
                    (((unsigned int) buf_uart[POS_F_M+i*PAR_NUM]) << 8) + 
                    (((unsigned int)buf_uart[POS_F_H+i*PAR_NUM]) << 16);
            
            freq_abs[i] = KHZ_TO_HZ(freq_abs[i]);
            
            modulation_code[i] = buf_uart[POS_MOD+i*PAR_NUM];
            deviation_code[i] = buf_uart[POS_DEV+i*PAR_NUM];
            manipulation_code[i] = buf_uart[POS_MAN+i*PAR_NUM];
            duration_code[i] = buf_uart[POS_TIME+i*PAR_NUM];
        }
    }

    freq_is_valid = filter_switch(param, freq_abs[0]);
    
    if(freq_is_valid)
    {
        synthesizers_freq_t synt_freq = get_synthesizers_frequencies(freq_abs[0]);
        
        adf4351_set_frequency(synt_freq.adf4351_freq);
        LMX2592_Frequency_setting_MHz(syntLMX1, synt_freq.lmx2592_freq);
        
        curr_modulation = modulation_code[0];
        switch(curr_modulation)
        {
            case FNM:
            {
                uchCurNoise = noise_table[0];
                freq_fnm_start = synt_freq.ad9914_freq - fnm_df_table[deviation_code[0] - 1];
                freq_fnm_curr = freq_fnm_start +  uchCurNoise * fnm_step_table[deviation_code[0] - 1];
                ftw[0] = convert_hz_to_ftw(freq_fnm_curr, synt_freq.adf4351_freq);
                set_mode_fsk_prn(ftw[0]);
                fnm_freq_switch_on();
                break;
            }
            
            case FSK2:
            case FSK4:
            case FSK8:    
            {
                unsigned int freq_num = 1 << ((curr_modulation - FSK2) + 1);
                freq_fsk_arr[0] = synt_freq.ad9914_freq - fsk_df_table[deviation_code[0] - 1];
                ftw[0] = convert_hz_to_ftw(freq_fsk_arr[0], synt_freq.adf4351_freq);
                for(i = 1; i < freq_num; i++)
                {
                    freq_fsk_arr[i] = freq_fsk_arr[i-1] + fsk_step_table[curr_modulation - FSK2][deviation_code[0] - 1];
                    ftw[i] = convert_hz_to_ftw(freq_fsk_arr[i], synt_freq.adf4351_freq);
                }
                set_mode_fsk(ftw, fsk_man_table[manipulation_code[0] - 1], curr_modulation);
                break;
            }
            
            case PSK2:
            case PSK4:
            case PSK8:        
            {
                ftw[0] = convert_hz_to_ftw(synt_freq.ad9914_freq, synt_freq.adf4351_freq);
                set_mode_psk(ftw[0], psk_man_table[manipulation_code[0]-1], curr_modulation);
                break;
            }
            
            case BARRIER:
            {
                ftw[0] = convert_hz_to_ftw(synt_freq.ad9914_freq, synt_freq.adf4351_freq);
                set_mode_psk(ftw[0], psk_man_table[manipulation_code[0]-1], PSK2);
                break;
            }
            
            case LFM:
            {
                ftw[0] = convert_hz_to_ftw(synt_freq.ad9914_freq, synt_freq.adf4351_freq);
                
                set_mode_lfm(ftw[0], MHZ_TO_KHZ(deviation_code[0]), manipulation_code[0],
                        synt_freq.adf4351_freq);
                break;
            }
        }
    }
    else
    {
        ret = 1;
    }
    
    TMR2 = 0x00;
    T2CONbits.TON = 1;
    return ret;
}

void signal_switch_handler(void)
{
    unsigned char curr_noise;
    unsigned long long int freq_fnm_start, freq_fnm_curr, freq_fsk_arr[8];
    int i;
    unsigned int freq_is_valid;
    unsigned int ftw[0];

    if(iri_num > 1)
    {
        duration_step_counter = duration_step_counter + 1;
        if(duration_step_counter >= duration_code[cur_iri_step])
        {
            duration_step_counter = 0;
            cur_iri_step = cur_iri_step + 1;
            if(cur_iri_step >= iri_num)
            {
                cur_iri_step = 0;
            }
            
            fnm_freq_switch_off();
            radiation_off();
            
            freq_is_valid = filter_switch(&param, freq_abs[cur_iri_step]);
            
            if(freq_is_valid)
            {
                synthesizers_freq_t synt_freq = get_synthesizers_frequencies(freq_abs[cur_iri_step]);
        
                adf4351_set_frequency(synt_freq.adf4351_freq);
                LMX2592_Frequency_setting_MHz(syntLMX1, synt_freq.lmx2592_freq);
                
                curr_modulation = modulation_code[cur_iri_step];
                switch(curr_modulation)
                {
                    case FNM:
                    {
                        cur_fnm_step = cur_fnm_step + 1;
                        if(cur_fnm_step >= ARRAY_SIZE(noise_table))
                        {
                            cur_fnm_step = 0;
                        }
                        curr_noise = noise_table[cur_fnm_step];
                        freq_fnm_start = synt_freq.ad9914_freq - fnm_df_table[deviation_code[cur_iri_step] - 1];
                        freq_fnm_curr = freq_fnm_start +  curr_noise * fnm_step_table[deviation_code[cur_iri_step] - 1];
                        ftw[0] = convert_hz_to_ftw(freq_fnm_curr, synt_freq.adf4351_freq);
                        
                        set_mode_fsk_prn(ftw[0]);
                        fnm_freq_switch_on();
                        break;
                    }
                    
                    case FSK2:
                    case FSK4:
                    case FSK8:    
                    {
                        unsigned int freq_num = 1 << ((curr_modulation - FSK2) + 1);
                        freq_fsk_arr[0] = synt_freq.ad9914_freq - 
                                fsk_df_table[deviation_code[cur_iri_step] - 1];
                        ftw[0] = convert_hz_to_ftw(freq_fsk_arr[0], synt_freq.adf4351_freq);
                        
                        for(i = 1; i < freq_num; i++)
                        {
                            freq_fsk_arr[i] = freq_fsk_arr[i-1] + 
                                    fsk_step_table[curr_modulation - FSK2][deviation_code[cur_iri_step] - 1];
                            ftw[i] = convert_hz_to_ftw(freq_fsk_arr[i], synt_freq.adf4351_freq);
                        }
                        set_mode_fsk(ftw, fsk_man_table[manipulation_code[0] - 1], curr_modulation);
                        break;
                    }

                    case PSK2:
                    case PSK4:
                    case PSK8:    
                    {
                        ftw[0] = convert_hz_to_ftw(synt_freq.ad9914_freq, synt_freq.adf4351_freq);
                        set_mode_psk(ftw[0], psk_man_table[manipulation_code[cur_iri_step] - 1], curr_modulation);
                        break;
                    }
                    
                    case BARRIER:
                    {
                        ftw[0] = convert_hz_to_ftw(synt_freq.ad9914_freq, synt_freq.adf4351_freq);
                        set_mode_psk(ftw[0], psk_man_table[manipulation_code[cur_iri_step]-1], PSK2);
                        break;
                    }
                    
                    case LFM:
                    {
                        ftw[0]= convert_hz_to_ftw(synt_freq.ad9914_freq, synt_freq.adf4351_freq);
                        set_mode_lfm(ftw[0], MHZ_TO_KHZ(deviation_code[0]), manipulation_code[0], 
                                synt_freq.adf4351_freq);
                        break;
                    }
                }
            }
        }
    }
}

void fnm_freq_switch()
{
    unsigned char curr_noise;
    unsigned int freq_fnm_start, freq_fnm_curr;
    unsigned int ftw;
    
    if(curr_modulation == FNM)
    {
        synthesizers_freq_t synt_freq = get_synthesizers_frequencies(freq_abs[cur_iri_step]);
        
        cur_fnm_step = cur_fnm_step + 1;
        if(cur_fnm_step >= ARRAY_SIZE(noise_table))
        {
            cur_fnm_step = 0;
        }
        curr_noise = noise_table[cur_fnm_step];
        freq_fnm_start = synt_freq.ad9914_freq - fnm_df_table[deviation_code[cur_iri_step] - 1];
        freq_fnm_curr = freq_fnm_start + curr_noise * fnm_step_table[deviation_code[cur_iri_step] - 1];
        ftw = convert_hz_to_ftw(freq_fnm_curr, synt_freq.adf4351_freq);

        set_freq_current(ftw);
    }
}

void fnm_freq_switch_on()
{
    TMR3 = 0x00;
    T3CONbits.ON = 1;
}

void fnm_freq_switch_off()
{
    T3CONbits.ON = 0;
    TMR3 = 0x00;
}

void fun_frh_init(unsigned char mode, unsigned char manipulation)
{
    
    unsigned short period = 0;
    switch(mode)
    {
        case FSK2:
        case FSK4:
        case FSK8:
            period = fsk_man_table[manipulation - 1];
            break;
            
        case PSK2:
        case PSK4:
        case PSK8:
            period = psk_man_table[manipulation - 1];
            break;
            
        case FNM:
            break;
            
        case BARRIER:
            period = psk_man_table[manipulation - 1];
            break;
            
        default:
            break;
    }
    
    frh_init(mode, period);
}

void fun_frh_on(unsigned char mode, unsigned int freq, unsigned char deviation)
{
    unsigned int freq_arr[8];
    int i;
    unsigned int freq_num = 1 << ((mode - FSK2) + 1);
    
    radiation_off();

    if(filter_switch(&param, freq))
    {    
        freq = KHZ_TO_FTW(freq);
    
        switch(mode)
        {
            case FSK2: 
            case FSK4:
            case FSK8:    
                freq_arr[0] = freq - fsk_df_table[deviation - 1];
                for(i = 1; i < freq_num; i++)
                {
                    freq_arr[i] = freq_arr[i-1] + fsk_step_table[mode - FSK2][deviation - 1];
                }          
                break;

            case PSK2:
            case PSK4:
            case PSK8:
            case FNM:
            case BARRIER:
                for(i = 0; i < 8; i++)
                {
                    freq_arr[i] = freq;
                }
                break;
        }

        set_mode_frh(freq_arr);
    }
}

void frh_handler(void)
{
    unsigned long int freq_code;
    unsigned int counter;
    unsigned char frh_data[8];
    
    /* 
     * �������� ���������, ��������� ���� �� ����������� ��������� "�������"
     * �������� ������� ��������� ����� ������������� � ����� ������� �� 
     * ������, ��� ��������� ����������� �� ������ ���������� 
     */
    amp_disable();

    if(suppression_mode == FIXED_FREQ)
    {
        read_buf_init();
    }
    
    if(suppression_mode == FREQ_HOPING)
    {
        fpga_read_mode();

        for(counter = 0; counter < 2; counter++)
            frh_data[counter] = read_fpga_data(counter);
        read_fpga_data(7);

        freq_code = (frh_data[0] << 8) | frh_data[1];

        fpga_write_mode();

        /* 
         * �� ����������� ������� ��������� ��� "���������� ��������" � ����������
         * 0xFFFF, ������� ��� ������� �������� ������� ������ 
         */
        if(freq_code == 0xFFFF)
        {
            radiation_off();
        }
        
        else
        {
            switch(duration_code[0])
            {
                case HIGH_RES:
                    if(freq_code > HIGH_RES_POINTS)
                    {
                        freq_code = HIGH_RES_POINTS;
                    }

                    if(freq_code > (HIGH_RES_POINTS / 2))
                    {
                        freq_abs[0] = freq_hop_start_abs + (/*16384 - */(freq_code - (HIGH_RES_POINTS / 2))) * 
                                DSP_FREQ_BAND / HIGH_RES_POINTS;
                    }
                    else
                    {
                        freq_abs[0] = freq_hop_start_abs + (/*16384 - */(freq_code + (HIGH_RES_POINTS / 2))) * 
                                DSP_FREQ_BAND / HIGH_RES_POINTS;
                    }
                    break;
                    
                case MEDIUM_RES:
                    if(freq_code > MEDIUM_RES_POINTS)
                    {
                        freq_code = MEDIUM_RES_POINTS;
                    }
                    if(freq_code > (MEDIUM_RES_POINTS / 2))
                    {
                        freq_abs[0] = freq_hop_start_abs + (/*8192 - */(freq_code - (MEDIUM_RES_POINTS / 2))) * 
                                DSP_FREQ_BAND / MEDIUM_RES_POINTS;
                    }
                    else
                    {
                        freq_abs[0] = freq_hop_start_abs + (/*8192 - */(freq_code + (MEDIUM_RES_POINTS / 2))) * 
                                DSP_FREQ_BAND / MEDIUM_RES_POINTS;
                    }
                    break;
                    
                case LOW_RES:
                    if(freq_code > LOW_RES_POINTS)
                    {
                        freq_code = LOW_RES_POINTS;
                    }
                    if(freq_code > (LOW_RES_POINTS / 2))
                    {
                        freq_abs[0] = freq_hop_start_abs + (/*4096 - */(freq_code - (LOW_RES_POINTS / 2))) * 
                                DSP_FREQ_BAND / LOW_RES_POINTS;
                    }
                    else
                    {
                        freq_abs[0] = freq_hop_start_abs + (/*4096 - */(freq_code + (LOW_RES_POINTS / 2))) * 
                                DSP_FREQ_BAND / LOW_RES_POINTS;
                    }
                    break;
                    
                /* 
                 * � ������ ������� � ���������� ���������� ��������� ���������� ��� ��������� ������ 
                 */
                default:
                    radiation_off();
                    break;
            }

            fun_frh_on(modulation_code[0], freq_abs[0], deviation_code[0]);
        }
    }
}

void fun_button_handler(struct button_s *btn, unsigned int central_freq)
{
    enum button_status status;
    static unsigned char radiation_arm_on = 0;
    
    if(!flg.radiation_on)
    {
        status = button_handler(btn);
        if(status == ON)
        {
            signal_off();
            repeat_init_former();
            set_mode_test(central_freq);
        }
        else if(status == OFF)
        {
            signal_off();
        }
        
        radiation_arm_on = 0;
    }
    
    if(flg.radiation_on && !radiation_arm_on)
    {
        off_led(btn);
        radiation_arm_on = 1;
    }
}

unsigned int filter_switch(struct lit_parameters *param, unsigned long long freq)
{
    unsigned int valid_freq = 0;
    
    switch(param->lit_num)
    {
        case LIT10:
        {
            if(freq >= IF1_MIN && freq < IF1_MAX)
            {
                valid_freq = 1;
                rf_channel(CHANNEL2);    
            }
            else if(freq >= IF2_MIN && freq < IF2_MAX)
            {
                valid_freq = 1;
                rf_channel(CHANNEL3);
            }
            else if(freq >= IF3_MIN && freq <= IF3_MAX)
            {
                valid_freq = 1;
                rf_channel(CHANNEL4);
            }
        }
    }
    
    return valid_freq;
}

void read_buf_init(void)
{
    fpga_read_mode();
    read_fpga_data(7);
    fpga_write_mode();
}

void formet_lit_init(struct lit_parameters *param)
{
    int i;
    for(i = 0; i < LIT_NUM; i++)
    {
        lit_amp[i] = get_amp_val(i);
    }
    
    param->addr_rx = LIT10_ADDR_RX;
    param->addr_tx = LIT10_ADDR_TX;
    param->central_freq = KHZ_TO_FTW(750000);
    param->lit_num = LIT10;

//    rf1_channel(CHANNEL2);
//    rf2_channel(CHANNEL1);
    set_amp(lit_amp[LIT10]);
}

unsigned int get_amp_val(enum lit_num num)
{
    unsigned int read_val;
    
    eeprom_read_array(ADR_AMP_BASE + AMP_SIZE * num, (unsigned char*)&read_val,
            sizeof(read_val));
    
    if(read_val < MIN_AMP || read_val > MAX_AMP)
    {
        switch(num)
        {
            case LIT10:
                read_val = LIT10_AMP;
            break;
            default:
                read_val = MIN_AMP;
            break;
        }
    }
    
    return read_val;
}

unsigned char set_amp_val(enum lit_num num, unsigned int val)
{
    unsigned int read_val = 0;
    
    if(val < MIN_AMP || val > MAX_AMP)
        val = MIN_AMP;
    
    eeprom_write_array(ADR_AMP_BASE + AMP_SIZE * num, (unsigned char*)&val,
            sizeof(val));
    
    eeprom_read_array(ADR_AMP_BASE + AMP_SIZE * num, (unsigned char*)&read_val,
            sizeof(read_val));
    
    return !(val == read_val);
}

void __ISR(_TIMER_2_VECTOR, IPL4AUTO) timer2_handler(void)
{
    signal_switch_handler();
    IFS0bits.T2IF = 0;
}

void __ISR(_TIMER_3_VECTOR, IPL3AUTO) timer3_handler(void)
{
    fnm_freq_switch();
    IFS0bits.T3IF = 0;
}

void __ISR(_TIMER_5_VECTOR, IPL7AUTO) timer5_handler(void)
{
    IFS0bits.T5IF = 0;
}

void __ISR(_EXTERNAL_1_VECTOR, IPL7AUTO) int1_handler(void)
{
    frh_handler();
    IFS0bits.INT1IF = 0;
}

void __ISR(_TIMER_4_VECTOR, IPL5AUTO) Timer4Handler(void)
{
    IFS0bits.T4IF = 0;
    flg.cmd_eo_time = 1;
    number_buf = 0;
}

void __ISR(_UART_1_VECTOR, IPL6AUTO) uart1_handler(void)
{
    unsigned char buff;
    if (U1STAbits.URXDA == 1)
    {
        buff = U1RXREG;

        if (flg.cmd_eo_time == 1)
        {
            number_buf = 0;
            TMR4 = 0;
            flg.cmd_eo_time = 0;
        }
        else
        {
           TMR4 = 0;
        }
        if(number_buf < 256)
        {
            buf_uart[number_buf] = buff;
            read_buf = 1;
        }
        if(number_buf == 2)
        {
            data_len = buf_uart[2];
        }
        number_buf++;
    }
    IFS0bits.U1RXIF = 0;
}

void __ISR(_UART_2_VECTOR, IPL6AUTO) uart2_handler(void)
{
    unsigned char buff;
    if (U2STAbits.URXDA == 1)
    {
        buff = U2RXREG;

        if (flg.cmd_eo_time == 1)
        {
            number_buf = 0;
            TMR4 = 0;
            flg.cmd_eo_time = 0;
        }
        else
        {
            TMR4 = 0;
        }
        if(number_buf < 256)
        {
            buf_uart[number_buf] = buff;
            read_buf = 1;
        }
        if(number_buf == 2)
        {
            data_len = buf_uart[2];
        }
        number_buf++;
    }
    IFS1bits.U2RXIF = 0;
}

