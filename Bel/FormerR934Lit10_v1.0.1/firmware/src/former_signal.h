#ifndef _FORMER_SIGNAL_H    /* Guard against multiple inclusion */
#define _FORMER_SIGNAL_H

#include <xc.h>
#include "definitions.h" 
/*
 * RF_READY         LATF1
 * BUS_DIR_FPGA     LATF0
 * DATA_DIRECTION   TRISEbits 0-7
 */
//#define rf_ready_set()      LATFSET = 0x02
//#define rf_ready_clr()      LATFCLR = 0x02

//#define bus_fpga_write()    BUS_DIR_FPGA = 0x01
//#define bus_fpga_read()     LATFCLR = 0x01

#define data_dir_input()    TRISESET = 0xFF
#define data_dir_output()   TRISECLR = 0xFF


#define CHANNEL1            1
#define CHANNEL2            2
#define CHANNEL3            3
#define CHANNEL4            4

#define FSK2_PROFPIN        0x11
#define FSK4_PROFPIN        0x13
#define FSK8_PROFPIN        0x17

#define PSK2_PROFPIN        0x01
#define PSK4_PROFPIN        0x03
#define PSK8_PROFPIN        0x07
#define PHASE0              0x00000000
#define PHASE45             0x00001FFF
#define PHASE90             0x00003FFF
#define PHASE135            0x00005FFF
#define PHASE180            0x00007FFF
#define PHASE225            0x00009FFF
#define PHASE270            0x0000BFFF
#define PHASE315            0x0000DFFF

#define CTRL_DDS_REG1       0b00010000
#define CTRL_DDS_REG2       0x00

#define LFM_POINT_TIME      8         //in nano seconds //103
#define RAMP_RATE           0x00010001  //0x000F000F

#define IF1                         1100000000ULL
#define IF1_MIN                     3000000000ULL
#define IF1_MAX                     4250000000ULL

#define IF2                         1600000000ULL   //1500000000
#define IF2_MIN                     4250000000ULL
#define IF2_MAX                     5150000000ULL   //5000000000

#define IF3                         1300000000ULL   //1400000000
#define IF3_MIN                     5150000000ULL   //5000000000
#define IF3_MAX                     6000000000ULL 
/* 
 * Convert frequency in kHz to FTW 
 */
#define KHZ_TO_FTW(freq)    (((unsigned long long)(freq)) * 1227 + \
                            ((((unsigned long long)(freq)) * 4375) >> 15))

/* Convert frequency from kHz to Hz */
#define KHZ_TO_HZ(freq)     ((freq) * 1000)

/* Convert frequency in Hz to FTW */
#define HZ_TO_FTW(freq)     (((unsigned long long)(freq)) + \
                            ((7443 * ((unsigned long long)(freq))) >> 15))
///* Convert frequency from Hz to FTW */
//#define HZ_TO_FTW(freq)     ((freq) * 1.227133513)

#define MHZ_TO_KHZ(freq)    ((freq) * 1000)

#define HZ_TO_KHZ(freq)     ((freq) / 1000)

#define HZ_TO_MHZ(freq)     ((freq) / 1000000)
/********************************* VARIABLES **********************************/
enum modulation_types
{
    FNM = 1,
    FSK2,
    FSK4,
    FSK8,
    PSK2,
    PSK4,
    PSK8,
    BARRIER,
    LFM = 0x0B,
    MOD_NUM
};

/*
 *  mapped DATAbits struct and DATA variable to first 8 bits of LATE; 
 * DATA_READ variable to first 8 bits of PORTE 
 */
extern volatile uint8_t DATA;
extern volatile uint8_t DATA_READ;

typedef struct
{
    unsigned BIT0:1;
    unsigned BIT1:1;
    unsigned BIT2:1;
    unsigned BIT3:1;
    unsigned BIT4:1;
    unsigned BIT5:1;
    unsigned BIT6:1;
    unsigned BIT7:1;
} DATAbits_t;

extern volatile DATAbits_t DATAbits;

typedef struct
{
    uint64_t adf4351_freq;
    uint32_t ad9914_freq;
    uint8_t SW1ref; 
    uint8_t SW2ref;     
    
} synthesizers_freq_t;

/********************************* FUNCTIONS **********************************/
//#define amp_disable()               LATGCLR = 0x01

/* 
 * commutator switching function 
 */
void rf_channel(unsigned int channel);
void if_channel(unsigned int channel);

/* 
 * ON/OFF HF signals 
 */
void signal_on(void);
void signal_off(void);
void radiation_on(void);
void radiation_off(void);

/* 
 * set former frequency and amplitude 
 */
void set_amp(uint32_t amp);
void set_freq_current(uint32_t freq);

/* 
 * control functions 
 */
void former_init(void);
void repeat_init_former(void);
void set_mode_test(uint32_t freq);
void set_mode_fsk_prn(uint32_t freq);
void set_mode_fsk(uint32_t *freq_arr, uint16_t period, uint8_t mode);
void set_mode_psk(uint32_t freq, uint16_t period, uint8_t mode);
void set_mode_lfm(uint64_t freq, uint64_t dev, uint32_t speed, uint64_t Freq_ref);
void frh_init(uint8_t mode, uint16_t period);
void set_mode_frh(uint32_t *freq);
unsigned char read_fpga_data(uint16_t addr);
void fpga_read_mode(void);
void fpga_write_mode(void);

uint32_t convert_hz_to_ftw(uint64_t freq_abs, uint64_t ad9914_sys_freq);
synthesizers_freq_t get_synthesizers_frequencies(uint64_t freq_abs);

void set_mode_freq(uint64_t  freq, uint64_t Freq_ref);
#endif /* _FORMER_SIGNAL_H */
