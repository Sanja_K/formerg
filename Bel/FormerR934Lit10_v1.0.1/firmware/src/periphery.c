#include <xc.h>
#include "common.h"
#include "periphery.h"
//#include "plib.h"

void port_init(void)
{ 
//    AD1PCFG = 0xFFFF;
//    DDPCONbits.JTAGEN = 0; // ���������� JTAG 
//    
//    LATA = 0x0000;
//    TRISA = 0xF901;
//    
//    LATB = 0x0000;
//    TRISB = 0x882F;
//    _LATB4 = 1;
//    
//    LATC = 0x0000;
//    TRISC = 0xFFFA;
//    
//    LATD = 0x0000;
//    TRISD = 0xB009;
//    
//    LATE = 0x0000;
//    TRISE = 0xFF00;
//    
//    LATF = 0x0003;
//    TRISF = 0xCFF4;
//    
//    LATG = 0x0000;
//    TRISG = 0x0DFC;
    
//    //------------------------------ adf
//    TRISBbits.TRISB13 = 0;    
//    LATBbits.LATB13 = 0;
//    
//    TRISBbits.TRISB12 = 0;    
//    LATBbits.LATB12 = 0;    
//    
//    TRISBbits.TRISB14 = 0;    
//    LATBbits.LATB14 = 0;   
//    
//    TRISBbits.TRISB11 = 0;   
//    
//    TRISBbits.TRISB10 = 0;    
//    LATBbits.LATB10 = 0;  
//    
//    //------------------------------ power dds
//    
//    TRISCbits.TRISC2 = 0;    
//    LATCbits.LATC2 = 0;
//    
//    TRISCbits.TRISC3 = 0;    
//    LATCbits.LATC3 = 0;
//    
//    TRISAbits.TRISA10 = 0;    
//    LATAbits.LATA10 = 0;
    
}

//void spi1_init(void)
//{
//    __attribute__ ((unused)) unsigned char temp;
//    
//    SPI1CONbits.ON = 0;
//    temp = SPI1BUF;
//    /*8 bits mode*/
//    SPI1CONbits.MODE32 = 0;
//    SPI1CONbits.MODE16 = 0;
//    /*active high, data changes from idle to active clock state*/
//    SPI1CONbits.CKE = 1;
//    SPI1CONbits.CKP = 0;
//    /*CS controlled by port function*/
//    SPI1CONbits.SSEN = 0;
//    /*master mode*/
//    SPI1STATbits.SPIROV = 0;
//    SPI1CONbits.MSTEN = 1;
//    /*interrupt happened when the last transfer is shifted out of SPISR and transmit operations are complete*/
//    SPI1CONbits.STXISEL = 0b00;
//    /*baudrate*/
//    SPI1BRG = SPIBRGVAL;
//    /*enable interrupt*/
//    IPC5bits.SPI1IP = 7;
//    IPC5bits.SPI1IS = 1;
//    SPI1CONbits.ENHBUF = 1;
//    SPI1CONbits.ON = 1;
//}

void spi2_init(void)
{
    __attribute__ ((unused)) unsigned char temp;
    
    SPI2CONbits.ON = 0;
    temp = SPI2BUF;
    /*8 bits mode*/
    SPI2CONbits.MODE32 = 0;
    SPI2CONbits.MODE16 = 0;
    /*active high, data changes from idle to active clock state*/
    SPI2CONbits.CKE = 1;
    SPI2CONbits.CKP = 0;
    /*CS controlled by port function*/
    SPI2CONbits.SSEN = 0;
    /*master mode*/
    SPI2STATbits.SPIROV = 0;
    SPI2CONbits.MSTEN = 1;
    /*interrupt happened when the last transfer is shifted out of SPISR and transmit operations are complete*/
    SPI2CONbits.STXISEL = 0b00;
    /*baudrate*/
    SPI2BRG = SPIBRGVAL;
    /*enable interrupt*/
    IPC7bits.SPI2IP = 7;
    IPC7bits.SPI2IS = 1;
    SPI2CONbits.ENHBUF = 1;
    SPI2CONbits.ON = 1;
}

void uart1_init(void)
{
    //#ifdef __DEBUG
    U1MODEbits.STSEL = 0; // 1-stop bit
    U1MODEbits.PDSEL = 0; // No Parity, 8-data bits
    U1MODEbits.ABAUD = 0; // Auto-Baud Disabled
    U1MODEbits.BRGH = 1; // Low Speed mode
    U1BRG = BRGVAL; // BAUD Rate Setting
    U1STAbits.UTXISEL0 = 0; // Interrupt after one Tx character is transmitted
    U1STAbits.UTXISEL1 = 0;
    IPC6bits.U1IP = 6;
    IFS0bits.U1RXIF = 0;
    IEC0bits.U1RXIE = 1; // ��������� ����������
    U1MODEbits.UARTEN = 1; // Enable UART
    U1STAbits.UTXEN = 1; // Enable UART Tx
    U1STAbits.URXEN = 1;
    //#endif
}

void uart2_init(void)
{
    TRISFbits.TRISF3 = 0;   //TX/RX enable
    U2MODEbits.STSEL = 0; // 1-stop bit
    U2MODEbits.PDSEL = 0; // No Parity, 8-data bits
    U2MODEbits.ABAUD = 0; // Auto-Baud Disabled
    U2MODEbits.BRGH = 1; // High Speed mode
    U2BRG = BRGVAL; // BAUD Rate Setting
    U2STAbits.UTXISEL0 = 0; // Interrupt after one Tx character is transmitted
    U2STAbits.UTXISEL1 = 0;
    IPC8bits.U2IP = 6;
    IEC1bits.U2RXIE = 1; // ��������� ����������
    U2MODEbits.UARTEN = 1; // Enable UART
    U2STAbits.UTXEN = 1; // Enable UART Tx
    U2STAbits.URXEN = 1;
}

void int1_init(void)
{
    //TRISEbits.TRISE8 = 1;
//    INTCONbits.INT1EP = 1;      // Rising edge interrupt
//    IPC1bits.INT1IP = 7;
//    IFS0bits.INT1IF = 0;
//    IEC0bits.INT1IE = 1;        // Enable interrupt
}

//void irq_init(void)
//{
//    INTCONbits.MVEC = 1;   // Multi-vector mode
//    __builtin_enable_interrupts();
//}

void system_init(void)
{
//    DDPCONbits.JTAGEN = 0;
//    DDPCONbits.TDOEN = 0;
//    DDPCONbits.TROEN = 0;
//    SYSTEMConfigPerformance(FCY);
}

void timer2_init(void)
{
    T2CONbits.TON = 0; 		// Disable Timer
    T2CONbits.TCS = 0; 		// Internal Clock
    T2CONbits.TGATE = 0;	// Disable Gated Timer mode
    T2CONbits.TCKPS = 0b100;// Select 1:16 Prescaler
    TMR2 = 0x00;			// Clear timer register
    PR2 = 0x61A8;			// Period 500 ��� = PR2*TCKPS/PBclk  //0x9C40
    IPC2bits.T2IP = 0x04;	// Set Timer2 Interrupt Priority Level
    IFS0bits.T2IF = 0;		// Clear Timer2 Interrupt Flag
    IEC0bits.T2IE = 1;		// Enable Timer2 interrupt
}

void timer3_init(void)
{
    T3CONbits.TON = 0; 		// Disable Timer
    T3CONbits.TCS = 0; 		// Internal Clock
    T3CONbits.TGATE = 0;	// Disable Gated Timer mode
    T3CONbits.TCKPS = 0b00; // Select 1:1 Prescaler
    TMR3 = 0x00;			// Clear timer register
    PR3 = 0x1F3F;			// Period 100 usec
    IPC3bits.T3IP = 0x03;	// Set Timer3 Interrupt Priority Level
    IFS0bits.T3IF = 0;		// Clear Timer3 Interrupt Flag
    IEC0bits.T3IE = 1;		// Enable Timer3 interrupt
}

void  timer4_init(void)
{
    PR4 = 0x08FC;       //Period 230 usec
    IPC4bits.T4IP = 5;
    T4CON = 0b1000000000110000;
    IFS0bits.T4IF = 0;
    IEC0bits.T4IE = 1;
}

void timer5_init(void)
{
    T5CONbits.TON = 0; 		// Disable Timer
    T5CONbits.TCS = 0; 		// Internal Clock
    T5CONbits.TGATE = 0;	// Disable Gated Timer mode
    T5CONbits.TCKPS = 0b00; // Select 1:1 Prescaler
    TMR5 = 0x00;			// Clear timer register
    PR5 = 0x270F;			// Period 125 usec
    IPC5bits.T5IP = 0x07;
    IFS0bits.T5IF = 0;
    IEC0bits.T5IE = 1;
}

static unsigned char spi2_transmit(unsigned char byte)
{
    SPI2BUF = byte;
    while(SPI2STATbits.SPIRBF == 0);
    
    return SPI2BUF;
}

void spi2_write_byte(unsigned char byte)
{
    spi2_transmit(byte);
}

unsigned char spi2_read_byte(void)
{
    return spi2_transmit(0x00);
}

void u2_transmit_arr(unsigned char *TX, unsigned int len)
{
    unsigned int i;
    RS485_TX_EN_1;
    for(i=0;i<len;i++)
    {
        U2TXREG = TX[i];
        while (U2STAbits.UTXBF == 1);
    }
    while(!U2STAbits.TRMT);
    RS485_TX_EN_0;
}

void u2_transmit_ch(unsigned char TXbuf)
{
    IFS1bits.U2TXIF = 0;
    RS485_TX_EN_1;
    U2TXREG = TXbuf;
    while(!U2STAbits.TRMT)
    {
        Nop();
    }
    RS485_TX_EN_0;
    IFS1bits.U2TXIF = 0;
}

#ifdef __DEBUG
void debug_msg(unsigned char *TX, unsigned int len)
{
    unsigned int i;
    for(i=0;i<len;i++)
    {
        U1TXREG = TX[i];
        while (U1STAbits.UTXBF == 1);
    }
    //asm("Exit_TransmitPU1:");
}

void debug_char(unsigned char TXbuf)
{
    U1TXREG = TXbuf;
    while (U1STAbits.UTXBF == 1)
    {
        Nop();
    }
    //asm("Exit_TransmitU1:");
}
#endif
