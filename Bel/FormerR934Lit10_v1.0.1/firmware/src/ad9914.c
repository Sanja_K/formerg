#include <xc.h>
#include <sys/attribs.h>
#include "ad9914.h"
#include "definitions.h" 

const struct ad9914 ad9914_reg = {
    .CFR1       = 0x00,
    .CFR2       = 0x01,
    .CFR3       = 0x02,
    .CFR4       = 0x03,
    .DRAMP_LO   = 0x04,
    .DRAMP_HI   = 0x05,
    .DRAMP_RIS  = 0x06,
    .DRAMP_FAL  = 0x07,
    .DRAMP_RATE = 0x08,
    .PROF_F     = 
        {0x0B, 0x0D, 0x0F, 0x11, 0x13, 0x15, 0x17, 0x19},
    .PROF_PA   = 
        {0x0C, 0x0E, 0x10, 0x12, 0x14, 0x16, 0x18, 0x1A},
};

/* 
 * functions read/write command and 1/2/4 bytes of data to AD9914
 * available values for reg_size is: REG_BYTE (1 byte), REG_WORD (2 bytes), 
 * REG_DWORD (4 bytes), other values are  forbidden
 */

/* 
 * at start, we select dds and start transfer in its register, 
 * after that we leave function and dds will be unselected when 
 * all data will be transmit and interrupt will happen 
 */
void ad9914_write_reg(uint8_t reg, void *buf, uint32_t reg_size)
{
#ifdef __DEBUG
    if(reg_size != REG_BYTE && reg_size != REG_WORD && 
            reg_size != REG_DWORD)
        return;
#endif
    
    while(SPI2STATbits.SRMT == 0);
    ad9914_select_dds();
    
    SPI2BUF = reg;
    
    for(; reg_size > 0; reg_size--)
    {
        SPI2BUF = ((unsigned char*)buf)[reg_size - 1];
    }

    while(SPI2STATbits.SRMT == 0);
    
    ad9914_io_update_set();
    Nop();
    Nop();
    Nop();
    ad9914_io_update_clr();
    
    ad9914_unselect_dds();
}
void ad9914_io_update (void)
{
    ad9914_io_update_set();
    ad9914_io_update_clr();
}


void ad9914_read_reg(uint8_t reg, void *buf, uint32_t reg_size)
{
    static uint32_t data;
    __attribute__ ((unused)) uint8_t temp;
    int i = reg_size;
    
    data = 0;
        
    while(SPI2STATbits.SPIRBE == 0)
    {
        temp = SPI2BUF;
    }
    SPI2STATbits.SPIROV = 0;
    
    while(SPI2STATbits.SRMT == 0);
    ad9914_select_dds();
    
    while(SPI2STATbits.SPIRBE == 0)
    {
        temp = SPI2BUF;
    }
    
    SPI2BUF = reg | READ_MASK;
    while(SPI1STATbits.SPIRBE == 1);
    temp = SPI2BUF;
    
    for(; i > 0; i--)
    {
        SPI2BUF = 0x00;
        while(SPI1STATbits.SPIRBE == 1);
        data <<= 8;
        data |= SPI2BUF;
    }

    switch(reg_size) 
    {
        case REG_BYTE:
        {
            uint8_t *ptr = buf;
            *ptr = (unsigned char)data;
            break;
        }

        case REG_WORD:
        {
            uint16_t *ptr = buf;
            *ptr = (unsigned short)data;
            break;
        }

        case REG_DWORD:
        {
            uint32_t *ptr = buf;
            *ptr = (unsigned int)data;
            break;
        }

        default:
        {
            uint32_t *ptr = buf;
            *ptr = 0x00000000;
            break;
        }
    }
    
    while(SPI2STATbits.SRMT == 0);
    ad9914_unselect_dds();
}

void ad9914_spi_3wire_mode(void)
{
    static uint32_t reg_data = SPI_3WIRE;
    ad9914_write_reg(ad9914_reg.CFR1, &reg_data, REG_DWORD);
}

void ad9914_spi_3wire_osk_mode(void)
{ 
  
    static uint32_t reg_data = ADSPI_3WIRE_OSK;
    ad9914_write_reg(ad9914_reg.CFR1, &reg_data, REG_DWORD);    
}


void ad9914_init_dac_cal(void)
{
    uint32_t reg_data = DAC_CAL_EN;
    ad9914_write_reg(ad9914_reg.CFR4, &reg_data, REG_DWORD);
    
    reg_data = DAC_CAL_DIS;
    ad9914_write_reg(ad9914_reg.CFR4, &reg_data, REG_DWORD);
    
}

void ad9914_set_profile_mode(void)
{
    static uint32_t reg_data = PROF_MODE;
    ad9914_write_reg(ad9914_reg.CFR2, &reg_data, REG_DWORD);
}

void ad9914_set_lfm_mode(void)
{
    uint32_t reg_data = LFM_MODE;
    ad9914_write_reg(ad9914_reg.CFR2, &reg_data, REG_DWORD);
}

void ad9914_set_freq(uint8_t reg_addr, uint32_t freq_curr)
{
    ad9914_write_reg(reg_addr, &freq_curr, REG_DWORD);
}

void ad9914_set_phase(uint8_t reg_addr, uint32_t phase_curr)
{
    ad9914_write_reg(reg_addr, &phase_curr, REG_DWORD);
}

void ad9914_dac_on(void)
{
    static uint32_t reg_data = DAC_ON;
    ad9914_write_reg(ad9914_reg.CFR1, &reg_data, REG_DWORD);
}

void ad9914_dac_off(void)
{
    static uint32_t reg_data = DAC_OFF;
    ad9914_write_reg(ad9914_reg.CFR1, &reg_data, REG_DWORD);
}

void ad9914_set_dds_st1(void)
{
    while(1)
    {
        ad9914_set_freq(ad9914_reg.PROF_F[0], FREQ_860 / 15);
        Nop();
        ad9914_set_freq(ad9914_reg.PROF_F[0], FREQ_1000);
        Nop();
        ad9914_set_freq(ad9914_reg.PROF_F[0], FREQ_1215);
        Nop();
    }
}

void __ISR(_SPI_2_VECTOR, IPL7AUTO) IntSpi2Handler(void)
{
    IFS1bits.SPI2TXIF = 0;
    IEC1bits.SPI2TXIE = 0;

    ad9914_io_update_set();
    ad9914_io_update_clr();
    
    ad9914_unselect_dds();
}
