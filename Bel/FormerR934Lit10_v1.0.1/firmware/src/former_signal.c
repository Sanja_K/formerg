#include "former_signal.h"
#include "ad9914.h"
#include "common.h"
#include "ADF435x.h"
#include "adl5240.h"
#include "hmc1122.h"
#include "channel.h"
/* 
 * EMERGENCY        LATG1
 * MOD_ON_OFF       LATD7
 * AMP_ON_OFF       LATG0
 * TEST_MODE_SUPPR  LATD6
 * CHAN_ON_OFF      LATD5
 */
#define emergency_on()              CH2_TEST_IND_Set()
#define emergency_off()             CH2_TEST_IND_Clear()

#define channel_on()                STR_ADR_XC_Set()
#define channel_off()               STR_ADR_XC_Clear()

//#define mod_on()                    LATDSET = 0x80
//#define mod_off()                   LATDCLR = 0x80

//#define amp_enable()                LATGSET = 0x01

//#define test_mode_suppression_on()  LATDSET = 0x40
//#define test_mode_suppression_off() LATDCLR = 0x40

#define XC_RW_1 RnW_XC_Set();
#define XC_RW_0 RnW_XC_Clear();   

/*
 * SLI      LATE7
 * CLK      LATE5
 * CE       LATE6
 */
#define ce_set()            P6_XC_Set();
#define ce_clr()            P6_XC_Clear();
//
#define clk_set()           P5_XC_Set();
#define clk_clr()           P5_XC_Clear();
//
#define sli_set()           P7_XC_Set();
#define sli_clr()           P7_XC_Clear();

static uint32_t amplitude_reg = 0;

static uint32_t psk2_phase[] = {PHASE0, PHASE180};
static uint32_t psk4_phase[] = {PHASE0, PHASE90, PHASE180, PHASE270};
static uint32_t psk8_phase[] = {PHASE0, PHASE45, PHASE90, PHASE135, 
                                    PHASE180, PHASE225, PHASE270, PHASE270};

#define DATA_dt(dt) {dt &= 0x001f;  LATESET = dt;}
#define DATA_clr  LATECLR = 0x001f;

#define DATA_READ (PORTE&0x0FF);
//volatile DATAbits_t DATAbits  __attribute__((address(0xBF886120)));

unsigned short int set_Freq_MHz;

/* 
 * ADR_XC0  LATG12
 * ADR_XC1  LATG13
 * ADR_XC2  LATG14
 * ADR_XC3  LATG15 
 */

#define addr_clr()                  {ADR_XC0_Clear();ADR_XC1_Clear();ADR_XC2_Clear();}
#define addr_set(addr)              {addr &= 0x07;  LATBSET = addr;} 

static void channel_0_on(void)
{

   ADR_XC0_Clear();
   ADR_XC1_Clear();
 
    RnW_XC_Set();    
    channel_on();
}

__attribute__ ((unused)) static void channel_1_on(void)
{
    ADR_XC0_Set(); 
    ADR_XC1_Clear(); 

    channel_on();
}


/* 
 * init FPGA register, witch control DDS 
 */
static void set_current_dds_ctrl1(void)
{ //uint8_t i;
    uint16_t dt_reg;

    dt_reg = CTRL_DDS_REG1;
    DATA_dt (dt_reg);
    channel_0_on();
    channel_off();
    
}

static void set_period_fsk_psk(uint16_t period)
{
    uint8_t i;
    
    ce_set();
    sli_clr();
    for(i = 0; i < 16; i++)
    {
        if(period & 0x8000)
        { sli_set();}
        
         clk_set();
        period <<= 1;
    clk_clr();
    sli_clr();
    }
    ce_clr();
}

static void set_init_freq(uint32_t freq)
{
    ad9914_set_freq(ad9914_reg.PROF_F[0], freq);
    ad9914_write_reg(ad9914_reg.PROF_PA[0], &amplitude_reg, REG_DWORD);
}

static void set_init_freq_phase(uint32_t freq, uint32_t phase)
{
    ad9914_set_freq(ad9914_reg.PROF_F[0], freq);
    ad9914_set_phase(ad9914_reg.PROF_PA[0], (phase | amplitude_reg));
}

void former_init(void)
{
    uint64_t ADF_freq;     
    ADF_freq = 3500*MHz;
    //uint64_t freqDDS;
   // freqDDS = 1330*MHz;
 
    Init_ADFApp(ADF4351_LE1,ADF_freq);
    
    ad9914_unselect_dds();
    Nop();
    ad9914_pow_on();
    set_current_dds_ctrl1();

    ad9914_reset();
    delay_us (2);
    ad9914_pow_on();
    delay_us (10);  
    ad9914_spi_3wire_mode();
    delay_us (1);
    ad9914_init_dac_cal();
    ad9914_set_profile_mode();
    delay_us (300);  

//    set_mode_freq(freqDDS, ADF_freq);
//    delay_us (300);   

}

/*
 * SWRFV1_A        LATA4
 * SWRFV2_B        LATA5
 * SWIFV1_A        LATA2
 * SWIFV2_B        LATA3 
 */
void rf_channel(unsigned int channel)
{
    switch(channel)
    {
        case CHANNEL1:
            /*
             * SWRFV1_A = 1
             * SWRFV2_B = 1
             */
           // LATASET = 0x30;
            break;
            
        case CHANNEL2:
            /*
             * SWRFV1_A = 0
             * SWRFV2_B = 0
             */
           // LATACLR = 0x30;
            break;

        case CHANNEL3:
            /*
             * SWRFV1_A = 1
             * SWRFV2_B = 0
             */
          //  LATASET = 0x10;
           // LATACLR = 0x20;
            break;

        case CHANNEL4:
            /*
             * SWRFV1_A = 0
             * SWRFV2_B = 1
             */
           // LATASET = 0x20;
           // LATACLR = 0x10;
            break;
    }
}

void if_channel(unsigned int channel)
{
    switch(channel)
    {
        case CHANNEL1:
            /*
             * SWIFV1_A = 1
             * SWIFV2_B = 1
             */
            LATASET = 0x0C;
            break;
            
        case CHANNEL2:
            /*
             * SWIFV1_A = 0
             * SWIFV2_B = 0
             */
            LATACLR = 0x0C;
            break;

        case CHANNEL3:
            /*
             * SWIFV1_A = 1
             * SWIFV2_B = 0
             */
            LATASET = 0x04;
            LATACLR = 0x08;
            break;

        case CHANNEL4:
            /*
             * SWIFV1_A = 0
             * SWIFV2_B = 1
             */
            LATASET = 0x08;
            LATACLR = 0x04;
            break;
    }
}

uint8_t read_fpga_data(uint16_t addr)
{
    unsigned char ret;
    
    RnW_XC_Clear();
    addr_clr();
    addr_set(addr);
    Nop();
    channel_on();
    Nop();
    Nop();
    ret = DATA_READ;
    channel_off();
    Nop();
    RnW_XC_Set();
    return ret;
}

void fpga_read_mode(void)
{
  //  rf_ready_clr();
    data_dir_input();
   // bus_fpga_read();
}

void fpga_write_mode(void)
{
  //  bus_fpga_write();
    data_dir_output();
  //  rf_ready_set();
}

void signal_on(void)
{
    ad9914_dac_on();
    radiation_on();
}

void signal_off(void)
{
//    amp_disable();
//    test_mode_suppression_off();
    ad9914_dac_off();
    radiation_off();
    emergency_off();
}

void radiation_on(void)
{
    //rf1_channel(CHANNEL2);
//    LATAbits.LATA9 = 1;
 //   delay_us(3);
 //   amp_enable();
}

void radiation_off(void)
{
    
 //   test_mode_suppression_off();
 //   amp_disable();
//    mod_off();
    
    channel_0_on(); 
   DATA_clr;

    
    channel_off();    
    //rf1_channel(CHANNEL1);
//    LATAbits.LATA9 = 0;
}

void repeat_init_former(void)
{
    radiation_off();
    ad9914_reset();
    ad9914_init_dac_cal();
    ad9914_set_profile_mode();
    ad9914_dac_on();
}

void set_amp(uint32_t amp)
{
    amplitude_reg = amp << 16;
}

void set_freq_current(uint32_t freq)
{
    set_current_dds_ctrl1();
    ad9914_set_freq(ad9914_reg.PROF_F[0], freq);
}

void set_mode_test(uint32_t freq)
{
    ad9914_set_profile_mode();
    set_init_freq(freq);
    radiation_on();
//    test_mode_suppression_on();
}

void set_mode_fsk_prn(uint32_t freq)
{
    ad9914_set_profile_mode();
    set_init_freq(freq);
    radiation_on();
    set_current_dds_ctrl1();
}

/* 
 * In set_mode_fsk and set_mode_psk functions we are at first set only one
 * frequency and turn on radiation, this method help to reduce time
 * between getting command and suppression start. After that, we init
 * other parameters(frequency for FSK and frequency and phase for PSK)
 * and turn on modulation 
 */

/* 
 * available values for mode is: FSK2, FSK4, FSK8 
 */
void set_mode_fsk(uint32_t *freq_arr, uint16_t period, uint8_t mode)
{
    uint8_t  i;
    uint16_t fsk_pin_data;
    uint8_t  pos_num;
    
    switch(mode)
    {
        case FSK2:
            fsk_pin_data = FSK2_PROFPIN;
            pos_num = 2;
            break;

        case FSK4:
            fsk_pin_data = FSK4_PROFPIN;
            pos_num = 4;
            break;

        case FSK8:
            fsk_pin_data = FSK8_PROFPIN;
            pos_num = 8;
            break;

        default:
            fsk_pin_data = 0;
            pos_num = 1;
            break;
    }
    
    ad9914_set_profile_mode();
    
    //set_init_freq_phase(freq_arr[0], PHASE0);
    
    radiation_on();
    
    for(i = 0; i < pos_num; i++)
    {
        ad9914_set_freq(ad9914_reg.PROF_F[i], freq_arr[i]);
        ad9914_set_phase(ad9914_reg.PROF_PA[i], amplitude_reg);
    }
    
    channel_0_on(); 
    set_period_fsk_psk(period);
    
    
    
    DATA_dt (fsk_pin_data);
    
    channel_off();
//    mod_on();
}

/* 
 * available values for mode is: PSK2, PSK4, PSK8 
 */
void set_mode_psk(uint32_t freq, uint16_t period, uint8_t mode)
{
    uint8_t i;
    uint16_t psk_pin_data;
    uint32_t *psk_phase;
    uint8_t pos_num;
    
    switch(mode)
    {
        case PSK2:
            psk_phase = psk2_phase;
            psk_pin_data = PSK2_PROFPIN;
            pos_num = 2;
            break;

        case PSK4:
            psk_phase = psk4_phase;
            psk_pin_data = PSK4_PROFPIN;
            pos_num = 4;
            break;

        case PSK8:
            psk_phase = psk8_phase;
            psk_pin_data = PSK8_PROFPIN;
            pos_num = 8;
            break;

        default:
            psk_phase = psk2_phase;
            psk_pin_data = 0;
            pos_num = 1;
            break;
    }
    
    ad9914_set_profile_mode();
    
    set_init_freq_phase(freq, psk_phase[0]);
    
    radiation_on();
    
    for(i = 1; i < pos_num; i++)
    {
        ad9914_set_freq(ad9914_reg.PROF_F[i], freq);
        ad9914_set_phase(ad9914_reg.PROF_PA[i], (psk_phase[i] | amplitude_reg));
    }
    
    channel_0_on(); 
    set_period_fsk_psk(period);

    DATA_dt (psk_pin_data);
    
    channel_off(); 
//    mod_on();
}

/* 
 * Special block for fast FRH signals generation
 */
static uint32_t frh_pos_num;
static uint32_t *frh_phase;

void frh_init(uint8_t mode, uint16_t period)
{
    uint16_t prof_pin_data;
    
    switch(mode)
    {
        case FSK2:
            frh_pos_num = 2;
            frh_phase = psk2_phase;
            prof_pin_data = FSK2_PROFPIN;
            break;
        
        case PSK2:
            frh_pos_num = 2;
            frh_phase = psk2_phase;
            prof_pin_data = PSK2_PROFPIN;
            break;
            
        case FSK4:
            frh_pos_num = 4;
            frh_phase = psk4_phase;
            prof_pin_data = FSK4_PROFPIN;
            break;
        
        case PSK4:
            frh_pos_num = 4;
            frh_phase = psk4_phase;
            prof_pin_data = PSK4_PROFPIN;
            break;
        
        case FSK8:
            frh_pos_num = 8;
            frh_phase = psk8_phase;
            prof_pin_data = FSK8_PROFPIN;
            break;
        
        case PSK8:
            frh_pos_num = 8;
            frh_phase = psk8_phase;
            prof_pin_data = PSK8_PROFPIN;
            break;
            
        case BARRIER:
            frh_pos_num = 2;
            frh_phase = psk2_phase;
            prof_pin_data = PSK2_PROFPIN;
            break;
            
        case FNM:
            frh_pos_num = 1;
            frh_phase = psk2_phase;
            prof_pin_data = PSK2_PROFPIN;
            break;
            
        default:
            frh_pos_num = 1;
            frh_phase = psk2_phase;
            prof_pin_data = 0;
            break;
    }
    
    channel_0_on();
    set_period_fsk_psk(period);
    
    DATA_dt (prof_pin_data);
    
    channel_off();
}

void set_mode_frh(uint32_t *freq)
{
    uint32_t i;
    
    set_init_freq_phase(freq[0], frh_phase[0]);
    //radiation_on();    
    for(i = 1; i < frh_pos_num; i++)
    {
        ad9914_set_freq(ad9914_reg.PROF_F[i], freq[i]);
        ad9914_set_phase(ad9914_reg.PROF_PA[i], (frh_phase[i] | amplitude_reg));
    }
    
    if(frh_pos_num > 1){}
    
//        mod_on();
}

uint32_t convert_hz_to_ftw(uint64_t freq_abs, uint64_t ad9914_sys_freq)
{
    return (uint32_t)((freq_abs * (1ULL << 32)) / ad9914_sys_freq);
}

synthesizers_freq_t get_synthesizers_frequencies (uint64_t freq_abs)
{
   static uint64_t F_DDS;
    uint8_t  SW1_set;   
    uint8_t  SW2_set; 
    
    synthesizers_freq_t freq = {
        .adf4351_freq = 3500000000ULL,
    };
    
    F_DDS = freq_abs/3;
    SW1_set = SWRF1;
    SW2_set = SWRF1; 
    
    EN_PWR_CH1_ON;       
    

            
    if((freq_abs >=  (2500000*kHz)) && (freq_abs < (3200000*kHz)))
    {

        SW1_set = SWRF2;// SWRF2;
        SW2_set = SWRF4;
        EN_PWR_CH13_ON;        
        
        freq.ad9914_freq = F_DDS;
        freq.adf4351_freq = fADF1_CH1_F1;
        
        hmc1122_set_gain_amp1(45);        
        
    }
    else if((freq_abs >= (3200000*kHz)) && (freq_abs < (4500000*kHz)))
    {
        SW1_set = SWRF2;
        SW2_set = SWRF2;
        EN_PWR_CH11_ON;        
        freq.ad9914_freq = F_DDS;
        freq.adf4351_freq = fADF1_CH1_F2;
        
        hmc1122_set_gain_amp1(20);        
    }
    
    else if((freq_abs >= (4500000*kHz)) && (freq_abs <= (6000000*kHz))) //--------
    {
        SW1_set = SWRF3;
        SW2_set = SWRF3;
        EN_PWR_CH12_ON;     
        RF1_AMPL_ON;       
        
        if ((freq_abs >= (4500000*kHz)) && (freq_abs < (4800000*kHz)))
        {
            F_DDS = fADF1_CH1_F3 - F_DDS;
            freq.adf4351_freq = fADF1_CH1_F3;  
        }
        else if ((freq_abs >= (4800000*kHz)) && (freq_abs < (5100000*kHz)))
        {
            F_DDS = fADF1_CH1_F4 - F_DDS;
            freq.adf4351_freq = fADF1_CH1_F4;
        }
        else if ((freq_abs >= (5100000*kHz)) && (freq_abs < (5400000*kHz)))
        {
            F_DDS = fADF1_CH1_F5 - F_DDS;
            freq.adf4351_freq = fADF1_CH1_F5;
        }
        else if ((freq_abs >= (5400000*kHz)) && (freq_abs < (5700000*kHz)))
        {
            F_DDS = fADF1_CH1_F6 - F_DDS;
            freq.adf4351_freq = fADF1_CH1_F6;
        }
        else if ((freq_abs >= (5700000*kHz)) && (freq_abs <= (6000000*kHz)))
        {
            F_DDS = fADF1_CH1_F7 - F_DDS;
            freq.adf4351_freq = fADF1_CH1_F7;

        }
        
        hmc1122_set_gain_amp1(63);
        freq.ad9914_freq = F_DDS;
        //freq.ADF_freq = fADF1_CH1_F3;
        
        
    }
   // SW2_set = SWRF1;
    freq.SW1ref = SW1_set;
    freq.SW2ref = SW2_set;
//    SW1DA3_RF1(SW1_set);//signal
//    SW2DA16_RF1(SW2_set); //signal  
    
    return freq;
} 

void set_mode_freq(uint64_t  freq, uint64_t Freq_ref)
{
    uint32_t freq_lo;    
    //freq  = freq*kHz; // kHz to Hz;  
    freq_lo = convert_hz_to_ftw(freq, Freq_ref);
    ad9914_set_freq(ad9914_reg.PROF_F[0], freq_lo);   

}


/* freq - [FTW], dev - [kHz], speed - [kHz/sec], ad9914_sys_freq - [Hz] */
void set_mode_lfm(uint64_t freq, uint64_t dev, uint32_t speed, uint64_t Freq_ref)
{
    
    uint32_t freq_ftw;
    uint32_t dev_ftw;
    freq = freq;
    dev = dev*kHz;
   
    freq_ftw = convert_hz_to_ftw(freq, Freq_ref);    
    dev_ftw = convert_hz_to_ftw(dev, Freq_ref);
    
    uint32_t freq_lo = freq_ftw - dev_ftw;
    uint32_t freq_hi = freq_ftw + dev_ftw;
    uint32_t step_size = (2ULL * dev * speed * 24) / Freq_ref;
    uint32_t ramp_rate = RAMP_RATE;
    
    if(step_size == 0)
    {
        step_size = 1;
    }
    step_size = convert_hz_to_ftw(step_size, Freq_ref);
    
    ad9914_write_reg(ad9914_reg.DRAMP_LO, &freq_lo, REG_DWORD);
    ad9914_write_reg(ad9914_reg.DRAMP_HI, &freq_hi, REG_DWORD);
    ad9914_write_reg(ad9914_reg.DRAMP_RIS, &step_size, REG_DWORD);
    ad9914_write_reg(ad9914_reg.DRAMP_FAL, &step_size, REG_DWORD);
    ad9914_write_reg(ad9914_reg.DRAMP_RATE, &ramp_rate, REG_DWORD);
    
    ad9914_set_lfm_mode();
    
    radiation_on();
}