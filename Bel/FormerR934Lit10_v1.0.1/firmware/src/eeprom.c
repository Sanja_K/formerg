#include "xc.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "common.h"
#include "eeprom.h"
#include "periphery.h"
#include "I2C.h"



void eeprom_write_byte(uint16_t addr, uint8_t byte)
{  uint8_t myTxData [3]; 
  //  uint8_t slaveaddr;
    
    myTxData[0] = (addr>>8)&0xFF;
    myTxData[1] = addr&0xFF;     
    myTxData[2] = byte;
    if(!I2C1_Write(EE_ADDR, myTxData, 3))
    {
        // error handling
    }      
    delay_us(10);
}

uint8_t eeprom_read_byte(uint16_t addr)
{  uint8_t myRxData [2];
    //uint8_t slaveaddr;
   uint8_t dataread = 0;
    
    myRxData[0] = (addr>>8)&0xFF;
    myRxData[1] = addr&0xFF;     
    
    if(!I2C1_WriteRead( EE_ADDR, myRxData,2,&dataread, 1 ))
    {
       // error handling
    }   
    delay_us(1);
    
    return dataread;
}

uint8_t eeprom_write_array(uint16_t addr, uint8_t *buf, uint16_t size)
{   
    uint8_t myTxData [EE_PAGE_SIZE+2]; //max 128 bytes
    uint16_t cntDtSend;
    
    int i;
        
    if ((addr+size)> EE_SIZE)
    {
        return 1;
    }
    
    i = 0;
    cntDtSend = 0;    
    while (size > 0)
    {
        if (size > EE_PAGE_SIZE)
        {
            cntDtSend = EE_PAGE_SIZE;
        }
        else
        {
            cntDtSend = size;
        }
        //---------------------------------
        
        myTxData[0] = (addr>>8)&0xFF;
        myTxData[1] = addr&0xFF;           
               
        memcpy (&myTxData [2], &buf[i], cntDtSend);
        if(!I2C1_Write(EE_ADDR, myTxData, cntDtSend+2))
        {
            // error handling
        }      
        if (size > EE_PAGE_SIZE)
        {        
            size -= cntDtSend;
            addr += cntDtSend;
            i += cntDtSend;               
        }
        else
        {
            size = 0;
        }
        delay_us(100);   
        
    }; 

        delay_us(10);
    return 0;    
}


uint8_t eeprom_read_array(uint16_t addr, uint8_t *buf, uint16_t size)
{
  uint8_t cmdread [2]; 
  uint8_t dataread [EE_PAGE_SIZE]; 
  uint16_t cntGetDt;  
  int i = 0;

  
    if ((addr+size)> EE_SIZE)
    {
        return 1;
    }

    for (i=0;i<EE_PAGE_SIZE;i++)
    {
         dataread[i]= 0;
    }
    cntGetDt = 0;
  
   
    i2c_data.Status = I2C_STATUS_IDLE;
    
    i = 0;
    
    while (size > 0)
    {
        
        if (size > EE_PAGE_SIZE)
        {
            cntGetDt = EE_PAGE_SIZE;
        }
        else
        {
            cntGetDt = size;
        }        
        
        cmdread[0] = (addr>>8)&0xFF;
        cmdread[1] = addr&0xFF; 
        
        
        if(!I2C1_WriteRead( EE_ADDR, cmdread,2,dataread,cntGetDt))
        {
           // error handling
        }   
        I2CStartBusy();
        delay_us(100);     
    
        
        memcpy (&buf[i], dataread, cntGetDt);
        
        if (size > EE_PAGE_SIZE)
        {  
            size -= cntGetDt;
            i += cntGetDt;
            addr += cntGetDt;
        }  
        else
        {
            size = 0;
        }

    }



    return 0;  
}


