/* 
 * File:   delay.h
 * Author: shaft
 *
 * Created on 23 ��� 2019 �., 10:09
 */
#include <xc.h> 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#ifndef DELAY_H
#define	DELAY_H

#ifdef	__cplusplus
extern "C" {
#endif

//#define FCY                 80000000UL
    
///* delay_ms() and delay_us() active delays, they are defined as macros. 
// * They depend on a user-supplied definition of FCY. If FCY is not defined, 
// * you'll get error
// * max val for delay_us fun: max_val = (2^32 - 1) / (FCY / 1000000),
// * for FCY = 80 MHz max_val =  53687091, for FCY = 10 MHz max_val = 429496729 
// * 
// * max val for delay_ms fun: max_val = (2^32 - 1) / (FCY / 1000),
// * for FCY = 80 MHz max_val =  53687, for FCY = 10 MHz max_val = 429496
// * 
// * for values > then max values overflow occurs */
//#if defined(FCY)
//#define delay_us(val) \
//    { delay_cycles((unsigned int)(((unsigned long long)val) * (FCY)/1000000ULL)); }
//#define delay_ms(val) \
//    { delay_cycles((unsigned int)(((unsigned long long)val) * (FCY)/1000ULL)); }
////#endif    


#ifdef	__cplusplus
}
#endif

#endif	/* DELAY_H */

