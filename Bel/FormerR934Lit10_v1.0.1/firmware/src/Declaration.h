/* 
 * File:   Declaration.h
 * Author: Alex
 *
 * Created on 26-07-2014
 */

#ifndef MAIN_H
#define	MAIN_H

/* Packet header */
#define POS_ADR                 0
#define POS_CID                 1
#define POS_LEN                 2

/* Fixed frequency mode */
#define POS_NUM                 3
#define POS_F_L                 4
#define POS_F_M                 5
#define POS_F_H                 6
#define POS_MOD                 7
#define POS_DEV                 8
#define POS_MAN                 9
#define POS_TIME                10
#define POS_DATA                3

/* FHSS mode */
#define POS_F_L_FHSS            3
#define POS_F_M_FHSS            4
#define POS_F_H_FHSS            5
#define POS_MOD_FHSS            6
#define POS_DEV_FHSS            7
#define POS_MAN_FHSS            8
#define POS_RES_FHSS            9

/*
 * Voice mode
 */
#define POS_F_L_VOICE           3
#define POS_F_M_VOICE           4
#define POS_F_H_VOICE           5
#define POS_DEV_VOICE           6


#define DSP_FREQ_BAND           50000UL

#define HIGH_RES                0
#define HIGH_RES_POINTS         16384        

#define MEDIUM_RES              1
#define MEDIUM_RES_POINTS       8192

#define LOW_RES                 2
#define LOW_RES_POINTS          4096

#define PAR_NUM                 7

#define CMD_STATUS              0x04
#define CMD_RADIATION_OFF       0x24
#define CMD_TX_FREQ_DATA        0x28
#define CMD_MODE_FRF            0x30
#define CMD_MODE_FHSS           0x32
#define CMD_MODE_VOICE          0x40
#define CMD_VOICE_DATA          0x42
#define CMD_GET_FIFO_SPACE      0x44
#define CMD_GET_VERSION         0x92
#define CMD_SET_AMP_VAL         0x94
#define CMD_GET_AMP_VAL         0x96
#define CMD_ERROR               0xF8
#define CMD_STATUS_OK           0xFC
#define CMD_TEST_ON             0x51
#define CMD_TEST_OFF            0x50

#define BUTTON_PORT             PORTF
#define BUTTON_PIN_NUM          8
#define LED_LAT                 LATA
#define LED_PIN_NUM             4

#define FPGA_INIT               PORTAbits.RA9

#define LIT10_MIN_FREQ          100000UL
#define LIT10_MAX_FREQ          6000000UL

#define LIT10_ADDR_RX           0x19                
#define LIT10_ADDR_TX           0x91
#define LIT10_AMP               0x0FFF
#define LIT10_TEST_FREQ         4500000000ULL

#define MIN_AMP                 0x0200
#define MAX_AMP                 0x0FFF

//eeprom map
#define ADDR_LIT10_AMP          0x0000

#define ADR_AMP_BASE            0x0000
#define AMP_SIZE                0x04

#define ARRAY_SIZE(x)           (sizeof(x) / sizeof((x)[0]))

//
unsigned long long int	ulAbsFreq[4];
unsigned long long int	ulAbsFreqHopStart;
//
/********************************* VARIABLES **********************************/
struct button_s btn;

struct lit_parameters
{
    unsigned char addr_tx;
    unsigned char addr_rx;
    unsigned long long central_freq;
    unsigned char lit_num;
};

struct flags
{
    unsigned busy           :1;
    unsigned cmd_eo_time    :1;
    unsigned radiation_on   :1;
    unsigned                :5;
};

enum lit_num
{
    LIT10 = 0x00,
    LIT_NUM,
};

enum suppression_modes
{
    FIXED_FREQ,
    FREQ_HOPING
};

/********************************* FUNCTIONS **********************************/
int                     fun_param(struct lit_parameters *param);
void                    fun_frh_init(unsigned char mode, unsigned char manipulation);
void                    fun_frh_on(unsigned char mode, unsigned int freq, unsigned char deviation);
int                     fun_uart_main(struct lit_parameters *param);
void                    fun_button_handler(struct button_s *btn, unsigned long long central_freq);
void                    signal_switch_handler(void);
void                    frh_handler(void);
void                    fnm_freq_switch();
void                    fnm_freq_switch_on();
void                    fnm_freq_switch_off();
unsigned int            filter_switch(struct lit_parameters *param, unsigned long long freq);
void                    read_buf_init(void);
void                    init_start_former(void);
void                    formet_lit_init(struct lit_parameters *param);
unsigned int            get_amp_val(enum lit_num num);
unsigned char           set_amp_val(enum lit_num num, unsigned int val);

#endif	/* MAIN_H */