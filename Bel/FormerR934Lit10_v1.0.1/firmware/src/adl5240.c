#include "adl5240.h"
#include "definitions.h" 

#define CLK_ADL_1 	CLK_ATT_Set() 
#define CLK_ADL_0 	CLK_ATT_Clear() 

#define DATA_ADL_1    DATA_ATT_Set()
#define DATA_ADL_0    DATA_ATT_Clear()

//#define LE1_ADL_1     LE_ATT1_Set()   //
//#define LE1_ADL_0     LE_ATT1_Clear() 

#define LE2_ADL_1     LE_ATT2_Set()   //
#define LE2_ADL_0     LE_ATT2_Clear() 

#define LE3_ADL_1     LE_ATT3_Set()   //
#define LE3_ADL_0     LE_ATT3_Clear() 


//#define amp1_le_seq()   {LE1_ADL_1; Nop(); Nop(); Nop(); LE1_ADL_0;}
#define amp2_le_seq()   {LE2_ADL_1; Nop(); Nop(); Nop(); LE2_ADL_0;}
#define amp3_le_seq()   {LE3_ADL_1; Nop(); Nop(); Nop(); LE3_ADL_0;}



static void adl5240_write(unsigned char val)
{
    unsigned char i;
    val <<= 2;
    
    for(i = 0; i < 6; i++)
    {
        if(val & 0x80)
            DATA_ADL_1;
        else
            DATA_ADL_0;
        
        CLK_ADL_1;
        val <<= 1;
        CLK_ADL_0;
    }
    
    DATA_ADL_0;
}
 void adl5240_set_gain_amp(unsigned char gain)
{
     
     if(gain > 63)
        gain = 0;   
     
//    amp1_le_seq();
    adl5240_write(gain);
//    amp1_le_seq();
}

// void adl5240_set_gain_amp1(unsigned char gain) // L2
// {
    // amp1_le_seq();
    // adl5240_set_gain_amp(gain);
    // amp1_le_seq();
// }

void adl5240_set_gain_amp2(unsigned char gain) // 100- 500
{

    amp2_le_seq();
    adl5240_set_gain_amp(gain);
    amp2_le_seq();
}
void adl5240_set_gain_amp3(unsigned char gain) // L1
{
 
    amp3_le_seq();
    adl5240_set_gain_amp(gain);
    amp3_le_seq();
}
