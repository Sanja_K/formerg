#ifndef _PERIPHERY_H    /* Guard against multiple inclusion */
#define _PERIPHERY_H

#define PB_CLK                  80000000
#define BAUDRATE                115200
#define BRGVAL                  (PB_CLK / (BAUDRATE * 4)) - 1

#define SPIBAUDRATE             20000000
#define SPIBRGVAL               (PB_CLK / (2 * SPIBAUDRATE)) - 1

//#define RS485_TX_EN             LATFbits.LATF3
//#define RS485_TX_EN             LATAbits.LATA10
#define RS485_TX_EN_1             ON_RS485_Set()
#define RS485_TX_EN_0             ON_RS485_Clear()


/********************************* FUNCTIONS **********************************/
#ifdef __DEBUG
    void                debug_char(unsigned char);
    void                debug_msg(unsigned char *, unsigned int);
#else
    #define debug_msg(x,y)
    #define debug_char(x)
#endif

#define clr_wdt()       WDTCONbits.WDTCLR = 1;

void                    port_init(void);
void                    int1_init(void);
void                    irq_init(void);
void                    system_init(void);
//void                    spi1_init(void);
void                    spi2_init(void);
void                    uart1_init(void);
void                    uart2_init(void);
void                    timer2_init(void);
void                    timer3_init(void);
void                    timer4_init(void);
void                    timer5_init(void);

void                    u2_transmit_ch(unsigned char);
void                    u2_transmit_arr(unsigned char *, unsigned int);

void                    spi2_write_byte(unsigned char byte);
unsigned char           spi2_read_byte(void);

#endif /* _PERIPHERY_H */

