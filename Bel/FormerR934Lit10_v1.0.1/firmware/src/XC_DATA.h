/* 
 * File:   XC_DATA.h
 * Author: Alexandr
 *
 * Created on 25 октября 2017 г., 10:07
 */

#ifndef XC_DATA_H
#define	XC_DATA_H

#ifdef	__cplusplus
extern "C" {
#endif

#define XC_v_o_DATA_0 SDI_XC_Clear();//+
#define XC_v_o_DATA_1 SDI_XC_Set(); 
  
#define XC_v_o_CLK_0 SCLK_XC_Clear() ;//+
#define XC_v_o_CLK_1 SCLK_XC_Set(); 
  
#define XC1_v_o_LE_0 LE_XC1_Clear();//XC1+
#define XC1_v_o_LE_1 LE_XC1_Set();

#define XC2_v_o_LE_0  LE_XC2_Clear();//XC2+
#define XC2_v_o_LE_1  LE_XC2_Set();
 

#define ADR_XC1_1 	ADR_XC0_Set();//
#define ADR_XC1_0 	ADR_XC0_Clear(); 
 
#define ADR_XC1_1 	ADR_XC1_Set();//
#define ADR_XC1_0 	ADR_XC1_Clear();   

#define ADR_XC1_1 	ADR_XC2_Set();//
#define ADR_XC1_0 	ADR_XC2_Clear();   

//---------------------------------------------------------------------
#define XC_strob_1 STR_ADR_XC_Set();
#define XC_strob_0 STR_ADR_XC_Clear();

#define XC_RW_1 RnW_XC_Set();
#define XC_RW_0 RnW_XC_Clear();      

//------------------------------------------------------------------------

#define data_reg_XC_status (PORTE&0x0FF);

#define P0_XC_1        P0_XC_Set();
#define P0_XC_1        P0_XC_Clear();

#define P1_XC_1        P1_XC_Set();
#define P1_XC_1        P1_XC_Clear();

#define P2_XC_1        P2_XC_Set();
#define P2_XC_1        P2_XC_Clear();

#define P3_XC_1        P3_XC_Set();
#define P3_XC_1        P3_XC_Clear();

#define P4_XC_1        P4_XC_Set();
#define P4_XC_1        P4_XC_Clear();

#define P5_XC_1        P5_XC_Set();
#define P5_XC_1        P5_XC_Clear();

#define P6_XC_1        P6_XC_Set();
#define P6_XC_1        P6_XC_Clear();


//-------------------------------------------------------------------------    
 void Init_XC_Dt(void);
 void Init_XC(void);    
 uint8_t Init_XC_freq (unsigned char DDSx, uint64_t F_man);   
 uint8_t Init_XC_ph (unsigned char DDSx, uint64_t F_man);
 
 uint8_t  XC_LD_DATA( void);
    
#ifdef	__cplusplus
}
#endif

#endif	/* XC_DATA_H */

