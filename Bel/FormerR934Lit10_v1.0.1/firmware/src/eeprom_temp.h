#ifndef _EEPROM_H    /* Guard against multiple inclusion */
#define _EEPROM_H

#include <xc.h>
#include "definitions.h" 

#define EEPROM_CS           LATGbits.LATG9
#define EEPROM_CS_TRIS      TRISGbits.TRISG9

#define EE_READ             0b00000011
#define EE_WRITE            0b00000010
#define EE_WRDI             0b00000100
#define EE_WREN             0b00000110
#define EE_RDSR             0b00000101
#define EE_WRSR             0b00000001

/* sizes in bytes*/
#define EE_PAGE_SIZE        128
#define EE_SIZE             65536

void eeprom_init(void);

void eeprom_write_byte(unsigned short addr, unsigned char byte);
void eeprom_write_array(unsigned short addr, unsigned char *byte, int size);

unsigned char eeprom_read_byte(unsigned short addr);
void eeprom_read_array(unsigned short addr, unsigned char *buf, int size);

#endif /* _EEPROM_H */

