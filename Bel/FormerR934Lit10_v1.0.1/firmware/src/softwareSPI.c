/*
Software SPI library functions - currently only works in mode 1 used for 25AA** EEPROM
*/
#include <xc.h> 
#include <math.h>
#include "common.h"
#include "definitions.h" 

//#define        FCY         39936000           // Instruction Cycle Frequency 79.872.000 Hz
//#include <libpic30.h>
#include "softwareSPI.h"

unsigned char writeSPIByte(unsigned char transmit)
{
  //make the transmission
  unsigned char mask = 0x80;                //0x80 Initialize to write and read bit 7
  unsigned char ret = 0;                    //Initialize read byte with 0
    
  SCK = 0;
  
  do{
    //Clock out current bit onto SPI Out line
    if (transmit & mask)
      MOSI = 1;
    else
      MOSI = 0;
    SCK = 0;            //Set SPI Clock line    
  //  delay_us(1);      //4 Ensure minimum delay of 500nS between SPI Clock high and SPI Clock Low
    SCK = 1;            //Set SPI Clock line
    mask = mask >> 1;   //Shift mask so that next bit is written and read from SPI lines 
  //  delay_us(1);      //4 Ensure minimum delay of 1000ns between bits    
  }while (mask != 0);
      SCK = 0;            //Set SPI Clock line
      
  return ret;
}
//==============================================================================
/*
0 - CSB1 - B11 - LE_ADF1 - LMX2592 - DA3
1 - CSB2 - B10 - LE_ADF2 - LMX2592 - DA16
2 - CSB3 - B13 - LE_ADF3 - LMX2572 - DA19
3 - CSB4 - B12 - LE_ADF4 - LMX2592 - DA34
*/
void RegWrite(unsigned char synthesizer, unsigned char address, unsigned short int data)
{
    switch(synthesizer) {
    case 0: CSB1 = 0; break;
    case 1: CSB2 = 0; break;
    case 2: CSB3 = 0; break;
    case 3: CSB4 = 0; break;
    default:break;
    }
//    CSB1 = 0;                         // Bring CS low (active)
    delay_us(1);//10    
    writeSPIByte(address);               // Output command
    writeSPIByte((data>>8)&0xFF);        // Output MSB of address
    writeSPIByte(data&0xFF);             // Output LSB of address
    delay_us(1);//10
//    CSB1 = 1;                         // Bring CS high (inactive)
    switch(synthesizer) {
    case 0: CSB1 = 1; break;
    case 1: CSB2 = 1; break;
    case 2: CSB3 = 1; break;
    case 3: CSB4 = 1; break;
    default:break;
    }    
//    __delay_ms(12);
    delay_us(1);     // ����� ������, �������� �������� ����������� ������� �� 100���    
} // end of LowDensByteWrite(...)
//------------------------------------------------------------------------------
int writeSPIWord(unsigned short int setting)
{
  int data;
  unsigned char b1, b2;
  b1 = writeSPIByte(setting >> 8);
  b2 = writeSPIByte(setting);
  data = b1 << 8 | b2;
  delay_us(50);
  return data;
}

//int writeSPI24bits(unsigned short long setting)
//{
//  unsigned short long data;
//  unsigned char b1, b2, b3;
//  b1 = writeSPIByte(setting >> (unsigned short long)16);
//  b2 = writeSPIByte(setting >> 8);
//  b3 = writeSPIByte(setting);
//  data = (unsigned short long)b1 << (unsigned short long)16 | b2 << 8 | b3;
//  __delay_us(50);
//  return data;
//}

int readSPIWord(){
  int data;
  unsigned char b1, b2;
  b1 = writeSPIByte(0x00);
  b2 = writeSPIByte(0x00);
  data = b1 << 8 | b2;
  return data;  
}
unsigned char readSPIByte(){
  unsigned char data;
  data = writeSPIByte(0x00);
  return data;
} 

