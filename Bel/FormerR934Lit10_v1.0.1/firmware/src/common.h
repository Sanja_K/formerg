#include <stdint.h>
#include "definitions.h" 
#ifndef _EXAMPLE_FILE_NAME_H    /* Guard against multiple inclusion */
#define _EXAMPLE_FILE_NAME_H

#define FCY                 80000000

#define BUTTON_DELAY        75          //in msec

#define GHz 1000000000LL
#define MHz    1000000LL
#define kHz       1000LL
#define  Hz          1LL

/********************************* FUNCTIONS **********************************/
/* active delay, minimum delay 50 cycles, accuracy ~2-3 ticks */
void delay_cycles(unsigned int cycles);

/* delay_ms() and delay_us() active delays, they are defined as macros. 
 * They depend on a user-supplied definition of FCY. If FCY is not defined, 
 * you'll get error
 * max val for delay_us fun: max_val = (2^32 - 1) / (FCY / 1000000),
 * for FCY = 80 MHz max_val =  53687091, for FCY = 10 MHz max_val = 429496729 
 * 
 * max val for delay_ms fun: max_val = (2^32 - 1) / (FCY / 1000),
 * for FCY = 80 MHz max_val =  53687, for FCY = 10 MHz max_val = 429496
 * 
 * for values > then max values overflow occurs */
#if defined(FCY)
#define delay_us(val) \
    { delay_cycles((unsigned int)(((unsigned long long)val) * (FCY)/1000000ULL)); }
#define delay_ms(val) \
    { delay_cycles((unsigned int)(((unsigned long long)val) * (FCY)/1000ULL)); }
#endif

/* function-wrapper, help to get value of CP counter */
unsigned int get_cp_count(void);

/* returns true if the time a is after time b */
#define time_after(a, b)            ((long)((b) - (a)) < 0)

/* helper macros to convert msec and usec to CP cycles */
#if defined(FCY)
#define uSec(val)                   (unsigned int)(((unsigned long long)val) * (FCY)/(2 * 1000000ULL))
#define mSec(val)                   (unsigned int)(((unsigned long long)val) * (FCY)/(2 * 1000ULL))
#endif

enum button_status
{
    OFF = 0,
    ON = 1,
    NO_CHANGES = 0xFFFFFFFF,
};

struct button_s
{
    unsigned int *button_port;
    unsigned int button_pin;
    unsigned int *led_lat;
    unsigned int led_pin;
    unsigned int was_pressed;
    unsigned int was_released;
    unsigned int timeout;
};

/* functions for handle status of the "clock" button with the led */

struct button_s button_init(unsigned int *button_port, unsigned int button_pin, unsigned int *led_lat, unsigned int led_pin);
enum button_status button_handler(struct button_s *curr_button);
void off_led(struct button_s *curr_button);
void LLDIV (uint64_t numer, uint64_t denom,  uint64_t *quot, uint64_t *rem);

#define EN_PWR_CH1_ON	EN_PWR_CH1_Set()
#define EN_PWR_CH1_OFF	EN_PWR_CH1_Clear()

#define EN_PWR_CH2_ON	EN_PWR_CH2_Set()
#define EN_PWR_CH2_OFF	EN_PWR_CH2_Clear()
//-------------------------------------
#define EN_PWR_CH3_ON	EN_PWR_CH3_Set()
#define EN_PWR_CH3_OFF	EN_PWR_CH3_Clear()
//------------------------------------------
#define EN_PWR_CH11_ON	EN_PWR_CH11_Set()
#define EN_PWR_CH11_OFF	EN_PWR_CH11_Clear()

#define EN_PWR_CH12_ON	EN_PWR_CH12_Set()
#define EN_PWR_CH12_OFF	EN_PWR_CH12_Clear()

#define EN_PWR_CH13_ON	EN_PWR_CH13_Set()
#define EN_PWR_CH13_OFF	EN_PWR_CH13_Clear()
//-------------------------------------------------
#define RF1_AMPL_ON	 	RF1_AMPL_ON_Set()
#define RF1_AMPL_OFF	RF1_AMPL_ON_Clear()
//----------------------------------------------------

//------------------CH1-----------------------------------   
#define   fADF1_CH1_F1   (3500000000ULL) // kHz
#define   fADF1_CH1_F2   (3500000000ULL)  
#define   fADF1_CH1_F3   (2500000000ULL) 
#define   fADF1_CH1_F4   (2600000000ULL) 
#define   fADF1_CH1_F5   (2700000000ULL) 
#define   fADF1_CH1_F6   (2800000000ULL) 
#define   fADF1_CH1_F7   (2900000000ULL) 



#define MCHP_SUCCESS                     0x00
#define MCHP_FAILURE                     0x01

#define MCHP_DEVICE_NOT_FOUND            0x02

#define MCHP_DEVICE_IS_STARTED           0x05
#define MCHP_DEVICE_IS_STOPPED           0x06


#define MCHP_DDS_ERROR                  0x07
#define MCHP_ADF_ERROR                  0x08

#define MCHP_INVALID_VERSION             0xF4
#define MCHP_CRC_ERROR                   0xF6
#define MCHP_RUN_ERROR                   0xF8//
#define MCHP_CMD_FAILURE                 0xFF 


#endif /* _EXAMPLE_FILE_NAME_H */
