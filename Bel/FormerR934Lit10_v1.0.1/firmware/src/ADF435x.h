/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef XC_AD435x_H
#define	XC_AD435x_H

#include <xc.h> // include processor files - each processor file is guarded. 
#include <stdint.h>
#include "ADF435x.h" 
#include "synt_AD4351.h"

//char Init_ADFApp(uint64_t freq_MHz, int ADF4351_LE);
uint8_t Init_ADFApp (int ADF4351_LE_num, uint64_t freq_kHz);
uint8_t  ADF4351_setFreq_num (int ADF4351_LE_num, uint64_t AD4351_Freq_Hz); 
uint8_t Set_RFoff_ADFApp (int ADF4351_LE_num);

//void Set_freq_ADF2App (uint64_t AD4351_Freq_Hz);
//void Set_RFoff_ADF2App (void);


#endif    