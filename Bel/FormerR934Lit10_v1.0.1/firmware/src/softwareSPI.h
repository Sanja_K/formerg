/*
Software SPI library functions
*/
#ifndef softwareSPI_h
#define softwareSPI_h

#include <stdio.h>
#include <stdlib.h>
#include "definitions.h" 

#define SCK   _LATB12
#define MOSI  _LATB13

#define CSB1 _LATB4    //CSB1
#define CSB2 _LATB11   //CSB2
#define CSB3 _LATB13   //CSB3
#define CSB4 _LATB12   //CSB4

#define EN_PWR_CH1  _LATD1 
#define EN_PWR_CH2  _LATD14


struct Synthesizers
    {
        unsigned short int Fpd;
    
        unsigned short int R0;
        unsigned short int R1;
        unsigned short int R7;
        unsigned short int R30;
        unsigned short int R31;
        unsigned short int R35;
        unsigned short int R36;
        unsigned short int R37;
        unsigned short int R38;
        unsigned short int R44;
        unsigned short int R45;
        unsigned short int R46;
        unsigned short int R47;        
    } ;

unsigned char writeSPIByte(unsigned char transmit);
int writeSPIWord(unsigned short int setting);
//int writeSPI24bits(unsigned short long setting);
int readSPIWord();
unsigned char readSPIByte();
void RegWrite(unsigned char synthesizer, unsigned char address, unsigned short int data);

#endif

