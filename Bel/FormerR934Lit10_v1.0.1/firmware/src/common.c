#include <xc.h>
#include <sys/attribs.h>
#include <cp0defs.h>
#include "common.h"
#include <stdint.h>

void delay_cycles(unsigned int cycles)
{
    unsigned int start;
    unsigned int stop;

    if(cycles < 50)
        cycles = 0;
    else
        cycles -= 50;
    
    start = get_cp_count();
    stop = start + cycles / 2;
    
    while(!time_after(get_cp_count(), stop));
}

unsigned int get_cp_count(void)
{
    return _CP0_GET_COUNT();
}

unsigned int time_out(unsigned int time)
{
    unsigned int curr_time = get_cp_count();
    
    return time_after(curr_time, time);
}

struct button_s button_init(unsigned int *button_port, unsigned int button_pin, unsigned int *led_lat, unsigned int led_pin)
{
     struct button_s curr_button = {
        .button_port    = button_port,
        .button_pin     = button_pin,
        .led_lat        = led_lat,
        .led_pin        = led_pin,
        .was_pressed    = 0,
        .was_released   = 1,
    };
    
    return curr_button;
}

static unsigned int get_button_val(struct button_s *curr_button)
{
    return !(!(*(curr_button->button_port) & (1 << curr_button->button_pin)));
}

static unsigned int get_led_val(struct button_s *curr_button)
{
    return !(!(*(curr_button->led_lat) & (1 << curr_button->led_pin)));
}

static void change_led_val(struct button_s *curr_button)
{
    *(curr_button->led_lat) ^= 1 << curr_button->led_pin;
}

void off_led(struct button_s *curr_button)
{
    curr_button->was_pressed = 0;
    curr_button->was_released = 1;
    *(curr_button->led_lat) &= ~(1 << curr_button->led_pin);
}

enum button_status button_handler(struct button_s *curr_button)
{
    enum button_status ret = NO_CHANGES;
    
    if(!curr_button->was_pressed & !get_button_val(curr_button))
    {
        curr_button->timeout = get_cp_count() + mSec(BUTTON_DELAY);
        curr_button->was_pressed = 1;
    }
    
    else if(curr_button->was_pressed)
    {
        if(time_out(curr_button->timeout))
        {
            if(!get_button_val(curr_button) && curr_button->was_released)
            {
                change_led_val(curr_button);
                curr_button->was_pressed = 0;
                curr_button->was_released = 0;
                ret = get_led_val(curr_button);
            }
            else if(!curr_button->was_released)
            {
                if(get_button_val(curr_button))
                {
                    curr_button->was_released = 1;
                    curr_button->was_pressed = 0;
                }
            }
            else
            {
                curr_button->was_pressed = 0;
            }
        }
    }
    
    return ret;
}

  void LLDIV (uint64_t numer, uint64_t denom,  uint64_t *quot, uint64_t *rem)
{
    uint64_t a,b,b1;
    uint64_t q,r;// �������, ������� 
    a= numer;
    b = denom;
    b1 = b;
    while (b1<=a) b1*=2;
     q = 0;
     r = a;
     while (b1!=b) {
      b1/=2; q*=2;
      if (r>=b1) { r-=b1; q++; }
     }
     *rem = r;
     *quot = q;
}