/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/
#include <xc.h>         /* XC8 General Include File */
#include "common.h"
#include "XC_DATA.h"




void Init_XC(void)
{
    

}

uint8_t  XC_LD_DATA( void)
{
    uint8_t LD_data;

    XC_ADR_status_0;
    XC_RW_status_1;
    XC_strob_status_0;
    delay_us (1);    
    XC_strob_status_1;
    delay_us (1);     
    LD_data = data_reg_XC_status;
    XC_strob_status_0;    

    return LD_data;
}


static void  XC_v_o_DATA( unsigned int ena )
{
    if(ena == 1)
        XC_v_o_DATA_1
    else
        XC_v_o_DATA_0;   
}  

static void  XC_writeReg1( unsigned int reg )
{ unsigned int i=0;
    XC_v_o_CLK_0;

    delay_us( 10 );

    for( i=0; i<16; i++ ) {
        XC_v_o_DATA( (reg>>15)&0x01 );
        delay_us( 10 );
        XC_v_o_CLK_1;
        reg = reg << 1;
        delay_us( 10 );
       XC_v_o_CLK_0;
    }
    delay_us( 10 );

} 
static void  XC_writeReg2( unsigned int reg )
{ unsigned int i=0;
    XC_v_o_CLK_0;

    delay_us( 10 );

    for( i=0; i<4; i++ ) {
        XC_v_o_DATA( (reg>>3)&0x01 ); // starshim vpered
        delay_us( 10 );
        XC_v_o_CLK_1;
        reg = reg << 1;
        delay_us( 10 );
       XC_v_o_CLK_0;
    }
    delay_us( 10 );

} 
void Init_XC_Dt (void)
{  
     unsigned int XCx_dt;
     
     XCx_dt = (SetGPSL1<<2)|(SetGLL1<<3)|(SetGPSL2<<1)|(SetGLL2); 
     XC1_v_o_LE_1;
     XC_writeReg2(XCx_dt);

     XC1_v_o_LE_0;
    
}

uint8_t Init_XC_freq (unsigned char DDSx, uint64_t F_man)
{  
     unsigned int XCx_dt;
     if (F_man > 1*MHz)
     { return 1; } // 
     
     XCx_dt = 65536 - (25000000/F_man); 
     
     XC2_v_o_LE_1; 

     XC_writeReg1(XCx_dt);

     XC2_v_o_LE_0;
     return 0;
}

uint8_t Init_XC_ph (unsigned char DDSx, uint64_t F_man)
{  
     unsigned int XCx_dt;
     if (F_man > 20*MHz)
     { return 1; } //
     
     XCx_dt = 65536 - (25000000/(2*F_man)); 
     
     XC2_v_o_LE_1; 

     XC_writeReg1(XCx_dt);

      XC2_v_o_LE_0;
     return 0;
}