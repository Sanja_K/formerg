#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default_PIC32.mk)" "nbproject/Makefile-local-default_PIC32.mk"
include nbproject/Makefile-local-default_PIC32.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default_PIC32
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/FormerR934Lit10.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/FormerR934Lit10.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/config/default_PIC32/peripheral/clk/plib_clk.c ../src/config/default_PIC32/peripheral/evic/plib_evic.c ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c ../src/config/default_PIC32/stdio/xc32_monitor.c ../src/config/default_PIC32/initialization.c ../src/config/default_PIC32/interrupts.c ../src/config/default_PIC32/exceptions.c ../src/Main.c ../src/ad9914.c ../src/adl5240.c ../src/common.c ../src/eeprom.c ../src/former_signal.c ../src/periphery.c ../src/softwareSPI.c ../src/hmc1122.c ../src/ADF435x.c ../src/I2C.c ../src/channel.c ../src/synt_AD4351.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1471492474/plib_clk.o ${OBJECTDIR}/_ext/1628442791/plib_evic.o ${OBJECTDIR}/_ext/1628496619/plib_gpio.o ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o ${OBJECTDIR}/_ext/279352370/xc32_monitor.o ${OBJECTDIR}/_ext/1177533048/initialization.o ${OBJECTDIR}/_ext/1177533048/interrupts.o ${OBJECTDIR}/_ext/1177533048/exceptions.o ${OBJECTDIR}/_ext/1360937237/Main.o ${OBJECTDIR}/_ext/1360937237/ad9914.o ${OBJECTDIR}/_ext/1360937237/adl5240.o ${OBJECTDIR}/_ext/1360937237/common.o ${OBJECTDIR}/_ext/1360937237/eeprom.o ${OBJECTDIR}/_ext/1360937237/former_signal.o ${OBJECTDIR}/_ext/1360937237/periphery.o ${OBJECTDIR}/_ext/1360937237/softwareSPI.o ${OBJECTDIR}/_ext/1360937237/hmc1122.o ${OBJECTDIR}/_ext/1360937237/ADF435x.o ${OBJECTDIR}/_ext/1360937237/I2C.o ${OBJECTDIR}/_ext/1360937237/channel.o ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1471492474/plib_clk.o.d ${OBJECTDIR}/_ext/1628442791/plib_evic.o.d ${OBJECTDIR}/_ext/1628496619/plib_gpio.o.d ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o.d ${OBJECTDIR}/_ext/279352370/xc32_monitor.o.d ${OBJECTDIR}/_ext/1177533048/initialization.o.d ${OBJECTDIR}/_ext/1177533048/interrupts.o.d ${OBJECTDIR}/_ext/1177533048/exceptions.o.d ${OBJECTDIR}/_ext/1360937237/Main.o.d ${OBJECTDIR}/_ext/1360937237/ad9914.o.d ${OBJECTDIR}/_ext/1360937237/adl5240.o.d ${OBJECTDIR}/_ext/1360937237/common.o.d ${OBJECTDIR}/_ext/1360937237/eeprom.o.d ${OBJECTDIR}/_ext/1360937237/former_signal.o.d ${OBJECTDIR}/_ext/1360937237/periphery.o.d ${OBJECTDIR}/_ext/1360937237/softwareSPI.o.d ${OBJECTDIR}/_ext/1360937237/hmc1122.o.d ${OBJECTDIR}/_ext/1360937237/ADF435x.o.d ${OBJECTDIR}/_ext/1360937237/I2C.o.d ${OBJECTDIR}/_ext/1360937237/channel.o.d ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1471492474/plib_clk.o ${OBJECTDIR}/_ext/1628442791/plib_evic.o ${OBJECTDIR}/_ext/1628496619/plib_gpio.o ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o ${OBJECTDIR}/_ext/279352370/xc32_monitor.o ${OBJECTDIR}/_ext/1177533048/initialization.o ${OBJECTDIR}/_ext/1177533048/interrupts.o ${OBJECTDIR}/_ext/1177533048/exceptions.o ${OBJECTDIR}/_ext/1360937237/Main.o ${OBJECTDIR}/_ext/1360937237/ad9914.o ${OBJECTDIR}/_ext/1360937237/adl5240.o ${OBJECTDIR}/_ext/1360937237/common.o ${OBJECTDIR}/_ext/1360937237/eeprom.o ${OBJECTDIR}/_ext/1360937237/former_signal.o ${OBJECTDIR}/_ext/1360937237/periphery.o ${OBJECTDIR}/_ext/1360937237/softwareSPI.o ${OBJECTDIR}/_ext/1360937237/hmc1122.o ${OBJECTDIR}/_ext/1360937237/ADF435x.o ${OBJECTDIR}/_ext/1360937237/I2C.o ${OBJECTDIR}/_ext/1360937237/channel.o ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o

# Source Files
SOURCEFILES=../src/config/default_PIC32/peripheral/clk/plib_clk.c ../src/config/default_PIC32/peripheral/evic/plib_evic.c ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c ../src/config/default_PIC32/stdio/xc32_monitor.c ../src/config/default_PIC32/initialization.c ../src/config/default_PIC32/interrupts.c ../src/config/default_PIC32/exceptions.c ../src/Main.c ../src/ad9914.c ../src/adl5240.c ../src/common.c ../src/eeprom.c ../src/former_signal.c ../src/periphery.c ../src/softwareSPI.c ../src/hmc1122.c ../src/ADF435x.c ../src/I2C.c ../src/channel.c ../src/synt_AD4351.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default_PIC32.mk dist/${CND_CONF}/${IMAGE_TYPE}/FormerR934Lit10.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX795F512L
MP_LINKER_FILE_OPTION=,--script="..\src\config\default_PIC32\p32MX795F512L.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1471492474/plib_clk.o: ../src/config/default_PIC32/peripheral/clk/plib_clk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1471492474" 
	@${RM} ${OBJECTDIR}/_ext/1471492474/plib_clk.o.d 
	@${RM} ${OBJECTDIR}/_ext/1471492474/plib_clk.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1471492474/plib_clk.o.d" -o ${OBJECTDIR}/_ext/1471492474/plib_clk.o ../src/config/default_PIC32/peripheral/clk/plib_clk.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1471492474/plib_clk.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1628442791/plib_evic.o: ../src/config/default_PIC32/peripheral/evic/plib_evic.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1628442791" 
	@${RM} ${OBJECTDIR}/_ext/1628442791/plib_evic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1628442791/plib_evic.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1628442791/plib_evic.o.d" -o ${OBJECTDIR}/_ext/1628442791/plib_evic.o ../src/config/default_PIC32/peripheral/evic/plib_evic.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1628442791/plib_evic.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1628496619/plib_gpio.o: ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1628496619" 
	@${RM} ${OBJECTDIR}/_ext/1628496619/plib_gpio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1628496619/plib_gpio.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1628496619/plib_gpio.o.d" -o ${OBJECTDIR}/_ext/1628496619/plib_gpio.o ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1628496619/plib_gpio.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o: ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/997135251" 
	@${RM} ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o.d 
	@${RM} ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o.d" -o ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/279352370/xc32_monitor.o: ../src/config/default_PIC32/stdio/xc32_monitor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/279352370" 
	@${RM} ${OBJECTDIR}/_ext/279352370/xc32_monitor.o.d 
	@${RM} ${OBJECTDIR}/_ext/279352370/xc32_monitor.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/279352370/xc32_monitor.o.d" -o ${OBJECTDIR}/_ext/279352370/xc32_monitor.o ../src/config/default_PIC32/stdio/xc32_monitor.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/279352370/xc32_monitor.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1177533048/initialization.o: ../src/config/default_PIC32/initialization.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/initialization.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/initialization.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1177533048/initialization.o.d" -o ${OBJECTDIR}/_ext/1177533048/initialization.o ../src/config/default_PIC32/initialization.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1177533048/initialization.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1177533048/interrupts.o: ../src/config/default_PIC32/interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/interrupts.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1177533048/interrupts.o.d" -o ${OBJECTDIR}/_ext/1177533048/interrupts.o ../src/config/default_PIC32/interrupts.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1177533048/interrupts.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1177533048/exceptions.o: ../src/config/default_PIC32/exceptions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1177533048/exceptions.o.d" -o ${OBJECTDIR}/_ext/1177533048/exceptions.o ../src/config/default_PIC32/exceptions.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1177533048/exceptions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/Main.o: ../src/Main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/Main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/Main.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/Main.o.d" -o ${OBJECTDIR}/_ext/1360937237/Main.o ../src/Main.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/Main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/ad9914.o: ../src/ad9914.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad9914.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad9914.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/ad9914.o.d" -o ${OBJECTDIR}/_ext/1360937237/ad9914.o ../src/ad9914.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/ad9914.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/adl5240.o: ../src/adl5240.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/adl5240.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/adl5240.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/adl5240.o.d" -o ${OBJECTDIR}/_ext/1360937237/adl5240.o ../src/adl5240.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/adl5240.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/common.o: ../src/common.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/common.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/common.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/common.o.d" -o ${OBJECTDIR}/_ext/1360937237/common.o ../src/common.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/common.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/eeprom.o: ../src/eeprom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eeprom.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eeprom.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/eeprom.o.d" -o ${OBJECTDIR}/_ext/1360937237/eeprom.o ../src/eeprom.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/eeprom.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/former_signal.o: ../src/former_signal.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/former_signal.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/former_signal.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/former_signal.o.d" -o ${OBJECTDIR}/_ext/1360937237/former_signal.o ../src/former_signal.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/former_signal.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/periphery.o: ../src/periphery.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/periphery.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/periphery.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/periphery.o.d" -o ${OBJECTDIR}/_ext/1360937237/periphery.o ../src/periphery.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/periphery.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/softwareSPI.o: ../src/softwareSPI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/softwareSPI.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/softwareSPI.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/softwareSPI.o.d" -o ${OBJECTDIR}/_ext/1360937237/softwareSPI.o ../src/softwareSPI.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/softwareSPI.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/hmc1122.o: ../src/hmc1122.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/hmc1122.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/hmc1122.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/hmc1122.o.d" -o ${OBJECTDIR}/_ext/1360937237/hmc1122.o ../src/hmc1122.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/hmc1122.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/ADF435x.o: ../src/ADF435x.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ADF435x.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ADF435x.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/ADF435x.o.d" -o ${OBJECTDIR}/_ext/1360937237/ADF435x.o ../src/ADF435x.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/ADF435x.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/I2C.o: ../src/I2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/I2C.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/I2C.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/I2C.o.d" -o ${OBJECTDIR}/_ext/1360937237/I2C.o ../src/I2C.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/I2C.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/channel.o: ../src/channel.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/channel.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/channel.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/channel.o.d" -o ${OBJECTDIR}/_ext/1360937237/channel.o ../src/channel.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/channel.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/synt_AD4351.o: ../src/synt_AD4351.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_REAL_ICE=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/synt_AD4351.o.d" -o ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o ../src/synt_AD4351.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/synt_AD4351.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/_ext/1471492474/plib_clk.o: ../src/config/default_PIC32/peripheral/clk/plib_clk.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1471492474" 
	@${RM} ${OBJECTDIR}/_ext/1471492474/plib_clk.o.d 
	@${RM} ${OBJECTDIR}/_ext/1471492474/plib_clk.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1471492474/plib_clk.o.d" -o ${OBJECTDIR}/_ext/1471492474/plib_clk.o ../src/config/default_PIC32/peripheral/clk/plib_clk.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1471492474/plib_clk.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1628442791/plib_evic.o: ../src/config/default_PIC32/peripheral/evic/plib_evic.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1628442791" 
	@${RM} ${OBJECTDIR}/_ext/1628442791/plib_evic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1628442791/plib_evic.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1628442791/plib_evic.o.d" -o ${OBJECTDIR}/_ext/1628442791/plib_evic.o ../src/config/default_PIC32/peripheral/evic/plib_evic.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1628442791/plib_evic.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1628496619/plib_gpio.o: ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1628496619" 
	@${RM} ${OBJECTDIR}/_ext/1628496619/plib_gpio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1628496619/plib_gpio.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1628496619/plib_gpio.o.d" -o ${OBJECTDIR}/_ext/1628496619/plib_gpio.o ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1628496619/plib_gpio.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o: ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/997135251" 
	@${RM} ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o.d 
	@${RM} ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o.d" -o ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/279352370/xc32_monitor.o: ../src/config/default_PIC32/stdio/xc32_monitor.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/279352370" 
	@${RM} ${OBJECTDIR}/_ext/279352370/xc32_monitor.o.d 
	@${RM} ${OBJECTDIR}/_ext/279352370/xc32_monitor.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/279352370/xc32_monitor.o.d" -o ${OBJECTDIR}/_ext/279352370/xc32_monitor.o ../src/config/default_PIC32/stdio/xc32_monitor.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/279352370/xc32_monitor.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1177533048/initialization.o: ../src/config/default_PIC32/initialization.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/initialization.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/initialization.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1177533048/initialization.o.d" -o ${OBJECTDIR}/_ext/1177533048/initialization.o ../src/config/default_PIC32/initialization.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1177533048/initialization.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1177533048/interrupts.o: ../src/config/default_PIC32/interrupts.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/interrupts.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1177533048/interrupts.o.d" -o ${OBJECTDIR}/_ext/1177533048/interrupts.o ../src/config/default_PIC32/interrupts.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1177533048/interrupts.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1177533048/exceptions.o: ../src/config/default_PIC32/exceptions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1177533048/exceptions.o.d" -o ${OBJECTDIR}/_ext/1177533048/exceptions.o ../src/config/default_PIC32/exceptions.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1177533048/exceptions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/Main.o: ../src/Main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/Main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/Main.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/Main.o.d" -o ${OBJECTDIR}/_ext/1360937237/Main.o ../src/Main.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/Main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/ad9914.o: ../src/ad9914.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad9914.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad9914.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/ad9914.o.d" -o ${OBJECTDIR}/_ext/1360937237/ad9914.o ../src/ad9914.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/ad9914.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/adl5240.o: ../src/adl5240.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/adl5240.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/adl5240.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/adl5240.o.d" -o ${OBJECTDIR}/_ext/1360937237/adl5240.o ../src/adl5240.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/adl5240.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/common.o: ../src/common.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/common.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/common.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/common.o.d" -o ${OBJECTDIR}/_ext/1360937237/common.o ../src/common.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/common.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/eeprom.o: ../src/eeprom.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eeprom.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eeprom.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/eeprom.o.d" -o ${OBJECTDIR}/_ext/1360937237/eeprom.o ../src/eeprom.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/eeprom.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/former_signal.o: ../src/former_signal.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/former_signal.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/former_signal.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/former_signal.o.d" -o ${OBJECTDIR}/_ext/1360937237/former_signal.o ../src/former_signal.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/former_signal.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/periphery.o: ../src/periphery.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/periphery.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/periphery.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/periphery.o.d" -o ${OBJECTDIR}/_ext/1360937237/periphery.o ../src/periphery.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/periphery.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/softwareSPI.o: ../src/softwareSPI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/softwareSPI.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/softwareSPI.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/softwareSPI.o.d" -o ${OBJECTDIR}/_ext/1360937237/softwareSPI.o ../src/softwareSPI.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/softwareSPI.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/hmc1122.o: ../src/hmc1122.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/hmc1122.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/hmc1122.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/hmc1122.o.d" -o ${OBJECTDIR}/_ext/1360937237/hmc1122.o ../src/hmc1122.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/hmc1122.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/ADF435x.o: ../src/ADF435x.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ADF435x.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ADF435x.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/ADF435x.o.d" -o ${OBJECTDIR}/_ext/1360937237/ADF435x.o ../src/ADF435x.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/ADF435x.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/I2C.o: ../src/I2C.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/I2C.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/I2C.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/I2C.o.d" -o ${OBJECTDIR}/_ext/1360937237/I2C.o ../src/I2C.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/I2C.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/channel.o: ../src/channel.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/channel.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/channel.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/channel.o.d" -o ${OBJECTDIR}/_ext/1360937237/channel.o ../src/channel.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/channel.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/_ext/1360937237/synt_AD4351.o: ../src/synt_AD4351.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/synt_AD4351.o.d" -o ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o ../src/synt_AD4351.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/synt_AD4351.o.d" $(SILENT) -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/FormerR934Lit10.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../src/config/default_PIC32/p32MX795F512L.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g -mdebugger -D__MPLAB_DEBUGGER_REAL_ICE=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/FormerR934Lit10.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   -mreserve=data@0x0:0x1FC -mreserve=boot@0x1FC02000:0x1FC02FEF -mreserve=boot@0x1FC02000:0x1FC024FF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=__MPLAB_DEBUGGER_REAL_ICE=1,--defsym=_min_heap_size=512,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -mdfp="${DFP_DIR}"
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/FormerR934Lit10.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../src/config/default_PIC32/p32MX795F512L.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/FormerR934Lit10.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=512,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -mdfp="${DFP_DIR}"
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/FormerR934Lit10.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default_PIC32
	${RM} -r dist/default_PIC32

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
