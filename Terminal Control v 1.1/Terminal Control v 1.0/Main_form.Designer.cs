﻿namespace Terminal_Control_v_1._1
{
    partial class Main_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_form));
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel_err = new System.Windows.Forms.ToolStripStatusLabel();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.tb_ref_offset = new System.Windows.Forms.TextBox();
            this.bt_ref_offset = new System.Windows.Forms.Button();
            this.txt_SWRF_num = new System.Windows.Forms.TextBox();
            this.cb_ks_devices = new System.Windows.Forms.ComboBox();
            this.chBaAvrg = new System.Windows.Forms.CheckBox();
            this.txt_SW_num = new System.Windows.Forms.TextBox();
            this.bt_clear_richtext = new System.Windows.Forms.Button();
            this.bttn_setSpan = new System.Windows.Forms.Button();
            this.bttn_refAmp_KS = new System.Windows.Forms.Button();
            this.bttn_Get_data = new System.Windows.Forms.Button();
            this.bt_preset = new System.Windows.Forms.Button();
            this.bttn_seat_peak_KS = new System.Windows.Forms.Button();
            this.bttn_Set_param_KS = new System.Windows.Forms.Button();
            this.bt_saveEEParams = new System.Windows.Forms.Button();
            this.getParams = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.bt_delParams = new System.Windows.Forms.Button();
            this.bt_setParams = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.label47 = new System.Windows.Forms.Label();
            this.cb_literaCH_fps3 = new System.Windows.Forms.ComboBox();
            this.label44 = new System.Windows.Forms.Label();
            this.listBoxErr_fps3 = new System.Windows.Forms.ListBox();
            this.label26 = new System.Windows.Forms.Label();
            this.bt_set_switch = new System.Windows.Forms.Button();
            this.bt_test_rf = new System.Windows.Forms.Button();
            this.label31 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.label49 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.bt_get_status_fps3 = new System.Windows.Forms.Button();
            this.checkBox_GPS_L2_fps3 = new System.Windows.Forms.CheckBox();
            this.checkBox_GPS_L1_fps3 = new System.Windows.Forms.CheckBox();
            this.checkBox_GL_L2_fps3 = new System.Windows.Forms.CheckBox();
            this.checkBox_GL_L1_fps3 = new System.Windows.Forms.CheckBox();
            this.label37 = new System.Windows.Forms.Label();
            this.lb_txrefAmp = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.tb_dev1_fps3 = new System.Windows.Forms.TextBox();
            this.tb_tscan_fps3 = new System.Windows.Forms.TextBox();
            this.cb_man1_fps3 = new System.Windows.Forms.ComboBox();
            this.cb_mod1_fps3 = new System.Windows.Forms.ComboBox();
            this.cb_tim1_fps3 = new System.Windows.Forms.ComboBox();
            this.label40 = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.tbSpanFreq = new System.Windows.Forms.TextBox();
            this.tb_ref_amp = new System.Windows.Forms.TextBox();
            this.tb_freq2_fps3 = new System.Windows.Forms.TextBox();
            this.tb_shag_scan = new System.Windows.Forms.TextBox();
            this.tb_stop_scan = new System.Windows.Forms.TextBox();
            this.tb_start_scan = new System.Windows.Forms.TextBox();
            this.tb_freq1_fps3 = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.bttn_close = new System.Windows.Forms.Button();
            this.butConnect = new System.Windows.Forms.Button();
            this.bt_shag = new System.Windows.Forms.Button();
            this.bt_stop_scan = new System.Windows.Forms.Button();
            this.bt_start_scan = new System.Windows.Forms.Button();
            this.bttn_set_param_fps = new System.Windows.Forms.Button();
            this.bt_set_param_FPS3 = new System.Windows.Forms.Button();
            this.bt_power_off_All_FPS3 = new System.Windows.Forms.Button();
            this.bt_power_off_FPS3 = new System.Windows.Forms.Button();
            this.label46 = new System.Windows.Forms.Label();
            this.cb_litera_fps3 = new System.Windows.Forms.ComboBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.checkBox_color = new System.Windows.Forms.CheckBox();
            this.textBox_buf_delay = new System.Windows.Forms.TextBox();
            this.checkBox_visibleTxRx = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button_buffer_delay = new System.Windows.Forms.Button();
            this.button_update_ports = new System.Windows.Forms.Button();
            this.comboBox_portname = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.button_Close = new System.Windows.Forms.Button();
            this.button_Open = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBox_baudrate = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.comboBox_parity = new System.Windows.Forms.ComboBox();
            this.comboBox_stop_bits = new System.Windows.Forms.ComboBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.bt_GetAmp_fps3 = new System.Windows.Forms.Button();
            this.bt_DDS_clr_fps3 = new System.Windows.Forms.Button();
            this.cb_literaK_fps3 = new System.Windows.Forms.ComboBox();
            this.txtSWRF2 = new System.Windows.Forms.TextBox();
            this.cb_ch_sw_fps3 = new System.Windows.Forms.ComboBox();
            this.label29 = new System.Windows.Forms.Label();
            this.bt_Get_Ver_FPS2 = new System.Windows.Forms.Button();
            this.checkBoxService = new System.Windows.Forms.CheckBox();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.richTextBoxOut = new System.Windows.Forms.RichTextBox();
            this.button_terminal = new System.Windows.Forms.Button();
            this.button_terminal_clear = new System.Windows.Forms.Button();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.openFileDialogHex = new System.Windows.Forms.OpenFileDialog();
            this.timerWaitStOK = new System.Windows.Forms.Timer(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.toolStripStatusLabel_err});
            this.statusStrip1.Location = new System.Drawing.Point(0, 716);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1243, 22);
            this.statusStrip1.TabIndex = 14;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(103, 17);
            this.toolStripStatusLabel1.Text = "Port not initialized";
            // 
            // toolStripStatusLabel_err
            // 
            this.toolStripStatusLabel_err.Name = "toolStripStatusLabel_err";
            this.toolStripStatusLabel_err.Size = new System.Drawing.Size(0, 17);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 72);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1243, 451);
            this.tabControl1.TabIndex = 50;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.tb_ref_offset);
            this.tabPage5.Controls.Add(this.bt_ref_offset);
            this.tabPage5.Controls.Add(this.txt_SWRF_num);
            this.tabPage5.Controls.Add(this.cb_ks_devices);
            this.tabPage5.Controls.Add(this.chBaAvrg);
            this.tabPage5.Controls.Add(this.txt_SW_num);
            this.tabPage5.Controls.Add(this.bt_clear_richtext);
            this.tabPage5.Controls.Add(this.bttn_setSpan);
            this.tabPage5.Controls.Add(this.bttn_refAmp_KS);
            this.tabPage5.Controls.Add(this.bttn_Get_data);
            this.tabPage5.Controls.Add(this.bt_preset);
            this.tabPage5.Controls.Add(this.bttn_seat_peak_KS);
            this.tabPage5.Controls.Add(this.bttn_Set_param_KS);
            this.tabPage5.Controls.Add(this.bt_saveEEParams);
            this.tabPage5.Controls.Add(this.getParams);
            this.tabPage5.Controls.Add(this.button7);
            this.tabPage5.Controls.Add(this.button6);
            this.tabPage5.Controls.Add(this.button5);
            this.tabPage5.Controls.Add(this.bt_delParams);
            this.tabPage5.Controls.Add(this.bt_setParams);
            this.tabPage5.Controls.Add(this.dataGridView1);
            this.tabPage5.Controls.Add(this.label47);
            this.tabPage5.Controls.Add(this.cb_literaCH_fps3);
            this.tabPage5.Controls.Add(this.label44);
            this.tabPage5.Controls.Add(this.listBoxErr_fps3);
            this.tabPage5.Controls.Add(this.label26);
            this.tabPage5.Controls.Add(this.bt_set_switch);
            this.tabPage5.Controls.Add(this.bt_test_rf);
            this.tabPage5.Controls.Add(this.label31);
            this.tabPage5.Controls.Add(this.label51);
            this.tabPage5.Controls.Add(this.label50);
            this.tabPage5.Controls.Add(this.label49);
            this.tabPage5.Controls.Add(this.label2);
            this.tabPage5.Controls.Add(this.label1);
            this.tabPage5.Controls.Add(this.label4);
            this.tabPage5.Controls.Add(this.label3);
            this.tabPage5.Controls.Add(this.label48);
            this.tabPage5.Controls.Add(this.bt_get_status_fps3);
            this.tabPage5.Controls.Add(this.checkBox_GPS_L2_fps3);
            this.tabPage5.Controls.Add(this.checkBox_GPS_L1_fps3);
            this.tabPage5.Controls.Add(this.checkBox_GL_L2_fps3);
            this.tabPage5.Controls.Add(this.checkBox_GL_L1_fps3);
            this.tabPage5.Controls.Add(this.label37);
            this.tabPage5.Controls.Add(this.lb_txrefAmp);
            this.tabPage5.Controls.Add(this.label38);
            this.tabPage5.Controls.Add(this.label39);
            this.tabPage5.Controls.Add(this.tb_dev1_fps3);
            this.tabPage5.Controls.Add(this.tb_tscan_fps3);
            this.tabPage5.Controls.Add(this.cb_man1_fps3);
            this.tabPage5.Controls.Add(this.cb_mod1_fps3);
            this.tabPage5.Controls.Add(this.cb_tim1_fps3);
            this.tabPage5.Controls.Add(this.label40);
            this.tabPage5.Controls.Add(this.label41);
            this.tabPage5.Controls.Add(this.label42);
            this.tabPage5.Controls.Add(this.label43);
            this.tabPage5.Controls.Add(this.tbSpanFreq);
            this.tabPage5.Controls.Add(this.tb_ref_amp);
            this.tabPage5.Controls.Add(this.tb_freq2_fps3);
            this.tabPage5.Controls.Add(this.tb_shag_scan);
            this.tabPage5.Controls.Add(this.tb_stop_scan);
            this.tabPage5.Controls.Add(this.tb_start_scan);
            this.tabPage5.Controls.Add(this.tb_freq1_fps3);
            this.tabPage5.Controls.Add(this.label45);
            this.tabPage5.Controls.Add(this.bttn_close);
            this.tabPage5.Controls.Add(this.butConnect);
            this.tabPage5.Controls.Add(this.bt_shag);
            this.tabPage5.Controls.Add(this.bt_stop_scan);
            this.tabPage5.Controls.Add(this.bt_start_scan);
            this.tabPage5.Controls.Add(this.bttn_set_param_fps);
            this.tabPage5.Controls.Add(this.bt_set_param_FPS3);
            this.tabPage5.Controls.Add(this.bt_power_off_All_FPS3);
            this.tabPage5.Controls.Add(this.bt_power_off_FPS3);
            this.tabPage5.Controls.Add(this.label46);
            this.tabPage5.Controls.Add(this.cb_litera_fps3);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(1235, 425);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Управление ФПС";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // tb_ref_offset
            // 
            this.tb_ref_offset.Location = new System.Drawing.Point(559, 349);
            this.tb_ref_offset.Name = "tb_ref_offset";
            this.tb_ref_offset.Size = new System.Drawing.Size(78, 20);
            this.tb_ref_offset.TabIndex = 147;
            this.tb_ref_offset.Text = "5";
            // 
            // bt_ref_offset
            // 
            this.bt_ref_offset.Enabled = false;
            this.bt_ref_offset.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bt_ref_offset.Location = new System.Drawing.Point(559, 389);
            this.bt_ref_offset.Name = "bt_ref_offset";
            this.bt_ref_offset.Size = new System.Drawing.Size(78, 23);
            this.bt_ref_offset.TabIndex = 146;
            this.bt_ref_offset.Text = "RefOffset";
            this.bt_ref_offset.UseVisualStyleBackColor = true;
            this.bt_ref_offset.Click += new System.EventHandler(this.bt_ref_offset_Click);
            // 
            // txt_SWRF_num
            // 
            this.txt_SWRF_num.Location = new System.Drawing.Point(578, 262);
            this.txt_SWRF_num.Name = "txt_SWRF_num";
            this.txt_SWRF_num.Size = new System.Drawing.Size(65, 20);
            this.txt_SWRF_num.TabIndex = 117;
            this.txt_SWRF_num.Text = "1";
            // 
            // cb_ks_devices
            // 
            this.cb_ks_devices.FormattingEnabled = true;
            this.cb_ks_devices.Items.AddRange(new object[] {
            "USBN9344",
            "ETHN9030B"});
            this.cb_ks_devices.Location = new System.Drawing.Point(19, 250);
            this.cb_ks_devices.Name = "cb_ks_devices";
            this.cb_ks_devices.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_ks_devices.Size = new System.Drawing.Size(98, 21);
            this.cb_ks_devices.TabIndex = 145;
            this.cb_ks_devices.Text = "ETHN9030B";
            this.cb_ks_devices.SelectedIndexChanged += new System.EventHandler(this.cb_ks_devices_SelectedIndexChanged);
            // 
            // chBaAvrg
            // 
            this.chBaAvrg.AutoSize = true;
            this.chBaAvrg.Location = new System.Drawing.Point(377, 283);
            this.chBaAvrg.Name = "chBaAvrg";
            this.chBaAvrg.Size = new System.Drawing.Size(48, 17);
            this.chBaAvrg.TabIndex = 144;
            this.chBaAvrg.Text = "Avrg";
            this.chBaAvrg.UseVisualStyleBackColor = true;
            // 
            // txt_SW_num
            // 
            this.txt_SW_num.Location = new System.Drawing.Point(579, 232);
            this.txt_SW_num.Name = "txt_SW_num";
            this.txt_SW_num.Size = new System.Drawing.Size(65, 20);
            this.txt_SW_num.TabIndex = 117;
            this.txt_SW_num.Text = "0";
            // 
            // bt_clear_richtext
            // 
            this.bt_clear_richtext.Location = new System.Drawing.Point(109, 378);
            this.bt_clear_richtext.Name = "bt_clear_richtext";
            this.bt_clear_richtext.Size = new System.Drawing.Size(68, 21);
            this.bt_clear_richtext.TabIndex = 12;
            this.bt_clear_richtext.Text = "Очистить";
            this.bt_clear_richtext.UseVisualStyleBackColor = true;
            this.bt_clear_richtext.Click += new System.EventHandler(this.Bt_clear_richtext_Click);
            // 
            // bttn_setSpan
            // 
            this.bttn_setSpan.Enabled = false;
            this.bttn_setSpan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bttn_setSpan.Location = new System.Drawing.Point(470, 348);
            this.bttn_setSpan.Name = "bttn_setSpan";
            this.bttn_setSpan.Size = new System.Drawing.Size(78, 23);
            this.bttn_setSpan.TabIndex = 143;
            this.bttn_setSpan.Text = "setSpan";
            this.bttn_setSpan.UseVisualStyleBackColor = true;
            this.bttn_setSpan.Click += new System.EventHandler(this.Bttn_setSpan_Click);
            // 
            // bttn_refAmp_KS
            // 
            this.bttn_refAmp_KS.Enabled = false;
            this.bttn_refAmp_KS.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bttn_refAmp_KS.Location = new System.Drawing.Point(470, 284);
            this.bttn_refAmp_KS.Name = "bttn_refAmp_KS";
            this.bttn_refAmp_KS.Size = new System.Drawing.Size(78, 23);
            this.bttn_refAmp_KS.TabIndex = 143;
            this.bttn_refAmp_KS.Text = "refAmp";
            this.bttn_refAmp_KS.UseVisualStyleBackColor = true;
            this.bttn_refAmp_KS.Click += new System.EventHandler(this.Bttn_refAmp_KS_Click);
            // 
            // bttn_Get_data
            // 
            this.bttn_Get_data.Enabled = false;
            this.bttn_Get_data.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bttn_Get_data.Location = new System.Drawing.Point(19, 378);
            this.bttn_Get_data.Name = "bttn_Get_data";
            this.bttn_Get_data.Size = new System.Drawing.Size(78, 23);
            this.bttn_Get_data.TabIndex = 143;
            this.bttn_Get_data.Text = "Get data";
            this.bttn_Get_data.UseVisualStyleBackColor = true;
            this.bttn_Get_data.Click += new System.EventHandler(this.Bttn_seat_peak_KS_Click);
            // 
            // bt_preset
            // 
            this.bt_preset.Enabled = false;
            this.bt_preset.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bt_preset.Location = new System.Drawing.Point(471, 389);
            this.bt_preset.Name = "bt_preset";
            this.bt_preset.Size = new System.Drawing.Size(78, 23);
            this.bt_preset.TabIndex = 143;
            this.bt_preset.Text = "SysPreset";
            this.bt_preset.UseVisualStyleBackColor = true;
            this.bt_preset.Click += new System.EventHandler(this.bt_preset_Click);
            // 
            // bttn_seat_peak_KS
            // 
            this.bttn_seat_peak_KS.Enabled = false;
            this.bttn_seat_peak_KS.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bttn_seat_peak_KS.Location = new System.Drawing.Point(271, 283);
            this.bttn_seat_peak_KS.Name = "bttn_seat_peak_KS";
            this.bttn_seat_peak_KS.Size = new System.Drawing.Size(78, 23);
            this.bttn_seat_peak_KS.TabIndex = 143;
            this.bttn_seat_peak_KS.Text = "Set Peak";
            this.bttn_seat_peak_KS.UseVisualStyleBackColor = true;
            this.bttn_seat_peak_KS.Click += new System.EventHandler(this.Bttn_seat_peak_KS_Click);
            // 
            // bttn_Set_param_KS
            // 
            this.bttn_Set_param_KS.Enabled = false;
            this.bttn_Set_param_KS.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bttn_Set_param_KS.Location = new System.Drawing.Point(19, 349);
            this.bttn_Set_param_KS.Name = "bttn_Set_param_KS";
            this.bttn_Set_param_KS.Size = new System.Drawing.Size(78, 23);
            this.bttn_Set_param_KS.TabIndex = 143;
            this.bttn_Set_param_KS.Text = "Set KS";
            this.bttn_Set_param_KS.UseVisualStyleBackColor = true;
            this.bttn_Set_param_KS.Click += new System.EventHandler(this.Bttn_Set_KS_Click);
            // 
            // bt_saveEEParams
            // 
            this.bt_saveEEParams.Enabled = false;
            this.bt_saveEEParams.ForeColor = System.Drawing.Color.Fuchsia;
            this.bt_saveEEParams.Location = new System.Drawing.Point(699, 351);
            this.bt_saveEEParams.Name = "bt_saveEEParams";
            this.bt_saveEEParams.Size = new System.Drawing.Size(83, 35);
            this.bt_saveEEParams.TabIndex = 142;
            this.bt_saveEEParams.Text = "Save param EEPROM";
            this.bt_saveEEParams.UseVisualStyleBackColor = true;
            this.bt_saveEEParams.Click += new System.EventHandler(this.saveParamAtBooster_Click);
            // 
            // getParams
            // 
            this.getParams.Enabled = false;
            this.getParams.ForeColor = System.Drawing.Color.Fuchsia;
            this.getParams.Location = new System.Drawing.Point(699, 194);
            this.getParams.Name = "getParams";
            this.getParams.Size = new System.Drawing.Size(83, 35);
            this.getParams.TabIndex = 141;
            this.getParams.Text = "Get Params";
            this.getParams.UseVisualStyleBackColor = true;
            this.getParams.Click += new System.EventHandler(this.getParams_Click);
            // 
            // button7
            // 
            this.button7.Enabled = false;
            this.button7.ForeColor = System.Drawing.Color.Fuchsia;
            this.button7.Location = new System.Drawing.Point(898, 15);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(83, 35);
            this.button7.TabIndex = 140;
            this.button7.Text = "Clean up table";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.CleanUpButton_Click);
            // 
            // button6
            // 
            this.button6.Enabled = false;
            this.button6.ForeColor = System.Drawing.Color.Fuchsia;
            this.button6.Location = new System.Drawing.Point(1004, 15);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(83, 35);
            this.button6.TabIndex = 139;
            this.button6.Text = "Load table from";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.LoadButton_Click);
            // 
            // button5
            // 
            this.button5.Enabled = false;
            this.button5.ForeColor = System.Drawing.Color.Fuchsia;
            this.button5.Location = new System.Drawing.Point(1116, 15);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(83, 35);
            this.button5.TabIndex = 138;
            this.button5.Text = "Save table to";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.SaveButton_Click);
            // 
            // bt_delParams
            // 
            this.bt_delParams.Enabled = false;
            this.bt_delParams.ForeColor = System.Drawing.Color.Fuchsia;
            this.bt_delParams.Location = new System.Drawing.Point(699, 304);
            this.bt_delParams.Name = "bt_delParams";
            this.bt_delParams.Size = new System.Drawing.Size(83, 35);
            this.bt_delParams.TabIndex = 137;
            this.bt_delParams.Text = "Remove Params";
            this.bt_delParams.UseVisualStyleBackColor = true;
            this.bt_delParams.Click += new System.EventHandler(this.button_Remove_Params);
            // 
            // bt_setParams
            // 
            this.bt_setParams.Enabled = false;
            this.bt_setParams.ForeColor = System.Drawing.Color.Fuchsia;
            this.bt_setParams.Location = new System.Drawing.Point(699, 247);
            this.bt_setParams.Name = "bt_setParams";
            this.bt_setParams.Size = new System.Drawing.Size(83, 35);
            this.bt_setParams.TabIndex = 136;
            this.bt_setParams.Text = "Set Params";
            this.bt_setParams.UseVisualStyleBackColor = true;
            this.bt_setParams.Click += new System.EventHandler(this.button_Write_Params);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(791, 56);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(426, 366);
            this.dataGridView1.TabIndex = 135;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(382, 57);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(59, 13);
            this.label47.TabIndex = 134;
            this.label47.Text = "Номер CH";
            // 
            // cb_literaCH_fps3
            // 
            this.cb_literaCH_fps3.FormattingEnabled = true;
            this.cb_literaCH_fps3.Items.AddRange(new object[] {
            "1 | 100-500/ 400-440",
            "2 | 500-2500 /5725-5875",
            "3 | 2500-6000 /880-1370",
            "4 | GNSS / 2280-2600",
            "5 | All /GNSS"});
            this.cb_literaCH_fps3.Location = new System.Drawing.Point(385, 73);
            this.cb_literaCH_fps3.Name = "cb_literaCH_fps3";
            this.cb_literaCH_fps3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_literaCH_fps3.Size = new System.Drawing.Size(160, 21);
            this.cb_literaCH_fps3.TabIndex = 133;
            this.cb_literaCH_fps3.Text = "1 | 100-500";
            this.cb_literaCH_fps3.SelectedIndexChanged += new System.EventHandler(this.Cb_literaCH_fps3_SelectedIndexChanged);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.ForeColor = System.Drawing.Color.Black;
            this.label44.Location = new System.Drawing.Point(214, 57);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(52, 13);
            this.label44.TabIndex = 132;
            this.label44.Text = "Частота ";
            // 
            // listBoxErr_fps3
            // 
            this.listBoxErr_fps3.FormattingEnabled = true;
            this.listBoxErr_fps3.Location = new System.Drawing.Point(578, 20);
            this.listBoxErr_fps3.Name = "listBoxErr_fps3";
            this.listBoxErr_fps3.ScrollAlwaysVisible = true;
            this.listBoxErr_fps3.Size = new System.Drawing.Size(179, 134);
            this.listBoxErr_fps3.TabIndex = 131;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.Color.Black;
            this.label26.Location = new System.Drawing.Point(575, 4);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(44, 13);
            this.label26.TabIndex = 130;
            this.label26.Text = "Статус:";
            // 
            // bt_set_switch
            // 
            this.bt_set_switch.Enabled = false;
            this.bt_set_switch.ForeColor = System.Drawing.Color.Fuchsia;
            this.bt_set_switch.Location = new System.Drawing.Point(578, 200);
            this.bt_set_switch.Name = "bt_set_switch";
            this.bt_set_switch.Size = new System.Drawing.Size(66, 23);
            this.bt_set_switch.TabIndex = 122;
            this.bt_set_switch.Text = "testSW";
            this.bt_set_switch.UseVisualStyleBackColor = true;
            this.bt_set_switch.Click += new System.EventHandler(this.bt_set_switch_Click);
            // 
            // bt_test_rf
            // 
            this.bt_test_rf.Enabled = false;
            this.bt_test_rf.ForeColor = System.Drawing.Color.Fuchsia;
            this.bt_test_rf.Location = new System.Drawing.Point(578, 167);
            this.bt_test_rf.Name = "bt_test_rf";
            this.bt_test_rf.Size = new System.Drawing.Size(66, 23);
            this.bt_test_rf.TabIndex = 122;
            this.bt_test_rf.Text = "testGNSS";
            this.bt_test_rf.UseVisualStyleBackColor = true;
            this.bt_test_rf.Click += new System.EventHandler(this.Bt_test_rf_Click);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.ForeColor = System.Drawing.Color.Black;
            this.label31.Location = new System.Drawing.Point(554, 322);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(25, 13);
            this.label31.TabIndex = 118;
            this.label31.Text = "кГц";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.ForeColor = System.Drawing.Color.Black;
            this.label51.Location = new System.Drawing.Point(199, 250);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(57, 13);
            this.label51.TabIndex = 118;
            this.label51.Text = "Шаг [МГц]";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.ForeColor = System.Drawing.Color.Black;
            this.label50.Location = new System.Drawing.Point(199, 353);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(61, 13);
            this.label50.TabIndex = 118;
            this.label50.Text = "Стоп [МГц]";
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.ForeColor = System.Drawing.Color.Black;
            this.label49.Location = new System.Drawing.Point(199, 325);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(66, 13);
            this.label49.TabIndex = 118;
            this.label49.Text = "Старт [МГц]";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(16, 234);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 118;
            this.label2.Text = "KSDevices";
            this.label2.Click += new System.EventHandler(this.Label48_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(609, 333);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(28, 13);
            this.label1.TabIndex = 118;
            this.label1.Text = "дБм";
            this.label1.Click += new System.EventHandler(this.Label48_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(646, 265);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(21, 13);
            this.label4.TabIndex = 118;
            this.label4.Text = "RF";
            this.label4.Click += new System.EventHandler(this.Label48_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(646, 234);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 13);
            this.label3.TabIndex = 118;
            this.label3.Text = "SW";
            this.label3.Click += new System.EventHandler(this.Label48_Click);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.ForeColor = System.Drawing.Color.Black;
            this.label48.Location = new System.Drawing.Point(521, 239);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(28, 13);
            this.label48.TabIndex = 118;
            this.label48.Text = "дБм";
            this.label48.Click += new System.EventHandler(this.Label48_Click);
            // 
            // bt_get_status_fps3
            // 
            this.bt_get_status_fps3.Enabled = false;
            this.bt_get_status_fps3.ForeColor = System.Drawing.Color.Brown;
            this.bt_get_status_fps3.Location = new System.Drawing.Point(479, 21);
            this.bt_get_status_fps3.Name = "bt_get_status_fps3";
            this.bt_get_status_fps3.Size = new System.Drawing.Size(67, 23);
            this.bt_get_status_fps3.TabIndex = 111;
            this.bt_get_status_fps3.Text = "Status";
            this.bt_get_status_fps3.UseVisualStyleBackColor = true;
            this.bt_get_status_fps3.Click += new System.EventHandler(this.bt_get_status_fps3_Click);
            // 
            // checkBox_GPS_L2_fps3
            // 
            this.checkBox_GPS_L2_fps3.AutoSize = true;
            this.checkBox_GPS_L2_fps3.Enabled = false;
            this.checkBox_GPS_L2_fps3.Location = new System.Drawing.Point(479, 169);
            this.checkBox_GPS_L2_fps3.Name = "checkBox_GPS_L2_fps3";
            this.checkBox_GPS_L2_fps3.Size = new System.Drawing.Size(38, 17);
            this.checkBox_GPS_L2_fps3.TabIndex = 108;
            this.checkBox_GPS_L2_fps3.Text = "L2";
            this.checkBox_GPS_L2_fps3.UseVisualStyleBackColor = true;
            // 
            // checkBox_GPS_L1_fps3
            // 
            this.checkBox_GPS_L1_fps3.AutoSize = true;
            this.checkBox_GPS_L1_fps3.Enabled = false;
            this.checkBox_GPS_L1_fps3.Location = new System.Drawing.Point(404, 170);
            this.checkBox_GPS_L1_fps3.Name = "checkBox_GPS_L1_fps3";
            this.checkBox_GPS_L1_fps3.Size = new System.Drawing.Size(38, 17);
            this.checkBox_GPS_L1_fps3.TabIndex = 107;
            this.checkBox_GPS_L1_fps3.Text = "L1";
            this.checkBox_GPS_L1_fps3.UseVisualStyleBackColor = true;
            // 
            // checkBox_GL_L2_fps3
            // 
            this.checkBox_GL_L2_fps3.AutoSize = true;
            this.checkBox_GL_L2_fps3.Enabled = false;
            this.checkBox_GL_L2_fps3.Location = new System.Drawing.Point(479, 220);
            this.checkBox_GL_L2_fps3.Name = "checkBox_GL_L2_fps3";
            this.checkBox_GL_L2_fps3.Size = new System.Drawing.Size(38, 17);
            this.checkBox_GL_L2_fps3.TabIndex = 106;
            this.checkBox_GL_L2_fps3.Text = "L2";
            this.checkBox_GL_L2_fps3.UseVisualStyleBackColor = true;
            // 
            // checkBox_GL_L1_fps3
            // 
            this.checkBox_GL_L1_fps3.AutoSize = true;
            this.checkBox_GL_L1_fps3.Enabled = false;
            this.checkBox_GL_L1_fps3.Location = new System.Drawing.Point(403, 220);
            this.checkBox_GL_L1_fps3.Name = "checkBox_GL_L1_fps3";
            this.checkBox_GL_L1_fps3.Size = new System.Drawing.Size(38, 17);
            this.checkBox_GL_L1_fps3.TabIndex = 105;
            this.checkBox_GL_L1_fps3.Text = "L1";
            this.checkBox_GL_L1_fps3.UseVisualStyleBackColor = true;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.ForeColor = System.Drawing.Color.Black;
            this.label37.Location = new System.Drawing.Point(61, 173);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(145, 13);
            this.label37.TabIndex = 104;
            this.label37.Text = "Время сканирования [кГц]:";
            // 
            // lb_txrefAmp
            // 
            this.lb_txrefAmp.AutoSize = true;
            this.lb_txrefAmp.ForeColor = System.Drawing.Color.Black;
            this.lb_txrefAmp.Location = new System.Drawing.Point(554, 289);
            this.lb_txrefAmp.Name = "lb_txrefAmp";
            this.lb_txrefAmp.Size = new System.Drawing.Size(13, 13);
            this.lb_txrefAmp.TabIndex = 103;
            this.lb_txrefAmp.Text = "5";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.ForeColor = System.Drawing.Color.Black;
            this.label38.Location = new System.Drawing.Point(382, 141);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(135, 13);
            this.label38.TabIndex = 103;
            this.label38.Text = "Система навигации GPS:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.ForeColor = System.Drawing.Color.Black;
            this.label39.Location = new System.Drawing.Point(374, 194);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(164, 13);
            this.label39.TabIndex = 102;
            this.label39.Text = "Система навигации ГЛОНАСС:";
            // 
            // tb_dev1_fps3
            // 
            this.tb_dev1_fps3.Location = new System.Drawing.Point(217, 122);
            this.tb_dev1_fps3.Name = "tb_dev1_fps3";
            this.tb_dev1_fps3.Size = new System.Drawing.Size(65, 20);
            this.tb_dev1_fps3.TabIndex = 101;
            this.tb_dev1_fps3.Text = "----";
            // 
            // tb_tscan_fps3
            // 
            this.tb_tscan_fps3.Location = new System.Drawing.Point(217, 170);
            this.tb_tscan_fps3.Name = "tb_tscan_fps3";
            this.tb_tscan_fps3.Size = new System.Drawing.Size(65, 20);
            this.tb_tscan_fps3.TabIndex = 100;
            this.tb_tscan_fps3.Text = "----";
            // 
            // cb_man1_fps3
            // 
            this.cb_man1_fps3.FormattingEnabled = true;
            this.cb_man1_fps3.Items.AddRange(new object[] {
            "----",
            "0,08",
            "0,1",
            "0,133",
            "0,2",
            "0,5",
            "1,0",
            "5,0",
            "10,0",
            "20,0",
            "40,0",
            "80,0",
            "400,0"});
            this.cb_man1_fps3.Location = new System.Drawing.Point(217, 145);
            this.cb_man1_fps3.Name = "cb_man1_fps3";
            this.cb_man1_fps3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_man1_fps3.Size = new System.Drawing.Size(65, 21);
            this.cb_man1_fps3.TabIndex = 98;
            this.cb_man1_fps3.Text = "----";
            // 
            // cb_mod1_fps3
            // 
            this.cb_mod1_fps3.FormattingEnabled = true;
            this.cb_mod1_fps3.Items.AddRange(new object[] {
            "----",
            "ФМ-2",
            "ФМ-4",
            "ФМ-8",
            "ЛЧМ",
            "ЛЧМ-2"});
            this.cb_mod1_fps3.Location = new System.Drawing.Point(217, 97);
            this.cb_mod1_fps3.Name = "cb_mod1_fps3";
            this.cb_mod1_fps3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_mod1_fps3.Size = new System.Drawing.Size(65, 21);
            this.cb_mod1_fps3.TabIndex = 94;
            this.cb_mod1_fps3.Text = "----";
            this.cb_mod1_fps3.SelectedIndexChanged += new System.EventHandler(this.cb_mod1_fps3_SelectedIndexChanged);
            // 
            // cb_tim1_fps3
            // 
            this.cb_tim1_fps3.FormattingEnabled = true;
            this.cb_tim1_fps3.Items.AddRange(new object[] {
            "Непр.",
            "0,5 мс",
            "1,0 мс",
            "1,5 мс",
            "2,0 мс",
            "2,5 мс",
            "3,0 мс",
            "3,5 мс",
            "4,0 мс",
            "4,5 мс",
            "5,0 мс",
            "5,5 мс",
            "6,0 мс",
            "6,5 мс",
            "7,0 мс",
            "7,5 мс",
            "8,0 мс"});
            this.cb_tim1_fps3.Location = new System.Drawing.Point(217, 194);
            this.cb_tim1_fps3.Name = "cb_tim1_fps3";
            this.cb_tim1_fps3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_tim1_fps3.Size = new System.Drawing.Size(65, 21);
            this.cb_tim1_fps3.TabIndex = 93;
            this.cb_tim1_fps3.Text = "Непр.";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.ForeColor = System.Drawing.Color.Black;
            this.label40.Location = new System.Drawing.Point(99, 197);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(106, 13);
            this.label40.TabIndex = 91;
            this.label40.Text = "Длительность [мс]:";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.ForeColor = System.Drawing.Color.Black;
            this.label41.Location = new System.Drawing.Point(98, 148);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(107, 13);
            this.label41.TabIndex = 90;
            this.label41.Text = "Манипуляция [мкс]:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.ForeColor = System.Drawing.Color.Black;
            this.label42.Location = new System.Drawing.Point(116, 125);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(88, 13);
            this.label42.TabIndex = 89;
            this.label42.Text = "Девиация [кГц]:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.ForeColor = System.Drawing.Color.Black;
            this.label43.Location = new System.Drawing.Point(125, 76);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(79, 13);
            this.label43.TabIndex = 88;
            this.label43.Text = "Частота [кГц]:";
            // 
            // tbSpanFreq
            // 
            this.tbSpanFreq.Location = new System.Drawing.Point(471, 319);
            this.tbSpanFreq.Name = "tbSpanFreq";
            this.tbSpanFreq.Size = new System.Drawing.Size(77, 20);
            this.tbSpanFreq.TabIndex = 85;
            this.tbSpanFreq.Text = "100000";
            // 
            // tb_ref_amp
            // 
            this.tb_ref_amp.Location = new System.Drawing.Point(470, 255);
            this.tb_ref_amp.Name = "tb_ref_amp";
            this.tb_ref_amp.Size = new System.Drawing.Size(78, 20);
            this.tb_ref_amp.TabIndex = 85;
            this.tb_ref_amp.Text = "5";
            // 
            // tb_freq2_fps3
            // 
            this.tb_freq2_fps3.Enabled = false;
            this.tb_freq2_fps3.Location = new System.Drawing.Point(290, 73);
            this.tb_freq2_fps3.Name = "tb_freq2_fps3";
            this.tb_freq2_fps3.Size = new System.Drawing.Size(65, 20);
            this.tb_freq2_fps3.TabIndex = 84;
            this.tb_freq2_fps3.Text = "150000";
            // 
            // tb_shag_scan
            // 
            this.tb_shag_scan.Location = new System.Drawing.Point(271, 247);
            this.tb_shag_scan.Name = "tb_shag_scan";
            this.tb_shag_scan.Size = new System.Drawing.Size(65, 20);
            this.tb_shag_scan.TabIndex = 85;
            this.tb_shag_scan.Text = "100";
            // 
            // tb_stop_scan
            // 
            this.tb_stop_scan.Location = new System.Drawing.Point(271, 351);
            this.tb_stop_scan.Name = "tb_stop_scan";
            this.tb_stop_scan.Size = new System.Drawing.Size(65, 20);
            this.tb_stop_scan.TabIndex = 85;
            this.tb_stop_scan.Text = "2500";
            // 
            // tb_start_scan
            // 
            this.tb_start_scan.Location = new System.Drawing.Point(271, 325);
            this.tb_start_scan.Name = "tb_start_scan";
            this.tb_start_scan.Size = new System.Drawing.Size(65, 20);
            this.tb_start_scan.TabIndex = 85;
            this.tb_start_scan.Text = "500";
            // 
            // tb_freq1_fps3
            // 
            this.tb_freq1_fps3.Location = new System.Drawing.Point(217, 73);
            this.tb_freq1_fps3.Name = "tb_freq1_fps3";
            this.tb_freq1_fps3.Size = new System.Drawing.Size(65, 20);
            this.tb_freq1_fps3.TabIndex = 85;
            this.tb_freq1_fps3.Text = "150000";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.ForeColor = System.Drawing.Color.Black;
            this.label45.Location = new System.Drawing.Point(117, 100);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(87, 13);
            this.label45.TabIndex = 86;
            this.label45.Text = "Вид модуляции:";
            // 
            // bttn_close
            // 
            this.bttn_close.Enabled = false;
            this.bttn_close.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bttn_close.Location = new System.Drawing.Point(109, 284);
            this.bttn_close.Name = "bttn_close";
            this.bttn_close.Size = new System.Drawing.Size(78, 23);
            this.bttn_close.TabIndex = 83;
            this.bttn_close.Text = "Close";
            this.bttn_close.UseVisualStyleBackColor = true;
            this.bttn_close.Click += new System.EventHandler(this.Bttn_close_Click);
            // 
            // butConnect
            // 
            this.butConnect.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.butConnect.Location = new System.Drawing.Point(19, 284);
            this.butConnect.Name = "butConnect";
            this.butConnect.Size = new System.Drawing.Size(78, 23);
            this.butConnect.TabIndex = 83;
            this.butConnect.Text = "Connect";
            this.butConnect.UseVisualStyleBackColor = true;
            this.butConnect.Click += new System.EventHandler(this.ButConnect_Click);
            // 
            // bt_shag
            // 
            this.bt_shag.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bt_shag.Location = new System.Drawing.Point(109, 320);
            this.bt_shag.Name = "bt_shag";
            this.bt_shag.Size = new System.Drawing.Size(78, 23);
            this.bt_shag.TabIndex = 83;
            this.bt_shag.Text = "Add Shag";
            this.bt_shag.UseVisualStyleBackColor = true;
            this.bt_shag.Click += new System.EventHandler(this.Bt_shag_Click);
            // 
            // bt_stop_scan
            // 
            this.bt_stop_scan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bt_stop_scan.Location = new System.Drawing.Point(353, 350);
            this.bt_stop_scan.Name = "bt_stop_scan";
            this.bt_stop_scan.Size = new System.Drawing.Size(78, 23);
            this.bt_stop_scan.TabIndex = 83;
            this.bt_stop_scan.Text = "Stop Scan";
            this.bt_stop_scan.UseVisualStyleBackColor = true;
            this.bt_stop_scan.Click += new System.EventHandler(this.Bt_stop_scan_Click);
            // 
            // bt_start_scan
            // 
            this.bt_start_scan.Enabled = false;
            this.bt_start_scan.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bt_start_scan.Location = new System.Drawing.Point(353, 323);
            this.bt_start_scan.Name = "bt_start_scan";
            this.bt_start_scan.Size = new System.Drawing.Size(78, 23);
            this.bt_start_scan.TabIndex = 83;
            this.bt_start_scan.Text = "Start Scan";
            this.bt_start_scan.UseVisualStyleBackColor = true;
            this.bt_start_scan.Click += new System.EventHandler(this.Bt_start_scan_Click);
            // 
            // bttn_set_param_fps
            // 
            this.bttn_set_param_fps.Enabled = false;
            this.bttn_set_param_fps.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bttn_set_param_fps.Location = new System.Drawing.Point(19, 320);
            this.bttn_set_param_fps.Name = "bttn_set_param_fps";
            this.bttn_set_param_fps.Size = new System.Drawing.Size(78, 23);
            this.bttn_set_param_fps.TabIndex = 83;
            this.bttn_set_param_fps.Text = "Set Param";
            this.bttn_set_param_fps.UseVisualStyleBackColor = true;
            this.bttn_set_param_fps.Click += new System.EventHandler(this.bt_set_param_FPS3_Click);
            // 
            // bt_set_param_FPS3
            // 
            this.bt_set_param_FPS3.Enabled = false;
            this.bt_set_param_FPS3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            this.bt_set_param_FPS3.Location = new System.Drawing.Point(384, 21);
            this.bt_set_param_FPS3.Name = "bt_set_param_FPS3";
            this.bt_set_param_FPS3.Size = new System.Drawing.Size(78, 23);
            this.bt_set_param_FPS3.TabIndex = 83;
            this.bt_set_param_FPS3.Text = "Set Param";
            this.bt_set_param_FPS3.UseVisualStyleBackColor = true;
            this.bt_set_param_FPS3.Click += new System.EventHandler(this.bt_set_param_FPS3_Click);
            // 
            // bt_power_off_All_FPS3
            // 
            this.bt_power_off_All_FPS3.Enabled = false;
            this.bt_power_off_All_FPS3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bt_power_off_All_FPS3.Location = new System.Drawing.Point(479, 100);
            this.bt_power_off_All_FPS3.Name = "bt_power_off_All_FPS3";
            this.bt_power_off_All_FPS3.Size = new System.Drawing.Size(69, 23);
            this.bt_power_off_All_FPS3.TabIndex = 82;
            this.bt_power_off_All_FPS3.Text = "Sig all OFF";
            this.bt_power_off_All_FPS3.UseVisualStyleBackColor = true;
            this.bt_power_off_All_FPS3.Click += new System.EventHandler(this.Bt_power_off_All_FPS3_Click);
            // 
            // bt_power_off_FPS3
            // 
            this.bt_power_off_FPS3.Enabled = false;
            this.bt_power_off_FPS3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.bt_power_off_FPS3.Location = new System.Drawing.Point(385, 100);
            this.bt_power_off_FPS3.Name = "bt_power_off_FPS3";
            this.bt_power_off_FPS3.Size = new System.Drawing.Size(69, 23);
            this.bt_power_off_FPS3.TabIndex = 82;
            this.bt_power_off_FPS3.Text = "Signal OFF";
            this.bt_power_off_FPS3.UseVisualStyleBackColor = true;
            this.bt_power_off_FPS3.Click += new System.EventHandler(this.bt_power_off_FPS3_Click);
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.ForeColor = System.Drawing.Color.Black;
            this.label46.Location = new System.Drawing.Point(153, 26);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(47, 13);
            this.label46.TabIndex = 81;
            this.label46.Text = "Литера:";
            // 
            // cb_litera_fps3
            // 
            this.cb_litera_fps3.FormattingEnabled = true;
            this.cb_litera_fps3.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4"});
            this.cb_litera_fps3.Location = new System.Drawing.Point(216, 20);
            this.cb_litera_fps3.Name = "cb_litera_fps3";
            this.cb_litera_fps3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_litera_fps3.Size = new System.Drawing.Size(136, 21);
            this.cb_litera_fps3.TabIndex = 80;
            this.cb_litera_fps3.Text = "1";
            this.cb_litera_fps3.SelectedIndexChanged += new System.EventHandler(this.cb_litera_fps3_SelectedIndexChanged);
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.SystemColors.Control;
            this.tabPage2.Controls.Add(this.groupBox1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1235, 425);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Настройки";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.checkBox_color);
            this.groupBox1.Controls.Add(this.textBox_buf_delay);
            this.groupBox1.Controls.Add(this.checkBox_visibleTxRx);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.button_buffer_delay);
            this.groupBox1.Controls.Add(this.button_update_ports);
            this.groupBox1.Controls.Add(this.comboBox_portname);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.button_Close);
            this.groupBox1.Controls.Add(this.button_Open);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.comboBox_baudrate);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.comboBox_parity);
            this.groupBox1.Controls.Add(this.comboBox_stop_bits);
            this.groupBox1.Location = new System.Drawing.Point(3, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(568, 256);
            this.groupBox1.TabIndex = 16;
            this.groupBox1.TabStop = false;
            // 
            // checkBox_color
            // 
            this.checkBox_color.AutoSize = true;
            this.checkBox_color.Checked = true;
            this.checkBox_color.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_color.Location = new System.Drawing.Point(12, 136);
            this.checkBox_color.Name = "checkBox_color";
            this.checkBox_color.Size = new System.Drawing.Size(50, 17);
            this.checkBox_color.TabIndex = 34;
            this.checkBox_color.Text = "Color";
            this.checkBox_color.UseVisualStyleBackColor = true;
            // 
            // textBox_buf_delay
            // 
            this.textBox_buf_delay.ForeColor = System.Drawing.SystemColors.WindowText;
            this.textBox_buf_delay.Location = new System.Drawing.Point(456, 66);
            this.textBox_buf_delay.Name = "textBox_buf_delay";
            this.textBox_buf_delay.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.textBox_buf_delay.Size = new System.Drawing.Size(67, 20);
            this.textBox_buf_delay.TabIndex = 41;
            this.textBox_buf_delay.Text = "20";
            this.textBox_buf_delay.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // checkBox_visibleTxRx
            // 
            this.checkBox_visibleTxRx.AutoSize = true;
            this.checkBox_visibleTxRx.Checked = true;
            this.checkBox_visibleTxRx.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox_visibleTxRx.Location = new System.Drawing.Point(12, 107);
            this.checkBox_visibleTxRx.Name = "checkBox_visibleTxRx";
            this.checkBox_visibleTxRx.Size = new System.Drawing.Size(59, 17);
            this.checkBox_visibleTxRx.TabIndex = 34;
            this.checkBox_visibleTxRx.Text = "On/Off";
            this.checkBox_visibleTxRx.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.ForeColor = System.Drawing.Color.Black;
            this.button1.Location = new System.Drawing.Point(319, 65);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Сохранить настройки";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Visible = false;
            // 
            // button_buffer_delay
            // 
            this.button_buffer_delay.ForeColor = System.Drawing.Color.Black;
            this.button_buffer_delay.Location = new System.Drawing.Point(456, 9);
            this.button_buffer_delay.Name = "button_buffer_delay";
            this.button_buffer_delay.Size = new System.Drawing.Size(69, 50);
            this.button_buffer_delay.TabIndex = 12;
            this.button_buffer_delay.Text = "Размер\r\nбуфера [мс]";
            this.button_buffer_delay.UseVisualStyleBackColor = true;
            // 
            // button_update_ports
            // 
            this.button_update_ports.ForeColor = System.Drawing.Color.Black;
            this.button_update_ports.Location = new System.Drawing.Point(8, 15);
            this.button_update_ports.MinimumSize = new System.Drawing.Size(55, 23);
            this.button_update_ports.Name = "button_update_ports";
            this.button_update_ports.Size = new System.Drawing.Size(75, 48);
            this.button_update_ports.TabIndex = 16;
            this.button_update_ports.Text = "Обновить\r\nсписок\r\nпортов";
            this.button_update_ports.UseVisualStyleBackColor = true;
            this.button_update_ports.Click += new System.EventHandler(this.button_update_ports_Click);
            // 
            // comboBox_portname
            // 
            this.comboBox_portname.Cursor = System.Windows.Forms.Cursors.Default;
            this.comboBox_portname.FormattingEnabled = true;
            this.comboBox_portname.Location = new System.Drawing.Point(147, 17);
            this.comboBox_portname.Name = "comboBox_portname";
            this.comboBox_portname.Size = new System.Drawing.Size(64, 21);
            this.comboBox_portname.TabIndex = 6;
            this.comboBox_portname.Text = "COM1";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Enabled = false;
            this.label8.Location = new System.Drawing.Point(217, 43);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 26);
            this.label8.TabIndex = 9;
            this.label8.Text = "Стоп. \r\nбиты";
            this.label8.UseWaitCursor = true;
            // 
            // button_Close
            // 
            this.button_Close.Enabled = false;
            this.button_Close.ForeColor = System.Drawing.Color.Black;
            this.button_Close.Location = new System.Drawing.Point(389, 9);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(61, 50);
            this.button_Close.TabIndex = 15;
            this.button_Close.Text = "Закрыть\r\nпорт";
            this.button_Close.UseVisualStyleBackColor = true;
            this.button_Close.Click += new System.EventHandler(this.button_Close_Click);
            // 
            // button_Open
            // 
            this.button_Open.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_Open.ForeColor = System.Drawing.Color.Black;
            this.button_Open.Location = new System.Drawing.Point(319, 9);
            this.button_Open.Name = "button_Open";
            this.button_Open.Size = new System.Drawing.Size(66, 50);
            this.button_Open.TabIndex = 15;
            this.button_Open.Text = "Открыть\r\n порт";
            this.button_Open.UseVisualStyleBackColor = true;
            this.button_Open.Click += new System.EventHandler(this.button_Open_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(103, 20);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(32, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Порт";
            this.label5.UseWaitCursor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Enabled = false;
            this.label7.Location = new System.Drawing.Point(217, 21);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(35, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Четн.";
            this.label7.UseWaitCursor = true;
            // 
            // comboBox_baudrate
            // 
            this.comboBox_baudrate.Cursor = System.Windows.Forms.Cursors.Default;
            this.comboBox_baudrate.FormattingEnabled = true;
            this.comboBox_baudrate.Items.AddRange(new object[] {
            "1200",
            "1800",
            "2400",
            "4800",
            "7200",
            "9600",
            "14400",
            "19200",
            "38400",
            "57600",
            "115200"});
            this.comboBox_baudrate.Location = new System.Drawing.Point(147, 51);
            this.comboBox_baudrate.Name = "comboBox_baudrate";
            this.comboBox_baudrate.Size = new System.Drawing.Size(64, 21);
            this.comboBox_baudrate.TabIndex = 8;
            this.comboBox_baudrate.Text = "115200";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(90, 43);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 26);
            this.label6.TabIndex = 9;
            this.label6.Text = "Скорость\r\n(бит/с)";
            this.label6.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label6.UseWaitCursor = true;
            // 
            // comboBox_parity
            // 
            this.comboBox_parity.Cursor = System.Windows.Forms.Cursors.Default;
            this.comboBox_parity.FormattingEnabled = true;
            this.comboBox_parity.Items.AddRange(new object[] {
            "Чет",
            "Нечет",
            "Нет",
            "Маркер",
            "Пробел"});
            this.comboBox_parity.Location = new System.Drawing.Point(257, 18);
            this.comboBox_parity.Name = "comboBox_parity";
            this.comboBox_parity.Size = new System.Drawing.Size(50, 21);
            this.comboBox_parity.TabIndex = 8;
            this.comboBox_parity.Text = "Нет";
            // 
            // comboBox_stop_bits
            // 
            this.comboBox_stop_bits.Cursor = System.Windows.Forms.Cursors.Default;
            this.comboBox_stop_bits.FormattingEnabled = true;
            this.comboBox_stop_bits.Items.AddRange(new object[] {
            "None",
            "1",
            "1.5",
            "2"});
            this.comboBox_stop_bits.Location = new System.Drawing.Point(257, 52);
            this.comboBox_stop_bits.Name = "comboBox_stop_bits";
            this.comboBox_stop_bits.Size = new System.Drawing.Size(50, 21);
            this.comboBox_stop_bits.TabIndex = 8;
            this.comboBox_stop_bits.Text = "1";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.bt_GetAmp_fps3);
            this.tabPage1.Controls.Add(this.bt_DDS_clr_fps3);
            this.tabPage1.Controls.Add(this.cb_literaK_fps3);
            this.tabPage1.Controls.Add(this.txtSWRF2);
            this.tabPage1.Controls.Add(this.cb_ch_sw_fps3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1235, 425);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "tabTest";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // bt_GetAmp_fps3
            // 
            this.bt_GetAmp_fps3.Enabled = false;
            this.bt_GetAmp_fps3.ForeColor = System.Drawing.Color.Fuchsia;
            this.bt_GetAmp_fps3.Location = new System.Drawing.Point(88, 21);
            this.bt_GetAmp_fps3.Name = "bt_GetAmp_fps3";
            this.bt_GetAmp_fps3.Size = new System.Drawing.Size(59, 23);
            this.bt_GetAmp_fps3.TabIndex = 122;
            this.bt_GetAmp_fps3.Text = "-";
            this.bt_GetAmp_fps3.UseVisualStyleBackColor = true;
            this.bt_GetAmp_fps3.Click += new System.EventHandler(this.bt_GetAmp_fps3_Click);
            // 
            // bt_DDS_clr_fps3
            // 
            this.bt_DDS_clr_fps3.Enabled = false;
            this.bt_DDS_clr_fps3.ForeColor = System.Drawing.Color.Fuchsia;
            this.bt_DDS_clr_fps3.Location = new System.Drawing.Point(153, 21);
            this.bt_DDS_clr_fps3.Name = "bt_DDS_clr_fps3";
            this.bt_DDS_clr_fps3.Size = new System.Drawing.Size(59, 23);
            this.bt_DDS_clr_fps3.TabIndex = 122;
            this.bt_DDS_clr_fps3.Text = "-";
            this.bt_DDS_clr_fps3.UseVisualStyleBackColor = true;
            this.bt_DDS_clr_fps3.Click += new System.EventHandler(this.Bt_DDS_clr_fps3_Click);
            // 
            // cb_literaK_fps3
            // 
            this.cb_literaK_fps3.FormattingEnabled = true;
            this.cb_literaK_fps3.Items.AddRange(new object[] {
            "DDS2",
            "DDS1"});
            this.cb_literaK_fps3.Location = new System.Drawing.Point(256, 23);
            this.cb_literaK_fps3.Name = "cb_literaK_fps3";
            this.cb_literaK_fps3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_literaK_fps3.Size = new System.Drawing.Size(65, 21);
            this.cb_literaK_fps3.TabIndex = 80;
            this.cb_literaK_fps3.Text = "DDS2";
            this.cb_literaK_fps3.SelectedIndexChanged += new System.EventHandler(this.cb_litera_fps3_SelectedIndexChanged);
            // 
            // txtSWRF2
            // 
            this.txtSWRF2.Location = new System.Drawing.Point(175, 65);
            this.txtSWRF2.Name = "txtSWRF2";
            this.txtSWRF2.Size = new System.Drawing.Size(65, 20);
            this.txtSWRF2.TabIndex = 117;
            this.txtSWRF2.Text = "2";
            // 
            // cb_ch_sw_fps3
            // 
            this.cb_ch_sw_fps3.FormattingEnabled = true;
            this.cb_ch_sw_fps3.Items.AddRange(new object[] {
            "CH0",
            "CH1",
            "CH2",
            "CH3",
            "CH01",
            "CH02",
            "CH03",
            "CH12",
            "CH13",
            "CH23"});
            this.cb_ch_sw_fps3.Location = new System.Drawing.Point(104, 64);
            this.cb_ch_sw_fps3.Name = "cb_ch_sw_fps3";
            this.cb_ch_sw_fps3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.cb_ch_sw_fps3.Size = new System.Drawing.Size(65, 21);
            this.cb_ch_sw_fps3.TabIndex = 80;
            this.cb_ch_sw_fps3.Text = "0";
            this.cb_ch_sw_fps3.SelectedIndexChanged += new System.EventHandler(this.cb_litera_fps3_SelectedIndexChanged);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(547, 14);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(28, 13);
            this.label29.TabIndex = 127;
            this.label29.Text = "ver=";
            // 
            // bt_Get_Ver_FPS2
            // 
            this.bt_Get_Ver_FPS2.Enabled = false;
            this.bt_Get_Ver_FPS2.ForeColor = System.Drawing.Color.Fuchsia;
            this.bt_Get_Ver_FPS2.Location = new System.Drawing.Point(463, 9);
            this.bt_Get_Ver_FPS2.Name = "bt_Get_Ver_FPS2";
            this.bt_Get_Ver_FPS2.Size = new System.Drawing.Size(78, 23);
            this.bt_Get_Ver_FPS2.TabIndex = 126;
            this.bt_Get_Ver_FPS2.Text = "Get Ver";
            this.bt_Get_Ver_FPS2.UseVisualStyleBackColor = true;
            this.bt_Get_Ver_FPS2.Click += new System.EventHandler(this.bt_Get_Ver_FPS2_Click);
            // 
            // checkBoxService
            // 
            this.checkBoxService.AutoSize = true;
            this.checkBoxService.Location = new System.Drawing.Point(15, 10);
            this.checkBoxService.Name = "checkBoxService";
            this.checkBoxService.Size = new System.Drawing.Size(95, 17);
            this.checkBoxService.TabIndex = 69;
            this.checkBoxService.Text = "Добавить УУ";
            this.checkBoxService.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(127, 13);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(123, 12);
            this.progressBar1.TabIndex = 46;
            // 
            // richTextBoxOut
            // 
            this.richTextBoxOut.BackColor = System.Drawing.Color.Beige;
            this.richTextBoxOut.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.richTextBoxOut.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBoxOut.Location = new System.Drawing.Point(0, 523);
            this.richTextBoxOut.Name = "richTextBoxOut";
            this.richTextBoxOut.Size = new System.Drawing.Size(1243, 193);
            this.richTextBoxOut.TabIndex = 50;
            this.richTextBoxOut.Text = "";
            this.richTextBoxOut.TextChanged += new System.EventHandler(this.richTextBoxOut_TextChanged);
            // 
            // button_terminal
            // 
            this.button_terminal.Location = new System.Drawing.Point(315, 9);
            this.button_terminal.Name = "button_terminal";
            this.button_terminal.Size = new System.Drawing.Size(68, 23);
            this.button_terminal.TabIndex = 12;
            this.button_terminal.Text = "Терминал";
            this.button_terminal.UseVisualStyleBackColor = true;
            this.button_terminal.Click += new System.EventHandler(this.button_terminal_Click);
            // 
            // button_terminal_clear
            // 
            this.button_terminal_clear.Location = new System.Drawing.Point(389, 9);
            this.button_terminal_clear.Name = "button_terminal_clear";
            this.button_terminal_clear.Size = new System.Drawing.Size(68, 23);
            this.button_terminal_clear.TabIndex = 12;
            this.button_terminal_clear.Text = "Очистить";
            this.button_terminal_clear.UseVisualStyleBackColor = true;
            this.button_terminal_clear.Click += new System.EventHandler(this.button_terminal_clear_Click);
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // openFileDialogHex
            // 
            this.openFileDialogHex.FileName = "openFileDialogHex";
            // 
            // timerWaitStOK
            // 
            this.timerWaitStOK.Interval = 1000;
            this.timerWaitStOK.Tick += new System.EventHandler(this.timerWaitStOK_Tick);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button_terminal_clear);
            this.panel1.Controls.Add(this.button_terminal);
            this.panel1.Controls.Add(this.progressBar1);
            this.panel1.Controls.Add(this.checkBoxService);
            this.panel1.Controls.Add(this.bt_Get_Ver_FPS2);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 24);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1243, 48);
            this.panel1.TabIndex = 51;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1243, 24);
            this.menuStrip1.TabIndex = 52;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripSeparator1,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(90, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(93, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // Main_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1243, 738);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.richTextBoxOut);
            this.Controls.Add(this.statusStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Location = new System.Drawing.Point(200, 200);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main_form";
            this.Text = "Терминал устройства управления v 4.2";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_form_FormClosing);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel_err;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox_buf_delay;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button_buffer_delay;
        private System.Windows.Forms.Button button_update_ports;
        private System.Windows.Forms.ComboBox comboBox_portname;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button_Close;
        private System.Windows.Forms.Button button_Open;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox_baudrate;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox comboBox_parity;
        private System.Windows.Forms.ComboBox comboBox_stop_bits;
        private System.Windows.Forms.Button button_terminal;
        private System.Windows.Forms.Button button_terminal_clear;
        private System.Windows.Forms.CheckBox checkBox_color;
        private System.Windows.Forms.CheckBox checkBox_visibleTxRx;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.CheckBox checkBoxService;
        private System.Windows.Forms.OpenFileDialog openFileDialogHex;
        private System.Windows.Forms.RichTextBox richTextBoxOut;
        private System.Windows.Forms.Timer timerWaitStOK;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.ListBox listBoxErr_fps3;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Button bt_Get_Ver_FPS2;
        private System.Windows.Forms.Button bt_GetAmp_fps3;
        private System.Windows.Forms.Button bt_get_status_fps3;
        private System.Windows.Forms.CheckBox checkBox_GPS_L2_fps3;
        private System.Windows.Forms.CheckBox checkBox_GPS_L1_fps3;
        private System.Windows.Forms.CheckBox checkBox_GL_L2_fps3;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox tb_dev1_fps3;
        private System.Windows.Forms.TextBox tb_tscan_fps3;
        private System.Windows.Forms.ComboBox cb_man1_fps3;
        private System.Windows.Forms.ComboBox cb_mod1_fps3;
        private System.Windows.Forms.ComboBox cb_tim1_fps3;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox tb_freq1_fps3;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button bt_set_param_FPS3;
        private System.Windows.Forms.Button bt_power_off_FPS3;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.ComboBox cb_litera_fps3;
        private System.Windows.Forms.TextBox tb_freq2_fps3;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox cb_literaK_fps3;
        private System.Windows.Forms.Button bt_power_off_All_FPS3;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.ComboBox cb_literaCH_fps3;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button bt_setParams;
        private System.Windows.Forms.Button bt_delParams;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button bt_saveEEParams;
        private System.Windows.Forms.Button getParams;
        private System.Windows.Forms.Button bttn_Set_param_KS;
        private System.Windows.Forms.Button butConnect;
        private System.Windows.Forms.Button bttn_set_param_fps;
        private System.Windows.Forms.Button bttn_seat_peak_KS;
        private System.Windows.Forms.Button bttn_refAmp_KS;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox tb_ref_amp;
        private System.Windows.Forms.Button bttn_close;
        private System.Windows.Forms.Label lb_txrefAmp;
        private System.Windows.Forms.Button bttn_Get_data;
        private System.Windows.Forms.Button bttn_setSpan;
        private System.Windows.Forms.TextBox tbSpanFreq;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox tb_shag_scan;
        private System.Windows.Forms.TextBox tb_stop_scan;
        private System.Windows.Forms.TextBox tb_start_scan;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Button bt_start_scan;
        private System.Windows.Forms.CheckBox checkBox_GL_L1_fps3;
        private System.Windows.Forms.Button bt_shag;
        private System.Windows.Forms.Button bt_clear_richtext;
        private System.Windows.Forms.Button bt_stop_scan;
        private System.Windows.Forms.TextBox txtSWRF2;
        private System.Windows.Forms.TextBox txt_SW_num;
        private System.Windows.Forms.TextBox txt_SWRF_num;
        private System.Windows.Forms.ComboBox cb_ch_sw_fps3;
        private System.Windows.Forms.Button bt_DDS_clr_fps3;
        private System.Windows.Forms.Button bt_test_rf;
        private System.Windows.Forms.CheckBox chBaAvrg;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.ComboBox cb_ks_devices;
        private System.Windows.Forms.Button bt_preset;
        private System.Windows.Forms.TextBox tb_ref_offset;
        private System.Windows.Forms.Button bt_ref_offset;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button bt_set_switch;
    }
}

