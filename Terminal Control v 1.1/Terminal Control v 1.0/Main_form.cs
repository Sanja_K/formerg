﻿using Ivi.Visa.Interop;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Terminal_Control_v_1._1
{
    enum DATA_COMAND : byte
    {
        /* Application's state machine's initial state. */
        NO_CMD = 0x00,
        READ_BOOT_INFO = 0x01,
        PROGRAM_FLASH = 0x81,
        ERASE_FLASH = 0x82,
        READ_FLASH = 0x83,
        JUMP_App = 0x84,
        SET_segADDR = 0x85
    }

    
    public partial class Main_form : Form
    {
        Ivi.Visa.Interop.ResourceManager rm = new Ivi.Visa.Interop.ResourceManager();
        FormattedIO488 src = new FormattedIO488();
        Terminal_form TermForm;
     //   string Openfilename;
        List<string> fileLines = new List<string>();

        int baudrate; // для предварительной записи параметров из настроек, потом проверяется из сущ списка портов
        string portName;
        //    int curIndex;

        string firstFr;
        string secondFr;
        string modulation;

        string manipulation;
        string deviation;
        string timeScan;
        string timRadiation;
        //        string gain;
        //-----------GNSS-----------------
        bool gPS_L1_fps3IsChecked;
        bool gPS_L2_fps3IsChecked;
        bool gL_L1_fps3IsEChecked;
        bool gL_L2_fps3IsEChecked;
        //-------------------------------------
        string SW_num;
        string SWRF_num;
        //-------------------KS-----------------
        string start_fr;
        string stop_fr;
        string shag_fr;
        string span_fr;
        string refAmp;
        string refOffset;
        int sw_devices_index;
        //----------------------------------------
        double[] dataFreq;
        double[] dataPower;

        bool flagSelGNSS = false;
        //bool flgEnd = false;

        int curRow;
        bool idIsTaken = false;
        int serialPort1cntrData = 0;

        BindingList<Info> dataGrid = new BindingList<Info>();
        ModulationParams curModulationParams;

        // HEX_EVENT_def HEX_EVENT;
        // вот этот самый класс, который мы будем сохранять
        public class ParamSettings // iniSet имя выбрано просто для читаемости кода впоследствии
        {
            //public int num_ports; // это собственно для хранения X верхнего левого угла окна
            public int theForm_width;
            public int theForm_height;
            public int theForm_top;
            public int theForm_left;
            public int theTermForm_width;
            public int theTermForm_height;
            public int theTermForm_top;
            public int theTermForm_left;

            // ну а это, соответственно Y

            public string portName;
            public int baudrate;

            public string firstFr;
            public string secondFr;
            public string modulation;
            public string manipulation;
            public string deviation;
            public string timeScan;
            public string timeRad;
            //-------------------------------------------
            public string gain;
            public string litera;

            public string SW_num;
            public string SWRF_num;
            //--------------KS Scan----------------------
            public string start_fr;
            public string stop_fr;
            public string shag_fr;
            public string span_fr;
            public string refAmp;
            public string refOffset;
            public int sw_devices_index;  
            //----------------GNSS-----------------------
            public bool GPS_L1_fps3IsChecked;
            public bool GPS_L2_fps3IsChecked;
            public bool GL_L1_fps3IsChecked;
            public bool GL_L2_fps3IsChecked;

            public BindingList<Info> latestData;
        }

        public class Info
        {
            public uint Id { get; set; }
            public uint Fr1 { get; set; }
            public uint Fr2 { get; set; }
            public uint K1 { get; set; }
            public uint K2 { get; set; }
            public uint Status { get; set; }

        }

        public class ModulationParams
        {
            public int ModIndx { get; set; }
            public string Dev { get; set; }

            public int ManIndx { get; set; }
            public string TScan { get; set; }
            public string TCode { get; set; }
        }

        public Main_form()
        {
            TermForm = new Terminal_form(this);
            curModulationParams = new ModulationParams();
            InitializeComponent();
            getAvailablePorts();
            LoadSettings();
            SetPortBaudrate();
            FillDataGreed();
            bt_delParams.Enabled = true;
            button5.Enabled = true;
            button6.Enabled = true;
            button7.Enabled = true;
            getParams.Enabled = true;
            bt_saveEEParams.Enabled = true;

            dataGridView1.CellValidating += new DataGridViewCellValidatingEventHandler(dataGridView1_CellValidating);
            dataGridView1.CellClick += new DataGridViewCellEventHandler(dataGridView1_CellClick);
            dataGridView1.ColumnHeaderMouseClick += new DataGridViewCellMouseEventHandler(dataGridView1_HeaderClick);

            dataGridView1.CellEndEdit += new DataGridViewCellEventHandler(dataGridView1_IDValidating);



            tb_freq2_fps3.Enabled = true;

            dataFreq = new double[1000];
            dataPower = new double[1000];

        }


        private void LoadSettings()
        {
            LoadCPSettings(); // предварительная загрузка даннных
            SetPortName();
            SetPortBaudrate();
            SetFrAndGain();
            SetModulation();
            SetManipulation();
            SetTimRadiation();
            SetCheckers();

        }

        private void SetPortName()
        {
            for (int i = 0; i < comboBox_portname.Items.Count; i++)
            {
                comboBox_portname.SelectedIndex = i;
                if (comboBox_portname.Text == portName)
                    return;
            }
        }

        private void SetPortBaudrate()
        {
            for (int i = 0; i < comboBox_baudrate.Items.Count; i++)
            {
                comboBox_baudrate.SelectedIndex = i;
                if (comboBox_baudrate.Text == baudrate.ToString())
                    return;
            }
        }

        private void SetModulation()
        {
            if (modulation != null)
            {
                for (int i = 0; i < cb_mod1_fps3.Items.Count; i++)
                {
                    cb_mod1_fps3.SelectedIndex = i;
                    if (cb_mod1_fps3.Text == modulation.ToString())
                        return;
                }
            }
        }
        private void SetManipulation()
        {
            if (manipulation != null)
            {
                for (int i = 0; i < cb_man1_fps3.Items.Count; i++)
                {
                    cb_man1_fps3.SelectedIndex = i;
                    if (cb_man1_fps3.Text == manipulation.ToString())
                        return;
                }
            }
        }
        private void SetTimRadiation()
        {
            if (timRadiation != null)
            {
                for (int i = 0; i < cb_tim1_fps3.Items.Count; i++)
                {
                    cb_tim1_fps3.SelectedIndex = i;
                    if (cb_tim1_fps3.Text == timRadiation.ToString())
                        return;
                }
            }
        }

        private void SetFrAndGain()
        {
            if (firstFr != null)
                tb_freq1_fps3.Text = firstFr;
            if (secondFr != null)
                tb_freq2_fps3.Text = secondFr;
            if (start_fr != null)
                tb_start_scan.Text = start_fr;
            if (stop_fr != null)
                tb_stop_scan.Text = stop_fr;
            if (shag_fr != null)
                tb_shag_scan.Text = shag_fr;
            if (span_fr != null)
                tbSpanFreq.Text = span_fr;
            if (refAmp != null)
                tb_ref_amp.Text = refAmp;
            if (refOffset != null)
                tb_ref_offset.Text = refOffset;
            if (SW_num != null)
                txt_SW_num.Text = SW_num;
            if (SWRF_num != null)
                txt_SWRF_num.Text = SWRF_num;

            if (deviation != null)
                tb_dev1_fps3.Text = deviation;

            if (timeScan != null)
                tb_tscan_fps3.Text = timeScan;
            //----------------------------------------
            cb_ks_devices.SelectedIndex = sw_devices_index;
        }

        private void SetCheckers()
        {
            checkBox_GPS_L1_fps3.Checked = gPS_L1_fps3IsChecked;
            checkBox_GPS_L2_fps3.Checked = gPS_L2_fps3IsChecked;
            checkBox_GL_L1_fps3.Checked = gL_L1_fps3IsEChecked;
            checkBox_GL_L2_fps3.Checked = gL_L2_fps3IsEChecked;
        }



        void getAvailablePorts()
        {
            string[] ports = SerialPort.GetPortNames();
            button_update_ports.Enabled = false;
            comboBox_portname.Items.Clear();
            comboBox_portname.Items.AddRange(ports);
            if (comboBox_portname.Items.Count > 0)
            {
                comboBox_portname.SelectedIndex = 0;
            }
            else
            {
                comboBox_portname.Text = "----";
            }
            button_update_ports.Enabled = true;
        }

        private void button_update_ports_Click(object sender, EventArgs e)
        {
            getAvailablePorts();
        }

        private void button_Open_Click(object sender, EventArgs e)
        {
            try
            {

                if (comboBox_portname.Text == "" || comboBox_baudrate.Text == "")
                {

                    MessageBox.Show("Ошибка! Не выбран порт", "Внимание!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                }
                else
                {
                    serialPort1.PortName = comboBox_portname.Text;
                    serialPort1.BaudRate = Convert.ToInt32(comboBox_baudrate.Text);
                    button_Close.Enabled = true;

                    bt_power_off_All_FPS3.Enabled = true;

                    bt_test_rf.Enabled = true;
                   // butConnect.Enabled = true;
                    button_Open.Enabled = false;
                    comboBox_portname.Enabled = false;
                    comboBox_baudrate.Enabled = false;
                    comboBox_stop_bits.Enabled = false;
                    comboBox_parity.Enabled = false;
                    bt_DDS_clr_fps3.Enabled = true;
                    bt_set_param_FPS3.Enabled = true;
                    bt_power_off_FPS3.Enabled = true;
                    bt_get_status_fps3.Enabled = true;
                    bt_set_switch.Enabled = true;
                    bt_GetAmp_fps3.Enabled = true;
                    if (bttn_Set_param_KS.Enabled)
                    {
                        bt_start_scan.Enabled = true;
                        bttn_set_param_fps.Enabled = true;
                    }

                    //checkBox_GPS_L1_fps3.Enabled = true;
                    //checkBox_GPS_L2_fps3.Enabled = true;
                    //checkBox_GL_L1_fps3.Enabled = true;
                    //checkBox_GL_L2_fps3.Enabled = true;

                    tb_freq2_fps3.Enabled = true;
                    bt_Get_Ver_FPS2.Enabled = true;
                    bt_setParams.Enabled = true;

                    switch (comboBox_parity.SelectedIndex)
                    {
                        case 0:
                            serialPort1.Parity = Parity.Even;
                            break;
                        case 1:
                            serialPort1.Parity = Parity.Mark;
                            break;
                        case 2:
                            serialPort1.Parity = Parity.None;
                            break;
                        case 3:
                            serialPort1.Parity = Parity.Odd;
                            break;
                        case 4:
                            serialPort1.Parity = Parity.Space;
                            break;
                        default:
                            serialPort1.Parity = Parity.None;
                            break;
                    }
                    serialPort1.DataBits = 8;
                    switch (comboBox_stop_bits.SelectedIndex)
                    {
                        case 0:
                            serialPort1.StopBits = StopBits.None;
                            break;
                        case 1:
                            serialPort1.StopBits = StopBits.One;
                            break;
                        case 2:
                            serialPort1.StopBits = StopBits.OnePointFive;
                            break;
                        case 3:
                            serialPort1.StopBits = StopBits.Two;
                            break;
                        default:
                            serialPort1.StopBits = StopBits.One;
                            break;
                    }
                    serialPort1.RtsEnable = true;
                    serialPort1.DtrEnable = false;
                    serialPort1.ReadTimeout = 100;
                    //serialPort1.WriteTimeout = 10;
                    serialPort1.WriteBufferSize = 2048;
                    serialPort1.ReadBufferSize = 2048;

                    serialPort1.Open();
                    progressBar1.Step = 1;
                    progressBar1.Maximum = 100;

                    progressBar1.Value = 100;
                    toolStripStatusLabel1.Text = "Port = \"" + serialPort1.PortName + "\", BaudRate = " + serialPort1.BaudRate.ToString();

                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка подключения!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);

            }
        }

        private void button_Close_Click(object sender, EventArgs e)
        {
            serialPort1.Close();
            if (serialPort1.IsOpen == false)
            {
                toolStripStatusLabel1.Text = "Port not initialized";
                progressBar1.Value = 0;
                button_Close.Enabled = false;

                bt_power_off_All_FPS3.Enabled = false;
                //butConnect.Enabled = false;
                button_Open.Enabled = true;
                comboBox_portname.Enabled = true;
                comboBox_baudrate.Enabled = true;
                comboBox_stop_bits.Enabled = true;
                comboBox_parity.Enabled = true;
                bt_DDS_clr_fps3.Enabled = false;
                tb_freq2_fps3.Enabled = false;
                bt_test_rf.Enabled = false;
                bt_set_param_FPS3.Enabled = false;
                bt_power_off_FPS3.Enabled = false;
                bt_get_status_fps3.Enabled = false;
                bt_set_switch.Enabled = false;
                bt_GetAmp_fps3.Enabled = false;
                bt_Get_Ver_FPS2.Enabled = false;
                bt_setParams.Enabled = false;
                bt_start_scan.Enabled = false;
                bttn_set_param_fps.Enabled = false;
            }

        }

        private void button_terminal_Click(object sender, EventArgs e)
        {


            if (TermForm.Visible == true)
            {
                TermForm.Visible = false;
            }
            else
            {

                TermForm.Visible = true;
            }
        }

        private void button_terminal_clear_Click(object sender, EventArgs e)
        {
            CallBackMy.callbackEventHandlerCLC(null);

            richTextBoxOut.Clear();

        }

        private byte get_form_addr(int iIndex)
        {
            switch (iIndex)
            {
                case 0:
                case 2:
                    return 0x12; // 1 - 3
                case 1:
                case 3:
                    return 0x13; // 2 - 4
                case 4:
                    return 0x14; // 5
                case 5:
                    return 0x15; //6
                case 6:
                    return 0x16; // 7
                case 7:
                    return 0x17; //8
                case 8:
                    return 0x18; // 9
                default:
                    return 0x12; //1
            }
        }
        private byte get_form_addr_FPS2(int iIndex)
        {
            switch (iIndex)
            {
                case 0:
                case 1: // Адрес Литеры 1 и Литеры2
                    return 0x12;
                case 2: // Адрес Литеры 3 GPS&GLONASS

                    return 0x12;
                case 3: // Адрес Литеры 4 
                    return 0x14;
                default:
                    return 0x12;
            }
        }

        int getScanFreq;
        int cntrScanFreq; // dlya zapisi v excel dannyh
        int stopScanFreq;
        int shagScanFreq;
        int cntWaitScanFreq = 0;// cntr timer zapros
        int startscan;
        int addrLitscan;


        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Action action;
            int i, iNum, iLen, localCounter, id, fr1, fr2, k1, k2, status;
            int lengthOfRow = 10;
            // int version;
            bool n_error = true;
            System.Threading.Thread.Sleep(1);
            StringBuilder sb_hex = new StringBuilder();

            //--------------------------------------------------------
            iLen = serialPort1.BytesToRead; // количество принятых байт

            if (iLen != 0)
            {
                byte[] bBufRx = new byte[iLen];
                i = 0;

                while (i < iLen)
                {
                    bBufRx[i] = Convert.ToByte(serialPort1.ReadByte());
                    sb_hex.Append(bBufRx[i].ToString("X2") + " ");
                    i++;
                }

                serialPort1cntrData += 1;
                if (serialPort1cntrData == 100)
                { 
                    serialPort1.DiscardInBuffer();
                    serialPort1cntrData = 0;
                }

                action = () =>
                {
                    if (iLen > 3)
                    {
                        //----------------------------------------------------------
                        if (bBufRx[1] == 0x43)
                        {

                            //ФПС3
                            label29.Text = "ver:" + bBufRx[3];
                            listBoxErr_fps3.Items.Add("ver:" + bBufRx[3]);
                        }
                        else if (bBufRx[1] == 0x42)
                        {

                            //ФПС3
                            listBoxErr_fps3.Items.Add("Save to EEPROM parametrs:" + bBufRx[3]);

                        }
                        else if (bBufRx[1] == 0x28)
                        {
                            if (startscan == 1)
                            {

                                if (bBufRx[3] == 0x00)
                                {
                                    Thread.Sleep(500);
                                    string txCentrFreq = getScanFreq.ToString() + "000000";
                                    src.IO.WriteString("SENS:FREQ:CENT " + txCentrFreq);
                                    int cntrdt = 1;
                                    string YMARKData = "";
                                    string XMARKData = "";
                                    timerWaitStOK.Stop();
                                    float datagetks = 0;
                                    string txOPCData = "00";

                                    if (chBaAvrg.Checked)
                                    {
                                        cntrdt = 10;
                                    }

                                    while (cntrdt > 0)
                                    {
                                        cntrdt -= 1;

                                        src.IO.WriteString("INIT:CONT 0\n");
                                        src.IO.WriteString("INIT:IMM;*WAI\n");

                                        int CntrDataDelay = 0;
                                        switch (cb_ks_devices.SelectedIndex)
                                        {
                                            case 0:
                                                txOPCData = "00";
                                                while ((txOPCData != "+1") && (CntrDataDelay < 2000000))
                                                {
                                                    src.IO.Timeout = 2000;
                                                    CntrDataDelay++;
                                                    src.IO.WriteString("*OPC?");//ждем кода устройство отработает команду
                                                    txOPCData = src.IO.ReadString(2);
                                                }
                                                break;
                                            case 1:
                                                 txOPCData = "0\n";
                                                while ((txOPCData != "1\n") && (CntrDataDelay < 2000000))
                                                {
                                                    src.IO.Timeout = 2000;
                                                    CntrDataDelay++;
                                                    src.IO.WriteString("*OPC?");//ждем кода устройство отработает команду
                                                    txOPCData = src.IO.ReadString(2);
                                                }
                                                break;
                                        }



                                        if (CntrDataDelay > 2000000)
                                        {
                                            startscan = 0;
                                            richTextBoxOut.AppendText(" KS_WAIT_ERROR!!!\n ");
                                        }
                                        else
                                        {
                                            // Thread.Sleep(500);


                                            src.IO.WriteString("CALC:MARK:MAX\n");

                                            src.IO.WriteString("CALC:MARK:Y?\n");

                                            YMARKData = src.IO.ReadString(19);
                                            datagetks += float.Parse(YMARKData, CultureInfo.InvariantCulture.NumberFormat);

                                            CntrDataDelay = 0;
                                            switch (cb_ks_devices.SelectedIndex)
                                            {
                                                case 0:
                                                    txOPCData = "00";
                                                    while ((txOPCData != "+1") && (CntrDataDelay < 2000000))
                                                    {
                                                        src.IO.Timeout = 2000;
                                                        CntrDataDelay++;
                                                        src.IO.WriteString("*OPC?");//ждем кода устройство отработает команду
                                                        txOPCData = src.IO.ReadString(2);
                                                    }
                                                    break;
                                                case 1:
                                                    txOPCData = "0\n";
                                                    while ((txOPCData != "1\n") && (CntrDataDelay < 2000000))
                                                    {
                                                        src.IO.Timeout = 2000;
                                                        CntrDataDelay++;
                                                        src.IO.WriteString("*OPC?");//ждем кода устройство отработает команду
                                                        txOPCData = src.IO.ReadString(2);
                                                    }
                                                    break;
                                            }



                                        }
                                        src.IO.WriteString("INIT:CONT ON\n");
                                    }
                                    if (chBaAvrg.Checked)
                                    {
                                        datagetks = datagetks / 10;
                                    }

                                    dataFreq[cntrScanFreq] = getScanFreq;
                                    dataPower[cntrScanFreq] = datagetks;

                                    cntrScanFreq += 1;
                                    //YMARKData = YMARKData.Replace(".", ",");
                                    YMARKData = datagetks.ToString("0.0000");
                                    YMARKData = YMARKData.Replace(".", ",");
                                    XMARKData = getScanFreq.ToString();
                                    richTextBoxOut.AppendText(XMARKData + " : " + YMARKData +"\n");//+ "\n"
                                        

                                   
                                    //Thread.Sleep(500);
                                    //--------------------------------------------
                                    getScanFreq += shagScanFreq; // nex freq

                                        if (getScanFreq <= stopScanFreq)
                                        {

                                            if (getScanFreq == stopScanFreq)
                                            {
                                                getScanFreq -= 1;
                                            }

                                            int tempScanFreq = getScanFreq * 1000;
                                            Send_CMD_FRCH(tempScanFreq);
                                            timerWaitStOK.Start();

                                    }
                                        else
                                        {
                                            SaveExcel(dataFreq, dataPower);
                                            startscan = 0;
                                            richTextBoxOut.AppendText(" End_Scan!\n ");
                                        }
                                }
                                else
                                {
                                    startscan = 0;
                                    richTextBoxOut.AppendText(" CRC_ERROR!!!\n ");
                                }
                            }

                        }
                        else if (bBufRx[1] == 0x04)
                        {
                            int iErr = bBufRx[3] & 0x08;


                            listBoxErr_fps3.Items.Clear();

                            if ((bBufRx[3] & 0x01) == 0x01)
                            {

                                listBoxErr_fps3.Items.Add("нет LD_LMX1");
                            }
                            if ((bBufRx[3] & 0x02) == 0x02)
                            {

                                listBoxErr_fps3.Items.Add("нет LD_LMX2");
                            }
                            if ((bBufRx[3] & 0x04) == 0x04)
                            {

                                listBoxErr_fps3.Items.Add("нет LD_LMX3");
                            }
                            if ((bBufRx[3] & 0x08) == 0x08)
                            {

                                listBoxErr_fps3.Items.Add("нет LD_LMX4");
                            }
                            if ((bBufRx[3] & 0x010) == 0x010)
                            {

                                listBoxErr_fps3.Items.Add(" ");
                            }
                            if ((bBufRx[3] & 0x020) == 0x020)
                            {

                                listBoxErr_fps3.Items.Add("нет ВЧ1 (L1_L2) 2500-6000");
                            }
                            if ((bBufRx[3] & 0x040) == 0x040)
                            {

                                listBoxErr_fps3.Items.Add("нет ВЧ2");
                            }
                            if ((bBufRx[3] & 0x080) == 0x080)
                            {

                                listBoxErr_fps3.Items.Add("нет ВЧ3 100-500 /500-2500");
                            }

                        }
                        else if (bBufRx[1] == 0x41)
                        {
                            //ФПС3
                            for (int j = 0; j < iLen / lengthOfRow; j++)
                            {
                                id = Convert.ToInt32(bBufRx[3 + 10 * j]);
                                fr1 = Byte2UIintConverter(bBufRx[4 + 10 * j], bBufRx[5 + 10 * j], bBufRx[6 + 10 * j]);
                                fr2 = Byte2UIintConverter(bBufRx[7 + 10 * j], bBufRx[8 + 10 * j], bBufRx[9 + 10 * j]);
                                k1 = Convert.ToInt32(bBufRx[10 + 10 * j]);
                                k2 = Convert.ToInt32(bBufRx[11 + 10 * j]);
                                status = Convert.ToInt32(bBufRx[12 + 10 * j]);
                                DrawToTable(id, fr1, fr2, k1, k2, status);
                            }
                        }

                        CallBackMy.callbackEventHandlerAq(sb_hex);

                    }
                };
                if (InvokeRequired)
                {
                    Invoke(action);
                }
                else
                {
                    action();
                }
            }
        }

        private int Byte2UIintConverter(byte b3, byte b2, byte b1)
        {
            int r = b1 << 16 | b2 << 8 | b3;
            return r;
        }

        private void DrawToTable(int id, int fr1, int fr2, int k1, int k2, int status)
        {
            try
            {
                int rowID = 0;
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    int test = Int32.Parse(dataGridView1.Rows[row.Index].Cells[0].Value.ToString());
                    if (test == id)
                    {
                        rowID = row.Index;
                        break;
                    }
                }

                var rowToUpdate = dataGridView1.Rows[rowID].Cells;
                rowToUpdate[1].Value = fr1;
                rowToUpdate[2].Value = fr2;
                rowToUpdate[3].Value = k1;
                rowToUpdate[4].Value = k2;
                rowToUpdate[5].Value = status;
            }
            catch (Exception exp){ }
        }

     
        // вызывается по событию пользователя, таймера или последовательно порта
 

        private void timerWaitStOK_Tick(object sender, EventArgs e)
        { // ожидаем ответ по тому кто инициировал событие 
            richTextBoxOut.AppendText(" repeat send\n ");

            if (cntWaitScanFreq > 0)
            {
                cntWaitScanFreq -= 1;

                int tempScanFreq = getScanFreq * 1000;
                Send_CMD_FRCH(tempScanFreq);
                timerWaitStOK.Enabled = true;

            }
            else
            {
                timerWaitStOK.Enabled = false; // отключаем таймер           
            }

        }


        private void richTextBoxOut_TextChanged(object sender, EventArgs e)
        {

            richTextBoxOut.ScrollToCaret();
        }

        private void LoadCPSettings()
        {

            // загружаем данные из файла program.xml 

            if (File.Exists("settings.xml"))
            {
                using (Stream stream = new FileStream("settings.xml", FileMode.Open))
                {

                    XmlSerializer serializer = new XmlSerializer(typeof(ParamSettings));


                    TextReader reader = new StreamReader(stream);
                    // в тут же созданную копию класса ParamSettings под именем iniSet


                    // ParamSettings iniSet = serializer.Deserialize(stream) as ParamSettings;

                    ParamSettings iniSet = (ParamSettings)serializer.Deserialize(stream);

                    // и устанавливаем 


                    portName = iniSet.portName.ToString();
                    baudrate = iniSet.baudrate;/////

                    firstFr = iniSet.firstFr;
                    secondFr = iniSet.secondFr;
                    modulation = iniSet.modulation;
                    //-------------------------------------------------

                    manipulation = iniSet.manipulation;
                    deviation = iniSet.deviation;
                    timeScan =  iniSet.timeScan;
                    timRadiation = iniSet.timeRad;

                    //------------------------------------------------
                    gPS_L1_fps3IsChecked = iniSet.GPS_L1_fps3IsChecked;
                    gPS_L2_fps3IsChecked = iniSet.GPS_L2_fps3IsChecked;
                    gL_L1_fps3IsEChecked = iniSet.GL_L1_fps3IsChecked;
                    gL_L2_fps3IsEChecked = iniSet.GL_L2_fps3IsChecked;

                    dataGrid = iniSet.latestData;
                    cb_litera_fps3.Text = iniSet.litera;
                    //--------------------------------------------------------
                    SW_num = iniSet.SW_num;
                    SWRF_num = iniSet.SWRF_num;
                    //-----------------------------------------------------------------------
                    start_fr = iniSet.start_fr;
                    stop_fr = iniSet.stop_fr;
                    shag_fr = iniSet.shag_fr;
                    span_fr = iniSet.span_fr;
                    refAmp = iniSet.refAmp;
                    refOffset = iniSet.refOffset;
                    sw_devices_index = iniSet.sw_devices_index;
                    //---------------------form position-----------------------------------
                    //проверим разрешение экрана
                    Size Screen = SystemInformation.PrimaryMonitorSize;
                    //если расположение за экраном вернем в начало
                    if ((Screen.Width < iniSet.theForm_left) || (iniSet.theForm_left < 0))
                    {
                        iniSet.theForm_left = 10;
                    }
                    if ((Screen.Height < iniSet.theForm_top) || (iniSet.theForm_top < 0))
                    {
                        iniSet.theForm_top = 10;
                    }

                    if ((Screen.Width < iniSet.theForm_width) || (iniSet.theForm_width < 10))
                    {

                        iniSet.theForm_width = Screen.Width;
                    }

                    if ((Screen.Height < iniSet.theForm_height) || (iniSet.theForm_height < 10))
                    {

                        iniSet.theForm_width = Screen.Height;
                    }

                    this.StartPosition = FormStartPosition.Manual;
                    //----------------------------------------------------------------------
                    this.Size = new Size(iniSet.theForm_width, iniSet.theForm_height);
                    this.Location = new Point(iniSet.theForm_left, iniSet.theForm_top);
                    //-----------------------------------------------------------------------
                    //------------------------------------------------------------------------

                    if ((Screen.Width < iniSet.theTermForm_left) || (iniSet.theTermForm_left < 0))
                    {
                        iniSet.theTermForm_left = 10;
                    }
                    if ((Screen.Height < iniSet.theTermForm_top) || (iniSet.theTermForm_top < 0))
                    {
                        iniSet.theTermForm_top = 10;
                    }

                    if ((Screen.Width < iniSet.theTermForm_width) || (iniSet.theTermForm_width < 10))
                    {

                        iniSet.theTermForm_width = 100;
                    }

                    if ((Screen.Height < iniSet.theTermForm_height) || (iniSet.theTermForm_height < 10))
                    {

                        iniSet.theTermForm_width = 100;
                    }
                    TermForm.StartPosition = FormStartPosition.Manual;
                    //----------------------------------------------------------------------
                    TermForm.Size = new Size(iniSet.theTermForm_width, iniSet.theTermForm_height);
                    TermForm.Location = new Point(iniSet.theTermForm_left, iniSet.theTermForm_top);
                    //-----------------------------------------------------------------------

                    richTextBoxOut.AppendText("\n Скорость порта" + comboBox_baudrate.Text + "Номер порта:" + comboBox_portname.Text);
                }


            }
            else
            {
                ParamSettings iniSet = new ParamSettings();
                iniSet.portName = "COM1";
                iniSet.baudrate = 115200;

                iniSet.firstFr = "150000";
                iniSet.secondFr = "250000";
                iniSet.modulation = "----";

                SW_num = "0";
                SWRF_num = "1";

                iniSet.GPS_L1_fps3IsChecked = false;
                iniSet.GPS_L2_fps3IsChecked = false;
                iniSet.GL_L1_fps3IsChecked = false;
                iniSet.GL_L2_fps3IsChecked = false;

                iniSet.start_fr = "100000";
                iniSet.stop_fr = "500000";
                //----------------------------------------------
                iniSet.shag_fr = "100";
                iniSet.span_fr = "100000";
                iniSet.refAmp = "0";
                iniSet.refOffset = "0";
                iniSet.sw_devices_index = 0;
                //-----------------------------------------------
                iniSet.theForm_height = this.Height;
                iniSet.theForm_width = this.Width;
                iniSet.theForm_top = this.Top;
                iniSet.theForm_left = this.Left;

                iniSet.theTermForm_height = TermForm.Height;
                iniSet.theTermForm_width = TermForm.Width;
                iniSet.theTermForm_top = TermForm.Top;
                iniSet.theTermForm_left = TermForm.Left;
                //-----------------------------------------------
                // выкидываем класс iniSet целиком в файл settings.xml
                using (Stream writer = new FileStream("settings.xml", FileMode.Create))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ParamSettings));
                    serializer.Serialize(writer, iniSet);
                }

            }

        }

        private void Main_form_FormClosing(object sender, FormClosingEventArgs e)
        {
            SaveParam();
        }

        private void SaveParam()
        {
            ParamSettings iniSet = new ParamSettings();
            if (serialPort1 != null)
            {
                iniSet.portName = comboBox_baudrate.Text;
                iniSet.baudrate = Convert.ToInt32(comboBox_baudrate.Text);

                iniSet.firstFr = tb_freq1_fps3.Text;
                iniSet.secondFr = tb_freq2_fps3.Text;
                iniSet.modulation = cb_mod1_fps3.Text;
                //---------------------------------------------------------
                iniSet.manipulation = cb_man1_fps3.Text;
                iniSet.deviation = tb_dev1_fps3.Text;
                iniSet.timeScan = tb_tscan_fps3.Text;

                iniSet.timeRad = cb_tim1_fps3.Text;
                //---------------------------------------------------------
                iniSet.SW_num = txt_SW_num.Text;
                iniSet.SWRF_num = txt_SWRF_num.Text;
                iniSet.GPS_L1_fps3IsChecked = checkBox_GPS_L1_fps3.Checked;
                iniSet.GPS_L2_fps3IsChecked = checkBox_GPS_L2_fps3.Checked;
                iniSet.GL_L1_fps3IsChecked = checkBox_GL_L1_fps3.Checked;
                iniSet.GL_L2_fps3IsChecked = checkBox_GL_L2_fps3.Checked;
                iniSet.latestData = dataGrid;
                iniSet.litera = cb_litera_fps3.Text;
                //------------------------------------------------------------
                iniSet.start_fr = tb_start_scan.Text;
                iniSet.stop_fr = tb_stop_scan.Text;

                iniSet.shag_fr = tb_shag_scan.Text;
                iniSet.span_fr = tbSpanFreq.Text;
                iniSet.refAmp = tb_ref_amp.Text;
                iniSet.refOffset = tb_ref_offset.Text;
                iniSet.sw_devices_index = cb_ks_devices.SelectedIndex;
        //------------------------------------------------------------
                iniSet.theForm_height = this.Height;
                iniSet.theForm_width = this.Width;
                iniSet.theForm_top = this.Location.Y;
                iniSet.theForm_left = this.Location.X;

                iniSet.theTermForm_height = TermForm.Height;
                iniSet.theTermForm_width = TermForm.Width;
                iniSet.theTermForm_top = TermForm.Location.Y;
                iniSet.theTermForm_left = TermForm.Location.X;
                //------------------------------------------------------------
            }

            // выкидываем класс iniSet целиком в файл program.xml
            using (Stream writer = new FileStream("settings.xml", FileMode.Create))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(ParamSettings));
                serializer.Serialize(writer, iniSet);
            }
        }

        private void SaveExcel(double[] dataFirst, double[] dataSecond)
        {
            try
            {
                //запись в Exel
                using (var excel = new ExcelPackage())
                {

                    var ws = excel.Workbook.Worksheets.Add("MyWorksheet");
                    if (dataFirst.Length != 0)
                    {
                        for (int i = 0; i < dataFirst.Length; i++)
                        {
                            ws.Cells["A" + (i + 1).ToString()].Value = dataFirst[i];
                        }
                    }
                    if (dataSecond.Length != 0)
                    {
                        for (int i = 0; i < dataSecond.Length; i++)
                        {
                            ws.Cells["B" + (i + 1).ToString()].Value = dataSecond[i];
                        }
                    }
                    excel.SaveAs(new FileInfo("test.xlsx"));
                }
            }
            catch (Exception e) { }
        }

        //TODO: refactor this copypaste

        private void cb_litera_fps3_SelectedIndexChanged(object sender, EventArgs e)
        {
            int i;
            switch (cb_litera_fps3.SelectedIndex)
            {
                case 0:
                case 1:
                case 3:
                    checkBox_GPS_L1_fps3.Enabled = false;
                    checkBox_GPS_L2_fps3.Enabled = false;
                    checkBox_GL_L1_fps3.Enabled = false;
                    checkBox_GL_L2_fps3.Enabled = false;
                    flagSelGNSS = false;
                    tb_freq1_fps3.Enabled = true;
                    tb_freq2_fps3.Enabled = true;
                    cb_mod1_fps3.Enabled = true;
                    tb_dev1_fps3.Enabled = true;
                    cb_man1_fps3.Enabled = true;
                    tb_tscan_fps3.Enabled = true;
                    cb_tim1_fps3.Enabled = true;

                    break;
                case 2:

                    checkBox_GPS_L1_fps3.Enabled = true;
                    checkBox_GPS_L2_fps3.Enabled = true;
                    checkBox_GL_L1_fps3.Enabled = true;
                    checkBox_GL_L2_fps3.Enabled = true;
                    flagSelGNSS = true;
                    tb_freq1_fps3.Enabled = false;
                    tb_freq2_fps3.Enabled = false;
                    cb_mod1_fps3.Enabled = false;
                    tb_dev1_fps3.Enabled = false;
                    cb_man1_fps3.Enabled = false;
                    tb_tscan_fps3.Enabled = false;
                    cb_tim1_fps3.Enabled = false;

                    break;


            }
        }

        private void bt_set_param_FPS3_Click(object sender, EventArgs e)
        {
            SaveParam();
            StringBuilder sb_hex = new StringBuilder();
            int i, iFreqTmp;
            int addrLit;
            int iNum, iLen;
            uint CRC = 0;
            bool flg_Error_Param;
            byte[] arr = { 0, 0, 0, 0 };
            int GPS_GLONASS;
            UInt32 ui32Tmp;
            byte[] bBufTx;
            iNum = 256;
            bBufTx = new byte[iNum]; // задаем длину буфера
            flg_Error_Param = false;

            //------------- Адрес формирователя ---------------------------------
            i = 0;
            addrLit = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);// адрес

            if (checkBoxService.Checked)
            {
                bBufTx[0] = 0x04; // адрес АРМА
                bBufTx[1] = 0x05;// адрес УУ
                bBufTx[2] = 0x47;//команда ретрансляции
                bBufTx[3] = 0x00;//счетчик кодограмм            
                bBufTx[4] = 8;//длина пакета
                i = 5;

            }

            //-------------- Команда -------------------------------------------- 

            if (flagSelGNSS) // если выбрана третья литера
            {
                //-----1----------
                bBufTx[i] = (byte)addrLit;
                CRC = bBufTx[i];
                i++;

                bBufTx[i] = 0x34;   // команда GPS & GLONASS
                CRC += bBufTx[i];
                i++;
                bBufTx[i] = 1; // длина пакета
                CRC += bBufTx[i];
                i++;
                //------- Выбор диапазона -----------
                GPS_GLONASS = 0;
                if (checkBox_GPS_L1_fps3.Checked)
                {
                    GPS_GLONASS = (GPS_GLONASS | 0x01);
                }
                if (checkBox_GPS_L2_fps3.Checked)
                {
                    GPS_GLONASS = (GPS_GLONASS | 0x02);
                }
                if (checkBox_GL_L1_fps3.Checked)
                {
                    GPS_GLONASS = (GPS_GLONASS | 0x04);
                }
                if (checkBox_GL_L2_fps3.Checked)
                {
                    GPS_GLONASS = (GPS_GLONASS | 0x08);
                }
                bBufTx[i] = (byte)GPS_GLONASS;
                CRC += bBufTx[i];
                i++;
            }
            else // если выбраны другие литеры
            {
                //-----1----------
                bBufTx[i] = (byte)addrLit;
                CRC = bBufTx[i];
                i++;
                //2
                bBufTx[i] = 0x28; // команда
                CRC += bBufTx[i];
                i++;
                // длина пакета //3
                bBufTx[i] = 8;
                CRC += bBufTx[i];
                i++;
                // количество источников
                bBufTx[i] = 1; //4
                CRC += bBufTx[i];
                i++;

                iFreqTmp = Convert.ToInt32(tb_freq1_fps3.Text);
                ui32Tmp = Convert.ToUInt32(tb_freq1_fps3.Text);
                arr = BitConverter.GetBytes(ui32Tmp);
                bBufTx[i] = arr[0];// код частоты //5
                CRC += bBufTx[i];
                i++;
                bBufTx[i] = arr[1];// код частоты //6
                CRC += bBufTx[i];
                i++;
                bBufTx[i] = arr[2];// код частоты //7
                CRC += bBufTx[i];
                i++;
                //--------------модуляция----------------------------

                bBufTx[i] = Convert.ToByte(cb_mod1_fps3.SelectedIndex);
                CRC += bBufTx[i];//8
                i++;

                if (cb_mod1_fps3.SelectedIndex == 4) // если выбран ЛЧМ
                {

                    // проверяем  частоту девиации 10 кГц - 127 кГц / 1МГц - 120МГц
                    try
                    {
                        iFreqTmp = Convert.ToInt32(tb_dev1_fps3.Text);
                        if (iFreqTmp >= 10 && iFreqTmp <= 120)
                        {
                            flg_Error_Param = false;

                        }
                        else if (iFreqTmp >= 1000 && iFreqTmp <= 120000)
                        {
                            iFreqTmp = iFreqTmp / 1000;
                            iFreqTmp = iFreqTmp | 0x80;

                        }
                        else
                        {
                            flg_Error_Param = true;
                            MessageBox.Show("Частота девиации выходит за границы диапазона: 10 кГц - 127 кГц / 1МГц - 120МГц", "Error");
                            return;
                        }

                        bBufTx[i] = Convert.ToByte(iFreqTmp);//9
                        CRC += bBufTx[i];
                        i++;
                        // проверяем  выбранную частоту манипуляции 1 - 200кГц
                        iFreqTmp = Convert.ToInt32(tb_tscan_fps3.Text);

                        if (iFreqTmp >= 1 && iFreqTmp < 200)
                        {
                            flg_Error_Param = false;

                        }
                        else
                        {
                            flg_Error_Param = true;
                            MessageBox.Show("Частота манипуляции выходит за границы диапазона: 1 кГц - 200 кГц", "Error");
                            return;
                        }

                        bBufTx[i] = Convert.ToByte(iFreqTmp);
                        CRC += bBufTx[i];
                        i++;
                        bBufTx[i] = Convert.ToByte(cb_tim1_fps3.SelectedIndex);
                        CRC += bBufTx[i];
                        i++;
                    }

                    catch (System.Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка преобразования!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    }

                }
                else if (cb_mod1_fps3.SelectedIndex == 5)
                {
                    // проверяем  частоту девиации 10 кГц - 127 кГц / 1МГц - 120МГц
                    try
                    {
                        iFreqTmp = Convert.ToInt32(tb_dev1_fps3.Text);

                        if (iFreqTmp >= 1000 && iFreqTmp <= 160000)
                        {
                            iFreqTmp = iFreqTmp / 1000;
                        }
                        else
                        {
                            flg_Error_Param = true;
                            MessageBox.Show("Частота девиации выходит за границы диапазона: 1МГц - 160МГц", "Error");
                            return;
                        }

                        bBufTx[i] = Convert.ToByte(iFreqTmp);//9
                        CRC += bBufTx[i];
                        i++;
                        // проверяем  выбранную частоту манипуляции 1 - 200кГц
                        iFreqTmp = Convert.ToInt32(tb_tscan_fps3.Text);

                        if (iFreqTmp >= 1 && iFreqTmp < 200)
                        {
                            flg_Error_Param = false;

                        }
                        else
                        {
                            flg_Error_Param = true;
                            MessageBox.Show("Частота манипуляции выходит за границы диапазона: 1 кГц - 200 кГц", "Error");
                            return;
                        }

                        bBufTx[i] = Convert.ToByte(iFreqTmp);
                        CRC += bBufTx[i];
                        i++;
                        bBufTx[i] = Convert.ToByte(cb_tim1_fps3.SelectedIndex);
                        CRC += bBufTx[i];
                        i++;
                    }

                    catch (System.Exception ex)
                    {
                        MessageBox.Show(ex.Message, "Ошибка преобразования!", MessageBoxButtons.OK, MessageBoxIcon.Error, MessageBoxDefaultButton.Button1);
                    }
                }
                else
                {
                    //--------------если выбрана не ЛЧМ модуляция-------------
                    bBufTx[i] = 0; // девиации нет
                    CRC += bBufTx[i];
                    i++;
                    bBufTx[i] = Convert.ToByte(cb_man1_fps3.SelectedIndex);
                    CRC += bBufTx[i];
                    i++;
                    bBufTx[i] = Convert.ToByte(cb_tim1_fps3.SelectedIndex);
                    CRC += bBufTx[i];
                    i++;
                    //}

                    //}
                }
            }

            bBufTx[i] = Convert.ToByte(CRC % 255);
            i++;
            iLen = i; // длина пакета + заголовок
            //


            if (checkBoxService.Checked)
            {
                bBufTx[4] = (byte)(i -5);//длина пакета + CRC
            }
            //-------------------Отправка сообщения--------------------------------


            if (!flg_Error_Param)
            {

                serialPort1.Write(bBufTx, 0, iLen); // отправляем данные на плату


                if (checkBox_visibleTxRx.Checked == true)
                {
                    for (i = 0; i < iLen; i++)
                    {
                        sb_hex.Append(bBufTx[i].ToString("X2") + " ");

                    }
                    CallBackMy.callbackEventHandlerLY(sb_hex);
                }
            }
        }

        private void bt_get_status_fps3_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            uint CRC = 0;

            int iLen;
            iNum = 15;

            bBufTx = new byte[iNum];
            i = 0;
            if (checkBoxService.Checked)
            {
                bBufTx[0] = 0x04; // адрес АРМА
                bBufTx[1] = 0x05;// адрес УУ
                bBufTx[2] = 0x47;//команда ретрансляции
                bBufTx[3] = 0x00;//счетчик кодограмм            
                bBufTx[4] = 8;//длина пакета
                i = 5;
            }


            bBufTx[i] = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);
            CRC = bBufTx[i];
            i++;
            bBufTx[i] = 0x04; // запросить состояние
            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = 0x00;
            i++;
            bBufTx[i] = Convert.ToByte(CRC % 255);
            i++;
            iLen = i;

            if (checkBoxService.Checked)
            {
                bBufTx[4] = (byte)(i - 5);//длина пакета + CRC
            }
            serialPort1.Write(bBufTx, 0, iLen);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iLen; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }

        }

        private void bt_power_off_FPS3_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            uint CRC = 0;
            int iLen;
            iNum = 15;

            bBufTx = new byte[iNum];
            i = 0;
            if (checkBoxService.Checked)
            {
                bBufTx[0] = 0x04; // адрес АРМА
                bBufTx[1] = 0x05;// адрес УУ
                bBufTx[2] = 0x47;//команда ретрансляции
                bBufTx[3] = 0x00;//счетчик кодограмм            
                bBufTx[4] = 8;//длина пакета
                i = 5;
            }
           

            bBufTx[i] = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);

            CRC = bBufTx[i];
            i++;
            bBufTx[i] = 0x24;
            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = 0x01;
            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = Convert.ToByte(cb_literaCH_fps3.SelectedIndex + 1);
            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = Convert.ToByte(CRC % 255);
            i++;
            iLen = i;

            if (checkBoxService.Checked)
            {
                bBufTx[4] = (byte)(i - 5);//длина пакета + CRC
            }

            serialPort1.Write(bBufTx, 0, iLen);




            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iLen; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }
        }

        private void cb_mod1_fps3_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (cb_mod1_fps3.SelectedIndex)
            {
                case 0:
                   // SaveModulationParam();

                    //tb_dev1_fps3.Text = "----";
                    //tb_tscan_fps3.Text = "----";
                    //cb_man1_fps3.Items.Clear();
                    //cb_man1_fps3.Items.Add("----");
                    //cb_man1_fps3.Text = "----";
                    tb_dev1_fps3.Enabled = false;
                    tb_tscan_fps3.Enabled = false;
                    cb_man1_fps3.Enabled = false;
                    break;
                case 1:
                case 2:
                case 3:
                  //  SaveModulationParam();

                   // tb_dev1_fps3.Text = "----";
                    //tb_tscan_fps3.Text = "----";
                    //
                    tb_dev1_fps3.Enabled = false;
                    tb_tscan_fps3.Enabled = false;
                    cb_man1_fps3.Enabled = true;

                    //cb_man1_fps3.Items.Clear();
                    //cb_man1_fps3.Items.Add("----");
                    //cb_man1_fps3.Items.Add("0,08");
                    //cb_man1_fps3.Items.Add("0,1");
                    //cb_man1_fps3.Items.Add("0,133");
                    //cb_man1_fps3.Items.Add("0,2");
                    //cb_man1_fps3.Items.Add("0,5");
                    //cb_man1_fps3.Items.Add("1,0");
                    //cb_man1_fps3.Items.Add("5,0");
                    //cb_man1_fps3.Items.Add("10,0");
                    //cb_man1_fps3.Items.Add("20,0");
                    //cb_man1_fps3.Items.Add("40,0");
                    //cb_man1_fps3.Items.Add("80,0");
                    //cb_man1_fps3.Items.Add("400,0");

                    //cb_man1_fps3.Text = "0,2";
                    break;
                case 4:
                    //
                    //tb_dev1_fps3.Text = "10";
                    //tb_tscan_fps3.Text = "100";
                    tb_dev1_fps3.Enabled = true;
                    tb_tscan_fps3.Enabled = true;
                    cb_man1_fps3.Enabled = false;
                    //
                    //cb_man1_fps3.Items.Clear();
                    //cb_man1_fps3.Items.Add("----");
                    //cb_man1_fps3.Text = "----";

                   // LoadModulationParam();
                    break;

            }
        }

        private void SaveModulationParam()
        {
            curModulationParams.Dev = tb_dev1_fps3.Text;
            curModulationParams.ManIndx = cb_man1_fps3.SelectedIndex;
            curModulationParams.TScan = tb_tscan_fps3.Text;
            curModulationParams.TCode = cb_tim1_fps3.Text;
        }

        private void LoadModulationParam()
        {
            tb_dev1_fps3.Text = curModulationParams.Dev;
            tb_tscan_fps3.Text = curModulationParams.TScan;
            cb_tim1_fps3.Text = curModulationParams.TCode;
        }



        private void bt_GetAmp_fps3_Click(object sender, EventArgs e)
        {

        }


        private void bt_Get_Ver_FPS2_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            int iLen;
            uint CRC = 0;

            iNum = 15;

            bBufTx = new byte[iNum];

            i = 0;
            if (checkBoxService.Checked)
            {
                bBufTx[0] = 0x04; // адрес АРМА
                bBufTx[1] = 0x05;// адрес УУ
                bBufTx[2] = 0x47;//команда ретрансляции
                bBufTx[3] = 0x00;//счетчик кодограмм            
                bBufTx[4] = 8;//длина пакета
                i = 5;
            }


            bBufTx[i] = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);
            CRC = bBufTx[i];
            i++;
            bBufTx[i] = 0x43; // запросить версию программы
            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = 0x00; // количество данных
            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = Convert.ToByte(CRC % 255);
            i++;
            iLen = i;

            if (checkBoxService.Checked)
            {
                bBufTx[4] = (byte)(i - 5);//длина пакета + CRC
            }

            serialPort1.Write(bBufTx, 0, iLen);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iLen; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }
        }

        private void Bt_power_off_All_FPS3_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;
            uint CRC = 0;
            int iLen;
            iNum = 15;

            bBufTx = new byte[iNum];
            i = 0;
            if (checkBoxService.Checked)
            {
                bBufTx[0] = 0x04; // адрес АРМА
                bBufTx[1] = 0x05;// адрес УУ
                bBufTx[2] = 0x47;//команда ретрансляции
                bBufTx[3] = 0x00;//счетчик кодограмм            
                bBufTx[4] = 8;//длина пакета
                i = 5;
            }

            int addrLit = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);

            if (addrLit == 0x13) // если выбрана третья литера
            {
                addrLit = 0x12;

            }

            bBufTx[i] = (byte)addrLit;
            CRC = bBufTx[i];
            i++;
            bBufTx[i] = 0x24;
            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = 0x01;
            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = 0xFF;
            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = Convert.ToByte(CRC % 255);
            i++;
            iLen = i;

            if (checkBoxService.Checked)
            {
                bBufTx[4] = (byte)(i - 5);//длина пакета + CRC
            }
            serialPort1.Write(bBufTx, 0, iLen);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iLen; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }
        }


        private void FillDataGreed()
        {
            dataGridView1.ScrollBars = ScrollBars.Vertical;
            VerticalScroll.Visible = true;

            dataGridView1.DataSource = dataGrid;
            dataGridView1.RowHeadersWidth = 25;
            dataGridView1.Columns[0].Width = 30;
            dataGridView1.Columns[1].Width = 90;
            dataGridView1.Columns[2].Width = 90;
            dataGridView1.Columns[3].Width = 65;
            dataGridView1.Columns[4].Width = 65;
            dataGridView1.Columns[5].Width = 65;

            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {

                column.SortMode = DataGridViewColumnSortMode.Automatic;
            }

        }

        private void button_Write_Params(object sender, EventArgs e)
        {
            SaveParam();
            int dataLength;
            int bytesInDT = 9;
            byte frByte;
            byte scByte;
            byte thByte;
            byte idK1K2;
            List<int> rowIndexs = new List<int>();
            List<byte> byteList = new List<byte>();
            List<byte> mainByteList = new List<byte>();
            List<byte> subList = new List<byte>();
            StringBuilder sb_hex = new StringBuilder();
            DataGridViewCellCollection curCells = null;

            try
            {
                //------------- Адрес формирователя --------------------------------
                int i;
                int addrLit;
                int iLen;
                uint CRC = 0;


                i = 0;
                addrLit = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);// адрес

                if (checkBoxService.Checked)
                {
                    mainByteList.Add(0x04); // адрес АРМА
                    mainByteList.Add(0x05);// адрес УУ
                    mainByteList.Add(0x47);//команда ретрансляции
                    mainByteList.Add(0x00);//счетчик кодограмм            
                    mainByteList.Add(8);//длина пакета
                    i = 5;
                }

                //-----1----------
                mainByteList.Add((byte)addrLit);
                CRC = mainByteList[i];
                i++;
                //-------------- Команда -------------------------------------------- 
                //2
                mainByteList.Add(0x40); // команда
                CRC += mainByteList[i];
                i++;


                var selectedCells = dataGridView1.SelectedCells;
                foreach (DataGridViewCell item in selectedCells)
                    if (!rowIndexs.Contains(item.RowIndex))
                        rowIndexs.Add(item.RowIndex);

                // длина пакета //3
                //dataLength = bytesInDT * dataGridView1.SelectedRows.Count;
                dataLength = bytesInDT * rowIndexs.Count;
                mainByteList.Add(Convert.ToByte(dataLength));
                CRC += mainByteList[i];
                i++;

                rowIndexs.Reverse();
                foreach (int rowIndex in rowIndexs)
                {
                    foreach (DataGridViewRow row in dataGridView1.Rows)
                    {
                        if (row.Index == rowIndex)
                        {
                            curCells = row.Cells;
                            for (int j = 0; j < curCells.Count; j++)
                            {
                                if (j == 0 || j == 3 || j  == 4 )
                                {
                                    idK1K2 = Convert.ToByte(curCells[j].Value);
                                    byteList.Add(idK1K2);
                                }
                                if ( j  == 1 || j  == 2 )
                                {
                                    FromUInt(Convert.ToUInt32(curCells[j].Value), out frByte, out scByte, out thByte);
                                    byteList.Add(frByte);
                                    byteList.Add(scByte);
                                    byteList.Add(thByte);
                                }
                            }
                        }
                    }
                    subList.AddRange(byteList);
                    byteList.Clear();
                }

                foreach (byte curByte in subList)
                {
                    CRC += curByte;
                    i++;
                }

                mainByteList.AddRange(subList);

                mainByteList.Add(Convert.ToByte(CRC % 255));
                i++;
                iLen = i; // длина пакета + заголовок


                if (checkBoxService.Checked)
                    mainByteList[4] = (byte)(i -5);//длина пакета + CRC

                //-------------------Отправка сообщения--------------------------------

                //final array
                byte[] bBufDT = mainByteList.ToArray();

                serialPort1.Write(bBufDT, 0, iLen);

                if (checkBox_visibleTxRx.Checked == true)
                {
                    for (int j = 0; j < iLen; j++)
                    {
                        sb_hex.Append(bBufDT[j].ToString("X2") + " ");
                    }
                    CallBackMy.callbackEventHandlerLY(sb_hex);
                }
            }
            catch (Exception exp) { }
        }

        static void FromUInt(uint number, out byte byte1, out byte byte2, out byte byte3)
        {
            byte[] arr = BitConverter.GetBytes(number);
            byte1 = arr[0];// код частоты //5
            byte2 = arr[1];// код частоты //6
            byte3 = arr[2];// код частоты //7

            //byte2 = (byte)(number >> 8);
            //byte1 = (byte)(number & 255);
        }

        private void ReverseDGVRows(DataGridView dgv)
        {
            List<DataGridViewRow> rows = new List<DataGridViewRow>();
            rows.AddRange(dgv.Rows.Cast<DataGridViewRow>());
            rows.Reverse();
            dgv.Rows.Clear();
            dgv.Rows.AddRange(rows.ToArray());
        }

        //
        private void dataGridView1_CellValidating(object sender, DataGridViewCellValidatingEventArgs e)
        {
            int testInt;
            try
            {
                 if (int.TryParse(dataGridView1.EditingControl.Text, out testInt))
                {
                    if (e.ColumnIndex == 0)
                        idIsTaken = dataGrid.Where(x => x.Id == Convert.ToUInt32(dataGridView1.CurrentCell.EditedFormattedValue)).Any();
                }
                else 
                {
                    dataGridView1.EditingControl.Text = "0";
                }
            }
            catch { }
        }


        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            curRow = e.RowIndex;
        }



        private void dataGridView1_HeaderClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            var sortedListInstance = new BindingList<Info>();
            switch (e.ColumnIndex)
            {
                case 0:
                    sortedListInstance = new BindingList<Info>(dataGrid.OrderBy(x => x.Id).ToList());
                    break;
                case 1:
                    sortedListInstance = new BindingList<Info>(dataGrid.OrderBy(x => x.Fr1).ToList());
                    break;
                case 2:
                    sortedListInstance = new BindingList<Info>(dataGrid.OrderBy(x => x.Fr2).ToList());
                    break;
                case 3:
                    sortedListInstance = new BindingList<Info>(dataGrid.OrderBy(x => x.K1).ToList());
                    break;
                case 4:
                    sortedListInstance = new BindingList<Info>(dataGrid.OrderBy(x => x.K2).ToList());
                    break;
                default:
                    sortedListInstance = new BindingList<Info>(dataGrid.OrderBy(x => x.Id).ToList());
                    break;
            }
            dataGrid = sortedListInstance;
            dataGridView1.DataSource = dataGrid;
        }

        private void button_Remove_Params(object sender, EventArgs e)
        {
            try
            {
                dataGridView1.Rows.RemoveAt(curRow);
                //var rowToNul = dataGridView1.Rows[curRow].Cells;
                //for (int i = 0; i < rowToNul.Count; i++)
                //{
                //    rowToNul[i].Value = null;
                //}
            }
            catch
            {
                MessageBox.Show("Выберите строку");
            }
        }

        private void dataGridView1_IDValidating(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 0)
                {
                    var matches = dataGrid.Where(x => x.Id == Convert.ToUInt32(dataGridView1.CurrentCell.EditedFormattedValue)).Any();

                    var test1 = Convert.ToUInt32(dataGridView1.CurrentCell.EditedFormattedValue);

                    if (idIsTaken)
                    {
                        MessageBox.Show(string.Format("that id is takken: {0}", dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value));
                        dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value = "255";
                    }
                }
            }
            catch { }
        }


        private void SaveButton_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                string path = Path.GetFullPath(sfd.FileName);
                if (!path.EndsWith(".xml"))
                    path += ".xml";

                XmlSerializer xs = new XmlSerializer(typeof(BindingList<Info>));

                using (TextWriter tw = new StreamWriter(path))
                {
                    xs.Serialize(tw, dataGrid);
                }
            }
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {

            OpenFileDialog ofd = new OpenFileDialog();

            if (ofd.ShowDialog() == DialogResult.OK)
            {

                string path = Path.GetFullPath(ofd.FileName);
                XmlSerializer xs = new XmlSerializer(typeof(BindingList<Info>));

                using (var sr = new StreamReader(path))
                {
                    dataGrid = (BindingList<Info>)xs.Deserialize(sr);
                    dataGridView1.DataSource = dataGrid;
                }
            }

        }

        private void CleanUpButton_Click(object sender, EventArgs e)
        {
            dataGrid = new BindingList<Info>();
            dataGridView1.DataSource = dataGrid;
        }

        private void saveParamAtBooster_Click(object sender, EventArgs e)
        {
            int dataLength;
            List<byte> byteList = new List<byte>();
            List<byte> mainByteList = new List<byte>();
            StringBuilder sb_hex = new StringBuilder();

            try
            {
                //------------- Адрес формирователя --------------------------------
                int i;
                int addrLit;
                int iLen;
                uint CRC = 0;


                i = 0;
                addrLit = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);// адрес

                if (checkBoxService.Checked)
                {
                    mainByteList.Add(0x04); // адрес АРМА
                    mainByteList.Add(0x05);// адрес УУ
                    mainByteList.Add(0x47);//команда ретрансляции
                    mainByteList.Add(0x00);//счетчик кодограмм            
                    mainByteList.Add(8);//длина пакета
                    i = 5;
                }

                //-----1----------
                mainByteList.Add((byte)addrLit);
                CRC = mainByteList[i];
                i++;
                //-------------- Команда -------------------------------------------- 
                //2
                mainByteList.Add(0x42); // команда
                CRC += mainByteList[i];
                i++;

                dataLength = 0;
                mainByteList.Add(Convert.ToByte(dataLength));
                CRC += mainByteList[i];
                i++;

                mainByteList.Add(Convert.ToByte(CRC % 255));
                i++;
                iLen = i; // длина пакета + заголовок


                if (checkBoxService.Checked)
                {
                    mainByteList[4] = (byte)(i - 5);//длина пакета + CRC
                }
                //-------------------Отправка сообщения--------------------------------                

                //final array
                byte[] bBufDT = mainByteList.ToArray();

                serialPort1.Write(bBufDT, 0, iLen);

                if (checkBox_visibleTxRx.Checked == true)
                {
                    for (int j = 0; j < iLen; j++)
                    {
                        sb_hex.Append(bBufDT[j].ToString("X2") + " ");
                    }
                    CallBackMy.callbackEventHandlerLY(sb_hex);
                }
            }
            catch { }
        }

        private void getParams_Click(object sender, EventArgs e)
        {
            int dataLength;
            List<int> rowIndexs = new List<int>();
            List<byte> byteList = new List<byte>();
            List<byte> mainByteList = new List<byte>();
            StringBuilder sb_hex = new StringBuilder();

            try
            {
                //------------- Адрес формирователя --------------------------------
                int i;
                int addrLit;
                int iLen;
                uint CRC = 0;


                i = 0;
                addrLit = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);// адрес

                if (checkBoxService.Checked)
                {
                    mainByteList.Add(0x04); // адрес АРМА
                    mainByteList.Add(0x05);// адрес УУ
                    mainByteList.Add(0x47);//команда ретрансляции
                    mainByteList.Add(0x00);//счетчик кодограмм            
                    mainByteList.Add(8);//длина пакета
                    i = 5;
                }

                //-----1----------
                mainByteList.Add((byte)addrLit);
                CRC = mainByteList[i];
                i++;
                //-------------- Команда -------------------------------------------- 
                //2
                mainByteList.Add(0x41); // команда
                CRC += mainByteList[i];
                i++;

                var selectedCells = dataGridView1.SelectedCells;
                foreach (DataGridViewCell item in selectedCells)
                    if (!rowIndexs.Contains(Int32.Parse(dataGridView1.Rows[item.RowIndex].Cells[0].Value.ToString())))
                        rowIndexs.Add(Int32.Parse(dataGridView1.Rows[item.RowIndex].Cells[0].Value.ToString()));

                rowIndexs.Sort();
                dataLength = rowIndexs.Count();
                mainByteList.Add(Convert.ToByte(dataLength));
                CRC += mainByteList[i];
                i++;

                foreach (int rowIndex in rowIndexs)
                {
                    mainByteList.Add((byte)rowIndex);
                    CRC += mainByteList[i];
                    i++;
                }

                mainByteList.Add(Convert.ToByte(CRC % 255));
                i++;
                iLen = i; // длина пакета + заголовок

                if (checkBoxService.Checked)
                {
                    mainByteList[4] = (byte)(i - 5);//длина пакета + CRC
                }
                //-------------------Отправка сообщения--------------------------------
             
                //final array
                byte[] bBufDT = mainByteList.ToArray();

                serialPort1.Write(bBufDT, 0, iLen);

                if (checkBox_visibleTxRx.Checked == true)
                {
                    for (int j = 0; j < iLen; j++)
                    {
                        sb_hex.Append(bBufDT[j].ToString("X2") + " ");
                    }
                    CallBackMy.callbackEventHandlerLY(sb_hex);
                }
            }
            catch (Exception exp) { }
        }

        private void ButConnect_Click(object sender, EventArgs e)
        {
            string AGILENT_N9X = "";

            try
            {
                switch (cb_ks_devices.SelectedIndex)
                {
                    case 0:
                        AGILENT_N9X = "USB0::0x0957::0xFFEF::CN0604B269::0::INSTR";
                        break;
                    case 1:
                        AGILENT_N9X = "TCPIP0::10.101.25.135::hislip0::INSTR";
                        break;
                }

                //src.IO = (Ivi.Visa.Interop.IMessage)rm.Open("USB0::0x0957::0xFFEF::CN0604B269::0::INSTR", Ivi.Visa.Interop.AccessMode.NO_LOCK, 0, "");
                src.IO = (Ivi.Visa.Interop.IMessage)rm.Open(AGILENT_N9X, Ivi.Visa.Interop.AccessMode.NO_LOCK, 2000, "");

                //src.IO.Timeout = 2000;
                src.IO.WriteString("*IDN?");
                string temp = src.IO.ReadString(48);

                
                bttn_Set_param_KS.Enabled = true;
                if (bt_set_param_FPS3.Enabled)
                { 
                    bt_start_scan.Enabled = true;
                    bt_stop_scan.Enabled = true;
                    bttn_set_param_fps.Enabled = true;
                }

                bttn_seat_peak_KS.Enabled = true;
                bttn_refAmp_KS.Enabled = true;
                bttn_Get_data.Enabled = true;
                bttn_close.Enabled = true;
                bttn_setSpan.Enabled = true;
                bt_preset.Enabled = true;
                bt_ref_offset.Enabled = true;

                richTextBoxOut.AppendText("\n " + temp);


            }
            catch (Exception exp)
            {
                bttn_set_param_fps.Enabled = false;
                bttn_Set_param_KS.Enabled = false;
                bt_start_scan.Enabled = false;
                bttn_seat_peak_KS.Enabled = false;
                bttn_refAmp_KS.Enabled = false;
            }

        }

        private void Bttn_Set_KS_Click(object sender, EventArgs e)
        {
            string txCentrFreq = tb_freq1_fps3.Text + "000";
            src.IO.WriteString("SENS:FREQ:CENT " + txCentrFreq);
        }

        private void Bttn_seat_peak_KS_Click(object sender, EventArgs e)
        {
            richTextBoxOut.AppendText("\n Marker Peak Search \n");

            src.IO.WriteString("INIT:CONT 0\n");
            src.IO.WriteString("INIT:IMM;*WAI\n");



            switch (cb_ks_devices.SelectedIndex)
            {
                case 0:

                    string txData = "00";
                    while (txData != "+1")
                    {
                        src.IO.Timeout = 2000;
                        src.IO.WriteString("*OPC?");//ждем кода устройство отработает команду
                        txData = src.IO.ReadString(2);
                    }
                    src.IO.WriteString("CALC:MARK:MAX\n");
                    src.IO.WriteString("CALC:MARK:X?\n");
                    txData = src.IO.ReadString(18);// bez \n

                    src.IO.WriteString("CALC:MARK:Y?\n");

                    txData += ":" + src.IO.ReadString(19);
                    
                    richTextBoxOut.AppendText(txData);
                    break;
                case 1:
                    txData = "0\n";
                    while (txData != "1\n")
                    {
                        src.IO.Timeout = 2000;
                        src.IO.WriteString("*OPC?");//ждем кода устройство отработает команду
                        txData = src.IO.ReadString(2);
                    }

                
                    src.IO.WriteString("CALC:MARK:MAX\n");
                    src.IO.WriteString("CALC:MARK:X?\n");
                    txData = src.IO.ReadString(19);// bez \n
                    richTextBoxOut.AppendText("freq:"+txData);

                    //src.IO.WriteString("CALC:ACP:MARK:Y?\n");
                    //txData = src.IO.ReadString(19);
                    //richTextBoxOut.AppendText("ACP:" + txData);


                    //src.IO.WriteString("CALC:CHP:MARK:Y?\n");
                    //txData = src.IO.ReadString(19);
                    ////txData += ":" + src.IO.ReadString(19);
                    //richTextBoxOut.AppendText("CHP:" + txData);

                    src.IO.WriteString("CALC:MARK:Y?\n");
                    txData = src.IO.ReadString(19);
                    //txData += ":" + src.IO.ReadString(19);
                    richTextBoxOut.AppendText("MARK:" + txData);

                    txData = "0\n";
                    while (txData != "1\n")
                    {
                        src.IO.Timeout = 2000;
                        src.IO.WriteString("*OPC?");//ждем кода устройство отработает команду
                        txData = src.IO.ReadString(2);
                    }


                    break;
            }

            src.IO.WriteString("INIT:CONT ON\n");


            // src.IO.WriteString("CALC:MARK:CPE OFF");


        }

        private void Bttn_refAmp_KS_Click(object sender, EventArgs e)
        {
            string txrefAmp = lb_txrefAmp.Text;
            //src.IO.WriteString(":DISP:WIND:TRAC:Y:RLEV " + txrefAmp + " dBm");

            //src.IO.WriteString(":DISP:WIND:TRAC:Y:RLEV?\n");
            //txrefAmp = src.IO.ReadString(18);
            //richTextBoxOut.AppendText("Rlevel:" + txrefAmp);

  
            src.IO.WriteString(":DISP:WIND:TRAC:Y:RLEV " + txrefAmp + " dBm\n");

            src.IO.WriteString(":DISP:WIND:TRAC:Y:RLEV?\n");
            txrefAmp = src.IO.ReadString(18);
            richTextBoxOut.AppendText("Rlevel:" + txrefAmp);
        }

        private void Bttn_close_Click(object sender, EventArgs e)
        {
            src.IO.Clear();
            src.IO.Close();
            bttn_set_param_fps.Enabled = false;
            bttn_Set_param_KS.Enabled = false;
            bt_start_scan.Enabled = false;
            bttn_seat_peak_KS.Enabled = false;
            bttn_refAmp_KS.Enabled = false;
            bttn_Get_data.Enabled = false;
            bttn_close.Enabled = false;
            bttn_setSpan.Enabled = false;
            bt_preset.Enabled = false;
            bt_ref_offset.Enabled = false;
        }

        private void Bttn_Get_data_Click(object sender, EventArgs e)
        {

        }

        private void Bttn_setSpan_Click(object sender, EventArgs e)
        {
            string txSpanFreq = tbSpanFreq.Text + "000";

            src.IO.WriteString("SENS:FREQ:SPAN " + txSpanFreq);


            src.IO.WriteString("SENS:FREQ:SPAN?\n");
            txSpanFreq = src.IO.ReadString(18);
            richTextBoxOut.AppendText("OFFSetRlevel:" + txSpanFreq);
        }


        private void Label48_Click(object sender, EventArgs e)
        {

        }

        private void Send_CMD_FRCH(int setScanFreq) // freq kHz
        {

            StringBuilder sb_hex = new StringBuilder();
            int i, iFreqTmp;
            int addrLit;
            int iNum, iLen;
            uint CRC = 0;
            bool flg_Error_Param;
            byte[] arr = { 0, 0, 0, 0 };
            int ui32Tmp;
            byte[] bBufTx;
            iNum = 256;
            bBufTx = new byte[iNum]; // задаем длину буфера
            flg_Error_Param = false;

            //------------- Адрес формирователя ---------------------------------
            i = 0;
            addrLit = addrLitscan; 

            if (checkBoxService.Checked)
            {
                bBufTx[0] = 0x04; // адрес АРМА
                bBufTx[1] = 0x05;// адрес УУ
                bBufTx[2] = 0x47;//команда ретрансляции
                bBufTx[3] = 0x00;//счетчик кодограмм            
                bBufTx[4] = 8;//длина пакета
                i = 5;

            }

            //-----1----------
            bBufTx[i] = (byte)addrLit;
            CRC = bBufTx[i];
            i++;
            //-------------- Команда -------------------------------------------- 
            //2
            bBufTx[i] = 0x28; // команда
            CRC += bBufTx[i];
            i++;
            // длина пакета //3
            bBufTx[i] = 8;
            CRC += bBufTx[i];
            i++;
            // количество источников
            bBufTx[i] = 1; //4
            CRC += bBufTx[i];
            i++;

            ui32Tmp = setScanFreq;

            arr = BitConverter.GetBytes(ui32Tmp);
            bBufTx[i] = arr[0];// код частоты //5
            CRC += bBufTx[i];
            i++;
            bBufTx[i] = arr[1];// код частоты //6
            CRC += bBufTx[i];
            i++;
            bBufTx[i] = arr[2];// код частоты //7
            CRC += bBufTx[i];
            i++;
            //--------------модуляция----------------------------

            bBufTx[i] = 0;
            CRC += bBufTx[i];//8
            i++;


                //--------------если выбрана не ЛЧМ модуляция-------------
            bBufTx[i] = 0; // девиации нет
            CRC += bBufTx[i];
            i++;
            bBufTx[i] = 0;
            CRC += bBufTx[i];
            i++;
            bBufTx[i] = 0;
            CRC += bBufTx[i];
            i++;


            bBufTx[i] = Convert.ToByte(CRC % 255);
            i++;
            iLen = i; // длина пакета + заголовок
            //


            if (checkBoxService.Checked)
            {
                bBufTx[4] = (byte)(i - 5);//длина пакета + CRC
            }
            //-------------------Отправка сообщения--------------------------------


            if (!flg_Error_Param)
            {

                serialPort1.Write(bBufTx, 0, iLen); // отправляем данные на плату


                if (checkBox_visibleTxRx.Checked == true)
                {
                    for (i = 0; i < iLen; i++)
                    {
                        sb_hex.Append(bBufTx[i].ToString("X2") + " ");

                    }
                    CallBackMy.callbackEventHandlerLY(sb_hex);
                }
            }

        }

        private void Bt_shag_Click(object sender, EventArgs e)
        {

            int iFreqTmp = Convert.ToInt32(tb_freq1_fps3.Text);

            int iFreqShag =  Convert.ToInt32(tb_shag_scan.Text+"000");

            iFreqTmp = iFreqTmp + iFreqShag;

            tb_freq1_fps3.Text = iFreqTmp.ToString();
            
        }

        private void Bt_clear_richtext_Click(object sender, EventArgs e)
        {
            richTextBoxOut.Clear();
        }

        private void Bt_stop_scan_Click(object sender, EventArgs e)
        {
            timerWaitStOK.Enabled = false;
            startscan = 0;


        }

        private void Bt_start_scan_Click(object sender, EventArgs e)
        {
            addrLitscan = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);// адрес
            getScanFreq = Convert.ToInt32(tb_start_scan.Text);
            shagScanFreq = Convert.ToInt32(tb_shag_scan.Text);
            stopScanFreq = Convert.ToInt32(tb_stop_scan.Text);

            int razmerdata = (stopScanFreq - getScanFreq) / shagScanFreq + 1;

            Array.Resize(ref dataFreq, razmerdata);
            Array.Resize(ref dataPower, razmerdata);

            cntrScanFreq = 0;
            cntWaitScanFreq = 5; // cntr again send dataGrid to former

            if (getScanFreq < stopScanFreq)
            {
                startscan = 1;

                int tempScanFreq = getScanFreq * 1000;
                Send_CMD_FRCH(tempScanFreq);
                timerWaitStOK.Enabled = true; // timerWaitStOK.Enabled = false;
            }
            else
            {
                startscan = 0;
            }

        }

        private void Cb_literaCH_fps3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void Bt_DDS_clr_fps3_Click(object sender, EventArgs e)
        {


        }

        private void Bt_test_rf_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;

            uint CRC = 0;


            iNum = 255;
            bBufTx = new byte[iNum];

            //------------------------
            i = 0;

            if (checkBoxService.Checked)
            {
                bBufTx[i] = 0x04; // адрес АРМА
                i++;
                bBufTx[i] = 0x05;// адрес УУ
                i++;
                bBufTx[i] = 0x47;//команда ретрансляции
                i++;
                bBufTx[i] = 0x00;//счетчик кодограмм
                i++;
                bBufTx[i] = 4;//длина пакета
                i++;
            }


            bBufTx[i] = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);
            CRC = bBufTx[i];
            i++;
            bBufTx[i] = 0x30; // команда
            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = 0x00; // количество данных: 
            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = Convert.ToByte(CRC % 255);
            i++;
            iNum = i;
            if (checkBoxService.Checked)
            {
                bBufTx[4] = (byte)(i - 5);//длина пакета + CRC
            }
           
            serialPort1.Write(bBufTx, 0, i);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iNum; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }
        }

        private void bt_open_hex_Click(object sender, EventArgs e)
        {

        }

        private void cb_ks_devices_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void bt_preset_Click(object sender, EventArgs e)
        {
            
            switch (cb_ks_devices.SelectedIndex)
            {
                case 0:


                case 1:
                    src.IO.WriteString(":SYSTem:PRESet\n");

                    break;
            }

        }

        private void bt_ref_offset_Click(object sender, EventArgs e)
        {
            string txrefOffset = tb_ref_offset.Text;

            switch (cb_ks_devices.SelectedIndex)
            {
                case 0:
                //break;


                case 1:

                    //--------------------------------
                    //src.IO.WriteString(":DISPlay:WINDow:TRACe:Y:RLEVel:OFFSet?\n");
                    //txrefOffset = src.IO.ReadString(18);
                    //richTextBoxOut.AppendText("OFFSetRlevel:" + txrefOffset);
                    src.IO.WriteString(":DISPlay:WINDow:TRACe:Y:RLEVel:OFFSet " + txrefOffset+ "\n");
                    //--------------------------------
                    src.IO.WriteString(":DISPlay:WINDow:TRACe:Y:RLEVel:OFFSet?\n");
                    txrefOffset = src.IO.ReadString(18);
                    richTextBoxOut.AppendText("OFFSetRlevel:" + txrefOffset);


                    // src.IO.WriteString(":CORR:SA:GAIN -10\n");
                    // src.IO.WriteString(":CORR:SA:GAIN " + txrefOffset + "\n");
                    break;
            }

        }

        private void bt_set_switch_Click(object sender, EventArgs e)
        {
            byte[] bBufTx;
            StringBuilder sb_hex = new StringBuilder();
            int i;
            int iNum;

            uint CRC = 0;


            iNum = 255;
            bBufTx = new byte[iNum];

            //------------------------
            i = 0;

            if (checkBoxService.Checked)
            {
                bBufTx[i] = 0x04; // адрес АРМА
                i++;
                bBufTx[i] = 0x05;// адрес УУ
                i++;
                bBufTx[i] = 0x47;//команда ретрансляции
                i++;
                bBufTx[i] = 0x00;//счетчик кодограмм
                i++;
                bBufTx[i] = 4;//длина пакета
                i++;
            }


            bBufTx[i] = get_form_addr_FPS2(cb_litera_fps3.SelectedIndex);
            CRC = bBufTx[i];
            i++;
            bBufTx[i] = 0x31; // команда
            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = 0x02; // количество данных: 
            CRC = CRC + bBufTx[i];
            i++;
            
            bBufTx[i] = Convert.ToByte(txt_SW_num.Text); //номер SW: Cmdx
            CRC = CRC + bBufTx[i];
            i++;
            bBufTx[i] = Convert.ToByte(txt_SWRF_num.Text);  // rfCh: 
            CRC = CRC + bBufTx[i];
            i++;

            bBufTx[i] = Convert.ToByte(CRC % 255);
            i++;
            iNum = i;
            if (checkBoxService.Checked)
            {
                bBufTx[4] = (byte)(i - 5);//длина пакета + CRC
            }

            serialPort1.Write(bBufTx, 0, i);

            if (checkBox_visibleTxRx.Checked == true)
            {
                for (i = 0; i < iNum; i++)
                {
                    sb_hex.Append(bBufTx[i].ToString("X2") + " ");
                }
                CallBackMy.callbackEventHandlerLY(sb_hex);
            }
        }
    }
}
