﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text;

namespace Terminal_Control_v_1._1
{
    public static class CallBackMy // создаем подкласс для callbackEvent
    {
        public delegate void callbackEvent(StringBuilder sb_hex); // создаем динамическую строку sb_hex
        public static callbackEvent callbackEventHandlerWt;
        public static callbackEvent callbackEventHandlerLY;
        public static callbackEvent callbackEventHandlerLR;
        public static callbackEvent callbackEventHandlerLG;
        public static callbackEvent callbackEventHandlerLB;
        public static callbackEvent callbackEventHandlerAq;
        public static callbackEvent callbackEventHandlerOr;
        public static callbackEvent callbackEventHandlerPk;
        public static callbackEvent callbackEventHandlerCLC;
    }
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Main_form());
        }
    }
}
