    	enum HEX_EVENT
        {
            USER_HEX_dsPIC33_EVENT,// загрузка hex для dsPIC 
            USER_HEX_PIC32_EVENT,// загрузка hex для PIC32			
		
		}    

		enum SOURCE_EVENT
        {
            /* Application's state machine's initial state. */
			no_EVENT,
			USER_EVENT, // событие от пользователя
            TIMER_EVENT,// событие по таймеру
            SP_EVENT,// событие с приемника последовательного порта

            USER_ERASE_EVENT// команда очистки памяти
        }
		enum DATA_COMAND : byte
        {
            /* Application's state machine's initial state. */
            NOCMD = 0x00,
            READ_BOOT_INFO = 0x80,
            PROGRAM_FLASH = 0x81,
            ERASE_FLASH = 0x82,
            READ_FLASH = 0x83,
            JUMP_App = 0x84,
			SET_segADDR = 0x85
        }
		
		DataBootControl

		public struct HEX_RECORD_DEF
        {
            public uint DATA;
            public bool file_end;
            public int  fileLinesCount;
			private DATA_COMAND LASTCMD; // предыдущая программа
            private DATA_COMAND statusCMD;
            private SOURCE_EVENT srcEvent;
            private HEX_EVENT hexEvent;			
            public uint segmentAddress; // последний адрес записи блока данных программы
            public uint linearAddress;// последовательный адрес
            public uint setAddress;

            internal SOURCE_EVENT EVNT
            {
                get
                {
                    return srcEvent;
                }

                set
                {
                    srcEvent = value;
                }
            }

            internal DATA_COMAND CMD
            {
                get
                {
                    return statusCMD;
                }

                set
                {
                    statusCMD = value;
                }
            }
        }
		
		Data32BootControl(DATA_COMAND.ERASE_FLASH, HEX_RECORD.EVNT);//кладем команду и событие от чего пришла команда
		
		HEX_RECORD.EVNT = SOURCE_EVENT.no_EVENT;
		HEX_RECORD.CMD = DATA_COMAND.no_CMD;
						
					hexEVNT
						
		  HEX_RECORD.EVNT = SOURCE_EVENT.USER_HEX_PIC32_EVENT;



            if (HEX_RECORD.hexEVNT == HEX_EVENT.USER_HEX_PIC32_EVENT) // таймер сработал от события загрузки hex32 
            {
                if (HEX_RECORD.CMD == DATA_COMAND.ERASE_FLASH)
                {
                     Data32BootControl(DATA_COMAND.no_CMD,SOURCE_EVENT.TIMER_EVENT);
                }

            }		  