/*
 * File:   timers.c
 * Author: Alexandr
 *
 * Created on September 20, 2017, 4:53 PM
 */


#include "xc.h"
#include "system_cfg.h"
#include "user.h"
#include "executeCMD.h"
#include "channel.h"
#include "adl5240.h"
#include "hmc1122.h"
#include "ad9910.h"
#include "UART.h"
#include "I2C.h"
#include "synt_AD9959.h"
#include "DDS_AD9959.h"
#include "ad7995.h"
#include "XC_DATA.h"
#include "executeCMD.h"
#include "eeprom.h"
#include "LMX2572.h"
#include "LMX2592.h"
#include "softwareSPI.h"
#include "former_signal.h"
#include "eepromParam.h"


extern LITERA_param_def LITERA_param;
extern NAVIG_dt_def NAVIG_dt;

void testGNSS_init(void)
{ 
    NAVIG_dt.GPS_L2 = 1;
    NAVIG_dt.GL_L2 = 1;
    NAVIG_dt.GPS_L1 = 1;
    NAVIG_dt.GL_L1 = 1;    
    
    exeCMD34();
}

void test100_500(void)//
{ 
    setRF_100_500();
}

void test500_2500(void)//
{
    setRF_500_2500();
}

void test2500_6000(void)//
{
    setRF_2500_6000();
}

extern unsigned char addrL, addrO;

void test(void)//
{
    LITERA_param.freq = 0;//5855000;
    

//    AD9910_test ();

    
    
     if ( addrL == addrF4  )
     {
        if ((LITERA_param.freq >= 500000) && (LITERA_param.freq < 2500000))
        {
           //test500_2500();//     

        }
        else if ((LITERA_param.freq >= 2500000) && (LITERA_param.freq <= 6000000))
        {
          // test2500_6000();//      
        }     
     }
    
   LITERA_param.freq = 110000;
    
   if ((addrL == addrF1)||(addrL ==addrF3))
   {     
       // testGNSS_init();
        
    if ((LITERA_param.freq >= 100000) && (LITERA_param.freq <= 500000))
    {    
       // test100_500();
    }
        
        
   }
//    SW2DA3(SW2DA3RF1); 
//    SW2DA11(SW2DA11RF4);    
//    SW2DA14(SW2DA14RF2);
    // SW2DA21(SW2DA21RF1);
//     SW2DA29(SW2DA29RF3);     
//    SW2DA32(SW2DA32RF1); 
    

    
}
