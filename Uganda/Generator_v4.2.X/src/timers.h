/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef XC_timers_H
#define	XC_timers_H

#include <xc.h> // include processor files - each processor file is guarded.  


void  InitTimer4(void);
void  InitTimer2(void);



#endif    //XC_timers_H