#include "hmc1122.h"


#define CLK_hmc     LATAbits.LATA2
#define DATA_hmc    LATAbits.LATA3

#define LE1_hmc     LATAbits.LATA0  
#define LE2_hmc     LATAbits.LATA1  

#define LE1_hmc_seq()   {LE1_hmc = 1; Nop(); Nop(); Nop(); LE1_hmc = 0;}
#define LE2_hmc_seq()   {LE2_hmc = 1; Nop(); Nop(); Nop(); LE2_hmc = 0;}


void hmc1122_init(void)
{
////---------ADL5240--------------

    _TRISA2 = 0; // clk 
    LATAbits.LATA2 = 0;
    _TRISA3 = 0; // Data 
    LATAbits.LATA3 = 0;    
    _TRISA0 = 0; // LE 
    LATAbits.LATA0 = 0;    
    _TRISA1 = 0; // LE2 
    LATAbits.LATA1 = 0;   
    
}
    
static void hmc1122_write(unsigned char val)
{
    unsigned char i;
    val <<= 2;
    
    for(i = 0; i < 6; i++)
    {
        if(val & 0x80)
            DATA_hmc = 1;
        else
            DATA_hmc = 0;
        
        CLK_hmc = 1;
        val <<= 1;
        CLK_hmc = 0;
    }
    
    DATA_hmc = 0;
}

 void hmc1122_set_gain(unsigned char gain)
 {
     // gain <<= 1;
 
      if(gain > 63)
        gain = 0;   
    hmc1122_write(gain);

 }
 
 void hmc1122_set_gain_amp1(unsigned char gain) // 
{
    LE1_hmc_seq();
    hmc1122_set_gain(gain);
    LE1_hmc_seq();
}
 
 void hmc1122_set_gain_amp2(unsigned char gain) // 
{
    LE2_hmc_seq();
    hmc1122_set_gain(gain);
    LE2_hmc_seq();
}