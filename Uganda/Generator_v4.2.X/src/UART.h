/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef XC_UART_H
#define	XC_UART_H

#include <xc.h> // include processor files - each processor file is guarded.  


//#define        BAUDRATE    9600
//#define        BAUDRATE    57600
//#define        BAUDRATE    38400
#define        BAUDRATE    115200

#define        BRGVAL      (FCY/(16*BAUDRATE))-1

#define	AD485Rx_ON  LATDbits.LATD15 = 0; // 
#define	AD485Tx_ON  LATDbits.LATD15 = 1; // 



void Init_UART_485(void);
void Send_UART_485P(unsigned char *TX, unsigned int len);
void Send_UART_485(unsigned char TX);




#endif    //XC_UART_H