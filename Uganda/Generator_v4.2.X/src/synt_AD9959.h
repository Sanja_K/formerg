/******************************************************************************/
/*  Level #define Macros                                                */
/******************************************************************************/
#include <xc.h> 
#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#include <stdlib.h>        /* For true/false definition */
    

#ifndef AD9959_h
#define AD9959_h

#define   reference_freq  (25*MHz) // Use your crystal or reference frequency
//#define AD9959_FAST


#define _clock              (500*MHz)


#define _CS_DDS1_0           (LATDbits.LATD6 = 0)
#define _CS_DDS1_1           (LATDbits.LATD6 = 1)  

//#define _CS_DDS2_0           (LATDbits.LATD11 = 0)
//#define _CS_DDS2_1           (LATDbits.LATD11 = 1) 

#define _SCLK_0              LATGbits.LATG6 = 0  
#define _SCLK_1              LATGbits.LATG6 = 1
  
#define _IO_Update_0          LATDbits.LATD7 = 0
#define _IO_Update_1          LATDbits.LATD7 = 1  

#define _reset_DDS1_0        (LATDbits.LATD5 = 0)
#define _reset_DDS1_1        (LATDbits.LATD5 = 1)  

//#define _reset_DDS2_0        (LATDbits.LATD10 = 0)
//#define _reset_DDS2_1        (LATDbits.LATD10 = 1) 

#define _PWD_DDS1_0          (LATDbits.LATD4 = 0)
#define _PWD_DDS1_1          (LATDbits.LATD4 = 1)

//#define _PWD_DDS2_0          (LATDbits.LATD9 = 0)
//#define _PWD_DDS2_1          (LATDbits.LATD9 = 1) 

#define v_o_DATA_0           LATGbits.LATG8 = 0 //SDO2
#define v_o_DATA_1           LATGbits.LATG8 = 1
  
#define v_i_DATA             PORTGbits.RG8   /// SDI2


  typedef enum {
    DDS1   = 1,     // 
//    DDS2   = 0,     // 
  } DDS_CH_SEL;

// REGISTERS
typedef enum
{
  CSR               = 0x00,
  FR1               = 0x01,
  FR2               = 0x02,
  CFR               = 0x03,
  CTW0              = 0x04,
  CPW0              = 0x05,
  ACR               = 0x06,
  LSR               = 0x07,
  RDW               = 0x08,
  FDW               = 0x09,
  CTW1              = 0x0A,
  CTW2              = 0x0B,
  CTW3              = 0x0C,
  CTW4              = 0x0D,
  CTW5              = 0x0E,
  CTW6              = 0x0F,
  CTW7              = 0x10,
  CTW8              = 0x11,
  CTW9              = 0x12,
  CTW10             = 0x13,
  CTW11             = 0x14,
  CTW12             = 0x15,
  CTW13             = 0x16,
  CTW14             = 0x17,
  CTW15             = 0x18,
  READ              = 0x80 // not really a register
} ad9959_registers;

typedef enum{
  CH0               = 0x10,
  CH3               = 0x80,  
  CH03              = 0x90,    //два  канала 0 и 3 вместе   
  CH1               = 0x20,
  CH2               = 0x40,
  CH12              = 0x60,//два  канала 1 и 2 вместе
  CHx               = 0xF0
} ad9959_channels;

typedef enum {
    // Bit order selection (default MSB):
    MSB_First = 0x00,
    LSB_First = 0x01,
    // Serial I/O Modes (default IO2Wire):
    IO2Wire = 0x00,
    IO3Wire = 0x02,
    IO2Bit = 0x04,
    IO4Bit = 0x06,
  } CSR_Bits;

  typedef enum {    // Function Register 1 is 3 bytes wide.
    // Most significant byte:
    // Higher charge pump values decrease lock time and increase phase noise
    ChargePump0      = 0x00,
    ChargePump1      = 0x01,
    ChargePump2      = 0x02,
    ChargePump3      = 0x03,

    PllDivider       = 0x04, // multiply 4..20 by this (or shift 19)
    VCOGain          = 0x80, // Set low for VCO<160MHz, high for >255MHz

    // Middle byte:
    ModLevels2       = 0x00, // How many levels of modulation?
    ModLevels4       = 0x01,
    ModLevels8       = 0x02,
    ModLevels16      = 0x03,

    RampUpDownOff    = 0x00,
    RampUpDownP2P3   = 0x04, // Profile=0 means ramp-up, 1 means ramp-down
    RampUpDownP3     = 0x08, // Profile=0 means ramp-up, 1 means ramp-down
    RampUpDownSDIO123= 0x0C, // Only in 1-bit I/O mode

    Profile0         = 0x00,
    Profile7         = 0x07,

    // Least significant byte:
    SyncAuto         = 0x00, // Master SYNC_OUT->Slave SYNC_IN, with FR2
    SyncSoft         = 0x01, // Each time this is set, system clock slips one cycle
    SyncHard         = 0x02, // Synchronise devices by slipping on SYNC_IN signal

    // Software can power-down individual channels (using CFR[7:6])
    DACRefPwrDown    = 0x10, // Power-down DAC reference
    SyncClkDisable   = 0x20, // Don't output SYNC_CLK
    ExtFullPwrDown   = 0x40, // External power-down means full power-down (DAC&PLL)
    RefClkInPwrDown  = 0x80, // Disable reference clock input
  } FR1_Bits;

  typedef enum {
    AllChanAutoClearSweep    = 0x8000,// Clear sweep accumulator(s) on I/O_UPDATE
    AllChanClearSweep        = 0x4000,// Clear sweep accumulator(s) immediately
    AllChanAutoClearPhase    = 0x2000,// Clear phase accumulator(s) on I/O_UPDATE
    AllChanClearPhase        = 0x2000,// Clear phase accumulator(s) immediately
    AutoSyncEnable   = 0x0080,
    MasterSyncEnable = 0x0040,
    MasterSyncStatus = 0x0020,
    MasterSyncMask   = 0x0010,
    SystemClockOffset = 0x0003,      // Mask for 2-bit clock offset controls
  } FR2_Bits;

  typedef enum {
    ModulationMode   = 0xC00000,     // Mask for modulation mode
    AmplitudeModulation= 0x400000,   // Mask for modulation mode
    FrequencyModulation= 0x800000,   // Mask for modulation mode
    PhaseModulation  = 0xC00000,     // Mask for modulation mode
    SweepNoDwell     = 0x008000,     // No dwell mode
    SweepEnable      = 0x004000,     // Enable the sweep
    SweepStepTimerExt = 0x002000,    // Reset the sweep step timer on I/O_UPDATE
    DACFullScale     = 0x000300,     // 1/8, 1/4, 1/2 or full DAC current
    DigitalPowerDown = 0x000080,     // Power down the DDS core
    DACPowerDown     = 0x000040,     // Power down the DAC
    MatchPipeDelay   = 0x000020,     // Compensate for pipeline delays
    AutoclearSweep   = 0x000010,     // Clear the sweep accumulator on I/O_UPDATE
    ClearSweep       = 0x000008,     // Clear the sweep accumulator immediately
    AutoclearPhase   = 0x000004,     // Clear the phase accumulator on I/O_UPDATE
    ClearPhase       = 0x000002,     // Clear the phase accumulator immediately
    OutputSineWave   = 0x000001,     // default is cosine
  } CFR_Bits;


//public Adafruit_Sensor
/*
void  v_o_DATA( char ena );


void  SPIp_write( unsigned char reg );

unsigned char  SPIp_read( void );

void setChannels(unsigned char chan);




void AD9959_write_byte(ad9959_registers REG, unsigned char value );
void AD9959_write(ad9959_registers REG, unsigned char *buffer, int len);
//чтение регистра
unsigned char AD9959_read_byte(ad9959_registers REG);
//чтение и запись в массив данных
void AD9959_read(ad9959_registers REG, unsigned char *buffer, int len);
*/
  void AD9959_reset(void);
  uint8_t AD9959_begin(void);
  void AD9959_IOUpdate(void);
  void AD9959_set_PLL(uint8_t value) ;
  void AD9959_set_VCO(bool enable);
  int AD9959_get_VCO(void);
  void AD9959_set_Frequency(uint8_t CH, uint64_t frequency);
  uint64_t AD9959_Get_frequency(uint8_t CH);
  void AD9959_set_Phase(uint8_t CH, uint16_t phase);
  double AD9959_get_Phase(uint8_t CH);
  void AD9959_set_Amplitude(uint8_t CH, double amplitude);
  double AD9959_get_Amplitude(uint8_t CH);


void AD9959_sweep_Frequency(uint8_t CH, uint64_t frequency, bool follow );  
void AD9959_sweep_Phase(uint8_t CH, double phase, bool follow);
void AD9959_sweep_Amplitude(uint8_t CH, uint16_t amplitude, bool follow );
void AD9959_sweep_Rates(uint8_t CH, uint64_t dF1, uint8_t up_rate, uint64_t dF2, uint8_t down_rate);
void setChannels00(void);
void LFM (void);
void AD9959_set_DAC( uint8_t CH);

#endif