/* 
 * File:   executeCMD.h
 * Author: Alexandr
 *
 * Created on 28 сентября 2017 г., 11:36
 */

#ifndef EXECUTECMD_H
#define	EXECUTECMD_H



typedef struct  LITERA_PARAM_def {
	uint32_t  freq; // ������� // kHz
	unsigned char mod;// ���������
    uint32_t  dev; // �������� // kHz
    uint32_t  manip; // ����������� // kHz
    unsigned int timerad; // ������������ ���������
}LITERA_param_def;



typedef struct NAVIG_DATA_def {
    
	unsigned char GPS_L1; //GPS L1    
	unsigned char GPS_L2; //GPS L2    
    
	unsigned char GL_L1; //������� L1    
	unsigned char GL_L2; //������� L2

	unsigned char GNSS_GAIN_L1; // L1 
	unsigned char GNSS_GAIN_L2; // L2     
} NAVIG_dt_def; // �������������� ������


//------------former 1 ------------------------------------ 


//------------------CH2-----------------------------------    
#define   Freq_dds2_F1      1500     //MHz   LMX4 form 1
#define   Freq_dds2_F1_Hz   (1500*MHz)     //Hz   LMX4 form 1
#define   fDDS2_CH2_F1      490000   //kHz   DDS2 form 1
#define   Freq_get21_F1     1700     //MHz   LMX3 form 1
#define   Freq_get22_F1     1960     //MHz   LMX4 form 1
        
//------------former 2 ------------------------------------    
//------------------CH1-----------------------------------   
#define   fDDS1_CH1_F2      200000LL   //kHz   DDS1  CH1   form2 200000LL
    
#define   Freq_get11_F2     2180    //MHz   LMX 1 form2 �� ��������� 1760 MHz
#define   Freq_get12_F2     2795//2950    //MHz   LMX 1  ������ ��� ��� ������� ���� Freq_get12_F2 = Freq_get14_F2 - fDDS1_CH1_F2 = 2700-260
#define   Freq_get122_F2    2180 

#define   Freq_get13_F2     1980    //MHz    LMX 2 form2 1960
#define   Freq_get14_F2     2595    //MHz    LMX 2 form2  
#define   Freq_get144_F2    1980    //1960
//-----------------CH2------------------------------------
#define   fDDS2_CH2_F2      260000LL   //kHz   DDS2 form 2   260000LL 

#define   Freq_get21_F2     3200    //MHz    LMX 3 form 2
#define   Freq_get22_F2     3460    //MHz    LMX 4 form 2   



uint8_t exeCMD4(void);
uint8_t exeCMD24(uint8_t CHx);
uint8_t exeCMD28(void);
uint8_t exeCMD30(void);
uint8_t exeCMD31(uint8_t Cmdx, uint8_t rfCh);// test switch
uint8_t exeCMD34(void);
uint8_t exeCMD40(uint8_t status);
//char exeCMD41(void);//test
uint8_t exeCMD41(uint8_t *IDparamP, uint8_t DtCnt);
uint8_t exeCMD42(void);
uint8_t exeCMD43(void);
uint8_t AnlRxDtUrt(void);
void off_all_pwr(void);
void AddrInit(void);
void setTest(void);
void setTest2(void) ;
char exeCMD9910(void);


uint8_t setRF_100_500(void);
uint8_t setRF_500_2500(void);
uint8_t setRF_2500_6000(void);
uint8_t exeCMD34 (void);

//unsigned char calcGain( unsigned short int strt_Freq_MHz, unsigned short int stp_Freq_MHz,int gainK, unsigned char gainN);
uint8_t getGainK(uint32_t Freq);
void getGainK_gnss(uint8_t *GainL1, uint8_t *GainL2);
#endif


