/*
 * File:   executeCMD.c
 * Author: Alexandr
 *
 * Created on 28 сентября 2017 г., 9:26
 * 08.11.2017 добавлен выбор номера и адреса платы по переключателю
 * вер. 1.3
 * 16.11.2017 проверка статуса ВЧ (LD, ADC) только тогда, когда было включение ВЧ канала
 * 18.11.2017 добавлен верхний порог на АЦП
 * 13.10.2017  *dlya Kaz dobavlen LFM2 dlya shirokogo  LFM
 * 26.10.2017  podpravlen vykluchenie otdelnoy comandy DA8 100-500 
 * 26.10.2017  podpravlen vykluchenie navigacii, vykluchalo 100-500
 * 
 */

#include <xc.h>
#include "system_cfg.h"
#include "UART.h"
#include "I2C.h"
#include "synt_AD9959.h"
#include "DDS_AD9959.h"
#include "ad7995.h"
#include "XC_DATA.h"
#include "user.h"
#include "hmc1122.h"
#include "adl5240.h"
#include "executeCMD.h"
#include "eeprom.h"
#include "LMX2572.h"
#include "LMX2592.h"
#include "softwareSPI.h"
#include "former_signal.h"
#include "channel.h"
#include "eepromParam.h"
#include "test.h"
extern unsigned char addrL, addrO; // addr ustroystva

extern unsigned int cntTMR;
extern unsigned int cntTMR22;
extern unsigned int cntTMR21;

uint64_t deltaFg; // smesch geterodina


extern unsigned char   addr_Dt;
extern unsigned char   cmd_Dt;
extern unsigned char   cnt_Dt;
extern unsigned char   data_Urt[255];
extern unsigned char   err_CRC;

extern unsigned char   DDS1SWPstat, DDS2SWPstat;
extern uint8_t SetGPSL1, SetGLL1, SetGPSL2, SetGLL2;
unsigned int error_flg_Dt;

extern uint8_t setParamEEprom [cntDtEE][cntPgEE]; 

#define ADC_POROGCH1_1_L2   300  //Ch1
#define ADC_POROGCH3_1_L1   300  //Ch3

#define ADC_POROGCH2_1    292//Ch2 100- 500

#define ADC_POROGCH1_2    230 // 2500- 6000
#define ADC_POROGCH2_2    280 // 500- 2500




#define   CH1           1  
#define   CH2           2  
#define   CH13          3 

uint64_t  Freq_man[14] = {0,12500000,10000000,7518797,5000000,2000000,1000000,200000,100000,50000,25000,12500,2500};
// convert dlit KFM v chastotu

unsigned char   dl5240gain;
unsigned char   hmc1122gain;

#define offCMD_CH2_100500       1  // 100-500 
#define offCMD_CH2_5002500      2  // 500-2500
#define offCMD_CH1_25006000     3  // 2500-6000
#define offCMD_CH1_GNSS         4  //  L1
#define offCMD_All              0xFF  //  L1

unsigned char   genFlgOnCH2_L1; // L1
unsigned char   genFlgOnCH1_L2; // L2 
unsigned char   genFlgOnCH2;
unsigned char   genFlgOnCH3_100_500; // 100-500 // 500-2500
unsigned char   genFlgOnCH2_500_2500;
unsigned char   genFlgOnCH1_2500_6000; // 2500-6000


struct LITERA_DATA_def {
	unsigned char addr;
	unsigned char cmd;
	unsigned char cntDt;// dlina pack data
	unsigned char cntSrc;  // kolich istochnikov    
} LITERA_dt; //


LITERA_param_def LITERA_param;

NAVIG_dt_def NAVIG_dt;


//static unsigned char GetNLdet(unsigned char Cmdx) //
//{
//   unsigned char NLdetect;
//   unsigned char  flg_nNL1, flg_nNL2, flg_nNL3, flg_nNL4;//inv znach lock detect
//   
//   if (Cmdx == CH1) //
//   {
//      // LMX1 LMX2 CH1 
//      flg_nNL1 = 0; 
//      flg_nNL2 = 0; 
//      NLdetect = flg_nNL1&flg_nNL2;      
//   }
//   else// LMX3 LMX4 CH2       
//   {
//       flg_nNL3 = 0;    
//       flg_nNL4 = 0;  
//       NLdetect = flg_nNL3&flg_nNL4;         
//   }
//   return NLdetect;
//}
//static uint8_t GetDDSNLdet(uint8_t Cmdx) //
//{
//   uint8_t NLdetect;
//   uint8_t  flg_nNL1, flg_nNL2;// 
//   
//   if (Cmdx == CH1) // test AD9910
//   {
//      // DDS1  CH1 
//      flg_nNL1 = 0; 
//      NLdetect = flg_nNL1;      
//   } else if (Cmdx == CH13) //
//   {
//      // DDS1  CH13 
//      flg_nNL1 = 0; // test AD9959
//      NLdetect = flg_nNL1;      
//   }   
//   else// DDS2 CH2       
//   {
//      flg_nNL2 = 0; // test AD9910
//      NLdetect = flg_nNL2;         
//   }
//   return NLdetect;
//}
//----------------exeCMD4----������ ����������----------------  
// ������ ����������
uint8_t exeCMD4(void) {
     
    uint8_t status;     
    uint8_t  bittest;
    unsigned int  len_DT;
    unsigned char inf_Dt[10];     
    unsigned int  ADC_Res;     
    
    status = 0;  
    error_flg_Dt = 0x00; // sbros flaga oshibki
    bittest = 0;
    if ( addrL == addrF4 )
    { 
        //------------test--LMX1-----LMX4----------------------------
        bittest = BITREAD(XC_LD_DATA(),0);
        if (!bittest)
        {
           error_flg_Dt |= 1;
        }
        bittest = BITREAD(XC_LD_DATA(),1);
        if (!bittest)
        {
           error_flg_Dt |= (1<<1);
        }  
        bittest = BITREAD(XC_LD_DATA(),2);
        if (!bittest)
        {
           error_flg_Dt |= (1<<2);
        }   
        bittest = BITREAD(XC_LD_DATA(),3);
        if (!bittest)
        {
           error_flg_Dt |= (1<<3);
        }          
        
//        if (!BITREAD(XC_LD_DATA(),0))
//        {
//           error_flg_Dt |= 1;
//        }
//        if (!BITREAD(XC_LD_DATA(),1))
//        {
//           error_flg_Dt |= (1<<1);
//        }        
//        if (!BITREAD(XC_LD_DATA(),2))
//        {
//           error_flg_Dt |= (1<<2);
//        }   
//        if (!BITREAD(XC_LD_DATA(),3))
//        {
//           error_flg_Dt |= (1<<3);
//        }            
    //-------------POWER------------------
        
        if (genFlgOnCH1_2500_6000)
        {
            ADC_Res = fun_Read7995(RF_CH1,AD7995_ADDR); // 2500-6000
            if (ADC_Res <= ADC_POROGCH1_2)
            {
                error_flg_Dt |= (1<<5); 
            }          
        }
        else
        {
            error_flg_Dt |= (1<<5);         
        }
        
        if (genFlgOnCH2_500_2500)
        { 
            ADC_Res = fun_Read7995(RF_CH2,AD7995_ADDR); // Ch2    500- 2500      
            if (ADC_Res <= ADC_POROGCH2_2)
            {
                error_flg_Dt |= (1<<6); 
            }      
        }
        else
        {
                error_flg_Dt |= (1<<6); 
        }
    }
    else
    {
        //XC_LD_DATA();
       //-------DDS-------------------------------
        bittest = BITREAD(XC_LD_DATA(),0);
        if (!bittest)
        {
           error_flg_Dt |= 1;
        }
        bittest = BITREAD(XC_LD_DATA(),1);
        if (!bittest)
        {
           error_flg_Dt |= (1<<1);
        }        
        bittest = BITREAD(XC_LD_DATA(),2);
        if (!bittest)
        {
           error_flg_Dt |= (1<<2);
        }                  
    //-------------POWER------------------
        
        if((genFlgOnCH1_L2)||(genFlgOnCH2_L1))
        {        
            ADC_Res = fun_Read7995(RF_CH1,AD7995_ADDR); // L2 //
            if (ADC_Res <= ADC_POROGCH1_1_L2)
            {
                error_flg_Dt |= (1<<5); 
            }   
        }
        else
        {
            error_flg_Dt |= (1<<5); 
        
        }        
        //------------------------------------------------------------------
        if(genFlgOnCH2)
        {         
            ADC_Res = fun_Read7995(RF_CH2,AD7995_ADDR); // L1
            if (ADC_Res <= ADC_POROGCH3_1_L1)
            {
                error_flg_Dt |= (1<<6); 
            } 
         }
        else
        {
            error_flg_Dt |= (1<<6); 
        }                     
        //---------------------------------------------------------------------
        if(genFlgOnCH3_100_500)
        {
            ADC_Res = fun_Read7995(RF_CH3,AD7995_ADDR); // 100-500 // Ch2    500- 2500      
            if (ADC_Res <= ADC_POROGCH2_1)
            {
                error_flg_Dt |= (1<<7); 
            }              
        }
        else
        {
            error_flg_Dt |= (1<<7); 
        }
    }
           
//            ADC_Res = fun_Read7995(ADC_Ch3,AD7995_ADDR);  //          
    //-----------------------------------------------------------
    // 
       len_DT = 1;//                      
       error_flg_Dt = error_flg_Dt&0xFF;
       inf_Dt [0] = (unsigned char )error_flg_Dt;  //   

       TransmitDtUrt (0x04,inf_Dt,len_DT);     //
       return status;
}
//*****************************************************************************
// vykluchit izluchenie
//*****************************************************************************
uint8_t exeCMD24(uint8_t Cmdx) 
{ // 
    
    uint8_t status;     
    unsigned int len_DT;
    uint8_t inf_Dt[10];        
   
    status = 0;
    genFlgOnCH2 = 0;
    if(Cmdx == offCMD_CH2_100500)
    {
        genFlgOnCH3_100_500 = 0;
        adl5240_set_gain_amp3(0);
        ad9914_deinit_3();  
        SWtest100500(SWALLTEST);
        //_EN_PWR_CH2_OFF;  
        DeInit_LMX2572(syntLMX3);
        _EN_PWR_CH3_OFF;        
 
    }
    else if (Cmdx == offCMD_CH2_5002500)
    {
        genFlgOnCH2_500_2500 = 0;
        ad9910_deinit_2();
        SWtest5002500(SWALLTEST);
        _EN_PWR_CH2_OFF;      
    
    } 
    else if (Cmdx == offCMD_CH1_25006000)
    {
        genFlgOnCH1_2500_6000 = 0;
        ad9910_deinit_1();
        SWtest25006000(SWALLTEST);
        _EN_PWR_CH1_OFF;      
    
    }
    else if (Cmdx == offCMD_CH1_GNSS)
   {
        genFlgOnCH2_L1 = 0;
        genFlgOnCH1_L2 = 0;
        adl5240_set_gain_amp2(0);  // L1 
        adl5240_set_gain_amp1(0);  // L2  
        SWtestGNSS(SWALLTEST);
        Stop_AD9959();
        
//        
        SetGPSL1 = 0; SetGLL1 = 0; 
        SetGPSL2 = 0; SetGLL2 = 0;
        DDS1Mod_XC_0;      
        GNSS_OFF_XC;
// 
//        _PWD_DDS1_1;
        if (!genFlgOnCH3_100_500)
        {_EN_PWR_CH1_OFF; }
        else
        {
            Stop_AD9959();
            DeInit_LMX2572(syntLMX1);
            DeInit_LMX2572(syntLMX2);            
        }
        

   }  
    else if (Cmdx == offCMD_All)
    {
        genFlgOnCH2  = 0;
        genFlgOnCH2_L1 = 0; // L1
        genFlgOnCH1_L2 = 0; // L2 
        genFlgOnCH3_100_500 = 0; // 100-500 // 500-2500
        genFlgOnCH2_500_2500 = 0;
        genFlgOnCH1_2500_6000 = 0; // 2500-6000
        
        SetGPSL1 = 0; SetGLL1 = 0; 
        SetGPSL2 = 0; SetGLL2 = 0;  
        
//        DDS1Mod_XC_0;        

        if ((addrL == addrF1)||(addrL ==addrF3))
        {     
 
            adl5240_set_gain_amp2(0);  // L1 
            adl5240_set_gain_amp1(0);  // L2   
            adl5240_set_gain_amp3(0);
            Stop_AD9959();
            ad9914_deinit_3();
            SWtestGNSS(SWALLTEST);
            SWtest100500(SWALLTEST);
            _EN_PWR_CH3_OFF;                        
        }
        else
        {
            hmc1122_set_gain_amp1(0);
            hmc1122_set_gain_amp2(0);  
            ad9910_deinit_1();
            ad9910_deinit_2();
            

        }

            _EN_PWR_CH1_OFF;
            _EN_PWR_CH2_OFF;  
    //        _RF0_OFF;    
    }     
    
   //---------------- Send DATA ----------------       
    // 
    len_DT = 1;// 
    inf_Dt [0] = 0;  // ��        
    TransmitDtUrt (0x24,inf_Dt,len_DT);    
    return status;
}

    unsigned int  CntTest = 0;
    #define CntTestPorog    140

//------------------------------------------------------------
uint8_t setRF_100_500(void)//
{   uint8_t status;
    uint32_t  freqDDS ;
    int cntr;
    uint8_t  bittest;
//    uint64_t  set_Freq_Hz;     
    uint16_t  set_Freq_MHz;
    uint32_t deviation ; //kHz
    uint32_t lfm_speed; //kHz  
    uint8_t  GainData;
        //---------------- set param DATA ---------------- 
        status = 0;  

        SWtest100500(SWALLTEST);
        _EN_PWR_CH3_ON;    
        _EN_PWR_CH1_ON; 
        _EN_PWR_CH2_ON;  
        delay_ms(1);
        genFlgOnCH3_100_500 = 1;                  
        adl5240_set_gain_amp3 (0);   //LE_ATT2  

        set_Freq_MHz = Freq_dds2_F1;//1500

        Init_LMX2572 (syntLMX3); 
        LMX2572_Frequency_setting_MHz(syntLMX3,set_Freq_MHz);  
        LMX2572_Power_A(syntLMX3, 60);
        LMX2572_switch_RFoutA(syntLMX3,1); 
        LMX2572_Power_B(syntLMX3, 60);    
        LMX2572_switch_RFoutB(syntLMX3,1);
        //--------------syntLMX3---------------------------
        cntr = 0;     
        bittest = 0;
        while ((!bittest)&&(cntr<=100))
        {
            bittest = BITREAD(XC_LD_DATA(),2);
            delay_ms (10);
            cntr ++;
        }          
        if (cntr >= 100)
        {
           status = MCHP_LMX_ERROR;
        }
        //------------------------------------------------
        status = former_init_3(); 

        if (status != MCHP_SUCCESS)
        {
            status = MCHP_DDS_ERROR;
        }
        
        freqDDS = LITERA_param.freq;   
        
        if (LITERA_param .mod == 4) 
        {     
            deviation = LITERA_param.dev;
            lfm_speed = LITERA_param.manip;
            set_mode_lfm_3(freqDDS, deviation, lfm_speed);                                    
        }
        else
        {
            set_mode_freq_3(freqDDS);
        }
//
            SWtest100500(SWRFOUT);
            GainData = getGainK(LITERA_param.freq);
            adl5240_set_gain_amp3 (GainData);   //LE_ATT2  
        //    adl5240_set_gain_amp3 (61);   //LE_ATT2  
//            //------------------------------------------------
            
    return status;
}    
//------------------------------------------------------------------
uint8_t setRF_500_2500(void)//
{   uint8_t status;
    static uint32_t freqDDS ; //kHz
    uint16_t set_Freq_MHz;
    uint32_t deviation ; //kHz
    uint32_t lfm_speed; //kHz  
    uint8_t  GainData;
    uint8_t  bittest;
	int cntr;    
    status = 0;  

    SWtest5002500(SWALLTEST);
    
   _EN_PWR_CH2_ON; 
   delay_ms(10);
    genFlgOnCH2_500_2500 = 1;      
    hmc1122_set_gain_amp2(0);     
//            
    former_init_2();           
  
    freqDDS = fDDS2_CH2_F2;     
    delay_ms(10);
    if (LITERA_param .mod == 4) 
     {     
         deviation = LITERA_param.dev;
         lfm_speed = LITERA_param.manip;
         set_mode_lfm_2(freqDDS, deviation, lfm_speed);                                    
     }
     else
     {
         set_mode_freq_2(freqDDS);
     }   
   //---------------------------------
        _RF1_OFF;
        set_Freq_MHz = Freq_get21_F2; //3200            
        Init_LMX2572 (syntLMX3);
    //    Init_LMX2572 (syntLMX3); 
        LMX2572_Frequency_setting_MHz(syntLMX3,Freq_get21_F2);  
        LMX2572_Power_A(syntLMX3, 60);
        LMX2572_switch_RFoutA(syntLMX3,1);    

        //------------test--LMX3----------------------------------
        
            cntr = 0; 
            bittest = 0;
            while ((!bittest)&&(cntr<=100))
            {
                bittest = BITREAD(XC_LD_DATA(),2);
                delay_ms (10);
                cntr ++;
            }          
            if (cntr >= 100)
            {
               status = MCHP_LMX_ERROR;
            }              
        
        //------------------------------------------------------
        set_Freq_MHz = Freq_get22_F2 + LITERA_param.freq /1000;//


        Init_LMX2572 (syntLMX4); 
      //  Init_LMX2572 (syntLMX4);             

        LMX2572_Frequency_setting_MHz(syntLMX4,set_Freq_MHz);  
//           LMX2572_Power_A(syntLMX4, 60);
        LMX2572_Power_frequency_A(syntLMX4,set_Freq_MHz);         

        LMX2572_switch_RFoutA(syntLMX4,1);      
        SW2DA21(SW2DA21RF1);//SW2DA21RF2 test   
        delay_ms (10);    
        //------------test--LMX4----------------------------------
            cntr = 0; 
            bittest = 0;
            while ((!bittest)&&(cntr<=100))
            {
                bittest = BITREAD(XC_LD_DATA(),3);
                delay_ms (10);
                cntr ++;
            }          
            if (cntr >= 100)
            {
               status = MCHP_LMX_ERROR;
            }                    
       //---------------------------------------------------------------     
        if ((LITERA_param.freq >= 500000) && (LITERA_param.freq < 1500000))
        {
            SW2DA29(SW2DA29RF2);                
        } 
        else if ((LITERA_param.freq >= 1500000) && (LITERA_param.freq < 2500000))
        {
            SW2DA29(SW2DA29RF3);
        }            
        else 
        {      
            SW2DA29(SW2DA29RF1);              
        }  
           GainData = (uint8_t) (getGainK( LITERA_param.freq)); //<================================

           hmc1122_set_gain_amp2(GainData);
          // hmc1122_set_gain_amp2(30); //20          
            
            SW2DA32(SW2DA32RF2);                  
            
    return status;
}

uint8_t setRF_2500_6000(void)//
{   uint8_t  status;
    uint32_t freqDDS ; //kHz
    uint16_t set_Freq_MHz;
    uint32_t deviation ; //kHz
    uint32_t lfm_speed; //kHz  
    uint8_t  GainData;    
    uint8_t  bittest;        
    int cntr;
    status = 0;
    SWtest25006000(SWALLTEST);
    _EN_PWR_CH1_ON;  
    delay_ms(10);
    genFlgOnCH1_2500_6000 = 1;
    hmc1122_set_gain_amp1(0);
   
    former_init_1(); 
    freqDDS = fDDS1_CH1_F2; //
    delay_ms(10);
    if (LITERA_param .mod == 4) 
    {     
        deviation = LITERA_param.dev;
        lfm_speed = LITERA_param.manip;
        set_mode_lfm_1(freqDDS, deviation, lfm_speed);                                    
    }
    else
    {
        set_mode_freq_1(freqDDS);
    }  
//----------------------------------------    
    Init_LMX2572 (syntLMX1);
//    Init_LMX2572 (syntLMX3);           
    if((LITERA_param.freq >= 2500000) && (LITERA_param.freq < 3600000)) // 1
    {
        set_Freq_MHz = Freq_get11_F2;//
        _RF0_OFF;
    }
    else if ((LITERA_param.freq >= 3600000) && (LITERA_param.freq < 4700000)) // 2
    {
        
        if ((LITERA_param.freq >= 3600000) && (LITERA_param.freq < 4700000))    //4500000
        {
            set_Freq_MHz = Freq_get12_F2;// pervuy get dlya 2 diapazona
            _RF0_ON;             
        }
        else 
        {
            set_Freq_MHz = Freq_get122_F2;//    
             _RF0_OFF;            
        }

    }
    else if ((LITERA_param.freq >= 4700000) && (LITERA_param.freq <= 6000000)) //3
    {
        set_Freq_MHz = Freq_get11_F2;// 
        _RF0_OFF;
    }  
    else
    { 
        status = MCHP_CMD_FAILURE;
        return status;
    }
    

    LMX2572_Frequency_setting_MHz(syntLMX1,set_Freq_MHz);  
    LMX2572_Power_A(syntLMX1, 60);
    LMX2572_switch_RFoutA(syntLMX1,1);    
    
    //------------test--LMX1----------------------------------
        cntr = 0; 
        bittest = 0;
        while ((!bittest)&&(cntr<=100))
        {
            bittest = BITREAD(XC_LD_DATA(),0);
            delay_ms (10);
            cntr ++;
        }          
        if (cntr >= 100)
        {
           status = MCHP_LMX_ERROR;
        }   
    

    if((LITERA_param.freq >= 2500000) && (LITERA_param.freq <   3600000))
    {
        set_Freq_MHz = Freq_get13_F2 + LITERA_param.freq /1000;// 
        SW2DA3(SW2DA3RF1); //RF1
        SW2DA11(SW2DA11RF2);
    }
    else if ((LITERA_param.freq >= 3600000) && (LITERA_param.freq < 4700000))
    {
        
        if ((LITERA_param.freq >= 3600000) && (LITERA_param.freq < 4700000))    //4500000
        {
            set_Freq_MHz = Freq_get14_F2 + LITERA_param.freq /1000;//  
            SW2DA3(SW2DA3RF4);
        }
        else
        {
            SW2DA3(SW2DA3RF1);
            set_Freq_MHz = Freq_get144_F2 + LITERA_param.freq /1000;//  
        }
             
            SW2DA11(SW2DA11RF3);
            delay_us( 1 );               

    }
    else if ((LITERA_param.freq >= 4700000) && (LITERA_param.freq <= 6000000))
    {
        set_Freq_MHz = Freq_get13_F2 + LITERA_param.freq /1000;//   
        SW2DA3(SW2DA3RF1); //SW2DA3RF1
        SW2DA11(SW2DA11RF4);
//                SW2DA9(SWRF4);
    }            


    Init_LMX2592 (syntLMX2);  

    LMX2592_Frequency_setting_MHz(syntLMX2,set_Freq_MHz);  
    LMX2592_Power_frequency_A(syntLMX2,set_Freq_MHz);             
//    LMX2592_Power_A(syntLMX2, 0);               
   
    LMX2592_switch_RFoutA(syntLMX2,1);       
    delay_ms (10);    
    
    //------------test--LMX2----------------------------------
            cntr = 0; 
            bittest = 0;
            while ((!bittest)&&(cntr<=100))
            {
                bittest = BITREAD(XC_LD_DATA(),1);
                delay_ms (10);
                cntr ++;
            }          
            if (cntr >= 100)
            {
               status = MCHP_LMX_ERROR;
            }  

    SW2DA14(SW2DA14RF1);

    GainData = getGainK(LITERA_param.freq);
    hmc1122_set_gain_amp1(GainData); //20
   // hmc1122_set_gain_amp1(40); //20   


    return status;
}
    
uint8_t exeCMD28(void) 
{
    uint8_t status;

    unsigned int len_DT;
    uint8_t inf_Dt[10];

    if (addrL == addrF1)
    {
        if((LITERA_param.freq >= 100000) && (LITERA_param.freq <= 500000))
        {           
            status = setRF_100_500();
        }
    }
    else if ( addrL == addrF4)
    {           
        if ((LITERA_param.freq >= 500000) && (LITERA_param.freq < 2500000))
        {
            status = setRF_500_2500();
        }           
        else 
        if((LITERA_param.freq >= 2500000) && (LITERA_param.freq <= 6000000))
        { 
            status = setRF_2500_6000();    
        } 
        else
        {
            status = 0xFF;
        }
    }

//---------------- Send DATA ----------------       
    if (LITERA_dt.cntSrc == 0)
    {

           len_DT = 1;//            
           inf_Dt [0] = status;  // 

           TransmitDtUrt (0x28,inf_Dt,len_DT); // 

    }
     return status;    
}

uint8_t exeCMD30 (void) //  test
{
  uint8_t  Status;
  unsigned char inf_Dt[10];
  unsigned int len_DT;
// 
////  uint64_t  F_DDS1;  
////  unsigned int ADC_Res;
////  int  gainN;
//
////  uint64_t  F_DDS2;  
////  uint64_t  set_Freq_Hz ;
    Status = 0x00;  
//  error_flg_Dt = 0xFF;
//   
// 
// 
    if ( addrL == addrF1)
    {
    
        DDS1Mod_XC_0;

        if (NAVIG_dt.GPS_L2)
        {
             Set_param_AD9959_GPS_L2();        
        }
        if (NAVIG_dt.GPS_L1)
        {
             Set_param_AD9959_GPS_L1();   
        }
        if (NAVIG_dt.GL_L2)
        {
           Set_param_AD9959_Glonass_L2();     
        }    
        if (NAVIG_dt.GL_L1)
        {
          Set_param_AD9959_Glonass_L1();    
        }

    }
    else
    {
        
        Status = MCHP_RUN_ERROR;  // error vypolneniya
    
    }
//    //---------------- Send DATA ----------------       
  // 
    len_DT = 1;//                       
    
    inf_Dt [0] = Status;  // 

    TransmitDtUrt (0x30,inf_Dt,len_DT); // 
        
    return Status;
}

uint8_t exeCMD31 (uint8_t Cmdx, uint8_t rfCh) // test
{
  uint8_t  Status;
  uint8_t inf_Dt[10];
  unsigned int len_DT;
// 
 
     if ( addrL == addrF1)
    { 
        switch ( Cmdx ) {
        case 0:
            SW1DA15 (rfCh);
          break;
        case 1:
            SW1DA16 (rfCh);
          break;
        case 2:
            SW1DA18 (rfCh);
          break;
        case 3:
            SW1DA19 (rfCh);
          break;          
        case 4:
            SW1DA29 (rfCh);
          break;
        case 5:
            SW1DA30 (rfCh);
          break;            
        case 6:
            SW1DA34 (rfCh);
          break;
        case 7:
            SW1DA40 (rfCh);
          break;           
        /*...*/
        default:
             SWtestGNSS(SWALLTEST);   
             SWtest100500 (SWALLTEST);
          break;
        }
     }
     else if ( addrL == addrF4)
     {
     
        switch ( Cmdx ) {
        case 0:
            SW2DA3 (rfCh);
          break;
        case 1:
            SW2DA11 (rfCh);
          break;
        case 2:
            SW2DA14 (rfCh);
          break;
        case 3:
            SW2DA21 (rfCh);
          break;          
        case 4:
            SW2DA29 (rfCh);
          break;
        case 5:
            SW2DA32 (rfCh);
          break;            
        
        /*...*/
        default:
             SWtest5002500(SWALLTEST);   
             SWtest25006000 (SWALLTEST);
          break;
        }
     
     }
     else
     {
        Status = MCHP_RUN_ERROR;
     }

//    //---------------- Send DATA ----------------       
  //   
    len_DT = 1;//                         
    inf_Dt [0] = Status;  // 

    TransmitDtUrt (0x31,inf_Dt,len_DT); //
        
    return Status;
}


#define Fget_GPS_GL_L2_test    2300//(3350*MHz+F0DDS_GL_L2+1241.4*MHz)/MHz
//===================== GNSS ===========================
uint8_t exeCMD34 (void)
{   uint8_t   Status;
    uint16_t  error_flg_Dt;
    uint8_t  inf_Dt[10];
    unsigned int len_DT;
//    uint64_t   F_DDS1;  
//    unsigned int ADC_Res;
    uint8_t   gain_gnss;
    uint8_t   GainL1;
    uint8_t   GainL2;
    uint8_t   bittest;
    int cntr;
    Status = 0x00;  
        //-----------------------------------------------------
    DDS1Mod_XC_0;
    SetGPSL1 = 0; SetGLL1 = 0; 
    SetGPSL2 = 0; SetGLL2 = 0; 
    getGainK_gnss(&GainL1,&GainL2);
    
   if ((addrL == addrF1)||(addrL ==addrF3))
   {          
        SWtestGNSS(SWALLTEST);   
       
        _EN_PWR_CH1_ON;           
        delay_us(100);  
        
        adl5240_set_gain_amp1(0);  // L2    

        Status = Init_AD9959();
        
        if (Status == MCHP_FAILURE)                         
       {
            Status = MCHP_DDS_ERROR;
       }        

            Init_LMX2572 (syntLMX1);     //DA10
            LMX2572_Frequency_setting_MHz(syntLMX1,Fget1_GPS_GL_L1L2);
            LMX2572_Power_A(syntLMX1, 60);
            LMX2572_switch_RFoutA(syntLMX1,1);
            LMX2572_Power_B(syntLMX1, 60);    
            LMX2572_switch_RFoutB(syntLMX1,1);    
            LMX2572_switch_RFoutB_multiply_A( syntLMX1, 1);   
            
        //---------------test--LMX1--------------------
            cntr = 0; 
            bittest = 0;
            while ((!bittest)&&(cntr<=100))
            {
                bittest = BITREAD(XC_LD_DATA(),0);
                delay_ms (10);
                cntr ++;
            }          
            if (cntr >= 100)
            {
               Status = MCHP_LMX_ERROR;
            }              
        //---------------------------------------------            
////********************L2****************L2*********************        
        if (NAVIG_dt.GPS_L2||NAVIG_dt.GL_L2)
        {
            
            genFlgOnCH1_L2 = 1;            
//            _EN_PWR_CH1_ON;             

         //----------------------L2------------------------
            //   Set_param_AD9959_Glonass_L1();
            if (NAVIG_dt.GPS_L2)
            {
                Set_param_AD9959_GPS_L2();
                SetGPSL2 = 1;// for plis
             // =========  test=====================         
               
              //  F_DDS1 = F0DDS_GPS_L2; // ��   GPS
               // Set_param_AD9959_1RCH (F_DDS1); 
                 
             // ==============================                 
            }
            if (NAVIG_dt.GL_L2)
            {    
               Set_param_AD9959_Glonass_L2();
               SetGLL2 = 1;
              // =========  test=====================                
              //  F_DDS1 = F0DDS_GL_L2; // ��    Glonass
              //  Set_param_AD9959_0RCH (F_DDS1);  
             // ==============================                 
            }  
            //------------------------------------------------------------
            Init_LMX2572 (syntLMX2);
            LMX2572_Frequency_setting_MHz(syntLMX2,Fget2_GPS_GL_L2);
           // LMX2572_Frequency_setting_MHz(syntLMX2,Fget_GPS_GL_L2_test);            
            LMX2572_Power_A(syntLMX2, 22);    
            LMX2572_switch_RFoutA(syntLMX2,1); 
            
            //------------------LMX2---------------------------------
            cntr = 0; 
            bittest = 0;
            while ((!bittest)&&(cntr<=100))
            {
                bittest = BITREAD(XC_LD_DATA(),1);
                delay_ms (10);
                cntr ++;
            }          
            if (cntr >= 100)
            {
               Status = MCHP_LMX_ERROR;
            }                     
            //------------------------------------------------------------
            gain_gnss = GainL2 - NAVIG_dt.GNSS_GAIN_L2*(GainL2/4);       //60/4=15 
            adl5240_set_gain_amp1(gain_gnss);  // L2  
            
            SWtestGNSS(SWRFL2);// sw to Rf
//            adl5240_set_gain_amp1(40); 
            
        //    SW1DA16(1);              
        //    delay_ms(2);           
            error_flg_Dt = 0;//****************
        }
////**************L1***********L1*******L1*************************
        if (NAVIG_dt.GPS_L1||NAVIG_dt.GL_L1)
        {
            error_flg_Dt = 0;
            genFlgOnCH2_L1 = 1;
             _EN_PWR_CH2_ON
             delay_us(10);
             
             adl5240_set_gain_amp2(0);  // L1 
//           // _RF0_ON; // � DA11 // ����� 2    
       //----------------------L1----------------------------------------
            if (NAVIG_dt.GPS_L1)
            {    
                Set_param_AD9959_GPS_L1();
                SetGPSL1 = 1;
            // =========  test=====================
             //   F_DDS1 = F0DDS_GPS_L1; // 120  GPS
             //   Set_param_AD9959_2RCH (F_DDS1);    
            // =====================================                
            }            
            if (NAVIG_dt.GL_L1)
            {   
               Set_param_AD9959_Glonass_L1();
               SetGLL1 = 1; // for plis
             // =========  test=====================               
              //  F_DDS1 = F0DDS_GL_L1; //  120 Glonass
              //  Set_param_AD9959_3RCH (F_DDS1);                 
            // =====================================                 
            }
           // _RF0_ON; //  DA12  
            
            gain_gnss = GainL1 - NAVIG_dt.GNSS_GAIN_L1*(GainL1/4);
            adl5240_set_gain_amp2(gain_gnss);  // L1 
            
//             adl5240_set_gain_amp2(40); 
             SWtestGNSS(SWRFL1);
//            genFlgOnCH3_L1 = 1;
//            ADC_Res = fun_Read7995(RF_CH3,AD7995_ADDR);     
            error_flg_Dt = 0;//****************
        }    
        
        Init_XC_GPSGL();
        DDS1Mod_XC_1;
        GNSS_ON_XC;
   }
  //  
        len_DT = 1;//              
        error_flg_Dt = error_flg_Dt&0xFF;
        inf_Dt [0] = (uint8_t )error_flg_Dt;  //

        TransmitDtUrt (0x34,inf_Dt,len_DT);        

    return Status;
 //   return 0;    
}

//--------------SetPARAM--------------------------
uint8_t exeCMD40(uint8_t status)
{
    unsigned int len_DT;
    uint8_t inf_Dt[10];
  //----------------------------------------------

  //----------------------------------------------  
    len_DT = 1;//                 

    inf_Dt [0] = (uint8_t )status;  // 

    TransmitDtUrt (0x40,inf_Dt,len_DT);   

 return status;
}
//-------------------------------------------------
//--------------SavePARAM--------------------------
uint8_t exeCMD42(void)
{  
    unsigned int len_DT;
    uint8_t inf_Dt[10];
    uint8_t Status;
  //----------------------------------------------
     
     Status = SaveAllEEParam();
     error_flg_Dt = 0x00;
     if (Status != XST_SUCCESS)
     {
        error_flg_Dt = 0xFF;
     }
 //------------------------------------------------    
    len_DT = 1;//                 

    inf_Dt [0] = Status;  // 

    TransmitDtUrt (0x42,inf_Dt,len_DT);   
    
 return Status;
}

//--------------getPARAM--------------------------
uint8_t exeCMD41(uint8_t *IDparamP, uint8_t DtCnt)
{
  uint8_t Status;
  unsigned int i,j,Nparam;
//  unsigned char pageGetParam[16]; 
  unsigned int len_DT;
  uint8_t IDparam1;
  uint8_t inf_Dt[cntDtEEall];  
  
    for (i=0; i < cntPgEE; i++) //dt
    {
      inf_Dt[i] = 0;            
    }     
    

      j=0;
      Nparam = 0;
      while (Nparam < DtCnt)
      {               
        IDparam1 = IDparamP[Nparam];        // i = 0 -> ID   
        if (IDparam1>cntDtEE) // cntDtEE- maz cnt partams
        {        
                //------------------------------------------------    
           len_DT = 1;//        
           inf_Dt [0] = 0xFF;  // 
           TransmitDtUrt (0x41,inf_Dt,len_DT);             

        }
        
        inf_Dt[j] = IDparam1;
        j++;
        for (i=1; i < cntInfDtEE; i++) //dt
        { 
          inf_Dt[j] = setParamEEprom[IDparam1][i];    
          j++;
        }         
        inf_Dt[j] = setParamEEprom[IDparam1][cntInfDtEE+1];// status // KK propuskaem           
        IDparam1++;
        j++;   
        Nparam++;
      }     
        len_DT = j;//
      
  
//  }else
//  {
//        inf_Dt[0] = IDparam1;
//        // i = 0 -> ID
//        for (i=1; i < cntInfDtEE; i++) //dt
//        {
//          inf_Dt[i] = setParamEEprom[IDparam1][i];            
//        }         
//        inf_Dt[cntInfDtEE] = setParamEEprom[IDparam1][cntInfDtEE+1];// status // KK propuskaem
//        len_DT = cntInfDtEE+1;//               
//  }
//  

          TransmitDtUrt (0x41,inf_Dt,len_DT);   
    Status = 0;
 return Status;
}
// ======================send version============================
uint8_t exeCMD43(void)
{
  uint8_t Status;

  unsigned int len_DT;
  uint8_t inf_Dt[10];  
  
    Status = 0;
    len_DT = 1;//             
    inf_Dt [0] = (uint8_t )versionPgm;  // 

    TransmitDtUrt (0x43,inf_Dt,len_DT);   
    
 return Status;
}


void getGainK_gnss(uint8_t *GainL1, uint8_t *GainL2)// frq v kHz
{  
    
    *GainL1 = setParamEEprom [cntDtEE-1][7];
    *GainL2 = setParamEEprom [cntDtEE-1][8];
    
}

//----------------------------------------------
//#define gK1f          ((F1fv - F1fn)/(N1fv - N1fn))
 //gainN  =  (unsigned)((int)(f_DDS - F1fn))/gK1f + N1fn; 
//----------------------------------------------
uint8_t getGainK(uint32_t Freq)// frq v kHz
{
    uint32_t FreqN;//nignajy
    uint32_t FreqV;//verhnja
    uint32_t freq3,freq2,freq1;    
    //int64_t  GainK; // coeff 
    uint8_t  GainN;    
    uint32_t GainNn;  //usil nignee
    uint32_t GainNv;  //usil verhnee   
    uint16_t j;    
    //----------test--------------
//    uint8_t GainL1;
//    uint8_t GainL2;
//    uint8_t tester;    
    //-------------------------
//    setParamEEprom [cntDtEE][cntPgEE];
    GainN = 255;
    Freq = Freq/kHz;    //> MHz
        
    
//    getGainK_gnss( &GainL1,&GainL2);

    
    for (j=0; j < (cntDtEE-1);j++)// addrGNSS = cntDtEE
    {
        freq1 = (uint32_t) setParamEEprom [j][1];
        freq2 = (uint32_t) setParamEEprom [j][2];
        freq3 = (uint32_t) setParamEEprom [j][3];
        FreqN =   ((freq3 <<16)|(freq2 <<8)|(freq1)) ;
        freq1 = (uint32_t) setParamEEprom [j][4];
        freq2 = (uint32_t) setParamEEprom [j][5];
        freq3 = (uint32_t) setParamEEprom [j][6];        
        FreqV =    ((freq3 <<16)|(freq2 <<8)|(freq1)) ;      
        GainNn = (uint32_t) setParamEEprom [j][7];
        GainNv = (uint32_t) setParamEEprom [j][8];  
        
        if ((Freq >= FreqN)&&(Freq < FreqV))
        {  
            if (FreqN == FreqV)
            {
               GainN = GainNn;
            }
            else
            {
                GainN = (GainNv*(Freq-FreqN)+GainNn*(FreqV-Freq))/(FreqV-FreqN); 
                // optimize function
//                GainK = (((int64_t)FreqStop - (int64_t)FreqStart))/((int64_t)GainNv - (int64_t)GainNn);
//                GainN = ((uint32_t)((int64_t)Freq - (int64_t)FreqStart))/GainK + GainNn; // (uint32_t)            
            }
            break;
        }
        else
        {// 
            GainN = 0;
        }
    }

        return GainN;
}
//-----------------------------------------------
uint8_t AnlRxDtUrt(void)
{ unsigned int i,j;
  unsigned int len_DT/*,shagDt*/;
  unsigned char inf_Dt[10];
//  unsigned char /*lit_K_num*/;
  uint8_t GPGL;
  uint32_t freq3,freq2,freq1;
  
//  uint32_t freq;  
  
  uint8_t Status; 
  uint8_t IDparam/*,IDparam1,IDparam2 */; 
  uint8_t IDparamN[cntDtEE];
  unsigned int  sumEE;

  
  Status = 0;
  if (addr_Dt != addrL)// 
  {
   
        return Status; // 
  }
  //----------------------------------------
   	if (err_CRC) //  CRC
	{
    
        Status = MCHP_CRC_ERROR;
        len_DT = 1;//  
        inf_Dt [0] = Status; // error                         
        TransmitDtUrt (cmd_Dt,inf_Dt,len_DT);
        return Status; 
    }  

    LITERA_dt.cmd = cmd_Dt; // 
    LITERA_dt.cntDt = cnt_Dt; // 
//----------------exeCMD4----status----------------  
  	if (LITERA_dt.cmd == 0x04)// 
	{
       Status = exeCMD4();        
	}
//----------------exeCMD4---vykl izluchenia------------  
    else if (cmd_Dt == 0x24) //
    {  
       Status = exeCMD24(data_Urt[3]);          
    }
//------exeCMD28-----vkl frch------      
    else if (cmd_Dt == 0x28) 
    {
       LITERA_dt.cntSrc = data_Urt[3]; // cnt istochnikov
        j = 4;
        
       while (LITERA_dt.cntSrc > 0 )
       {    
            freq1 = data_Urt[j]; j++;
            freq2 = data_Urt[j]; j++;
            freq3 = data_Urt[j]; j++;    

            LITERA_param .freq = 0;    
            LITERA_param .freq = ((freq3 <<16)|(freq2 <<8)|(freq1)); // kHz
            LITERA_param .mod = data_Urt[j]; 
            j++;       
            if (data_Urt[j]&0x80)
            {
               data_Urt[j]&=0x7F; 
               LITERA_param .dev = ((uint64_t) data_Urt[j])*1000;    // kHz      
            }    
            else
            {
               LITERA_param .dev =  data_Urt[j];        // kHz
            }
            j++;               
            if (LITERA_param .mod == 4||LITERA_param .mod == 2) 
            {
               LITERA_param .manip = data_Urt[j];  // kHz
            }else
            {
               LITERA_param .manip = data_Urt[j];
            }
            j++;           
            LITERA_param .timerad = data_Urt[j]; 
            j++;       
            LITERA_dt.cntSrc--;
            Status = exeCMD28(); // �������� ���������� ������� 0�28
       }
    }
    //------exeCMD30---- test GNSS modulation off
   else if (cmd_Dt == 0x30)
   {
        Status = exeCMD30();  
   }
    //------exeCMD31---- test switch--------
   else if (cmd_Dt == 0x31)
   { 
        Status = exeCMD31(data_Urt[3],data_Urt[4]);  
   }    
  //------------- exeCMD34---  GPS_GLONASS
   else if (cmd_Dt == 0x34) 
   { //
   
        GPGL = data_Urt[3];

        NAVIG_dt.GNSS_GAIN_L1 = (GPGL>>4)&0x03;
        NAVIG_dt.GNSS_GAIN_L2 = (GPGL>>6)&0x03;        
        if (GPGL&0x01)
        {NAVIG_dt.GPS_L1 = 1; }
        else
        { NAVIG_dt.GPS_L1 = 0;}
        
         if (GPGL&0x02)
        { NAVIG_dt.GPS_L2 = 1;}
         else
        { NAVIG_dt.GPS_L2 = 0;}   

        if (GPGL&0x04)
        { NAVIG_dt.GL_L1 = 1; }
        else
        { NAVIG_dt.GL_L1 = 0; }   
        
         if (GPGL&0x08)
        { NAVIG_dt.GL_L2 = 1; }
         else
        {  NAVIG_dt.GL_L2 = 0;}      

        Status = exeCMD34();
   }    
 //------------- exeCMD40--- izmenenie param usilitelya
  else if (cmd_Dt == 0x40) 
  {//cnt_Dt
      len_DT = 0;
      i = 0;
      j = 3;
      sumEE = 0;//
      
      while (len_DT < cnt_Dt)
      {     
            IDparam = data_Urt[j];
            
            if (IDparam > cntDtEE )
            {
                Status = exeCMD40(MCHP_CMD_FAILURE);
                return MCHP_FAILURE;
            }
            
            setParamEEprom [IDparam][0] = IDparam; 
            for (i=1;i<cntInfDtEE;i++)
            {
                setParamEEprom [IDparam][i] = data_Urt[j+i];         
            }
            j=j+cntInfDtEE;
          
            len_DT = len_DT + cntInfDtEE;
      }
      len_DT = 0;
     // while (len_DT < ((unsigned int)cnt_Dt))    

      
      Status = exeCMD40(MCHP_SUCCESS); // 
    //----------test-----------  
//      freq = 100000;//kHz
      
//    error_flg = getGainK(freq);
      
     //----------test-----------     
  }  
  else if (cmd_Dt == 0x41) //  
  {
      i = 0;
      while(i < cnt_Dt)
      {
          
        IDparamN[i] = data_Urt[3+i]; 
          
        i++;
      }
       
       Status = exeCMD41(IDparamN,cnt_Dt);      
  }      
  else if (cmd_Dt == 0x42) //save params
  {
   // 
     Status = exeCMD42();

  
  }    
  else if (cmd_Dt == 0x43) //
  {

      Status = exeCMD43();
  
  }    
   else
   {  
        Status = 0xFF; 
        len_DT = 1;//   
        inf_Dt [0] = Status;  //     

        TransmitDtUrt (cmd_Dt,inf_Dt,len_DT);      
   }    

 return Status;
}


void off_all_pwr(void)// frq v kHz
{
    
 exeCMD24(offCMD_All);
    
}

