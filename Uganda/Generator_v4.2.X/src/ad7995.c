/*
 * File:   ad7995.c
 * Author: Alexandr
 *
 * Created on 27 сентября 2017 г., 13:58
 */


#include "xc.h"
#include "system_cfg.h"
#include "I2C.h"
#include "ad7995.h"



unsigned int fun_Read7995(unsigned char channel,unsigned char addr)
{
    unsigned int res;
    unsigned char msb, lsb;
    
    if(channel > 4)
    {
        return 0;
    }
    
    unsigned char cfg = 1 << (channel + 4);
    
    //Выбор канала для измерения
    StartI2C();
    IdleI2C();
    if (addr == AD7995_ADDR)
    {
        WriteI2C(AD7995_ADDR | ADWRITE_MASK);    
    }else
    {
        WriteI2C(AD7995_ADDR2 | ADWRITE_MASK); 
    }

    IdleI2C();
    WriteI2C(cfg);
    IdleI2C();
    StopI2C();
    delay_ms(5);
    
    //�?змерение
    StartI2C();
    IdleI2C();
    if (addr == AD7995_ADDR)
    {    
        WriteI2C(AD7995_ADDR | ADREAD_MASK);
    }else
    {
        WriteI2C(AD7995_ADDR2 | ADREAD_MASK);
    }
    IdleI2C();
    msb = ReadI2C();
    AckI2C();
    IdleI2C();
    lsb = ReadI2C();
    NotAckI2C();
    IdleI2C();
    StopI2C();
    IdleI2C();
  //  delay_ms(1);
    
    return (res = (unsigned int)(((msb & 0x0F) << 8) | lsb) >> 2);
}
