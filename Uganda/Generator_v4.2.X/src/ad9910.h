#ifndef _AD9910_H    /* Guard against multiple inclusion */
#define _AD9910_H

#include <xc.h>
#include "system_cfg.h"
#define REG_BYTE        1
#define REG_WORD        2
#define REG_DWORD       4
#define REG_QWORD       8

#define READ_MASK       0x80

#define DAC_ON          0x00800002
#define DAC_OFF         0x00000042
#define AMPL_FS         0x000000FF
#define SPI_3WIRE       0x00800002
#define INIT_PLL        0x1D3FC150
#define AMPL_SCALE      0x00400820

#define LFM_MODE        0x004E0820

/********************************* VARIABLES **********************************/
struct ad9910
{
    uint8_t CFR1;
    uint8_t CFR2;
    uint8_t CFR3;
    uint8_t AUDAC_C;
    uint8_t DRL;
    uint8_t DRS;
    uint8_t DRR;
    uint8_t ST_PROF[8];
};

extern const struct ad9910 ad9910_reg;

/********************************* FUNCTIONS **********************************/

/* 
 * IO_UPDATE    LATD1
 * DDS_EXT_PD   LATD3
 * M_RESET      LATD11
 * CS_DDS       LATD9 
 */

#define _SCLK_0              LATGbits.LATG6 = 0  
#define _SCLK_1              LATGbits.LATG6 = 1
#define _v_o_DATA_0           LATGbits.LATG8 = 0 //SDO2
#define _v_o_DATA_1           LATGbits.LATG8 = 1
  
#define _v_i_DATA             PORTGbits.RG7   /// SDI2

#define ad9910_select_dds_1()     LATDbits.LATD6 = 0
#define ad9910_unselect_dds_1()   LATDbits.LATD6 = 1

#define ad9910_select_dds_2()     LATDbits.LATD11 = 0
#define ad9910_unselect_dds_2()   LATDbits.LATD11 = 1


  
#define _IOUpdate_0()          LATDbits.LATD7 = 0
#define _IOUpdate_1()          LATDbits.LATD7 = 1  


#define ad9910_reset_1()          {LATDbits.LATD5 = 1;     delay_us(1); LATDbits.LATD5 = 0;} 
#define ad9910_reset_2()          {LATDbits.LATD10 = 1;    delay_us(1); LATDbits.LATD10 = 0;} 

#define ad9910_on_1()             LATDbits.LATD4 = 0
#define ad9910_off_1()            LATDbits.LATD4 = 1

#define ad9910_on_2()             LATDbits.LATD9 = 0
#define ad9910_off_2()            LATDbits.LATD9 = 1

//LATDbits.LATD4 = 0


#define ad9910_io_update_set()  LATDbits.LATD7 = 1 
#define ad9910_io_update_clr()  LATDbits.LATD7 = 0 

/* 
 * Reg's read/write functions
 */
void ad9910_write_reg(uint8_t reg, void *buf, uint16_t reg_size);
void ad9910_read_reg(uint8_t reg, void *buf, uint16_t reg_size);

/* 
 * Control functions 
 */
char AD9910_test(void);


void ad9910_dac_on(void);
void ad9910_dac_off(void);
void ad9910_set_amp(uint32_t amp);
void ad9910_set_ampl_fs(void);
uint8_t ad9910_spi_3wire_mode(void);
uint8_t  ad9910_init_pll_1(void);
uint8_t  ad9910_init_pll_2(void);
void ad9910_enable_amp_scale(void);
void ad9910_set_freq(uint8_t reg_addr, uint64_t freq_curr);
void ad9910_set_freq_phase(uint8_t reg_addr, uint32_t freq_curr, uint32_t phase_curr);
void ad9910_enable_lfm_mode(void);
#endif /* _AD9910_H */
