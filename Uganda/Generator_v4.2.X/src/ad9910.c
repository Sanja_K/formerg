#include <xc.h>

#include "ad9910.h"
#include "system_cfg.h"     
#include "former_signal.h"
const struct ad9910 ad9910_reg = {
    .CFR1       = 0x00,
    .CFR2       = 0x01,
    .CFR3       = 0x02,
    .AUDAC_C    = 0x03,
    .DRL        = 0x0B,
    .DRS        = 0x0C,
    .DRR        = 0x0D,
    .ST_PROF    = 
        {0x0E, 0x0F, 0x10, 0x11, 0x12, 0x13, 0x14, 0x15},
};


static void  v_o_DATA( char ena )
{
    if(ena)
        _v_o_DATA_1;
    else
        _v_o_DATA_0;   
}    

static void  SPIp_write( uint8_t reg )
{ unsigned int i=0;

    _SCLK_0;
    delay_us(1);
    for( i=0; i<8; i++ ) 
    {
        v_o_DATA(reg&0x80); // ������ ����� �� ������� ���
        delay_us( 1 );
        _SCLK_1;
        reg = reg << 1;
        delay_us( 1 );
        _SCLK_0;
    }
    delay_us(1);
    
} 
static uint8_t  SPIp_read( void )
{ unsigned int i=0;
    uint8_t value;
    //������ �������
    _SCLK_0;
    value = 0;
//    TRISGbits.TRISG8 = 1;//
    
    delay_us(1);
    // ��������� ������ ������
    for( i=0; i<8; i++ ) 
    {
        value = (value<<1);
        delay_us( 1 );
        _SCLK_1;
        if (_v_i_DATA)
         value |= 0x01; // ������������� ���
        else
         value &= 0xFE;// ���������� ���      
        delay_us( 1 );
        _SCLK_0;
    }   
 //    TRISGbits.TRISG8 = 0;//
    // ���������� ���������
    return value;
} 
/* 
 * functions read/write command and 1/2/4 bytes of data to AD9914
 * available values for reg_size is: REG_BYTE (1 byte), REG_WORD (2 bytes), 
 * REG_DWORD (4 bytes), other values are  forbidden
 */


//  #define  READ            0x80
//
//static void AD9910_read(uint8_t REG, uint8_t *buffer, int len)
//{ int i;
//
//    SPIp_write(REG | READ );
//    delay_us( 1 );
//    for (i=0; i< len; i++)
//    {
//        buffer[i] = SPIp_read();
//    }    
//
//}


//static void AD9910_write(uint8_t REG, uint8_t *buffer, int len)
//{ int i;
//
//
//    delay_us( 1 );    
//    SPIp_write(REG);
//    for (i=0; i< len; i++)
//    {
//        SPIp_write(buffer[i]);
//    }   
//    delay_us( 1);
//}


//char AD9910_get_Data(void)
//{
//  char error_flg;
//  uint32_t DtVCO;
//  uint8_t buffer[4];
//  error_flg = 0;
//  
//
//    ad9910_on_1();
//  
//
//    ad9910_select_dds_1() ; 
//
//    AD9910_read(0x01,buffer,4);
//
//    ad9910_unselect_dds_1();
//    DtVCO = 0;
//
//    DtVCO += (uint32_t)buffer[3] << 0;    
//    DtVCO += (uint32_t)buffer[2] << 8;
//    DtVCO += (uint32_t)buffer[1] << 16;
//    DtVCO += (uint32_t)buffer[0] << 24;
//
//
//    if (DtVCO!= 0xD00000)
//    {
//        error_flg =1;
//    }
//
//  return error_flg;
//}


 void AD9910_IOUpdate(void)
{
    ad9910_io_update_set();
    delay_us( 2 );    
    ad9910_io_update_clr();
    delay_us( 20 );    
}



//char AD9910_test(void)
//{
////  uint8_t buffer[4];
//  char error_flg;
//  error_flg = 0;
//
////  ad9910_unselect_dds_1();
////  ad9910_io_update_clr();  
////  ad9910_on_1();
////          
////  ad9910_reset_1();
//////0x1D3FC150
////  buffer[3] = 0x50;
////  buffer[2] = 0xC1;  
////  buffer[1] = 0x3F;
////  buffer[0] = 0x1D;
////  
////  
////  //former_init_1();
////  
////  ad9910_select_dds_1();  //
////
////  ad9910_write_reg(0x02,buffer,4);
////
////  AD9910_IOUpdate();
////  
////  ad9910_unselect_dds_1();  
//
//  former_init_1();
//  
//   ad9910_init_pll_1();   
//  
//  error_flg = AD9910_get_Data(); // 
//  
//  set_mode_freq_1(200000);
//  
//
//  
//  return error_flg;
//}


void ad9910_write_reg(uint8_t reg, void *buf, uint16_t reg_size)
{ 

//    while(SPI1STATbits.SRMT == 0);

    delay_us(1);
    SPIp_write(reg);
    delay_us(1);
    for(; reg_size > 0; reg_size--)
    {
        SPIp_write(((uint8_t*)buf)[reg_size - 1]);
    }
    delay_us(1);
    ad9910_io_update_set();
    delay_us(1);
    ad9910_io_update_clr();
    delay_us(3);

}

void ad9910_read_reg(uint8_t reg, void *buf, uint16_t reg_size)
{
    uint64_t data;
    int i = reg_size;
    
    data = 0;
        
//    delay_us(1);  
    SPIp_write(reg | READ_MASK);
//    delay_us(1);
    for(; i > 0; i--)
    {
     //   data = SPIp_read(); // esli apparatny SPI
        data <<= 8;
        data |= SPIp_read();
    }

    switch(reg_size) 
    {
        case REG_BYTE:  // 1 - 8
        {
            uint8_t *ptr = buf;
            *ptr = (uint8_t)data;
            break;
        }

        case REG_WORD: // 2 - 16
        {
            uint16_t *ptr = buf;
            *ptr = (uint16_t)data;
            break;
        }

        case REG_DWORD: //4- 32
        {
            uint32_t *ptr = buf;
            *ptr = (uint32_t)data;
            break;
        }
        
        case REG_QWORD: //8 - 64
        {
            uint64_t *ptr = buf;
            *ptr = (uint64_t)data;
            break;
        }

        default:
        {
            uint32_t *ptr = buf;
            *ptr = 0x00000000;
            break;
        }
    }
    
}

uint8_t ad9910_spi_3wire_mode(void)
{
    uint8_t status;
    static uint32_t reg_data = SPI_3WIRE;
    static uint32_t read_data;    
    ad9910_write_reg(ad9910_reg.CFR1, &reg_data, REG_DWORD);
    delay_ms (1);
    ad9910_read_reg(ad9910_reg.CFR1, &read_data, REG_DWORD);
    
    if (read_data == reg_data)
    {
        status = MCHP_SUCCESS;
    }
    else
    {
         status = MCHP_FAILURE;
    }
    
    
    return status;
}

uint8_t ad9910_init_pll_1(void)
{
    int status;
    static uint32_t reg_data = INIT_PLL;
    uint32_t read_data;
    
    ad9910_select_dds_1();
    ad9910_write_reg(ad9910_reg.CFR3, &reg_data, REG_DWORD);
    ad9910_unselect_dds_1();
    
    ad9910_select_dds_1();
    ad9910_read_reg(ad9910_reg.CFR3, &read_data, REG_DWORD);
    ad9910_unselect_dds_1();    
    if (reg_data!=read_data)
    {
        status = MCHP_SUCCESS;
    }
    else 
    {
        status = MCHP_FAILURE;
    }
    
    return status;
    
}

uint8_t ad9910_init_pll_2(void)
{   uint8_t status;
    static uint32_t reg_data = INIT_PLL;
    uint32_t read_data;
    
    ad9910_select_dds_2();
    ad9910_write_reg(ad9910_reg.CFR3, &reg_data, REG_DWORD);
    ad9910_unselect_dds_2();
    
    ad9910_select_dds_2();
    ad9910_read_reg(ad9910_reg.CFR3, &read_data, REG_DWORD);
    ad9910_unselect_dds_2();    
    if (reg_data!=read_data)
    {
        status = MCHP_SUCCESS;
    }
    else 
    {
        status = MCHP_FAILURE;
    }
    
    return status;
}

void ad9910_set_freq(uint8_t reg_addr, uint64_t freq_curr)
{
    static uint16_t ampl_curr = 0x08B5;
    static uint16_t phase_curr = 0x0000;
    uint64_t reg_data;
    
    reg_data = (((uint64_t)ampl_curr) << 48) | 
              (((uint64_t)phase_curr) << 32) | freq_curr;
    ad9910_write_reg(reg_addr, &reg_data, REG_QWORD);
}

void ad9910_set_freq_phase(uint8_t reg_addr, uint32_t freq_curr, uint32_t phase_curr)
{
    uint64_t reg_data = (((uint64_t)phase_curr) << 32) | freq_curr;

    ad9910_write_reg(reg_addr, &reg_data, REG_QWORD);
}

void ad9910_dac_on(void)
{
    static uint32_t reg_data = DAC_ON;
    
    ad9910_write_reg(ad9910_reg.CFR1, &reg_data, REG_DWORD);
}

void ad9910_dac_off(void)
{
    static uint32_t reg_data = DAC_OFF;
    
    ad9910_write_reg(ad9910_reg.CFR1, &reg_data, REG_DWORD);
}

void ad9910_set_amp(uint32_t amp)
{
    ad9910_write_reg(ad9910_reg.AUDAC_C, &amp, REG_DWORD);
}

void ad9910_set_ampl_fs(void)
{
    static uint32_t reg_data = AMPL_FS;
    
    ad9910_write_reg(ad9910_reg.AUDAC_C, &reg_data, REG_DWORD);
}

void ad9910_enable_amp_scale(void)
{
    static uint32_t reg_data = AMPL_SCALE;
    
    ad9910_write_reg(ad9910_reg.CFR2, &reg_data, REG_DWORD);
}

void ad9910_enable_lfm_mode(void)
{
    static uint32_t reg_data = LFM_MODE;
    
    ad9910_write_reg(ad9910_reg.CFR2, &reg_data, REG_DWORD);
}

