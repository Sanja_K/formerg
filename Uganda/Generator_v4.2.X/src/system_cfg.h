/******************************************************************************/
/* System Level #define Macros                                                */
/******************************************************************************/
#include <p33Fxxxx.h>
#include <stdint.h>        /* Includes uint16_t definition                    */
#include <stdbool.h>       /* Includes true/false definition                  */
#include <stdio.h>
#include <stdlib.h>
/* TODO Define system operating frequency */

#define versionPgm     42


/* Microcontroller MIPs (FCY) */
void __delay32(unsigned long cycles);
#define SYS_FREQ        80000000LL
#define FCY             (SYS_FREQ/2)

#define delay_us(x) __delay32(((x*FCY)/1000000L)) // delays x us
#define delay_ms(x) __delay32(((x*FCY)/1000L))  // delays x ms
#define __DELAY_H 1

#define GHz 1000000000LL
#define MHz    1000000LL
#define kHz       1000LL
#define  Hz          1LL



#define BIT_SET(reg, bit) ( reg |= (1<<bit) )
#define BIT_CLR(reg, bit) ( reg &= (~(1<<bit)))
#define BIT_INV(reg, bit) ( reg ^= (1<<bit))

#define BIT0(x) (0<<(x))
#define BIT1(x) (1<<(x))

#define  BITREAD(BYTE,BIT) (BYTE >> BIT) & 1


#define MCHP_SUCCESS                     0x00
#define MCHP_FAILURE                     0x01

#define MCHP_DEVICE_NOT_FOUND            0x02

#define MCHP_DEVICE_IS_STARTED           0x05
#define MCHP_DEVICE_IS_STOPPED           0x06


#define MCHP_DDS_ERROR                  0x07
#define MCHP_LMX_ERROR                  0x08

#define MCHP_INVALID_VERSION             0xF4
#define MCHP_CRC_ERROR                   0xF6
#define MCHP_RUN_ERROR                   0xF8//������ ����������
#define MCHP_CMD_FAILURE                 0xFF //������ �������
//=========================================================
//#define status_cmd_not_def                0xFF           
//#define status_cmd_run_error              0xF8 //����� ������
//#define status_cmd_crc_error              0x01

#define _EN_PWR_CH1_ON   LATDbits.LATD1 = 1;
#define _EN_PWR_CH1_OFF  LATDbits.LATD1 = 0;

#define _EN_PWR_CH2_ON   LATDbits.LATD14 = 1;
#define _EN_PWR_CH2_OFF  LATDbits.LATD14 = 0;

#define _EN_PWR_CH3_ON   LATDbits.LATD13 = 1;
#define _EN_PWR_CH3_OFF  LATDbits.LATD13 = 0;
//---------lit 2 --------------------------------------
#define _RF0_ON   LATFbits.LATF0 = 1;
#define _RF0_OFF  LATFbits.LATF0 = 0;
#define _RF1_ON   LATFbits.LATF1 = 1;
#define _RF1_OFF  LATFbits.LATF1 = 0;
//-----------------------------------------------

#define _EN_Det_1           (LATDbits.LATD12 = 1)
#define _nEN_Det_1           (LATDbits.LATD12 = 0)  

#define _EN_Det_2           (LATDbits.LATD15 = 1)
#define _nEN_Det_2           (LATDbits.LATD15 = 0) 

#define LED1_ON           (LATGbits.LATG15 = 1)
#define LED1_OFF           (LATGbits.LATG15 = 0) 

//#define LED2_ON           (LATFbits.LATF0 = 1)
//#define LED2_OFF           (LATFbits.LATF0 = 0) 
/******************************************************************************/
/* System init                                              */
/******************************************************************************/

#define   PlataNum  0x1 //

#define   addrF1   0x12 //
#define   addrO1   0x21 //



#define   addrF4   0x14 //
#define   addrO4   0x41 //

#define   addrF3   0x13 
#define   addrO3   0x31

//#define   addrF3  0x13 // адрес формирователя GPS
//#define   addrF3O  0x31 // адрес формирователя GPS
//
//#define   addrF4  0x14 // адрес формирователя
//#define   addrF4O  0x41 // адрес ответа

//#define   Diap1     (PORTAbits.RA1)
//#define   Diap2     (PORTAbits.RA9)
//#define   Diap3     (PORTAbits.RA10)

//========================================================

#define   addr1Param 0x00
#define   addr2Param 0x10


#define XST_SUCCESS                     0L
#define XST_FAILURE                     1L
#define XST_DEVICE_NOT_FOUND            2L
#define XST_DEVICE_BLOCK_NOT_FOUND      3L
#define XST_INVALID_VERSION             4L
#define XST_DEVICE_IS_STARTED           5L
#define XST_DEVICE_IS_STOPPED           6L
#define XST_FIFO_ERROR                  7L	

/*
#if defined (addrF1) // если задан формирователь 1 

 // адрес устройства
адрес ответа устройства

#elif defined(addrF3)
#define   addrL addrF3 // адрес устройства
#define   addrO  0x31 // адрес ответа устройства

#elif defined(addrF4)
#define   addrL addrF4 // адрес устройства
#define   addrO  0x41 // адрес ответа устройства


#endif
*/

/******************************************************************************/
/* System Function Prototypes                                                 */
/******************************************************************************/

/* Custom oscillator configuration funtions, reset source evaluation
functions, and other non-peripheral microcontroller initialization functions
go here. */
void AddrInit(void);
void ConfigureOscillator(void); /* Handles clock switching/osc initialization */
void ConfigurePorts(void);
void ConfigureTimers0(void);
void LLDIV (uint64_t numer, uint64_t denom,  uint64_t *quot, uint64_t *rem);
long Nod(long a, long b);
int CalcCRC(int dtSum, int dt);
int CalcCRCm(int dtSum, unsigned char* dt, int cnt);
int AddrCheck (void) ;
//void NopReg(int Nop);
