/*
 * File:   timers.c
 * Author: Alexandr
 *
 * Created on September 20, 2017, 4:53 PM
 */


#include <xc.h>
#include "system_cfg.h"
#include "user.h"
#include "executeCMD.h"
#include "channel.h"


void initChannelPorts1(void)
{

//-----------SW-- DA15 DA16-(lit1)---DA3(lit2)----------
    _TRISD3 = 0;
    _TRISD12 = 0;    
    LATDbits.LATD3 = 0;  ///   
    LATDbits.LATD12 = 0; //     
//------------SW--DA18----RF1 (lit1)-------------------
    _TRISF0 = 0;
    LATFbits.LATF0 = 0; //SW1RF1_V1
    _TRISF1 = 0;
    LATFbits.LATF1 = 0; //SW1RF1_V2
 //-------DA19 (lit1)----SW2DA14 SW---(lit2)-----------
    _TRISD2 = 0;  
    LATDbits.LATD2 = 0; // RF1_CTRL
 //-------SW1DA30 (lit1)---SW2DA29 SW---(lit2)---------   
    _TRISF12 = 0;  // SWRF2-V1
    _TRISF13 = 0;  // SWRF2-V2    
    LATFbits.LATF12 = 0; //SW1RF2_V1
    LATFbits.LATF13 = 0; //SW1RF2_V2          
 //-------DA34 (lit1)---------SW2DA21-(lit2)----------
    _TRISB9 = 0;  
    LATBbits.LATB9 = 0; // RF2_DIR
 //----------SW1DA40 (lit1)------SW2DA32 (lit2)---------      
    TRISDbits.TRISD8 = 0; // RF2_CTRL
    LATDbits.LATD8 = 0; //       

}

void initChannelPorts2(void)
{
 //-----------DA3 SW---(lit2)-----------
    _TRISD3 = 0;  
    _TRISD12 = 0;     
    LATDbits.LATD3 = 0; // SW2IF1_V1 
    LATDbits.LATD12 = 0; // SW2IF1_V2
    
 //--------------SW-DA11---RF0 (lit2)---------------   
    _TRISA9 = 0;
    _TRISA10 = 0;  
    LATAbits.LATA9 = 0;  ///   SW2RF1_V1
    LATAbits.LATA10 = 0; //   SW2RF1_V2
 //-----------DA14 SW---(lit2)-----------
    _TRISD2 = 0;  
    LATDbits.LATD2 = 0; // RF1_CTRL
 
 //----------SW2DA21---(lit2)----------------------  
    _TRISB8 = 0;  // 
    _TRISB9 = 0;  //
    LATBbits.LATB8 = 0; //SW2IF2_V1
    LATBbits.LATB9 = 0; //SW2IF2_V2
 
 //----------SW2DA29---(lit2)----------------------  
    _TRISF12 = 0;  // 
    _TRISF13 = 0;  //   
    LATFbits.LATF12 = 0; //SW1RF2_V1
    LATFbits.LATF13 = 0; //SW1RF2_V2   
 
 //--------------SW2DA32---(lit2)---------      
    TRISDbits.TRISD8 = 0; // RF2_CTRL
    LATDbits.LATD8 = 0; //       
    
}

//----------- HMC536LP2 ------------
#define SW1IF1_V1 LATDbits.LATD3
#define SW1IF1_V2 LATDbits.LATD12

//#define DA15RF1     1
//#define DA15RF2     2

void SW1DA15(char sw)// lit1 DA15 SWIF1_V1
{
    if (sw==1)
    {
        SW1IF1_V1 = 1;  /// RF1 SWIF1_CTRL-A //IF1 RF test
    }
    else 
    {
        SW1IF1_V1 = 0;// RF2
    }
}
//----------- HMC536LP2 ------------
//#define DA16RF1     1
//#define DA16RF2     2
void SW1DA16(char sw)// lit1 DA16 SWIF1_V2
{
    if (sw==1)
    {
        SW1IF1_V2 = 1;  /// RF1 SWIF1_CTRL-A //IF2
    }
    else 
    {
        SW1IF1_V2 = 0;// RF2 // test
    }
}


//-------- QPC6044----------------
#define SW1RF1_V1 LATFbits.LATF0
#define SW1RF1_V2 LATFbits.LATF1

//#define SW1DA18RF1     1
//#define SW1DA18RF2     2
//#define SW1DA18RF3     3
//#define SW1DA18RF4     4

//void SW1DA15(char sw)//1
void SW1DA18(char sw) //
{
    if (sw==1)
    {
        SW1RF1_V1 = 0;  ///   RFC => RF1  // 50 Ohm
        SW1RF1_V2 = 1; //    
    }
    else if (sw==2)//J1
    {
        SW1RF1_V1 = 1;  ///    RFC => RF2  //440-1500
        SW1RF1_V2 = 0; //  
    }
    else  if (sw==3)//J2
    {
        SW1RF1_V1 = 0;  ///  RFC => RF3  //1300 - 2290
        SW1RF1_V2 = 0; //      
    }
    else  if (sw==4)//J3
    {
        SW1RF1_V1 = 1;  ///  RFC => RF4 // test
        SW1RF1_V2 = 1; //  
    } 
}
//---------HMC536LP2----------------
#define SW1RF1_CTRL LATDbits.LATD2

//#define SW1DA19RF1     1
//#define SW1DA19RF2     2

void SW1DA19(char sw)   // lit 1
{
    if (sw == 1)
    {
         SW1RF1_CTRL = 1;  // RF1
    }
    else 
    {
        SW1RF1_CTRL = 0;   // RF2 test
    }

}
//---------HMC536LP2----------------
#define SW1RF1_CTRL LATDbits.LATD2

//#define SW1DA29RF1     1
//#define SW1DA29RF2     2

void SW1DA29(char sw)   // lit 1
{
    if (sw == 1)
    {
         SW1RF1_CTRL = 1;  // RF1
    }
    else 
    {
        SW1RF1_CTRL = 0;   // RF2 test
    }

}
//-------- QPC6044----------------
#define SW1RF2_V1 LATFbits.LATF12
#define SW1RF2_V2 LATFbits.LATF13

//#define SW1DA30RF1     1
//#define SW1DA30RF2     2
//#define SW1DA30RF3     3
//#define SW1DA30RF4     4

void SW1DA30(char sw)//lit 1 RF 2 new
{
    if (sw==1)
    {
         SW1RF2_V1 = 0;  ///  RFC => RF1  // 50 Ohm
         SW1RF2_V2 = 1; //  
    }
    else if (sw==2)//J1
    {
         SW1RF2_V1 = 1;  ///  RFC => RF2  //490-1575
         SW1RF2_V2 = 0; //
    }
    else  if (sw==3)//J2
    {
         SW1RF2_V1 = 0;  ///  RFC => RF3  //1500 - 2500
         SW1RF2_V2 = 0; // 
    }
    else  if (sw==4)//J3
    {
         SW1RF2_V1 = 1;  ///    RFC => RF4 // test
         SW1RF2_V2 = 1; //
    }    
}
//---------HMC536LP2----------------
#define SW1RF2_DIR LATBbits.LATB9

//#define SW1DA34RF1     1 //test
//#define SW1DA34RF2     2

void SW1DA34(char sw)//(lit 1) new
{
    if (sw == 1) // SWRF1_CTRL-A
    {
         SW1RF2_DIR = 1;  // RF1 ch_out
    }
    else 
    {
        SW1RF2_DIR = 0;  // RF2 RF_CH2_1
    }
}

//---------HMC536LP2----------------
#define SW1RF2_CTRL LATDbits.LATD8

//#define SW1DA40RF1     1 //test
//#define SW1DA40RF2     2
//void SW1DA34(char sw)(lit 1) old
void SW1DA40(char sw)//(lit 1) new 
{
    if (sw==1) // SWRF1_CTRL-A
    {
         SW1RF2_CTRL = 1;  // RF1 Test
    }
    else 
    {
        SW1RF2_CTRL = 0;  // RF2
    }
}
//----QPC6044-------------
#define SW2IF1_V1 LATDbits.LATD3
#define SW2IF1_V2 LATDbits.LATD12

//#define SW2DA3RF1     1
//#define SW2DA3RF2     2
//#define SW2DA3RF3     3
//#define SW2DA3RF4     4
//void SW1DA13(char sw)//lit1 old
//void SW2DA4(char sw)// lit2
void SW2DA3(char sw)  //
{
    if (sw == 1)
    {
        SW2IF1_V1 = 0;  /// RFC => RF1 
        SW2IF1_V2 = 1; //     
    }
    else     
    if (sw == 2)//J1 
    {
        SW2IF1_V1 = 1;  /// RFC => RF2  test
        SW2IF1_V2 = 0; //   
    }
    else  if (sw == 3)//J2
    {
        SW2IF1_V1 = 0;  ///  RFC => RF3  50 Ohm
        SW2IF1_V2 = 0; //  
    }
    else  if (sw == 4)//J3
    {
        SW2IF1_V1 = 1;  /// RFC => RF4 
        SW2IF1_V2 = 1; //     
    } 
}
//----QPC6044-------------
#define SW2RF1_V1 LATAbits.LATA9
#define SW2RF1_V2 LATAbits.LATA10

//#define SW2DA11RF1     1
//#define SW2DA11RF2     2
//#define SW2DA11RF3     3
//#define SW2DA11RF4     4
//void SW2DA9(char sw)// lit 2 old
void SW2DA11(char sw)// lit 2++
{
    if (sw == 1)
    {
        SW2RF1_V1 = 0;  /// RFC => RF1 test
        SW2RF1_V2 = 1; //     
    }
    else if (sw == 2)//J1 
    {
        SW2RF1_V1 = 1;  /// RFC => RF2 2975
        SW2RF1_V2 = 0; //   
    }
    else  if (sw == 3)//J2
    {
        SW2RF1_V1 = 0;  ///  RFC => RF3 4100 
        SW2RF1_V2 = 0; //  
    }
    else  if (sw == 4)//J3
    {
        SW2RF1_V1 = 1;  /// RFC => RF4 4600
        SW2RF1_V2 = 1; //     
    } 
}

///-----------HMC536LP2----------------------
#define SW2RF1_CTRL LATDbits.LATD2

//#define SW2DA14RF1     1
//#define SW2DA14RF2     2

void SW2DA14(char sw)// lit 2++
{
    if (sw==1)
    {
         SW2RF1_CTRL = 1;  ///RF1
    }
    else 
    {
        SW2RF1_CTRL = 0;// RF2 // test
    }

}

//--------QPC6044---------
#define SW2IF2_V1 LATBbits.LATB8
#define SW2IF2_V2 LATBbits.LATB9
//----QPC6044-------------
//#define SW2DA21RF1     1
//#define SW2DA21RF2     2
//#define SW2DA21RF3     3
//#define SW2DA21RF4     4

void SW2DA21(char sw)//SWDA20(SWJ0)// lit2
{
    if (sw == 1)
    {
         SW2IF2_V1 = 0;  // RFC => RF1 
         SW2IF2_V2 = 1; // 
    }
    else     
    if (sw == 2)
    {
         SW2IF2_V1 = 1;  //  RFC => RF2 test
         SW2IF2_V2 = 0; // 
    }
    else  if (sw == 3)
    {
         SW2IF2_V1 = 0;  //  RFC => RF3 50 Ohm
         SW2IF2_V2 = 0; //  
    }
    else  if (sw == 4)
    {
         SW2IF2_V1 = 1;  //  RFC => RF4 
         SW2IF2_V2 = 1; //  
    }    
}

//-------QPC6044-------------
#define SW2RF2_V1 LATFbits.LATF12
#define SW2RF2_V2 LATFbits.LATF13

//#define SW2DA29RF1     1 //test
//#define SW2DA29RF2     2
//#define SW2DA29RF3     3
//#define SW2DA29RF4     4
//void SW1DA33(char sw)//1
//void SW2DA25(char sw)//2

void SW2DA29(char sw)//2
{
    if (sw==1)
    {
        SW2RF2_V1 = 0;  //   RFC => RF1 test
        SW2RF2_V2 = 1; //   
    }
    else     
    if (sw==2)//
    {
        SW2RF2_V1 = 1;  //   RFC => RF2 440
        SW2RF2_V2 = 0; //    
    }
    else  if (sw==3)//
    {
        SW2RF2_V1 = 0;  //   RFC => RF3 1500
        SW2RF2_V2 = 0; //    
    }
    else  if (sw==4)//
    {
        SW2RF2_V1 = 1;  //   RFC => RF4 4600
        SW2RF2_V2 = 1; //   
    } 
}

//-----------HMC536LP2-----------------
#define SW2RF2_CTRL LATDbits.LATD8

//#define SW2DA32RF1     1 
//#define SW2DA32RF2     2
//void SW2DA28(char sw)//RF2_CTRL (lit 2)
void SW2DA32(char sw)//RF2_CTRL (lit 2)
{
    if (sw == 1)
    {
         SW2RF2_CTRL = 1;  ///RF1 test
    }
    else 
    {
        SW2RF2_CTRL = 0;// RF2_CTRL
    }

}




void SWtest5002500(char sw)//RF2_CTRL (lit 2)
{
    if (sw == SWALLTEST)
    {
        SW2DA21(SW2DA21RF2);// test
        SW2DA29(SW2DA29RF1);// test    
        SW2DA32(SW2DA32RF1);// test
    }
    else 
    {
        
        
    }

}

void SWtest25006000(char sw)//RF2_CTRL (lit 2)
{
    if (sw == SWALLTEST)
    {
        SW2DA3(SW2DA3RF2); 
        SW2DA11(SW2DA11RF1);
        SW2DA14(SW2DA14RF2);        
    }
    else 
    {
        
        
    }

}
//#define SWALLTEST     0 
//#define SWRFOUT         1 

void SWtest100500(char sw)//RF2_CTRL (lit 2)
{
    if (sw == SWALLTEST)
    {
      //  SW1DA19(SW1DA19RF2);// test 
        SW1DA40(SW1DA40RF1);
    }
    else 
    {
        SW1DA40(SW1DA40RF2);
    }

}

//#define SWALLTEST     0 
//#define SWRF1         1 
//#define SWRFL2        1 
//#define SWRF2         2
//#define SWRFL1        2
//#define SWRF3         3

void SWtestGNSS(char sw)//RF2_CTRL (lit 2)
{
    if (sw == SWALLTEST)
    {
        SW1DA15(DA15RF2);   // test point L1 L2  
        SW1DA16(DA16RF1);   // test point
        SW1DA18(SW1DA18RF4);// test point
        SW1DA30(SW1DA30RF4);// test point
        SW1DA19(SW1DA19RF2);  
        SW1DA29(SW1DA29RF2);     
    }
    else if (sw == SWRFL2)
    {
      
        SW1DA18(SW1DA18RF2);
        SW1DA19(SW1DA19RF1);             
        SW1DA15(DA15RF1);  

    }else if (sw == SWRFL1)
    {    
        SW1DA30(SW1DA30RF1); // DA21     
        SW1DA29(SW1DA29RF1); // DA21 //samne DA19
        SW1DA34(SW1DA34RF2);         
        SW1DA16(DA16RF2);
    
    }
    

}