/******************************************************************************/
/*Files to Include                                                            */
/******************************************************************************/
#include <xc.h>         /* XC8 General Include File */
#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#include "system_cfg.h"
#include "adl5240.h"
#include "hmc1122.h"
#include "channel.h"
#include "softwareSPI.h"


unsigned char plata_num, addrL, addrO;

#define   Diap1     (PORTBbits.RB0)
#define   Diap2     (PORTBbits.RB1)


void AddrInit(void)
{ 
    
    _TRISG15 = 0;
    _TRISB0 = 1;    
    _TRISB1 = 1;    
    
    if ((Diap1 == 0)&&(Diap2 == 1 ))
   {
        addrL = addrF1;// ����� ���������� 0x12
        addrO = addrO1; // ����� ������ 0x21    
        LED1_ON;
        delay_ms(300);
        LED1_OFF;
        delay_ms(300);
        delay_ms(300);        
        
            
        
    }else if ((Diap2 == 0 )&&(Diap1 == 1 ))
    {
        addrL = addrF4;// ����� ���������� 0x14
        addrO = addrO4; // ����� ������ 0x41   
        LED1_ON;
        delay_ms(300);
        LED1_OFF;
        delay_ms(300);
        LED1_ON;
        delay_ms(300);
        LED1_OFF;
        delay_ms(300);  
        delay_ms(300);
    
    } else if ((Diap1 == 0)&&(Diap2 == 0))
    {
        addrL = addrF3;// ����� ���������� 0x13
        addrO = addrO3; // ����� ������ 0x31  
        LED1_ON;
        delay_ms(300);
        LED1_OFF;
        delay_ms(300);
        LED1_ON;
        delay_ms(300);
        LED1_OFF;
        delay_ms(300);  
        LED1_ON;
        delay_ms(300);
        LED1_OFF;
        delay_ms(300);
        delay_ms(300);
 
    }
    else
    {
        while (1)
        {
        
            LED1_OFF;
            delay_ms(50);
            LED1_ON;
            delay_ms(50);        
 
        }
    
    }
}

void ConfigureOscillator(void)
{
    /* TODO Add clock switching code if appropriate.  */
// Configure Oscillator to operate the device at 40Mhz
// Fosc= Fin*M/(N1*N2), Fcy=Fosc/2
// Fosc= 8M*40/(2*2)=80Mhz for 8M input clock

	PLLFBD=38;					// PLLFBD+2 = M=40
	CLKDIVbits.PLLPOST=0;		// N1=2
	CLKDIVbits.PLLPRE=0;		// N2=2
	OSCTUN=0;					// Tune FRC oscillator, if FRC is used
    
// Disable Watch Dog Timer
	RCONbits.SWDTEN=0;

// clock switching to incorporate PLL
	__builtin_write_OSCCONH(0x03);		// Initiate Clock Switch to Primary Oscillator with PLL (NOSC=0x03)										
	__builtin_write_OSCCONL(OSCCON || 0x01);		// Start clock switching
	while (OSCCONbits.COSC != 0x03);	// Wait for Clock switch to occur
	while(OSCCONbits.LOCK!=1);		// Wait for PLL to lock
    /* Typical actions in this function are to tweak the oscillator tuning
    register, select new clock sources, and to wait until new clock sources
    are stable before resuming execution of the main project. */
    
    
}
void ConfigurePorts(void)
{

//    //-------LED-----------------
    _TRISG15 = 0; // out LED
    _LATG15 = 0;  

////---------ADL5240--------------
    
    adl5240_init();
    hmc1122_init();
   if ((addrL == addrF1)||(addrL ==addrF3))
   {      
    initChannelPorts1();
   }
    else if (addrL == addrF4)
   {  
        initChannelPorts2();  
        _TRISF0 = 0;
        LATFbits.LATF0 = 0; //_RF0_ON
        _TRISF1 = 0;
        LATFbits.LATF1 = 0; //_RF1_ON
        
   }
    else
    {
        return;
    }
////---------hmc1112--------------    
//    TRISGbits.TRISG12 = 0; // LE2    
//    LATGbits.LATG12 = 0;
// //---------DDS1--------------------
    LATDbits.LATD6 = 1;     
    TRISDbits.TRISD6 = 0; // out _CS_DDS1
  
    LATDbits.LATD11 = 1;     
    TRISDbits.TRISD11 = 0; // out _CS_DDS2  

    LATDbits.LATD0 = 0;     
    TRISDbits.TRISD0 = 0; // out _IO RESET 
    
    LATGbits.LATG6 = 0;     
    TRISGbits.TRISG6 = 0;// _SCLK
     
    LATDbits.LATD7 = 0;    
    TRISDbits.TRISD7 = 0;// _IOUpdate
   
    LATDbits.LATD5 = 0;    
    TRISDbits.TRISD5 = 0;// _reset_DDS1

    LATDbits.LATD10 = 0;     
    TRISDbits.TRISD10 = 0;// _reset_DDS2   
     
    LATDbits.LATD4 = 0;     //_PWD_DDS1    
    TRISDbits.TRISD4 = 0;// _PWD_DDS1

    LATDbits.LATD9 = 0;    //_PWD_DDS2    
    TRISDbits.TRISD9 = 0;//_PWD_DDS2     

    LATGbits.LATG8 = 0;    
    TRISGbits.TRISG8 = 0;// v_o_DATA  SDO2 

    TRISGbits.TRISG7 = 1;// v_o_DATA SDI2 
//
////---------------------------------    
   
   TRISBbits.TRISB0 = 1; // v_i_LD 
   TRISBbits.TRISB1 = 1; // v_o_CE v_o_RFOUT 


    TRISDbits.TRISD15 = 0; // TX 485
    LATDbits.LATD15 = 1; //    
    
    TRISDbits.TRISD1 = 0; // EN_PWR_CH1
    //-----------DA13 SW------------------------------
    // spi LMX2572    
    _TRISB15 = 0;   //SCK
    _TRISB14 = 0;   //MOSI

    _TRISB11 = 0;   //CSB1
    _TRISB10 = 0;   //CSB2
    _TRISB13 = 0;   //CSB3
    _TRISB12 = 0;   //CSB4

    CSB1 = 1;
    CSB2 = 1;
    CSB3 = 1;
    CSB4 = 1;
// power 3.3VCO1, 3.3VCO2
    _TRISD1 = 0;   //EN_PWR_CH1
//    EN_PWR_CH1 = 1;
// power 3.3VCO3, 3.3VCO4
    _TRISD14 = 0;   //EN_PWR_CH2
    _TRISD13 = 0;   //EN_PWR_CH3    
    
//    EN_PWR_CH2 = 1;  

}

// ���������� ������ � ������� �� �������
  void LLDIV (uint64_t numer, uint64_t denom,  uint64_t *quot, uint64_t *rem)
{
    uint64_t a,b,b1;
    uint64_t q,r;// �������, ������� 
    a= numer;
    b = denom;
    b1 = b;
    while (b1<=a) b1*=2;
     q = 0;
     r = a;
     while (b1!=b) {
      b1/=2; q*=2;
      if (r>=b1) { r-=b1; q++; }
     }
     *rem = r;
     *quot = q;
}
  
    // // �������� ������� ����� ��������
  long Nod(long a, long b)
{
    while (a && b)
        if (a >= b)
           a -= b;
        else
           b -= a;
    return a | b;
}
  
int CalcCRC(int dtSum, int dt) // ���������� CRC � ���������� ������
{
	dtSum = dtSum + dt;
	dtSum = dtSum % 255;

	return dtSum;
}

int CalcCRCm(int dtSum, unsigned char* dt, int cnt) // ���������� CRC, ������, ���������� ������ � �������
{
	int idt;
	idt = 0;
	while (idt < cnt)
	{
		dtSum = dtSum + dt[idt];
		dtSum = dtSum % 255;
		idt++;
	}
	return dtSum;
}
/*
void NopReg(int Nop)
{
    int i;
    for(i = 0; i < Nop; i++)
    {
        Nop();
    }
}
*/



  //  var |= (1 << 3) | (1 << 5); ���������� ���� 3 � 5
  // var &= ~((1 << 2) | (1 << 4)); �������� ���� 2 � 4