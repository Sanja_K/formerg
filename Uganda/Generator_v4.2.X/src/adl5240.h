#ifndef _ADL5240_H
#define _ADL5240_H

#include <xc.h>


//#define LE2_ADL     LATBbits.LATB6  
/*
enum adl5240 {
    PA1 = 0,
    PA2,
};
*/
void adl5240_init(void);

void adl5240_set_gain_amp1(unsigned char gain);
void adl5240_set_gain_amp2(unsigned char gain);
void adl5240_set_gain_amp3(unsigned char gain);

//void adl5240_set_gain(enum adl5240 amp, unsigned char gain);

#endif

