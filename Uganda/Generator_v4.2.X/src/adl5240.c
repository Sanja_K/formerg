#include "adl5240.h"



#define CLK_ADL     _LATA2
#define DATA_ADL    _LATA3
#define LE1_ADL     _LATA9    //DA20
#define LE2_ADL     _LATA1   //DA31
#define LE3_ADL     _LATA10  //DA41

#define amp1_le_seq()   {LE1_ADL = 1; Nop(); Nop(); Nop(); LE1_ADL = 0;}
#define amp2_le_seq()   {LE2_ADL = 1; Nop(); Nop(); Nop(); LE2_ADL = 0;}
#define amp3_le_seq()   {LE3_ADL = 1; Nop(); Nop(); Nop(); LE3_ADL = 0;}




void adl5240_init(void)
{
////---------ADL5240--------------

    _TRISA2 = 0; // clk 
    _LATA2 = 0;
    _TRISA3 = 0; // Data 
    _LATA3 = 0;    
    //--------------------------
    _TRISA9 = 0; // LE1 
    _LATA9 = 0;    
    _TRISA1 = 0; // LE2 
    _LATA1 = 0;   
    _TRISA10 = 0; // LE3 
    _LATA10 = 0; 
    
}


static void adl5240_write(unsigned char val)
{
    unsigned char i;
    val <<= 2;
    
    for(i = 0; i < 6; i++)
    {
        if(val & 0x80)
            DATA_ADL = 1;
        else
            DATA_ADL = 0;
        
        CLK_ADL = 1;
        val <<= 1;
        CLK_ADL = 0;
    }
    
    DATA_ADL = 0;
}
 void adl5240_set_gain_amp(unsigned char gain)
{
     
     if(gain > 63)
        gain = 0;   
     
//    amp1_le_seq();
    adl5240_write(gain);
//    amp1_le_seq();
}

void adl5240_set_gain_amp1(unsigned char gain) // L2
{
    amp1_le_seq();
    adl5240_set_gain_amp(gain);
    amp1_le_seq();
}

void adl5240_set_gain_amp2(unsigned char gain) // 100- 500
{

    amp2_le_seq();
    adl5240_set_gain_amp(gain);
    amp2_le_seq();
}
void adl5240_set_gain_amp3(unsigned char gain) // L1
{
 
    amp3_le_seq();
    adl5240_set_gain_amp(gain);
    amp3_le_seq();
}

/*
void adl5240_set_gain(enum adl5240 amp, unsigned char gain)
{
    if(gain > 63)
        gain = 63;
    
    if(amp == PA1)
        adl5240_set_gain_amp1(gain);
    else if(amp == PA2)
        adl5240_set_gain_amp2(gain);
}*/