#include "ad9910.h"
#include "ad9914.h"
#include "former_signal.h"
#include "system_cfg.h"
#include "executeCMD.h"

uint8_t former_init_1(void)
{
    uint8_t status;
    
    ad9910_off_1();
    ad9910_unselect_dds_1();
    delay_us(1);
    ad9910_on_1();
    delay_us(1);
    ad9910_reset_1(); 
    ad9910_select_dds_1();
    ad9910_spi_3wire_mode();
    ad9910_unselect_dds_1();    
    delay_ms(1);
    status = ad9910_init_pll_1();
    delay_ms(1);
    //ad9910_enable_amp_scale();
    return status;
}
void ad9910_deinit_1(void)
{
    ad9910_reset_1(); 
    ad9910_off_1();

 //   ad9910_enable_amp_scale();
}


uint8_t former_init_2(void)
{
    uint8_t status;
    
    ad9910_off_2();
    ad9910_unselect_dds_2();
    delay_us(1);
    ad9910_on_2();
    delay_us(1);
    ad9910_reset_2();    
    ad9910_select_dds_2();
    ad9910_spi_3wire_mode();
    ad9910_unselect_dds_2();
    delay_ms(1);
    status = ad9910_init_pll_2();
    delay_ms(1);
 //   ad9910_enable_amp_scale();
    return status;
}

void ad9910_deinit_2(void)
{
    ad9910_reset_2();
    ad9910_off_2();

 //   ad9910_enable_amp_scale();
}

uint8_t former_init_3(void)
{
    uint8_t status;
    ad9914_unselect_dds_2();
    Nop();

    ad9914_pow_on_2();
    Nop();
    ad9914_reset_2();    
    delay_us (2);
    
    ad9914_select_dds_2();
    delay_us (10);    
    status = ad9914_spi_3wire_mode();
    delay_us (1);
    ad9914_init_dac_cal();
    ad9914_set_profile_mode();
    ad9914_unselect_dds_2();    
    delay_us (300);    
 //   DDS9914_1_Select = 1;
 //   ad9910_enable_amp_scale();
    return status;
}



void  ad9914_deinit_3 (void)
{

    ad9914_pow_off_2();
}



void repeat_init_former(void)
{
    ad9910_reset_2();
//    ad9910_spi_3wire_mode();
    ad9910_init_pll_2();
    ad9910_enable_amp_scale();
}
void set_mode_lfm_1(uint32_t freq, uint32_t dev, uint32_t speed)
{
    uint32_t freq_lo = KHZ_TO_FTW(freq) - KHZ_TO_FTW(dev);
    uint32_t freq_hi = KHZ_TO_FTW(freq) + KHZ_TO_FTW(dev);
    uint32_t step_size = (2 * dev * speed * LFM_POINT_TIME) / 1000;
    uint32_t ramp_rate = DIGITAL_RAMP_RATE;
    uint64_t freq_hi_lo = (((uint64_t)freq_hi) << 32) +
                freq_lo;
    step_size = HZ_TO_FTW(step_size);
    uint64_t step_lo_hi = (((uint64_t)step_size) << 32) +
                step_size;
    ad9910_select_dds_1();
    ad9910_write_reg(ad9910_reg.DRL, &freq_hi_lo, REG_QWORD);
    ad9910_unselect_dds_1();
    delay_us (1);
    ad9910_select_dds_1();
    ad9910_write_reg(ad9910_reg.DRS, &step_lo_hi, REG_QWORD);
    ad9910_unselect_dds_1();
    delay_us (1);    
    ad9910_select_dds_1();
    ad9910_write_reg(ad9910_reg.DRR, &ramp_rate, REG_DWORD);
    ad9910_unselect_dds_1();  
    delay_us (1);     
    ad9910_select_dds_1();    
    ad9910_enable_lfm_mode();
    ad9910_unselect_dds_1(); 
    
    ad9910_select_dds_1();    
    ad9910_set_ampl_fs();
    ad9910_unselect_dds_1();     
    
}

void set_mode_lfm_2(uint32_t freq, uint32_t dev, uint32_t speed)
{
    uint32_t freq_lo = KHZ_TO_FTW(freq) - KHZ_TO_FTW(dev);
    uint32_t freq_hi = KHZ_TO_FTW(freq) + KHZ_TO_FTW(dev);
    uint32_t step_size = (2 * dev * speed * LFM_POINT_TIME) / 1000;
    uint32_t ramp_rate = DIGITAL_RAMP_RATE;
    uint64_t freq_hi_lo = (((uint64_t)freq_hi) << 32) +
                freq_lo;
    step_size = HZ_TO_FTW(step_size);
    uint64_t step_lo_hi = (((uint64_t)step_size) << 32) +
                step_size;
    ad9910_select_dds_2();
    ad9910_write_reg(ad9910_reg.DRL, &freq_hi_lo, REG_QWORD);
    ad9910_unselect_dds_2();
    delay_us (1);
    ad9910_select_dds_2();
    ad9910_write_reg(ad9910_reg.DRS, &step_lo_hi, REG_QWORD);
    ad9910_unselect_dds_2();
    delay_us (1);    
    ad9910_select_dds_2();
    ad9910_write_reg(ad9910_reg.DRR, &ramp_rate, REG_DWORD);
    ad9910_unselect_dds_2();  
    delay_us (1);     
    ad9910_select_dds_2();    
    ad9910_enable_lfm_mode();
    ad9910_unselect_dds_2(); 
}

void set_mode_freq_1(uint32_t freq)
{
    uint64_t freq_lo = KHZ_TO_FTW(freq) ;
    ad9910_select_dds_1();        
    ad9910_set_freq(ad9910_reg.ST_PROF[0], freq_lo);   
    ad9910_unselect_dds_1();  
    
    
     ad9910_select_dds_1();    
    ad9910_set_ampl_fs();
 //   ad9910_set_amp(50);
    ad9910_unselect_dds_1();    
}
void set_mode_freq_2(uint32_t  freq)
{
    uint64_t freq_lo = KHZ_TO_FTW(freq) ;    
        
    ad9910_select_dds_2();        
    ad9910_set_freq(ad9910_reg.ST_PROF[0], freq_lo);   
    ad9910_unselect_dds_2();   
    
     ad9910_select_dds_2();    
 //   ad9910_set_ampl_fs();
//    ad9910_set_amp(50);
    ad9910_unselect_dds_2();    
}

static uint32_t convert_hz_to_ftw(uint64_t freq_abs, uint64_t ad9914_sys_freq)
{
    return (uint32_t)((freq_abs * (1ULL << 32)) / ad9914_sys_freq);
}

//-------------------------------------------------------------
void set_mode_freq_3(uint64_t  freq)
{
    uint32_t freq_lo;    
    freq  = freq*kHz; // kHz to Hz;  
    freq_lo = convert_hz_to_ftw(freq,Freq_dds2_F1_Hz);
    ad9914_select_dds_2();        
    ad9914_set_freq(ad9914_reg.PROF_F[0], freq_lo);   
    ad9914_unselect_dds_2();  
    
    
}

//
void set_mode_lfm_3(uint64_t freq, uint64_t dev, uint32_t speed)
{
    uint32_t freq_ftw;
    uint32_t dev_ftw;
    freq = freq*kHz;
    dev = dev*kHz;
   
    freq_ftw = convert_hz_to_ftw(freq, Freq_dds2_F1_Hz);    
    dev_ftw = convert_hz_to_ftw(dev, Freq_dds2_F1_Hz);
    
    uint32_t freq_lo = freq_ftw - dev_ftw;
    uint32_t freq_hi = freq_ftw + dev_ftw;
    uint32_t step_size = (2ULL * dev * speed * 24) / Freq_dds2_F1_Hz;
    uint32_t ramp_rate = RAMP_RATE;
    
    if(step_size == 0)
    {
        step_size = 1;
    }
    step_size = convert_hz_to_ftw(step_size, Freq_dds2_F1_Hz);
    ad9914_select_dds_2();
    ad9914_write_reg(ad9914_reg.DRAMP_LO, &freq_lo, REG_DWORD);
    ad9914_write_reg(ad9914_reg.DRAMP_HI, &freq_hi, REG_DWORD);
    ad9914_write_reg(ad9914_reg.DRAMP_RIS, &step_size, REG_DWORD);
    ad9914_write_reg(ad9914_reg.DRAMP_FAL, &step_size, REG_DWORD);
    ad9914_write_reg(ad9914_reg.DRAMP_RATE, &ramp_rate, REG_DWORD);
    
    ad9914_set_lfm_mode();  
    ad9914_unselect_dds_2();   
}