#include <xc.h>
#include <stdint.h> 
#include "ad9914.h"
#include "system_cfg.h"
//#include "system_cfg.h"

//struct  ad9914_Reg_Def  AD9914RegN;

const struct ad9914 ad9914_reg = {
    .CFR1       = 0x00,
    .CFR2       = 0x01,
    .CFR3       = 0x02,
    .CFR4       = 0x03,
    .DRAMP_LO   = 0x04,
    .DRAMP_HI   = 0x05,
    .DRAMP_RIS  = 0x06,
    .DRAMP_FAL  = 0x07,
    .DRAMP_RATE = 0x08,
    .PROF_F     = 
        {0x0B, 0x0D, 0x0F, 0x11, 0x13, 0x15, 0x17, 0x19},
    .PROF_PA   = 
        {0x0C, 0x0E, 0x10, 0x12, 0x14, 0x16, 0x18, 0x1A},
	.PROF_P1_FTW   = 
        {0x34, 0x35, 0x36, 0x37},

};

/* 
 * functions read/write command and 1/2/4 bytes of data to AD9914
 * available values for reg_size is: REG_BYTE (1 byte), REG_WORD (2 bytes), 
 * REG_DWORD (4 bytes), other values are  forbidden
 */

static void  v_o_DATA( uint8_t ena )
{
    if(ena)
    {   v_o_DATA_1}
    else
    {    v_o_DATA_0;}   
}    

static void  SPIp_write( uint8_t reg )
{ unsigned int i=0;

    SCLK_0;
    delay_us(1);
    for( i=0; i<8; i++ ) 
    {
        v_o_DATA(reg&0x80); // ������ ����� �� ������� ���
        delay_us( 1 );
        SCLK_1;
        reg = reg << 1;
        delay_us( 1 );
        SCLK_0;
    }
    delay_us(1);
    
} 
static unsigned char  SPIp_read( void )
{ unsigned int i=0;
    unsigned char value;
    //������ �������
    SCLK_0;
    value = 0;
//    TRISGbits.TRISG8 = 1;//
    
    delay_us(1);
    // ��������� ������ ������
    for( i=0; i<8; i++ ) 
    {
        value = (value<<1);
        delay_us( 1 );
        SCLK_1;
        if (v_i_DATA)
         value |= 0x01; // ������������� ���
        else
         value &= 0xFE;// ���������� ���      
        delay_us( 1 );
        SCLK_0;
    }   
 //    TRISGbits.TRISG8 = 0;//
    // ���������� ���������
    return value;
} 

/* 
 * at start, we select dds and start transfer in its register, 
 * after that we leave function and dds will be unselected when 
 * all data will be transmit and interrupt will happen 
 */
void ad9914_write_reg(uint8_t reg, void *buf, uint32_t reg_size)
{
    Nop();
//    ad9914_select_dds();
    SPIp_write(reg);
    
    for(; reg_size > 0; reg_size--)
    {
        SPIp_write(((uint8_t*)buf)[reg_size - 1]);
    }    
//------------------------------------------------
    
    ad9914_io_update_set();
    Nop();
    Nop();
    Nop();
    ad9914_io_update_clr();
    
//    ad9914_unselect_dds();
}

void ad9914_read_reg(uint8_t reg, void *buf,uint32_t reg_size)
{
    uint32_t data;
    int i = reg_size;
   // ad9914_select_dds();
    data = 0;
     
    Nop();
    Nop();    
    SPIp_write(reg | READ_MASK);
    
    for(; i > 0; i--)
    {
    //    data = SPIp_read();
        data <<= 8;
        data |= SPIp_read();
    }

    switch(reg_size) 
    {
        case REG_BYTE: 
        {
            uint8_t *ptr = buf;
            *ptr = (uint8_t)data;
            break;
        }
        case REG_WORD:
        {
            uint16_t *ptr = buf;
            *ptr = (uint16_t)data;
            break;
        }
        case REG_DWORD:
        {
            uint32_t *ptr = buf;
            *ptr = (uint32_t)data;
            break;
        }        
        default:
        {
            unsigned int *ptr = buf;
            *ptr = 0x00000000;
            break;
        }
    }    
  //  ad9914_unselect_dds();
}

void ad9914_spi_3wire_mode(void)
{ 
    uint32_t read_data;    
    uint32_t reg_data = ADSPI_3WIRE;
    ad9914_write_reg(ad9914_reg.CFR1, &reg_data, REG_DWORD);
    delay_us (1);
    
    ad9914_read_reg(ad9914_reg.CFR1, &read_data, REG_DWORD);
    
    Nop();
    if (read_data != reg_data)
    {
        //LEDIND1_ON;
        delay_ms(100);
       // LEDIND1_OFF;
        delay_ms(100);     
    }
    
}
void ad9914_spi_3wire_osk_mode(void)
{ 
    uint32_t read_data;    
    uint32_t reg_data = ADSPI_3WIRE_OSK;
    ad9914_write_reg(ad9914_reg.CFR1, &reg_data, REG_DWORD);
    delay_us (1);
    
    ad9914_read_reg(ad9914_reg.CFR1, &read_data, REG_DWORD);
    Nop();
    
    if (read_data != reg_data)
    {
        //LEDIND1_ON;
        delay_ms(100);
       // LEDIND1_OFF;
        delay_ms(100);     
    }
    
}


void ad9914_init_dac_cal(void)
{
    uint32_t reg_data = DAC_CAL_EN;
    ad9914_write_reg(ad9914_reg.CFR4, &reg_data, REG_DWORD);
    
    reg_data = DAC_CAL_DIS;
    ad9914_write_reg(ad9914_reg.CFR4, &reg_data, REG_DWORD);
    
}

void ad9914_set_profile_mode(void)
{
    uint32_t reg_data = PROF_MODE;
    ad9914_write_reg(ad9914_reg.CFR2, &reg_data, REG_DWORD);
}

void ad9914_set_parallel_mode(void)
{

    uint32_t reg_data = PARALL_MODE;
    ad9914_write_reg(ad9914_reg.CFR2, &reg_data, REG_DWORD);

}

void ad9914_set_lfm_mode(void)
{
    uint32_t reg_data = ADLFM_MODE;
    ad9914_write_reg(ad9914_reg.CFR2, &reg_data, REG_DWORD);
}

void ad9914_set_freq(uint8_t reg_addr, uint32_t freq_curr)
{
    ad9914_write_reg(reg_addr, &freq_curr, REG_DWORD);
}

void ad9914_set_amp(uint8_t reg_addr, uint32_t amp)
{
//    uint32_t read_data; 


    ad9914_write_reg(reg_addr, &amp, REG_DWORD);

    Nop();
//    ad9914_select_dds_2();
//    ad9914_read_reg(reg_addr, &read_data, REG_DWORD);
//    ad9914_unselect_dds_2();   
//    
//    if (read_data != amp)
//    {
//        delay_ms(1);
//        delay_ms(1);     
//    }
}

void ad9914_set_phase(uint8_t reg_addr, uint32_t phase_curr)
{
    ad9914_write_reg(reg_addr, &phase_curr, REG_DWORD);
}

void ad9914_dac_on(void)
{
    uint32_t reg_data = ADDAC_ON;
    ad9914_write_reg(ad9914_reg.CFR1, &reg_data, REG_DWORD);
}

void ad9914_dac_off(void)
{
    uint32_t reg_data = ADDAC_OFF;
    ad9914_write_reg(ad9914_reg.CFR1, &reg_data, REG_DWORD);
}





