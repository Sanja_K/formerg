/*
 * File:   timers.c
 * Author: Alexandr
 *
 * Created on September 20, 2017, 4:53 PM
 */


#include "xc.h"
#include "system_cfg.h"
#include "user.h"
#include "executeCMD.h"
#include "channel.h"

char swtest;

//--------QPC6044--------------------
#define SW1RF1_L2_V1 LATDbits.LATD3
#define SW1RF1_L2_V2 LATDbits.LATD12
//--------------------------------------
void SW1DA2_RF1_L2(char sw)//1
{
    if (sw==1)
    {
        SW1RF1_L2_V1 = 1;  //   RFC => RF1 test
        SW1RF1_L2_V2 = 1; //   
    }
    else     
    if (sw==2)//
    {
        SW1RF1_L2_V1 = 0;  //   RFC => RF2 
        SW1RF1_L2_V2 = 0; //    
    }
    else  if (sw==3)//
    {
        SW1RF1_L2_V1 = 1;  //   RFC => RF3 
        SW1RF1_L2_V2 = 0; //    
    }
    else  if (sw==4)//
    {
        SW1RF1_L2_V1 = 0;  //   RFC => RF4 
        SW1RF1_L2_V2 = 1; //   
    } 
}

//--------QPC6044--------------------
#define SW2RF1_L2_V1 LATAbits.LATA9
#define SW2RF1_L2_V2 LATAbits.LATA10
//--------------------------------------
void SW2DA7_RF1_L2(char sw)//2
{
    if (sw==1)
    {
        SW2RF1_L2_V1 = 1;  //   RFC => RF1  test
        SW2RF1_L2_V2 = 1; //   
    }
    else     
    if (sw==2)//
    {
        SW2RF1_L2_V1 = 0;  //   RFC => RF2 
        SW2RF1_L2_V2 = 0; //    
    }
    else  if (sw==3)//
    {
        SW2RF1_L2_V1 = 1;  //   RFC => RF3 
        SW2RF1_L2_V2 = 0; //    
    }
    else  if (sw==4)//
    {
        SW2RF1_L2_V1 = 0;  //   RFC => RF4 
        SW2RF1_L2_V2 = 1; //   
    } 
}

//--------QPC6044--------------------
#define SW1RF2_L2_V1 LATBbits.LATB8
#define SW1RF2_L2_V2 LATBbits.LATB9
//--------------------------------------
void SW1DA4_RF2_L2(char sw)//3
{
    if (sw==1)
    {
        SW1RF2_L2_V1 = 1;  //   RFC => RF1 
        SW1RF2_L2_V2 = 1; //   
    }
    else     
    if (sw==2)//
    {
        SW1RF2_L2_V1 = 0;  //   RFC => RF2 
        SW1RF2_L2_V2 = 0; //    
    }
    else  if (sw==3)//
    {
        SW1RF2_L2_V1 = 1;  //   RFC => RF3 
        SW1RF2_L2_V2 = 0; //    
    }
    else  if (sw==4)//
    {
        SW1RF2_L2_V1 = 0;  //   RFC => RF4 
        SW1RF2_L2_V2 = 1; //   
    } 
}

//--------QPC6044--------------------
#define SW2RF2_L2_V1 LATFbits.LATF12
#define SW2RF2_L2_V2 LATFbits.LATF13
//--------------------------------------
void SW2DA25_RF2_L2(char sw)//4
{
    if (sw==1)
    {
        SW2RF2_L2_V1 = 1;  //   RFC => RF1 
        SW2RF2_L2_V2 = 1; //   
    }
    else     
    if (sw==2)//
    {
        SW2RF2_L2_V1 = 0;  //   RFC => RF2 
        SW2RF2_L2_V2 = 0; //    
    }
    else  if (sw==3)//
    {
        SW2RF2_L2_V1 = 1;  //   RFC => RF3 
        SW2RF2_L2_V2 = 0; //    
    }
    else  if (sw==4)//
    {
        SW2RF2_L2_V1 = 0;  //   RFC => RF4 
        SW2RF2_L2_V2 = 1; //   
    } 
}


//----------- HMC536LP2 ------------
#define SW1RF1_L1_V1 LATDbits.LATD2

//#define DA15RF1     1
//#define DA15RF2     2

void SW1DA18_RF1_L1(char sw)// lit1 DA15 SWIF1_V1
{
    if (sw==1)
    {
        SW1RF1_L1_V1 = 1;  /// RF1 SWIF1_CTRL-A //IF1 RF test
    }
    else 
    {
        SW1RF1_L1_V1 = 0;// RF2
    }
}

//----------- HMC536LP2 ------------
#define SW2RF2_L1_V1 LATBbits.LATB9

//#define DA15RF1     1
//#define DA15RF2     2

void SW1DA25_RF2_L1(char sw)// lit1 DA15 SWIF1_V1
{
    if (sw==1)
    {
        SW2RF2_L1_V1 = 1;  /// RF1 SWIF1_CTRL-A //IF1 RF test
    }
    else 
    {
        SW2RF2_L1_V1 = 0;// RF2
    }
}

//----------- HMC536LP2 ------------
#define SW1RF3_L1_V1 LATDbits.LATD8

//#define DA15RF1     1
//#define DA15RF2     2

void SW1DA29_RF3_L1(char sw)// lit1 DA29 SWIF1_V1
{
    if (sw==1)
    {
        SW1RF3_L1_V1 = 1;  /// RF1 SWIF1_CTRL-A //IF1 RF test
    }
    else 
    {
        SW1RF3_L1_V1 = 0;// RF2
    }
}