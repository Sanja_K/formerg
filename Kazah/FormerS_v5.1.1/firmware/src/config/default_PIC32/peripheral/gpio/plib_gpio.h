/*******************************************************************************
  GPIO PLIB

  Company:
    Microchip Technology Inc.

  File Name:
    plib_gpio.h

  Summary:
    GPIO PLIB Header File

  Description:
    This library provides an interface to control and interact with Parallel
    Input/Output controller (GPIO) module.

*******************************************************************************/

/*******************************************************************************
* Copyright (C) 2019 Microchip Technology Inc. and its subsidiaries.
*
* Subject to your compliance with these terms, you may use Microchip software
* and any derivatives exclusively with Microchip products. It is your
* responsibility to comply with third party license terms applicable to your
* use of third party software (including open source software) that may
* accompany Microchip software.
*
* THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
* EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
* WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
* PARTICULAR PURPOSE.
*
* IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
* INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
* WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
* BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
* FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
* ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
* THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
*******************************************************************************/

#ifndef PLIB_GPIO_H
#define PLIB_GPIO_H

#include <device.h>
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

    extern "C" {

#endif
// DOM-IGNORE-END

// *****************************************************************************
// *****************************************************************************
// Section: Data types and constants
// *****************************************************************************
// *****************************************************************************


/*** Macros for GPIO_LED1 pin ***/
#define GPIO_LED1_Set()               (LATGSET = (1<<15))
#define GPIO_LED1_Clear()             (LATGCLR = (1<<15))
#define GPIO_LED1_Toggle()            (LATGINV= (1<<15))
#define GPIO_LED1_OutputEnable()      (TRISGCLR = (1<<15))
#define GPIO_LED1_InputEnable()       (TRISGSET = (1<<15))
#define GPIO_LED1_Get()               ((PORTG >> 15) & 0x1)
#define GPIO_LED1_PIN                  GPIO_PIN_RG15

/*** Macros for SCLK_XC pin ***/
#define SCLK_XC_Set()               (LATCSET = (1<<1))
#define SCLK_XC_Clear()             (LATCCLR = (1<<1))
#define SCLK_XC_Toggle()            (LATCINV= (1<<1))
#define SCLK_XC_OutputEnable()      (TRISCCLR = (1<<1))
#define SCLK_XC_InputEnable()       (TRISCSET = (1<<1))
#define SCLK_XC_Get()               ((PORTC >> 1) & 0x1)
#define SCLK_XC_PIN                  GPIO_PIN_RC1

/*** Macros for SDI_XC pin ***/
#define SDI_XC_Set()               (LATCSET = (1<<2))
#define SDI_XC_Clear()             (LATCCLR = (1<<2))
#define SDI_XC_Toggle()            (LATCINV= (1<<2))
#define SDI_XC_OutputEnable()      (TRISCCLR = (1<<2))
#define SDI_XC_InputEnable()       (TRISCSET = (1<<2))
#define SDI_XC_Get()               ((PORTC >> 2) & 0x1)
#define SDI_XC_PIN                  GPIO_PIN_RC2

/*** Macros for LE_XC1 pin ***/
#define LE_XC1_Set()               (LATCSET = (1<<3))
#define LE_XC1_Clear()             (LATCCLR = (1<<3))
#define LE_XC1_Toggle()            (LATCINV= (1<<3))
#define LE_XC1_OutputEnable()      (TRISCCLR = (1<<3))
#define LE_XC1_InputEnable()       (TRISCSET = (1<<3))
#define LE_XC1_Get()               ((PORTC >> 3) & 0x1)
#define LE_XC1_PIN                  GPIO_PIN_RC3

/*** Macros for LE_XC2 pin ***/
#define LE_XC2_Set()               (LATCSET = (1<<4))
#define LE_XC2_Clear()             (LATCCLR = (1<<4))
#define LE_XC2_Toggle()            (LATCINV= (1<<4))
#define LE_XC2_OutputEnable()      (TRISCCLR = (1<<4))
#define LE_XC2_InputEnable()       (TRISCSET = (1<<4))
#define LE_XC2_Get()               ((PORTC >> 4) & 0x1)
#define LE_XC2_PIN                  GPIO_PIN_RC4

/*** Macros for SCLK_DDS pin ***/
#define SCLK_DDS_Set()               (LATGSET = (1<<6))
#define SCLK_DDS_Clear()             (LATGCLR = (1<<6))
#define SCLK_DDS_Toggle()            (LATGINV= (1<<6))
#define SCLK_DDS_OutputEnable()      (TRISGCLR = (1<<6))
#define SCLK_DDS_InputEnable()       (TRISGSET = (1<<6))
#define SCLK_DDS_Get()               ((PORTG >> 6) & 0x1)
#define SCLK_DDS_PIN                  GPIO_PIN_RG6

/*** Macros for SDO_DDS pin ***/
#define SDO_DDS_Set()               (LATGSET = (1<<7))
#define SDO_DDS_Clear()             (LATGCLR = (1<<7))
#define SDO_DDS_Toggle()            (LATGINV= (1<<7))
#define SDO_DDS_OutputEnable()      (TRISGCLR = (1<<7))
#define SDO_DDS_InputEnable()       (TRISGSET = (1<<7))
#define SDO_DDS_Get()               ((PORTG >> 7) & 0x1)
#define SDO_DDS_PIN                  GPIO_PIN_RG7

/*** Macros for SDIO_DDS pin ***/
#define SDIO_DDS_Set()               (LATGSET = (1<<8))
#define SDIO_DDS_Clear()             (LATGCLR = (1<<8))
#define SDIO_DDS_Toggle()            (LATGINV= (1<<8))
#define SDIO_DDS_OutputEnable()      (TRISGCLR = (1<<8))
#define SDIO_DDS_InputEnable()       (TRISGSET = (1<<8))
#define SDIO_DDS_Get()               ((PORTG >> 8) & 0x1)
#define SDIO_DDS_PIN                  GPIO_PIN_RG8

/*** Macros for GPIO_RG9 pin ***/
#define GPIO_RG9_Set()               (LATGSET = (1<<9))
#define GPIO_RG9_Clear()             (LATGCLR = (1<<9))
#define GPIO_RG9_Toggle()            (LATGINV= (1<<9))
#define GPIO_RG9_OutputEnable()      (TRISGCLR = (1<<9))
#define GPIO_RG9_InputEnable()       (TRISGSET = (1<<9))
#define GPIO_RG9_Get()               ((PORTG >> 9) & 0x1)
#define GPIO_RG9_PIN                  GPIO_PIN_RG9

/*** Macros for LE_ATT1 pin ***/
#define LE_ATT1_Set()               (LATASET = (1<<0))
#define LE_ATT1_Clear()             (LATACLR = (1<<0))
#define LE_ATT1_Toggle()            (LATAINV= (1<<0))
#define LE_ATT1_OutputEnable()      (TRISACLR = (1<<0))
#define LE_ATT1_InputEnable()       (TRISASET = (1<<0))
#define LE_ATT1_Get()               ((PORTA >> 0) & 0x1)
#define LE_ATT1_PIN                  GPIO_PIN_RA0

/*** Macros for CPLD_INT pin ***/
#define CPLD_INT_Set()               (LATESET = (1<<8))
#define CPLD_INT_Clear()             (LATECLR = (1<<8))
#define CPLD_INT_Toggle()            (LATEINV= (1<<8))
#define CPLD_INT_OutputEnable()      (TRISECLR = (1<<8))
#define CPLD_INT_InputEnable()       (TRISESET = (1<<8))
#define CPLD_INT_Get()               ((PORTE >> 8) & 0x1)
#define CPLD_INT_PIN                  GPIO_PIN_RE8

/*** Macros for PODAVL pin ***/
#define PODAVL_Set()               (LATESET = (1<<9))
#define PODAVL_Clear()             (LATECLR = (1<<9))
#define PODAVL_Toggle()            (LATEINV= (1<<9))
#define PODAVL_OutputEnable()      (TRISECLR = (1<<9))
#define PODAVL_InputEnable()       (TRISESET = (1<<9))
#define PODAVL_Get()               ((PORTE >> 9) & 0x1)
#define PODAVL_PIN                  GPIO_PIN_RE9

/*** Macros for IZL_READY pin ***/
#define IZL_READY_Set()               (LATBSET = (1<<5))
#define IZL_READY_Clear()             (LATBCLR = (1<<5))
#define IZL_READY_Toggle()            (LATBINV= (1<<5))
#define IZL_READY_OutputEnable()      (TRISBCLR = (1<<5))
#define IZL_READY_InputEnable()       (TRISBSET = (1<<5))
#define IZL_READY_Get()               ((PORTB >> 5) & 0x1)
#define IZL_READY_PIN                  GPIO_PIN_RB5

/*** Macros for FRF_nPRFH pin ***/
#define FRF_nPRFH_Set()               (LATBSET = (1<<4))
#define FRF_nPRFH_Clear()             (LATBCLR = (1<<4))
#define FRF_nPRFH_Toggle()            (LATBINV= (1<<4))
#define FRF_nPRFH_OutputEnable()      (TRISBCLR = (1<<4))
#define FRF_nPRFH_InputEnable()       (TRISBSET = (1<<4))
#define FRF_nPRFH_Get()               ((PORTB >> 4) & 0x1)
#define FRF_nPRFH_PIN                  GPIO_PIN_RB4

/*** Macros for TEST_nWORK pin ***/
#define TEST_nWORK_Set()               (LATBSET = (1<<3))
#define TEST_nWORK_Clear()             (LATBCLR = (1<<3))
#define TEST_nWORK_Toggle()            (LATBINV= (1<<3))
#define TEST_nWORK_OutputEnable()      (TRISBCLR = (1<<3))
#define TEST_nWORK_InputEnable()       (TRISBSET = (1<<3))
#define TEST_nWORK_Get()               ((PORTB >> 3) & 0x1)
#define TEST_nWORK_PIN                  GPIO_PIN_RB3

/*** Macros for RESERV pin ***/
#define RESERV_Set()               (LATBSET = (1<<2))
#define RESERV_Clear()             (LATBCLR = (1<<2))
#define RESERV_Toggle()            (LATBINV= (1<<2))
#define RESERV_OutputEnable()      (TRISBCLR = (1<<2))
#define RESERV_InputEnable()       (TRISBSET = (1<<2))
#define RESERV_Get()               ((PORTB >> 2) & 0x1)
#define RESERV_PIN                  GPIO_PIN_RB2

/*** Macros for DIAP2 pin ***/
#define DIAP2_Set()               (LATBSET = (1<<1))
#define DIAP2_Clear()             (LATBCLR = (1<<1))
#define DIAP2_Toggle()            (LATBINV= (1<<1))
#define DIAP2_OutputEnable()      (TRISBCLR = (1<<1))
#define DIAP2_InputEnable()       (TRISBSET = (1<<1))
#define DIAP2_Get()               ((PORTB >> 1) & 0x1)
#define DIAP2_PIN                  GPIO_PIN_RB1

/*** Macros for DIAP1 pin ***/
#define DIAP1_Set()               (LATBSET = (1<<0))
#define DIAP1_Clear()             (LATBCLR = (1<<0))
#define DIAP1_Toggle()            (LATBINV= (1<<0))
#define DIAP1_OutputEnable()      (TRISBCLR = (1<<0))
#define DIAP1_InputEnable()       (TRISBSET = (1<<0))
#define DIAP1_Get()               ((PORTB >> 0) & 0x1)
#define DIAP1_PIN                  GPIO_PIN_RB0

/*** Macros for SWRF1_V1 pin ***/
#define SWRF1_V1_Set()               (LATASET = (1<<9))
#define SWRF1_V1_Clear()             (LATACLR = (1<<9))
#define SWRF1_V1_Toggle()            (LATAINV= (1<<9))
#define SWRF1_V1_OutputEnable()      (TRISACLR = (1<<9))
#define SWRF1_V1_InputEnable()       (TRISASET = (1<<9))
#define SWRF1_V1_Get()               ((PORTA >> 9) & 0x1)
#define SWRF1_V1_PIN                  GPIO_PIN_RA9

/*** Macros for SWRF1_V2 pin ***/
#define SWRF1_V2_Set()               (LATASET = (1<<10))
#define SWRF1_V2_Clear()             (LATACLR = (1<<10))
#define SWRF1_V2_Toggle()            (LATAINV= (1<<10))
#define SWRF1_V2_OutputEnable()      (TRISACLR = (1<<10))
#define SWRF1_V2_InputEnable()       (TRISASET = (1<<10))
#define SWRF1_V2_Get()               ((PORTA >> 10) & 0x1)
#define SWRF1_V2_PIN                  GPIO_PIN_RA10

/*** Macros for SWDDS2_V1 pin ***/
#define SWDDS2_V1_Set()               (LATBSET = (1<<8))
#define SWDDS2_V1_Clear()             (LATBCLR = (1<<8))
#define SWDDS2_V1_Toggle()            (LATBINV= (1<<8))
#define SWDDS2_V1_OutputEnable()      (TRISBCLR = (1<<8))
#define SWDDS2_V1_InputEnable()       (TRISBSET = (1<<8))
#define SWDDS2_V1_Get()               ((PORTB >> 8) & 0x1)
#define SWDDS2_V1_PIN                  GPIO_PIN_RB8

/*** Macros for SWDDS2_V2 pin ***/
#define SWDDS2_V2_Set()               (LATBSET = (1<<9))
#define SWDDS2_V2_Clear()             (LATBCLR = (1<<9))
#define SWDDS2_V2_Toggle()            (LATBINV= (1<<9))
#define SWDDS2_V2_OutputEnable()      (TRISBCLR = (1<<9))
#define SWDDS2_V2_InputEnable()       (TRISBSET = (1<<9))
#define SWDDS2_V2_Get()               ((PORTB >> 9) & 0x1)
#define SWDDS2_V2_PIN                  GPIO_PIN_RB9

/*** Macros for LE_ADF2 pin ***/
#define LE_ADF2_Set()               (LATBSET = (1<<10))
#define LE_ADF2_Clear()             (LATBCLR = (1<<10))
#define LE_ADF2_Toggle()            (LATBINV= (1<<10))
#define LE_ADF2_OutputEnable()      (TRISBCLR = (1<<10))
#define LE_ADF2_InputEnable()       (TRISBSET = (1<<10))
#define LE_ADF2_Get()               ((PORTB >> 10) & 0x1)
#define LE_ADF2_PIN                  GPIO_PIN_RB10

/*** Macros for LE_ADF1 pin ***/
#define LE_ADF1_Set()               (LATBSET = (1<<11))
#define LE_ADF1_Clear()             (LATBCLR = (1<<11))
#define LE_ADF1_Toggle()            (LATBINV= (1<<11))
#define LE_ADF1_OutputEnable()      (TRISBCLR = (1<<11))
#define LE_ADF1_InputEnable()       (TRISBSET = (1<<11))
#define LE_ADF1_Get()               ((PORTB >> 11) & 0x1)
#define LE_ADF1_PIN                  GPIO_PIN_RB11

/*** Macros for LE_ATT2 pin ***/
#define LE_ATT2_Set()               (LATASET = (1<<1))
#define LE_ATT2_Clear()             (LATACLR = (1<<1))
#define LE_ATT2_Toggle()            (LATAINV= (1<<1))
#define LE_ATT2_OutputEnable()      (TRISACLR = (1<<1))
#define LE_ATT2_InputEnable()       (TRISASET = (1<<1))
#define LE_ATT2_Get()               ((PORTA >> 1) & 0x1)
#define LE_ATT2_PIN                  GPIO_PIN_RA1

/*** Macros for SWRF2_V2 pin ***/
#define SWRF2_V2_Set()               (LATFSET = (1<<13))
#define SWRF2_V2_Clear()             (LATFCLR = (1<<13))
#define SWRF2_V2_Toggle()            (LATFINV= (1<<13))
#define SWRF2_V2_OutputEnable()      (TRISFCLR = (1<<13))
#define SWRF2_V2_InputEnable()       (TRISFSET = (1<<13))
#define SWRF2_V2_Get()               ((PORTF >> 13) & 0x1)
#define SWRF2_V2_PIN                  GPIO_PIN_RF13

/*** Macros for SWRF2_V1 pin ***/
#define SWRF2_V1_Set()               (LATFSET = (1<<12))
#define SWRF2_V1_Clear()             (LATFCLR = (1<<12))
#define SWRF2_V1_Toggle()            (LATFINV= (1<<12))
#define SWRF2_V1_OutputEnable()      (TRISFCLR = (1<<12))
#define SWRF2_V1_InputEnable()       (TRISFSET = (1<<12))
#define SWRF2_V1_Get()               ((PORTF >> 12) & 0x1)
#define SWRF2_V1_PIN                  GPIO_PIN_RF12

/*** Macros for EN_PWR_CH1_RF3 pin ***/
#define EN_PWR_CH1_RF3_Set()               (LATBSET = (1<<12))
#define EN_PWR_CH1_RF3_Clear()             (LATBCLR = (1<<12))
#define EN_PWR_CH1_RF3_Toggle()            (LATBINV= (1<<12))
#define EN_PWR_CH1_RF3_OutputEnable()      (TRISBCLR = (1<<12))
#define EN_PWR_CH1_RF3_InputEnable()       (TRISBSET = (1<<12))
#define EN_PWR_CH1_RF3_Get()               ((PORTB >> 12) & 0x1)
#define EN_PWR_CH1_RF3_PIN                  GPIO_PIN_RB12

/*** Macros for EN_PWR_CH1_RF2 pin ***/
#define EN_PWR_CH1_RF2_Set()               (LATBSET = (1<<13))
#define EN_PWR_CH1_RF2_Clear()             (LATBCLR = (1<<13))
#define EN_PWR_CH1_RF2_Toggle()            (LATBINV= (1<<13))
#define EN_PWR_CH1_RF2_OutputEnable()      (TRISBCLR = (1<<13))
#define EN_PWR_CH1_RF2_InputEnable()       (TRISBSET = (1<<13))
#define EN_PWR_CH1_RF2_Get()               ((PORTB >> 13) & 0x1)
#define EN_PWR_CH1_RF2_PIN                  GPIO_PIN_RB13

/*** Macros for SDATA_ADF pin ***/
#define SDATA_ADF_Set()               (LATBSET = (1<<14))
#define SDATA_ADF_Clear()             (LATBCLR = (1<<14))
#define SDATA_ADF_Toggle()            (LATBINV= (1<<14))
#define SDATA_ADF_OutputEnable()      (TRISBCLR = (1<<14))
#define SDATA_ADF_InputEnable()       (TRISBSET = (1<<14))
#define SDATA_ADF_Get()               ((PORTB >> 14) & 0x1)
#define SDATA_ADF_PIN                  GPIO_PIN_RB14

/*** Macros for SCLK_ADF pin ***/
#define SCLK_ADF_Set()               (LATBSET = (1<<15))
#define SCLK_ADF_Clear()             (LATBCLR = (1<<15))
#define SCLK_ADF_Toggle()            (LATBINV= (1<<15))
#define SCLK_ADF_OutputEnable()      (TRISBCLR = (1<<15))
#define SCLK_ADF_InputEnable()       (TRISBSET = (1<<15))
#define SCLK_ADF_Get()               ((PORTB >> 15) & 0x1)
#define SCLK_ADF_PIN                  GPIO_PIN_RB15

/*** Macros for EN_PWR_CH2 pin ***/
#define EN_PWR_CH2_Set()               (LATDSET = (1<<14))
#define EN_PWR_CH2_Clear()             (LATDCLR = (1<<14))
#define EN_PWR_CH2_Toggle()            (LATDINV= (1<<14))
#define EN_PWR_CH2_OutputEnable()      (TRISDCLR = (1<<14))
#define EN_PWR_CH2_InputEnable()       (TRISDSET = (1<<14))
#define EN_PWR_CH2_Get()               ((PORTD >> 14) & 0x1)
#define EN_PWR_CH2_PIN                  GPIO_PIN_RD14

/*** Macros for ON_RS485_2 pin ***/
#define ON_RS485_2_Set()               (LATDSET = (1<<15))
#define ON_RS485_2_Clear()             (LATDCLR = (1<<15))
#define ON_RS485_2_Toggle()            (LATDINV= (1<<15))
#define ON_RS485_2_OutputEnable()      (TRISDCLR = (1<<15))
#define ON_RS485_2_InputEnable()       (TRISDSET = (1<<15))
#define ON_RS485_2_Get()               ((PORTD >> 15) & 0x1)
#define ON_RS485_2_PIN                  GPIO_PIN_RD15

/*** Macros for EN_PWR_CH2_IF2 pin ***/
#define EN_PWR_CH2_IF2_Set()               (LATFSET = (1<<3))
#define EN_PWR_CH2_IF2_Clear()             (LATFCLR = (1<<3))
#define EN_PWR_CH2_IF2_Toggle()            (LATFINV= (1<<3))
#define EN_PWR_CH2_IF2_OutputEnable()      (TRISFCLR = (1<<3))
#define EN_PWR_CH2_IF2_InputEnable()       (TRISFSET = (1<<3))
#define EN_PWR_CH2_IF2_Get()               ((PORTF >> 3) & 0x1)
#define EN_PWR_CH2_IF2_PIN                  GPIO_PIN_RF3

/*** Macros for GPIO_RG3 pin ***/
#define GPIO_RG3_Set()               (LATGSET = (1<<3))
#define GPIO_RG3_Clear()             (LATGCLR = (1<<3))
#define GPIO_RG3_Toggle()            (LATGINV= (1<<3))
#define GPIO_RG3_OutputEnable()      (TRISGCLR = (1<<3))
#define GPIO_RG3_InputEnable()       (TRISGSET = (1<<3))
#define GPIO_RG3_Get()               ((PORTG >> 3) & 0x1)
#define GPIO_RG3_PIN                  GPIO_PIN_RG3

/*** Macros for GPIO_RG2 pin ***/
#define GPIO_RG2_Set()               (LATGSET = (1<<2))
#define GPIO_RG2_Clear()             (LATGCLR = (1<<2))
#define GPIO_RG2_Toggle()            (LATGINV= (1<<2))
#define GPIO_RG2_OutputEnable()      (TRISGCLR = (1<<2))
#define GPIO_RG2_InputEnable()       (TRISGSET = (1<<2))
#define GPIO_RG2_Get()               ((PORTG >> 2) & 0x1)
#define GPIO_RG2_PIN                  GPIO_PIN_RG2

/*** Macros for CLK_ATT pin ***/
#define CLK_ATT_Set()               (LATASET = (1<<2))
#define CLK_ATT_Clear()             (LATACLR = (1<<2))
#define CLK_ATT_Toggle()            (LATAINV= (1<<2))
#define CLK_ATT_OutputEnable()      (TRISACLR = (1<<2))
#define CLK_ATT_InputEnable()       (TRISASET = (1<<2))
#define CLK_ATT_Get()               ((PORTA >> 2) & 0x1)
#define CLK_ATT_PIN                  GPIO_PIN_RA2

/*** Macros for DATA_ATT pin ***/
#define DATA_ATT_Set()               (LATASET = (1<<3))
#define DATA_ATT_Clear()             (LATACLR = (1<<3))
#define DATA_ATT_Toggle()            (LATAINV= (1<<3))
#define DATA_ATT_OutputEnable()      (TRISACLR = (1<<3))
#define DATA_ATT_InputEnable()       (TRISASET = (1<<3))
#define DATA_ATT_Get()               ((PORTA >> 3) & 0x1)
#define DATA_ATT_PIN                  GPIO_PIN_RA3

/*** Macros for CH2_TEST_IND pin ***/
#define CH2_TEST_IND_Set()               (LATASET = (1<<4))
#define CH2_TEST_IND_Clear()             (LATACLR = (1<<4))
#define CH2_TEST_IND_Toggle()            (LATAINV= (1<<4))
#define CH2_TEST_IND_OutputEnable()      (TRISACLR = (1<<4))
#define CH2_TEST_IND_InputEnable()       (TRISASET = (1<<4))
#define CH2_TEST_IND_Get()               ((PORTA >> 4) & 0x1)
#define CH2_TEST_IND_PIN                  GPIO_PIN_RA4

/*** Macros for CH1_TEST_IND pin ***/
#define CH1_TEST_IND_Set()               (LATASET = (1<<5))
#define CH1_TEST_IND_Clear()             (LATACLR = (1<<5))
#define CH1_TEST_IND_Toggle()            (LATAINV= (1<<5))
#define CH1_TEST_IND_OutputEnable()      (TRISACLR = (1<<5))
#define CH1_TEST_IND_InputEnable()       (TRISASET = (1<<5))
#define CH1_TEST_IND_Get()               ((PORTA >> 5) & 0x1)
#define CH1_TEST_IND_PIN                  GPIO_PIN_RA5

/*** Macros for EN_PWR_CH2_IF1 pin ***/
#define EN_PWR_CH2_IF1_Set()               (LATDSET = (1<<8))
#define EN_PWR_CH2_IF1_Clear()             (LATDCLR = (1<<8))
#define EN_PWR_CH2_IF1_Toggle()            (LATDINV= (1<<8))
#define EN_PWR_CH2_IF1_OutputEnable()      (TRISDCLR = (1<<8))
#define EN_PWR_CH2_IF1_InputEnable()       (TRISDSET = (1<<8))
#define EN_PWR_CH2_IF1_Get()               ((PORTD >> 8) & 0x1)
#define EN_PWR_CH2_IF1_PIN                  GPIO_PIN_RD8

/*** Macros for PWR_DOWN_DDS2 pin ***/
#define PWR_DOWN_DDS2_Set()               (LATDSET = (1<<9))
#define PWR_DOWN_DDS2_Clear()             (LATDCLR = (1<<9))
#define PWR_DOWN_DDS2_Toggle()            (LATDINV= (1<<9))
#define PWR_DOWN_DDS2_OutputEnable()      (TRISDCLR = (1<<9))
#define PWR_DOWN_DDS2_InputEnable()       (TRISDSET = (1<<9))
#define PWR_DOWN_DDS2_Get()               ((PORTD >> 9) & 0x1)
#define PWR_DOWN_DDS2_PIN                  GPIO_PIN_RD9

/*** Macros for M_RST_DDS2 pin ***/
#define M_RST_DDS2_Set()               (LATDSET = (1<<10))
#define M_RST_DDS2_Clear()             (LATDCLR = (1<<10))
#define M_RST_DDS2_Toggle()            (LATDINV= (1<<10))
#define M_RST_DDS2_OutputEnable()      (TRISDCLR = (1<<10))
#define M_RST_DDS2_InputEnable()       (TRISDSET = (1<<10))
#define M_RST_DDS2_Get()               ((PORTD >> 10) & 0x1)
#define M_RST_DDS2_PIN                  GPIO_PIN_RD10

/*** Macros for nCSDDS2 pin ***/
#define nCSDDS2_Set()               (LATDSET = (1<<11))
#define nCSDDS2_Clear()             (LATDCLR = (1<<11))
#define nCSDDS2_Toggle()            (LATDINV= (1<<11))
#define nCSDDS2_OutputEnable()      (TRISDCLR = (1<<11))
#define nCSDDS2_InputEnable()       (TRISDSET = (1<<11))
#define nCSDDS2_Get()               ((PORTD >> 11) & 0x1)
#define nCSDDS2_PIN                  GPIO_PIN_RD11

/*** Macros for I_O_RESET pin ***/
#define I_O_RESET_Set()               (LATDSET = (1<<0))
#define I_O_RESET_Clear()             (LATDCLR = (1<<0))
#define I_O_RESET_Toggle()            (LATDINV= (1<<0))
#define I_O_RESET_OutputEnable()      (TRISDCLR = (1<<0))
#define I_O_RESET_InputEnable()       (TRISDSET = (1<<0))
#define I_O_RESET_Get()               ((PORTD >> 0) & 0x1)
#define I_O_RESET_PIN                  GPIO_PIN_RD0

/*** Macros for SW_TEST_CH1 pin ***/
#define SW_TEST_CH1_Set()               (LATCSET = (1<<13))
#define SW_TEST_CH1_Clear()             (LATCCLR = (1<<13))
#define SW_TEST_CH1_Toggle()            (LATCINV= (1<<13))
#define SW_TEST_CH1_OutputEnable()      (TRISCCLR = (1<<13))
#define SW_TEST_CH1_InputEnable()       (TRISCSET = (1<<13))
#define SW_TEST_CH1_Get()               ((PORTC >> 13) & 0x1)
#define SW_TEST_CH1_PIN                  GPIO_PIN_RC13

/*** Macros for SW_TEST_CH2 pin ***/
#define SW_TEST_CH2_Set()               (LATCSET = (1<<14))
#define SW_TEST_CH2_Clear()             (LATCCLR = (1<<14))
#define SW_TEST_CH2_Toggle()            (LATCINV= (1<<14))
#define SW_TEST_CH2_OutputEnable()      (TRISCCLR = (1<<14))
#define SW_TEST_CH2_InputEnable()       (TRISCSET = (1<<14))
#define SW_TEST_CH2_Get()               ((PORTC >> 14) & 0x1)
#define SW_TEST_CH2_PIN                  GPIO_PIN_RC14

/*** Macros for EN_PWR_CH1 pin ***/
#define EN_PWR_CH1_Set()               (LATDSET = (1<<1))
#define EN_PWR_CH1_Clear()             (LATDCLR = (1<<1))
#define EN_PWR_CH1_Toggle()            (LATDINV= (1<<1))
#define EN_PWR_CH1_OutputEnable()      (TRISDCLR = (1<<1))
#define EN_PWR_CH1_InputEnable()       (TRISDSET = (1<<1))
#define EN_PWR_CH1_Get()               ((PORTD >> 1) & 0x1)
#define EN_PWR_CH1_PIN                  GPIO_PIN_RD1

/*** Macros for RF1_AMPL_ON pin ***/
#define RF1_AMPL_ON_Set()               (LATDSET = (1<<2))
#define RF1_AMPL_ON_Clear()             (LATDCLR = (1<<2))
#define RF1_AMPL_ON_Toggle()            (LATDINV= (1<<2))
#define RF1_AMPL_ON_OutputEnable()      (TRISDCLR = (1<<2))
#define RF1_AMPL_ON_InputEnable()       (TRISDSET = (1<<2))
#define RF1_AMPL_ON_Get()               ((PORTD >> 2) & 0x1)
#define RF1_AMPL_ON_PIN                  GPIO_PIN_RD2

/*** Macros for SWDDS1_V1 pin ***/
#define SWDDS1_V1_Set()               (LATDSET = (1<<3))
#define SWDDS1_V1_Clear()             (LATDCLR = (1<<3))
#define SWDDS1_V1_Toggle()            (LATDINV= (1<<3))
#define SWDDS1_V1_OutputEnable()      (TRISDCLR = (1<<3))
#define SWDDS1_V1_InputEnable()       (TRISDSET = (1<<3))
#define SWDDS1_V1_Get()               ((PORTD >> 3) & 0x1)
#define SWDDS1_V1_PIN                  GPIO_PIN_RD3

/*** Macros for SWDDS1_V2 pin ***/
#define SWDDS1_V2_Set()               (LATDSET = (1<<12))
#define SWDDS1_V2_Clear()             (LATDCLR = (1<<12))
#define SWDDS1_V2_Toggle()            (LATDINV= (1<<12))
#define SWDDS1_V2_OutputEnable()      (TRISDCLR = (1<<12))
#define SWDDS1_V2_InputEnable()       (TRISDSET = (1<<12))
#define SWDDS1_V2_Get()               ((PORTD >> 12) & 0x1)
#define SWDDS1_V2_PIN                  GPIO_PIN_RD12

/*** Macros for EN_PWR_CH1_RF1 pin ***/
#define EN_PWR_CH1_RF1_Set()               (LATDSET = (1<<13))
#define EN_PWR_CH1_RF1_Clear()             (LATDCLR = (1<<13))
#define EN_PWR_CH1_RF1_Toggle()            (LATDINV= (1<<13))
#define EN_PWR_CH1_RF1_OutputEnable()      (TRISDCLR = (1<<13))
#define EN_PWR_CH1_RF1_InputEnable()       (TRISDSET = (1<<13))
#define EN_PWR_CH1_RF1_Get()               ((PORTD >> 13) & 0x1)
#define EN_PWR_CH1_RF1_PIN                  GPIO_PIN_RD13

/*** Macros for PWR_DWN_DDS1 pin ***/
#define PWR_DWN_DDS1_Set()               (LATDSET = (1<<4))
#define PWR_DWN_DDS1_Clear()             (LATDCLR = (1<<4))
#define PWR_DWN_DDS1_Toggle()            (LATDINV= (1<<4))
#define PWR_DWN_DDS1_OutputEnable()      (TRISDCLR = (1<<4))
#define PWR_DWN_DDS1_InputEnable()       (TRISDSET = (1<<4))
#define PWR_DWN_DDS1_Get()               ((PORTD >> 4) & 0x1)
#define PWR_DWN_DDS1_PIN                  GPIO_PIN_RD4

/*** Macros for M_RST_DDS1 pin ***/
#define M_RST_DDS1_Set()               (LATDSET = (1<<5))
#define M_RST_DDS1_Clear()             (LATDCLR = (1<<5))
#define M_RST_DDS1_Toggle()            (LATDINV= (1<<5))
#define M_RST_DDS1_OutputEnable()      (TRISDCLR = (1<<5))
#define M_RST_DDS1_InputEnable()       (TRISDSET = (1<<5))
#define M_RST_DDS1_Get()               ((PORTD >> 5) & 0x1)
#define M_RST_DDS1_PIN                  GPIO_PIN_RD5

/*** Macros for nCSDDS1 pin ***/
#define nCSDDS1_Set()               (LATDSET = (1<<6))
#define nCSDDS1_Clear()             (LATDCLR = (1<<6))
#define nCSDDS1_Toggle()            (LATDINV= (1<<6))
#define nCSDDS1_OutputEnable()      (TRISDCLR = (1<<6))
#define nCSDDS1_InputEnable()       (TRISDSET = (1<<6))
#define nCSDDS1_Get()               ((PORTD >> 6) & 0x1)
#define nCSDDS1_PIN                  GPIO_PIN_RD6

/*** Macros for I_O_UPDATE pin ***/
#define I_O_UPDATE_Set()               (LATDSET = (1<<7))
#define I_O_UPDATE_Clear()             (LATDCLR = (1<<7))
#define I_O_UPDATE_Toggle()            (LATDINV= (1<<7))
#define I_O_UPDATE_OutputEnable()      (TRISDCLR = (1<<7))
#define I_O_UPDATE_InputEnable()       (TRISDSET = (1<<7))
#define I_O_UPDATE_Get()               ((PORTD >> 7) & 0x1)
#define I_O_UPDATE_PIN                  GPIO_PIN_RD7

/*** Macros for RF0 pin ***/
#define RF0_Set()               (LATFSET = (1<<0))
#define RF0_Clear()             (LATFCLR = (1<<0))
#define RF0_Toggle()            (LATFINV= (1<<0))
#define RF0_OutputEnable()      (TRISFCLR = (1<<0))
#define RF0_InputEnable()       (TRISFSET = (1<<0))
#define RF0_Get()               ((PORTF >> 0) & 0x1)
#define RF0_PIN                  GPIO_PIN_RF0

/*** Macros for RF1 pin ***/
#define RF1_Set()               (LATFSET = (1<<1))
#define RF1_Clear()             (LATFCLR = (1<<1))
#define RF1_Toggle()            (LATFINV= (1<<1))
#define RF1_OutputEnable()      (TRISFCLR = (1<<1))
#define RF1_InputEnable()       (TRISFSET = (1<<1))
#define RF1_Get()               ((PORTF >> 1) & 0x1)
#define RF1_PIN                  GPIO_PIN_RF1

/*** Macros for C2TX pin ***/
#define C2TX_Set()               (LATGSET = (1<<1))
#define C2TX_Clear()             (LATGCLR = (1<<1))
#define C2TX_Toggle()            (LATGINV= (1<<1))
#define C2TX_OutputEnable()      (TRISGCLR = (1<<1))
#define C2TX_InputEnable()       (TRISGSET = (1<<1))
#define C2TX_Get()               ((PORTG >> 1) & 0x1)
#define C2TX_PIN                  GPIO_PIN_RG1

/*** Macros for C2RX pin ***/
#define C2RX_Set()               (LATGSET = (1<<0))
#define C2RX_Clear()             (LATGCLR = (1<<0))
#define C2RX_Toggle()            (LATGINV= (1<<0))
#define C2RX_OutputEnable()      (TRISGCLR = (1<<0))
#define C2RX_InputEnable()       (TRISGSET = (1<<0))
#define C2RX_Get()               ((PORTG >> 0) & 0x1)
#define C2RX_PIN                  GPIO_PIN_RG0

/*** Macros for R_nW_XC pin ***/
#define R_nW_XC_Set()               (LATASET = (1<<6))
#define R_nW_XC_Clear()             (LATACLR = (1<<6))
#define R_nW_XC_Toggle()            (LATAINV= (1<<6))
#define R_nW_XC_OutputEnable()      (TRISACLR = (1<<6))
#define R_nW_XC_InputEnable()       (TRISASET = (1<<6))
#define R_nW_XC_Get()               ((PORTA >> 6) & 0x1)
#define R_nW_XC_PIN                  GPIO_PIN_RA6

/*** Macros for STR_ADR_XC pin ***/
#define STR_ADR_XC_Set()               (LATASET = (1<<7))
#define STR_ADR_XC_Clear()             (LATACLR = (1<<7))
#define STR_ADR_XC_Toggle()            (LATAINV= (1<<7))
#define STR_ADR_XC_OutputEnable()      (TRISACLR = (1<<7))
#define STR_ADR_XC_InputEnable()       (TRISASET = (1<<7))
#define STR_ADR_XC_Get()               ((PORTA >> 7) & 0x1)
#define STR_ADR_XC_PIN                  GPIO_PIN_RA7

/*** Macros for P0_XC pin ***/
#define P0_XC_Set()               (LATESET = (1<<0))
#define P0_XC_Clear()             (LATECLR = (1<<0))
#define P0_XC_Toggle()            (LATEINV= (1<<0))
#define P0_XC_OutputEnable()      (TRISECLR = (1<<0))
#define P0_XC_InputEnable()       (TRISESET = (1<<0))
#define P0_XC_Get()               ((PORTE >> 0) & 0x1)
#define P0_XC_PIN                  GPIO_PIN_RE0

/*** Macros for P1_XC pin ***/
#define P1_XC_Set()               (LATESET = (1<<1))
#define P1_XC_Clear()             (LATECLR = (1<<1))
#define P1_XC_Toggle()            (LATEINV= (1<<1))
#define P1_XC_OutputEnable()      (TRISECLR = (1<<1))
#define P1_XC_InputEnable()       (TRISESET = (1<<1))
#define P1_XC_Get()               ((PORTE >> 1) & 0x1)
#define P1_XC_PIN                  GPIO_PIN_RE1

/*** Macros for ADR_XC0 pin ***/
#define ADR_XC0_Set()               (LATGSET = (1<<14))
#define ADR_XC0_Clear()             (LATGCLR = (1<<14))
#define ADR_XC0_Toggle()            (LATGINV= (1<<14))
#define ADR_XC0_OutputEnable()      (TRISGCLR = (1<<14))
#define ADR_XC0_InputEnable()       (TRISGSET = (1<<14))
#define ADR_XC0_Get()               ((PORTG >> 14) & 0x1)
#define ADR_XC0_PIN                  GPIO_PIN_RG14

/*** Macros for ADR_XC1 pin ***/
#define ADR_XC1_Set()               (LATGSET = (1<<12))
#define ADR_XC1_Clear()             (LATGCLR = (1<<12))
#define ADR_XC1_Toggle()            (LATGINV= (1<<12))
#define ADR_XC1_OutputEnable()      (TRISGCLR = (1<<12))
#define ADR_XC1_InputEnable()       (TRISGSET = (1<<12))
#define ADR_XC1_Get()               ((PORTG >> 12) & 0x1)
#define ADR_XC1_PIN                  GPIO_PIN_RG12

/*** Macros for ADR_XC2 pin ***/
#define ADR_XC2_Set()               (LATGSET = (1<<13))
#define ADR_XC2_Clear()             (LATGCLR = (1<<13))
#define ADR_XC2_Toggle()            (LATGINV= (1<<13))
#define ADR_XC2_OutputEnable()      (TRISGCLR = (1<<13))
#define ADR_XC2_InputEnable()       (TRISGSET = (1<<13))
#define ADR_XC2_Get()               ((PORTG >> 13) & 0x1)
#define ADR_XC2_PIN                  GPIO_PIN_RG13

/*** Macros for P2_XC pin ***/
#define P2_XC_Set()               (LATESET = (1<<2))
#define P2_XC_Clear()             (LATECLR = (1<<2))
#define P2_XC_Toggle()            (LATEINV= (1<<2))
#define P2_XC_OutputEnable()      (TRISECLR = (1<<2))
#define P2_XC_InputEnable()       (TRISESET = (1<<2))
#define P2_XC_Get()               ((PORTE >> 2) & 0x1)
#define P2_XC_PIN                  GPIO_PIN_RE2

/*** Macros for P3_XC pin ***/
#define P3_XC_Set()               (LATESET = (1<<3))
#define P3_XC_Clear()             (LATECLR = (1<<3))
#define P3_XC_Toggle()            (LATEINV= (1<<3))
#define P3_XC_OutputEnable()      (TRISECLR = (1<<3))
#define P3_XC_InputEnable()       (TRISESET = (1<<3))
#define P3_XC_Get()               ((PORTE >> 3) & 0x1)
#define P3_XC_PIN                  GPIO_PIN_RE3

/*** Macros for P4_XC pin ***/
#define P4_XC_Set()               (LATESET = (1<<4))
#define P4_XC_Clear()             (LATECLR = (1<<4))
#define P4_XC_Toggle()            (LATEINV= (1<<4))
#define P4_XC_OutputEnable()      (TRISECLR = (1<<4))
#define P4_XC_InputEnable()       (TRISESET = (1<<4))
#define P4_XC_Get()               ((PORTE >> 4) & 0x1)
#define P4_XC_PIN                  GPIO_PIN_RE4


// *****************************************************************************
/* GPIO Port

  Summary:
    Identifies the available GPIO Ports.

  Description:
    This enumeration identifies the available GPIO Ports.

  Remarks:
    The caller should not rely on the specific numbers assigned to any of
    these values as they may change from one processor to the next.

    Not all ports are available on all devices.  Refer to the specific
    device data sheet to determine which ports are supported.
*/

typedef enum
{
    GPIO_PORT_A = 0,
    GPIO_PORT_B = 1,
    GPIO_PORT_C = 2,
    GPIO_PORT_D = 3,
    GPIO_PORT_E = 4,
    GPIO_PORT_F = 5,
    GPIO_PORT_G = 6,
} GPIO_PORT;

// *****************************************************************************
/* GPIO Port Pins

  Summary:
    Identifies the available GPIO port pins.

  Description:
    This enumeration identifies the available GPIO port pins.

  Remarks:
    The caller should not rely on the specific numbers assigned to any of
    these values as they may change from one processor to the next.

    Not all pins are available on all devices.  Refer to the specific
    device data sheet to determine which pins are supported.
*/

typedef enum
{
    GPIO_PIN_RA0 = 0,
    GPIO_PIN_RA1 = 1,
    GPIO_PIN_RA2 = 2,
    GPIO_PIN_RA3 = 3,
    GPIO_PIN_RA4 = 4,
    GPIO_PIN_RA5 = 5,
    GPIO_PIN_RA6 = 6,
    GPIO_PIN_RA7 = 7,
    GPIO_PIN_RA9 = 9,
    GPIO_PIN_RA10 = 10,
    GPIO_PIN_RA14 = 14,
    GPIO_PIN_RA15 = 15,
    GPIO_PIN_RB0 = 16,
    GPIO_PIN_RB1 = 17,
    GPIO_PIN_RB2 = 18,
    GPIO_PIN_RB3 = 19,
    GPIO_PIN_RB4 = 20,
    GPIO_PIN_RB5 = 21,
    GPIO_PIN_RB6 = 22,
    GPIO_PIN_RB7 = 23,
    GPIO_PIN_RB8 = 24,
    GPIO_PIN_RB9 = 25,
    GPIO_PIN_RB10 = 26,
    GPIO_PIN_RB11 = 27,
    GPIO_PIN_RB12 = 28,
    GPIO_PIN_RB13 = 29,
    GPIO_PIN_RB14 = 30,
    GPIO_PIN_RB15 = 31,
    GPIO_PIN_RC1 = 33,
    GPIO_PIN_RC2 = 34,
    GPIO_PIN_RC3 = 35,
    GPIO_PIN_RC4 = 36,
    GPIO_PIN_RC12 = 44,
    GPIO_PIN_RC13 = 45,
    GPIO_PIN_RC14 = 46,
    GPIO_PIN_RC15 = 47,
    GPIO_PIN_RD0 = 48,
    GPIO_PIN_RD1 = 49,
    GPIO_PIN_RD2 = 50,
    GPIO_PIN_RD3 = 51,
    GPIO_PIN_RD4 = 52,
    GPIO_PIN_RD5 = 53,
    GPIO_PIN_RD6 = 54,
    GPIO_PIN_RD7 = 55,
    GPIO_PIN_RD8 = 56,
    GPIO_PIN_RD9 = 57,
    GPIO_PIN_RD10 = 58,
    GPIO_PIN_RD11 = 59,
    GPIO_PIN_RD12 = 60,
    GPIO_PIN_RD13 = 61,
    GPIO_PIN_RD14 = 62,
    GPIO_PIN_RD15 = 63,
    GPIO_PIN_RE0 = 64,
    GPIO_PIN_RE1 = 65,
    GPIO_PIN_RE2 = 66,
    GPIO_PIN_RE3 = 67,
    GPIO_PIN_RE4 = 68,
    GPIO_PIN_RE5 = 69,
    GPIO_PIN_RE6 = 70,
    GPIO_PIN_RE7 = 71,
    GPIO_PIN_RE8 = 72,
    GPIO_PIN_RE9 = 73,
    GPIO_PIN_RF0 = 80,
    GPIO_PIN_RF1 = 81,
    GPIO_PIN_RF2 = 82,
    GPIO_PIN_RF3 = 83,
    GPIO_PIN_RF4 = 84,
    GPIO_PIN_RF5 = 85,
    GPIO_PIN_RF8 = 88,
    GPIO_PIN_RF12 = 92,
    GPIO_PIN_RF13 = 93,
    GPIO_PIN_RG0 = 96,
    GPIO_PIN_RG1 = 97,
    GPIO_PIN_RG2 = 98,
    GPIO_PIN_RG3 = 99,
    GPIO_PIN_RG6 = 102,
    GPIO_PIN_RG7 = 103,
    GPIO_PIN_RG8 = 104,
    GPIO_PIN_RG9 = 105,
    GPIO_PIN_RG12 = 108,
    GPIO_PIN_RG13 = 109,
    GPIO_PIN_RG14 = 110,
    GPIO_PIN_RG15 = 111,

    /* This element should not be used in any of the GPIO APIs.
       It will be used by other modules or application to denote that none of the GPIO Pin is used */
    GPIO_PIN_NONE = -1

} GPIO_PIN;

typedef enum
{
  CN0_PIN = 1 << 0,
  CN1_PIN = 1 << 1,
  CN2_PIN = 1 << 2,
  CN3_PIN = 1 << 3,
  CN4_PIN = 1 << 4,
  CN5_PIN = 1 << 5,
  CN6_PIN = 1 << 6,
  CN7_PIN = 1 << 7,
  CN8_PIN = 1 << 8,
  CN9_PIN = 1 << 9,
  CN10_PIN = 1 << 10,
  CN11_PIN = 1 << 11,
  CN12_PIN = 1 << 12,
  CN13_PIN = 1 << 13,
  CN14_PIN = 1 << 14,
  CN15_PIN = 1 << 15,
  CN16_PIN = 1 << 16,
  CN17_PIN = 1 << 17,
  CN18_PIN = 1 << 18,
  CN19_PIN = 1 << 19,
  CN20_PIN = 1 << 20,
  CN21_PIN = 1 << 21,
}CN_PIN;


void GPIO_Initialize(void);

// *****************************************************************************
// *****************************************************************************
// Section: GPIO Functions which operates on multiple pins of a port
// *****************************************************************************
// *****************************************************************************

uint32_t GPIO_PortRead(GPIO_PORT port);

void GPIO_PortWrite(GPIO_PORT port, uint32_t mask, uint32_t value);

uint32_t GPIO_PortLatchRead ( GPIO_PORT port );

void GPIO_PortSet(GPIO_PORT port, uint32_t mask);

void GPIO_PortClear(GPIO_PORT port, uint32_t mask);

void GPIO_PortToggle(GPIO_PORT port, uint32_t mask);

void GPIO_PortInputEnable(GPIO_PORT port, uint32_t mask);

void GPIO_PortOutputEnable(GPIO_PORT port, uint32_t mask);

// *****************************************************************************
// *****************************************************************************
// Section: GPIO Functions which operates on one pin at a time
// *****************************************************************************
// *****************************************************************************

static inline void GPIO_PinWrite(GPIO_PIN pin, bool value)
{
    GPIO_PortWrite((GPIO_PORT)(pin>>4), (uint32_t)(0x1) << (pin & 0xF), (uint32_t)(value) << (pin & 0xF));
}

static inline bool GPIO_PinRead(GPIO_PIN pin)
{
    return (bool)(((GPIO_PortRead((GPIO_PORT)(pin>>4))) >> (pin & 0xF)) & 0x1);
}

static inline bool GPIO_PinLatchRead(GPIO_PIN pin)
{
    return (bool)((GPIO_PortLatchRead((GPIO_PORT)(pin>>4)) >> (pin & 0xF)) & 0x1);
}

static inline void GPIO_PinToggle(GPIO_PIN pin)
{
    GPIO_PortToggle((GPIO_PORT)(pin>>4), 0x1 << (pin & 0xF));
}

static inline void GPIO_PinSet(GPIO_PIN pin)
{
    GPIO_PortSet((GPIO_PORT)(pin>>4), 0x1 << (pin & 0xF));
}

static inline void GPIO_PinClear(GPIO_PIN pin)
{
    GPIO_PortClear((GPIO_PORT)(pin>>4), 0x1 << (pin & 0xF));
}

static inline void GPIO_PinInputEnable(GPIO_PIN pin)
{
    GPIO_PortInputEnable((GPIO_PORT)(pin>>4), 0x1 << (pin & 0xF));
}

static inline void GPIO_PinOutputEnable(GPIO_PIN pin)
{
    GPIO_PortOutputEnable((GPIO_PORT)(pin>>4), 0x1 << (pin & 0xF));
}


// DOM-IGNORE-BEGIN
#ifdef __cplusplus  // Provide C++ Compatibility

    }

#endif
// DOM-IGNORE-END
#endif // PLIB_GPIO_H
