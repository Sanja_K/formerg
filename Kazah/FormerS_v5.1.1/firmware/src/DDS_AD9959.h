/* 
 * File:   DDS_AD9959.h
 * Author: Alexandr
 *
 * Created on 28 сентября 2017 г., 9:32
 */

#ifndef DDS_AD9959_H
#define	DDS_AD9959_H
#include "synt_AD9959.h"
//-------------------------------------------

//#define deltaFg           (70*MHz) // смещение частоты гетеродина от сигнала 
//#define dtScan            (125*MHz) // dtScan  = 8*10^-9
#define dtScan            (12500*kHz) // dtScan  = 8*10^-8
#define Fs                (500*MHz) 
//---------------------L2--------------------------------------
//#define Fget_GPS_GL_L1L2           1700 
#define Fget2_GPS_GL_L2          4735// 2260//4730//(3350*MHz+F0DDS_GL_L2+1241.4*MHz)/MHz

#define FDDS_devGPS_L2           (1*MHz)
#define F0DDS_GPS_L2             (107400*kHz)  //152.4 // v 3.2 87600  Fget2_GPS_GL_L2 = 2260

#define FDDS_start_GPS_L2 (F0DDS_GPS_L2 - FDDS_devGPS_L2)//125
#define FDDS_stop_GPS_L2 (F0DDS_GPS_L2 + FDDS_devGPS_L2)

#define FDDS_devGL_L2            (4*MHz)
#define F0DDS_GL_L2              (89000*kHz)  /// 135.0 v 3.2 105000

#define FDDS_start_GL_L2 (F0DDS_GL_L2 - FDDS_devGL_L2)//150
#define FDDS_stop_GL_L2  (F0DDS_GL_L2 + FDDS_devGL_L2)

#define Fman_L2 (1*kHz)

#define dFscan_GPS_L2  Fman_L2*(FDDS_stop_GPS_L2-FDDS_start_GPS_L2)/dtScan

#define dFscan_GL_L2  Fman_L2*(FDDS_stop_GL_L2-FDDS_start_GL_L2)/dtScan

//---------------------L1--------------------------------------
#define Fget1_GPS_GL_L1L2         1700//   4775


#define FDDS_devGPS_L1           (500*kHz)
#define F0DDS_GPS_L1             (124580*kHz)  // 99580
#define FDDS_start_GPS_L1 (F0DDS_GPS_L1 - FDDS_devGPS_L1)
#define FDDS_stop_GPS_L1  (F0DDS_GPS_L1 + FDDS_devGPS_L1)

#define FDDS_devGL_L1            (4*MHz)
#define F0DDS_GL_L1              (98200*kHz)   //73600
#define FDDS_start_GL_L1 (F0DDS_GL_L1 - FDDS_devGL_L1)
#define FDDS_stop_GL_L1  (F0DDS_GL_L1 + FDDS_devGL_L1)

#define Fman_L1 (1*kHz)//��� ��� � ��� �������

#define dFscan_GPS_L1  Fman_L1*(FDDS_stop_GPS_L1-FDDS_start_GPS_L1)/dtScan

#define dFscan_GL_L1  Fman_L1*(FDDS_stop_GL_L1-FDDS_start_GL_L1)/dtScan



uint8_t  Init_AD9959(void);

char  Set_param_AD9959_1 (uint64_t F_DDS_start, uint64_t F_DDS_stop, uint64_t dF_scan);

char  Set_param_AD9959_2 (uint64_t F_DDS_start, uint64_t F_DDS_stop, uint64_t dF_scan);


 void  Set_param_AD9959_test1 (uint64_t F1_DDS,uint64_t F2_DDS);
 void  Set_param_AD9959_test2 (uint64_t F1_DDS,uint64_t F2_DDS);
 
  char  Set_param_AD9959_0RCH (uint64_t F_DDS); 
 char  Set_param_AD9959_1RCH (uint64_t F_DDS) ;
  char  Set_param_AD9959_2RCH (uint64_t F_DDS); 
 char  Set_param_AD9959_3RCH (uint64_t F_DDS) ; 
 
 
 uint8_t Set_param_AD9959_1QPSK (uint64_t F_DDS1,uint64_t F_dev);
 uint8_t Set_param_AD9959_2QPSK (uint64_t F_DDS2,uint64_t F_dev);
 
 uint8_t Set_param_AD9959_GPS_L1 (void);
 uint8_t Set_param_AD9959_Glonass_L1 (void);
 uint8_t Set_param_AD9959_GPS_L2 (void);
 uint8_t Set_param_AD9959_Glonass_L2 (void);
void Stop_AD9959(void);
  
  
#endif	/* DDS_AD9959_H */  
  