/*
 * File:   eepromParam.c
 * Author: User
 *
 * Created on 31 ���� 2019 �., 11:04
 */


#include "xc.h"
#include "eeprom.h"
#include "eepromParam.h"
#include "system_cfg.h"
#include <string.h>

// setaddreeprom

// (1) id (3) fn (3) fv (1)kn (1)kv (1)KK   mod255  all 10 byte + 1 Status

// odin dly raboty 
//uint8_t setparameeprom [28][cntPgEE]; //[ id ][]// Data//kk//      status = 1 Ok/0 nOK
//vtoroy dly zagruzki

uint8_t setParamEEprom [cntIDDtEE][cntParamEE]; //setParamEEprom[ id ][ ]/_____/id// Data//kk//status (= 1 Ok/0 nOK)//


uint8_t SaveAllEEParam(void)
{

    uint8_t Status;
    unsigned int i,j;
    uint8_t pageSetParam[cntParamEE];
    static uint8_t bufferEETx[cntDtEEall]; 
    static uint8_t bufferEERx[cntDtEEall];
    unsigned int  sumEE;
    int addr;      

    addr = 0;
    for (j=0; j < cntIDDtEE; j++) //dt
    {      
        sumEE = 0;    
        for (i=0; i < cntInfDtEE; i++) //dt
        {
            pageSetParam [i] = setParamEEprom [j][i];// copy data
            sumEE = CalcCRC(sumEE, pageSetParam [i]);  
        }
        pageSetParam [cntInfDtEE] = sumEE;
        setParamEEprom [j][cntInfDtEE+1] = 0;       // status clr 
        
        memcpy (&bufferEETx[addr],pageSetParam, cntParamEE);
        addr = addr + cntParamEE;          
    }  
    
    eeprom_write_array(0x0000,bufferEETx, cntDtEEall);
    delay_ms(100);
//--------------------------------Read------------------------------------    
    Status = eeprom_read_array(0x0000, bufferEERx, cntDtEEall);
    
    if (Status != MCHP_SUCCESS)
    {
        return MCHP_CMD_FAILURE;
    }

    for (j=0; j < cntDtEEall; j++) //dt
    {
        if (bufferEETx[j]!=bufferEERx[j])
        {
           return MCHP_CMD_FAILURE;
        }

    }
    
    return MCHP_SUCCESS;
}

uint8_t loadAllEEParam(void)
{
    uint8_t status;
    unsigned int i,j;
    static uint8_t pageGetParam[cntParamEE]; 
    static uint8_t bufferEERx[cntDtEEall]; 
//    unsigned char page1GetParam[cntdtEE];   
    uint16_t addr;
    
    unsigned int  sumEE;

    status = eeprom_read_array(0x0000, bufferEERx, cntDtEEall);
    if (status != MCHP_SUCCESS)
    {
        return status;
    }
    //-------------clear prarms buffer-------------------------
    for (j=0; j < cntIDDtEE; j++) // id 
    {
        for (i=0; i < cntParamEE; i++) //dt
        {
            setParamEEprom[j][i]= 0;            
        }        
    }        
    addr = 0;   
    //-------------------------------------------------    
    for (j=0; j < cntIDDtEE; j++) // id 
    {        
        memcpy (pageGetParam, &bufferEERx[addr], cntParamEE);

        addr = addr + cntParamEE;          
        sumEE = 0;// ����������� ���   
   
        for (i=0; i < cntInfDtEE; i++) //dt
        {
            sumEE = CalcCRC(sumEE, pageGetParam [i]);             
            setParamEEprom[j][i] = pageGetParam[i];   //chtenie dannyh parametrov i zapis v bufer            
        }                          
        setParamEEprom[j][cntInfDtEE] = pageGetParam[cntInfDtEE];//kk
        // proveryem summu KK
        if (sumEE == pageGetParam[cntInfDtEE])
        {
            setParamEEprom[j][cntInfDtEE+1] = 1;        // set status OK / nOK
        }
        else
        {
            setParamEEprom[j][cntInfDtEE+1] = 0;
            status = MCHP_CRC_ERROR;
        }   
        
    }    
    return status;
}

