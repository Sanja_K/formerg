/******************************************************************************/
/* User Level #define Macros                                                  */
/******************************************************************************/

/* TODO Application specific user parameters used in user.c may go here */

/******************************************************************************/
/* User Function Prototypes                                                   */
/******************************************************************************/
#ifndef XC_user_H
#define	XC_user_H

#include "system_cfg.h"

/* TODO User level functions prototypes (i.e. InitApp) go here */



uint8_t InitApp(void);         /* I/O and Peripheral Initialization */
void UserTaskApp (void);

typedef enum
{
    /* Application's state machine's initial state. */
    APP_STATE_INIT=0,
    APP_STATE_CHECK_READY,
    APP_STATE_WAIT_READY,
    APP_STATE_WAIT_SWITCH_PRESS,
    APP_STATE_READ,
    APP_STATE_PRINT,
    APP_STATE_WAIT_SWITCH_RELEASE,
    APP_STATE_ERROR,
    APP_STATE_IDLE,
    /* TODO: Define states used by the application state machine. */

} APP_STATES_def;
 
typedef enum { // ������ � user task
	app_task_init,
    app_task_event, 
    app_task_idle, 
    app_task_test, 
    app_task_error,
    app_task_exeCMD, 
}app_task_state_def; 



typedef struct
{
    /* The application's current state */
    APP_STATES_def              state;
    
    /* TODO: Define any additional data used by the application. */

} APP_DATA_def;


extern APP_DATA_def app_task;

#endif  