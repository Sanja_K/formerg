/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _EXAMPLE_FILE_NAME_H    /* Guard against multiple inclusion */
#define _EXAMPLE_FILE_NAME_H

#include "definitions.h"
/*************************************************************************** */
#define versionPgm     511

#define   addrF1   0x12 //
#define   addrO1   0x21 //

#define   addrF4   0x14 //
#define   addrO4   0x41 //

#define   addrF3   0x13 
#define   addrO3   0x31
/* ************************************************************************** */

#define GHz 1000000000LL
#define MHz    1000000LL
#define kHz       1000LL
#define  Hz          1LL

#define SYS_CLK_FREQ 80000000LL 


/* This section BIT macros************ ******************
 */
#define BIT_SET(reg, bit) ( reg |= (1<<bit) )
#define BIT_CLR(reg, bit) ( reg &= (~(1<<bit)))
#define BIT_INV(reg, bit) ( reg ^= (1<<bit))

#define BIT0(x) (0<<(x))
#define BIT1(x) (1<<(x))

#define  BITREAD(BYTE,BIT) (BYTE >> BIT) & 1
/* ************************************************************************** */
/* Section: status                                                    */
/* ************************************************************************** */
#define MCHP_SUCCESS                     0x00
#define MCHP_FAILURE                     0x01

#define MCHP_DEVICE_NOT_FOUND            0x02

#define MCHP_DEVICE_IS_STARTED           0x05
#define MCHP_DEVICE_IS_STOPPED           0x06


#define MCHP_DDS_ERROR                  0x07
#define MCHP_LMX_ERROR                  0x08

#define MCHP_INVALID_VERSION             0xF4
#define MCHP_CRC_ERROR                   0xF6
#define MCHP_RUN_ERROR                   0xF8//
#define MCHP_CMD_FAILURE                 0xFF 

#define Diap1 DIAP1_Get()
#define Diap2 DIAP2_Get()

#define LED1_ON  GPIO_LED1_Set()
#define LED1_OFF GPIO_LED1_Clear()

#define AD485Tx_ON ON_RS485_2_Set() 
#define AD485Rx_ON ON_RS485_2_Clear() 

#define EN_PWR_CH1_ON	EN_PWR_CH1_Set()
#define EN_PWR_CH1_OFF	EN_PWR_CH1_Clear()

#define EN_PWR_CH2_ON	EN_PWR_CH2_Set()
#define EN_PWR_CH2_OFF	EN_PWR_CH2_Clear()
//-------------------------------------
#define EN_PWR_CH3_ON	EN_PWR_CH2_Set()
#define EN_PWR_CH3_OFF	EN_PWR_CH2_Clear()

#define EN_PWR_CH2_IF1_ON   EN_PWR_CH2_IF1_Set()
#define EN_PWR_CH2_IF1_OFF  EN_PWR_CH2_IF1_Clear()

#define EN_PWR_CH2_IF2_ON   EN_PWR_CH2_IF2_Set()
#define EN_PWR_CH2_IF2_OFF  EN_PWR_CH2_IF2_Clear()

#define EN_PWR_CH1_RF1_ON    EN_PWR_CH1_RF1_Set()
#define EN_PWR_CH1_RF1_OFF   EN_PWR_CH1_RF1_Clear()

#define EN_PWR_CH1_RF2_ON    EN_PWR_CH1_RF2_Set()
#define EN_PWR_CH1_RF2_OFF   EN_PWR_CH1_RF2_Clear()

#define EN_PWR_CH1_RF3_ON    EN_PWR_CH1_RF3_Set()
#define EN_PWR_CH1_RF3_OFF   EN_PWR_CH1_RF3_Clear()

#define PWR_CH1_AMPL_ON   RF1_AMPL_ON_Set();
#define PWR_CH1_AMPL_OFF  RF1_AMPL_ON_Clear();
//----------------------------------------------------

#define wdt_clr() (WDTCONbits.WDTCLR = 1)
 

extern uint8_t addrL;
extern uint8_t addrO;
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */


/* TODO:  Include other files here if needed. */

void delay_us(uint32_t microseconds);
void delay_ms(uint32_t ms);

int CalcCRC(int dtSum, int dt);
void LLDIV (uint64_t numer, uint64_t denom,  uint64_t *quot, uint64_t *rem);
long Nod(long a, long b);
void AddrInit(void);

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
