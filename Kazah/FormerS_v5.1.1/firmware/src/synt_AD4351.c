/*
 * File:   UART.c
 * Author: Alexandr Kazeka
 *
 * Created on September 20, 2017, 4:20 PM
 */
#include "system_cfg.h"        /* System funct/params, like osc/peripheral config */
#include "ADF435x.h" 
#include "synt_AD4351.h"        /* System funct/params, like osc/peripheral config */

#define CHSPACE1   1000000 // 1 ���

struct  ADF4351_Reg_typeDef *ADF4351_Reg;
struct  ADF4351_Reg_typeDef ADF4351_RegNum;
  
struct ADF4351_initDef ADF4351_InitData = {false, 0,0,4095,enum_ADF_MUX_OUT_digitalLockDetect,enum_ADF_lockDetectFunction_digitalLockDetect,enum_ADF_outputPower_m4dBm,enum_ADF_outputPower_m4dBm };

   // 1 - lock, 0 - not, -1 - �� ��������������


int ADF4351_LE_num = ADF4351_LE1;


char  ADF4351_check_lockDetect( void )
{   char px;

    if (ADF4351_v_i_LD == 1)
     px = 0; 
    else
        px=1;
  
    return  px;
}

char  ADF4351_check_MUX_OUT( void )
{  char px;

//    if (ADF4351_v_i_MUX_OUT == 1)
//     px = 0; 
//    else
     px = 1;
    return  px;
}
void  ADF4351_v_o_DATA( uint32_t ena )
{
    if(ena == 1)
        ADF4351_v_o_DATA_1
    else
        ADF4351_v_o_DATA_0;   
}    


void  ADF4351_v_writeReg( uint32_t reg)
{ unsigned int i=0;
    ADF4351_v_o_CLK_0;
//    ADF4351_v_o_CE_1;     
    
if (ADF4351_LE_num == ADF4351_LE1)
{
    ADF4351_v_o_LE1_0;
}
else
{
    ADF4351_v_o_LE2_0;
}


    delay_us( 10 );

    for( i=0; i<32; i++ ) {
        ADF4351_v_o_DATA( (reg>>31)&1 );
        delay_us( 10 );
        ADF4351_v_o_CLK_1;
        reg = reg << 1;
        delay_us( 10 );
        ADF4351_v_o_CLK_0;
    }
    delay_us( 10 );

    ADF4351_v_o_LE1_1;
    ADF4351_v_o_LE2_1;
//    ADF4351_v_o_CE_0;
} 
  
char  ADF4351_initialize(struct ADF4351_initDef Init)//
{
	// �������� ���������, � �������� ����������� �������������
	 ADF4351_InitData = Init; // �������� ��������� �� ������
      ADF4351_Reg = &ADF4351_RegNum;
      
    // �������� ���������
	if( ADF4351_InitData.reference_freq_Hz==0 )   return 1;

	// ���� �� ������ ������� �������� ���������, �� ������ ������� ���������� ���� �
	// ��������� �� ������������
    if( ADF4351_InitData.phaseDet_freq_Hz==0 )   ADF4351_InitData.phaseDet_freq_Hz = ADF4351_InitData.reference_freq_Hz;

    // ������ ��������� ����������� � ��������� ���������
//    ADF4351_v_o_RFOUT_nEN();
    ADF4351_v_o_LE1_1;  
    ADF4351_v_o_LE2_1; 
    ADF4351_v_o_CLK_0;
    ADF4351_v_o_DATA(0);

	// �������� ����������
//	ADF4351_v_o_CE_1;

	// ������� ��� �������� � ��������� ������
     
    // ������� ��� �������� � ��������� ������

    ADF4351_Reg->register5 =  0x000400005; // 140 MHz
    ADF4351_Reg->register4 =  0x000C28024; // 0x000C28024; // 0x000C28124
    ADF4351_Reg->register3 =  0x0000004B3;
    ADF4351_Reg->register2 =  0x018014EC2;
    ADF4351_Reg->register1 =  0x008008011;
    ADF4351_Reg->register0 =  0x000E00000;

        
	// 0 = Enables the fractional-N mode; 1 = Enables the integer-N mode; The LDF bit must also be set to the appropriate mode.
    // ��������� �������� �������� FRAC1 = 0; FRAC2 = 0
       
    if(!ADF4351_InitData.mode_INT_nFRAC)
    {  ADF4351_Reg->Reg0.FRAC = 0;}   
 
	// MUX_OUT  
    ADF4351_Reg->Reg2.MUXOUT = ( (unsigned)ADF4351_InitData.enum_ADF_MUX_OUT>>0 )&0x7;
	// R divider
	unsigned  Rdiv = ADF4351_InitData.reference_freq_Hz / ADF4351_InitData.phaseDet_freq_Hz; // � �����������
	if( Rdiv<1 || Rdiv>1023 )    return  1;
        ADF4351_Reg->Reg2.REFCNT = Rdiv;
   
    if (ADF4351_InitData.mode_INT_nFRAC){
        ADF4351_Reg->Reg2.LDF = 1; // LD MODE 0 = FRACTIONAL-N; 1 = INTEGER-N (2.9ns) ���� mode_INT_nFRAC = true 1 = INTEGER-N
        ADF4351_Reg->Reg2.LDP = 0; //
    }
    else
    {
        ADF4351_Reg->Reg2.LDF = 0; // 
        ADF4351_Reg->Reg2.LDP = 1; //        
    }    
    
	// �������� �������� ������ A
	ADF4351_setPowerLevel( enum_ADF_RFOUT_A, ADF4351_InitData.enum_ADF_outputPower_RFOUT_A );
	// �������� �������� ������ B
	ADF4351_setPowerLevel( enum_ADF_RFOUT_B, ADF4351_InitData.enum_ADF_outputPower_RFOUT_B );

    ADF4351_Reg->Reg1.MOD = ADF4351_InitData.freq_modulusValue;
            
            // Lock detect function
	ADF4351_Reg->Reg5.LDPM = ADF4351_InitData.enum_ADF_lockDetectFunction;

    ADF4351_Reg->Reg2.PWRDN = 1;
    delay_ms(10);
	// �������� ��� �������� � ������ ������������������  
    ADF4351_update_REG();
    delay_ms(100);
	return  0;
} 
  
  
  
char  ADF4351_setFreq( uint64_t freq_Hz )
{
  uint64_t quot,rem; // �������, ������� 
  
  uint64_t  fVCO = freq_Hz;
  
  uint64_t  fVCO_min = 2.2 * 1000000000LL; // ��� ������� VCO 2.2GHz to 4.4GHz
  uint64_t Result;
  uint32_t FRAC, MOD;
  uint16_t INT;
  unsigned  divA = 0;
  
 if ((freq_Hz < 35*MHz)&&( 4400*MHz < freq_Hz))    
    
  {return  1;} // �������� ������� ��� ���������
  // ������������ �������� RF = divA

	while( fVCO < fVCO_min ) {
		fVCO = fVCO * 2;
		if( ++divA == 6 )  break;
	}
	if( fVCO<fVCO_min )  fVCO = fVCO_min;
  // ���������� ��������
  ADF4351_Reg->Reg4.RFDIV = divA;
  // ������������ �������

    LLDIV (fVCO,ADF4351_InitData.phaseDet_freq_Hz,&quot,&rem);  
 
    INT = (uint16_t)quot;
    
    if ((INT>23)&&(INT<32767))
        ADF4351_Reg->Reg1.PRESC = 0; // PRESCALER 0 = 4/5  �� 23 .. 32767
    else if ((INT>75)&&(INT<65535))
        ADF4351_Reg->Reg1.PRESC = 1; //1 = 8/9    1= 8/9  �� 75...65535
    else  return  1;
    
        ADF4351_Reg->Reg0.INT = INT; //  = INT ADF
    
    Result = rem; // �������
    
    MOD = ADF4351_InitData.freq_modulusValue; //����� ������� �� ������

    // ��������� ����� � ������� ����� FRAC1 = MOD1 * rem / InitData.phaseDet_freq_Hz
    

    Result = Result*MOD; // �������� ���������� FRAC �����
    
     
    FRAC = Result/ADF4351_InitData.phaseDet_freq_Hz; // �������� ����� �����
    ADF4351_Reg->Reg0.FRAC = FRAC;
    /*
	ADF4351_v_writeReg( ADF4351_Reg->register5 );
	ADF4351_v_writeReg( ADF4351_Reg->register4 );
	ADF4351_v_writeReg( ADF4351_Reg->register3 );
	ADF4351_v_writeReg( ADF4351_Reg->register2 );
	ADF4351_v_writeReg( ADF4351_Reg->register1 );  
	ADF4351_v_writeReg( ADF4351_Reg->register0 );
    */
   delay_ms(1); 
  return  0;
  
  }
  
char  ADF4351_disable( void )
{
    // ������ ��������� ����������� � ��������� ���������
    ADF4351_v_o_CLK_0;
    ADF4351_v_o_DATA( 0 );
//    ADF4351_v_o_LE_0();
//    ADF4351_v_o_RFOUT_nEN();

    // ��������� ����������
//    ADF4351_v_o_CE_0;

    return  0;
}

char  ADF4351_PWDWN( void )
{
        ADF4351_Reg->Reg2.PWRDN = 1;
        return 1;
}
char  ADF4351_PWUP( void )
{
        ADF4351_Reg->Reg2.PWRDN = 0;
        return 0;
}

char  ADF4351_setCH(unsigned char CH)
{
    switch (CH) {
        case RFCH1:   // RF1  Z12  Z21
            // 00
		//	ADF4351_Reg->Reg4.RFAEN = 0;
            ADF4351_Reg->Reg4.RFBEN = 1;
        break;    
        case RFCH2:   
            // 10     //RF3  Z13  Z22
		//	ADF4351_Reg->Reg4.RFAEN = 0;
            ADF4351_Reg->Reg4.RFBEN = 1;
   
        break;      
        case RFCH3:   
            // 11    //RF4   Z14  Z23  
			ADF4351_Reg->Reg4.RFAEN = 1;
            ADF4351_Reg->Reg4.RFBEN = 0;
   
        break;           
		case RFCH4:   //RF2   Z15  Z24
            // 01    
			ADF4351_Reg->Reg4.RFAEN = 1;
            ADF4351_Reg->Reg4.RFBEN = 0;
        break;
    }
    return 0;    
}


char  ADF4351_setPowerLevel( enum_ADF_RFOUT RFOUT, enum_ADF_outputPower_typeDef lev )
{
	// �������� �������� ������ A
	if( RFOUT==enum_ADF_RFOUT_A || RFOUT==enum_ADF_RFOUT_both ) {
		if( lev==enum_ADF_outputPower_disabled ) {
			ADF4351_Reg->Reg4.RFAEN = 0;
			ADF4351_Reg->Reg4.RFAPWR   = 0;
		} else {
			ADF4351_Reg->Reg4.RFAEN = 1;
			ADF4351_Reg->Reg4.RFAPWR   = (unsigned)lev & 3;
		}
	}

	// �������� �������� ������ B
	if( RFOUT==enum_ADF_RFOUT_B || RFOUT==enum_ADF_RFOUT_both ) {
		if( lev==enum_ADF_outputPower_disabled ) {
			ADF4351_Reg->Reg4.RFBEN = 0;
			ADF4351_Reg->Reg4.RFBPWR   = 0;
		} else {
			ADF4351_Reg->Reg4.RFBEN = 1;
			ADF4351_Reg->Reg4.RFBPWR   = (unsigned)lev & 3;
		}
	}

//	rf_Synt_ADF4351_v_writeReg( Reg->register6 );
    return 1;
}

char  ADF4351_update_REG()
  {
 	ADF4351_v_writeReg( ADF4351_Reg->register5);
	ADF4351_v_writeReg( ADF4351_Reg->register4);
	ADF4351_v_writeReg( ADF4351_Reg->register3);
	ADF4351_v_writeReg( ADF4351_Reg->register2);
	ADF4351_v_writeReg( ADF4351_Reg->register1);  
	ADF4351_v_writeReg( ADF4351_Reg->register0); 
     return  0;   
     
  }