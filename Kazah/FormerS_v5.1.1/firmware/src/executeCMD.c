/*
 * File:   executeCMD.c
 * Author: Alexandr
 *
 * Created on 12 11 2020, 9:26
 */


#include "xc.h"
#include "system_cfg.h"
#include "AD9914.h"
#include "hmc1122.h"
#include "ad7995.h"
#include "synt_AD9959.h"
#include "DDS_AD9959.h"
#include "ADF435x.h"
#include "adl5240.h"
#include "XC_DATA.h"
#include "LMX2572.h"
#include "former_signal.h"
#include "user.h"
#include "executeCMD.h"
#include "eepromParam.h"
#include "channel.h"
#include "ReceiveCMD.h"

#define offCMD_CH2_100500       1  // 100-500 
#define offCMD_CH2_5002500      2  // 500-2500
#define offCMD_CH1_25006000     3  // 2500-6000
#define offCMD_CH1_GNSS         4  //  L1
#define offCMD_All              0xFF  //  L1
//************************************************
#define ADC_POROGCH1_1_L2   300  //Ch1
#define ADC_POROGCH3_1_L1   300  //Ch3

#define ADC_POROGCH2_1    292//Ch2 100- 500

#define ADC_POROGCH1_2    230 // 2500- 6000
#define ADC_POROGCH2_2    280 // 500- 2500
//************************************************

uint64_t  Freq_man[14] = {0,12500000,10000000,7518797,5000000,2000000,1000000,200000,100000,50000,25000,12500,2500};
// convert

uint8_t   genFlgOnCH2_L1 = 0; // L1
uint8_t   genFlgOnCH1_L2 = 0; // L2 
uint8_t   genFlgOnCH2 = 0;
uint8_t   genFlgOnCH3_100_500 = 0; // 100-500 // 500-2500
uint8_t   genFlgOnCH2_500_2500 = 0;
uint8_t   genFlgOnCH1_2500_6000 = 0; // 2500-6000


uint8_t     error_flg_Dt =0;

LITERA_param_def    LITERA_param;
LITERA_dt_cmd_def   LITERA_cmd_dt;
NAVIG_param_def     NAVIG_dt;


//**************************************************
uint8_t setRF_100_500(void)//
{
	
   uint8_t status;
    uint32_t  freqDDS ;
    int cntr;
    uint8_t  bittest;
//    uint64_t  set_Freq_Hz;     
    uint32_t  set_Freq_MHz;
    uint32_t  Freq_ref;
    uint32_t deviation ; //kHz
    uint32_t lfm_speed; //kHz  
//    uint8_t  GainData;
        //---------------- set param DATA ---------------- 
        status = 0;  
        SW1DA29_RF3_L1(SWRF2);
    //    SWtest100500(SWALLTEST);
        EN_PWR_CH3_ON;    
 
        delay_ms(1);
        genFlgOnCH3_100_500 = 1;                  
        adl5240_set_gain_amp3 (0);   //LE_ATT2  

        set_Freq_MHz = Freq_dds2_F1;//1500
        Freq_ref = 0;
        Init_LMX2572 (syntLMX3); 
        LMX2572_Frequency_setting_MHz(syntLMX3,set_Freq_MHz);  
        LMX2572_Power_A(syntLMX3, 60);
        LMX2572_switch_RFoutA(syntLMX3,1); 
//        LMX2572_Power_B(syntLMX3, 60);    
//        LMX2572_switch_RFoutB(syntLMX3,1);
        //--------------syntLMX3---------------------------
        cntr = 0;     
        bittest = 0;
        while ((!bittest)&&(cntr<=100))
        {
            bittest = BITREAD(XC_LD_DATA(),2);
            delay_ms (10);
            cntr ++;
        }          
        if (cntr >= 100)
        {
           status = MCHP_LMX_ERROR;
        }
        //------------------------------------------------
        status = former9914_init(CS_DDS3); 

        if (status != MCHP_SUCCESS)
        {
            status = MCHP_DDS_ERROR;
        }
        
        freqDDS = LITERA_param.freq;   
        //-------------------------------------------
        if ((LITERA_param .mod == 4) ||(LITERA_param .mod == 5)) 
        {     

            deviation = LITERA_param.dev;
            lfm_speed = LITERA_param.manip;
            Freq_ref = set_Freq_MHz;
            set9914_mode_lfm(CS_DDS3,freqDDS, deviation, lfm_speed,Freq_ref);   
            
        }
        else
        {
            set9914_mode_freq(CS_DDS3,freqDDS,Freq_ref);
        }
//
            //SWtest100500(SWRFOUT);
            SW1DA29_RF3_L1(SWRF1);
        
        //    GainData = getGainK(LITERA_param.freq);
            adl5240_set_gain_amp3 (30); 	
    
    
	return 0;
}

uint8_t setRF_500_2500(void)//
{  
    uint64_t F_DDS;
    uint32_t deviation; //kHz
    uint32_t lfm_speed; //kHz  
    
    uint64_t ADF_freq;    
    uint8_t  SW3_set;   
    uint8_t  SW4_set;     

    
    
   SW1DA4_RF2_L2(SWRF1);//test
   SW2DA25_RF2_L2(SWRF1); //test    
   //-------------------------
    EN_PWR_CH2_ON;
   
   EN_PWR_CH2_IF1_ON;
   EN_PWR_CH2_IF2_ON;     
    
   //-------------------------    
    F_DDS = LITERA_param.freq;   
    ADF_freq = 0;
    SW3_set = 0;
    SW4_set = 0;  
    
    if((LITERA_param.freq >= 500000) && (LITERA_param.freq < 1200000)) // 1
    {
        ADF_freq = fADF2_CH2_F1;
        SW3_set = SWRF1;
        SW4_set = SWRF1;       
        
    }
    else if ((LITERA_param.freq >= 1200000) && (LITERA_param.freq < 1500000))
    {
        ADF_freq = fADF2_CH2_F2;
        SW3_set = SWRF4;
        SW4_set = SWRF4; 
    
    }    
    else if ((LITERA_param.freq >= 1500000) && (LITERA_param.freq < 2500000))
    {
        
    
        if ((LITERA_param.freq >= 1500000) && (LITERA_param.freq < 1700000))
        {
            SW3_set = SWRF3;
            SW4_set = SWRF2; 
            ADF_freq = fADF2_CH2_F3;
            
        }
        else if ((LITERA_param.freq >= 1700000) && (LITERA_param.freq < 1900000))
        {
        
            SW3_set = SWRF3;
            SW4_set = SWRF2;             
            ADF_freq = fADF2_CH2_F4;
        
        }    
        else if ((LITERA_param.freq >= 1900000) && (LITERA_param.freq < 2100000))
        {
            SW3_set = SWRF3;
            SW4_set = SWRF2;         
            ADF_freq = fADF2_CH2_F5;
        
        }  
        else if ((LITERA_param.freq >= 2100000) && (LITERA_param.freq < 2300000))
        {
            SW3_set = SWRF2;
            SW4_set = SWRF2;        
            ADF_freq = fADF2_CH2_F6;
        
        }          
        else if ((LITERA_param.freq >= 2300000) && (LITERA_param.freq < 2500000))
        {
            SW3_set = SWRF2;
            SW4_set = SWRF3;         
            ADF_freq = fADF2_CH2_F7;
        
        }else
        {
            return MCHP_CMD_FAILURE;
        }
        
    }     
    
    F_DDS = ADF_freq - F_DDS;
    Init_ADF2App(ADF_freq);     
    former9914_init(CS_DDS5);
    
    //-------------------------------------
    
    ad9914_select_dds_5();
    delay_us (10);    
    ad9914_spi_3wire_osk_mode();
    ad9914_unselect_dds_5();       
    
    //-------------------------------------    
    
    if (LITERA_param .mod == 4) 
    {     
        deviation = LITERA_param.dev;
        lfm_speed = LITERA_param.manip;
        
        set9914_mode_lfm(CS_DDS5,F_DDS, deviation, lfm_speed, ADF_freq);                                    
    }else
    {
        set9914_mode_freq(CS_DDS5,F_DDS, ADF_freq);
    }

    //---------------------------------------    
   set9914_mode_amp(CS_DDS5,1280);
   hmc1122_set_gain_amp2(43);
   
   SW1DA4_RF2_L2(SW3_set);//test
   SW2DA25_RF2_L2(SW4_set); //test   
     
   
    return MCHP_SUCCESS;
}

uint8_t setRF_2500_6000(void)//
{ //uint8_t status;

    uint64_t F_DDS;
    uint32_t deviation; //kHz
    uint32_t lfm_speed; //kHz  
    
    uint64_t ADF_freq;    
    uint8_t  SW1_set;   
    uint8_t  SW2_set;             
//    uint16_t  ADC_Res;            

  
   SW1DA2_RF1_L2(SWRF1);//test
   SW2DA7_RF1_L2(SWRF1); //test    

   EN_PWR_CH1_ON;
   
   EN_PWR_CH1_RF1_OFF;
   EN_PWR_CH1_RF2_OFF;
   EN_PWR_CH1_RF3_OFF;
   
   PWR_CH1_AMPL_OFF;
  

//   EN_PWR_CH2_IF1_ON;
//   EN_PWR_CH2_IF2_ON;   
   
   
    F_DDS = LITERA_param.freq/3;   
    ADF_freq = 0;
    SW1_set = 0;
    SW2_set = 0;  
    
    if((LITERA_param.freq >= 2500000) && (LITERA_param.freq < 3300000)) // 1
    {
        ADF_freq = fADF1_CH1_F1;
        SW1_set = SWRF2;
        SW2_set = SWRF4;       
        EN_PWR_CH1_RF3_ON;
    }
    else if ((LITERA_param.freq >= 3300000) && (LITERA_param.freq < 4500000))
    {
        ADF_freq = fADF1_CH1_F2; 
        SW1_set = SWRF2;
        SW2_set = SWRF2; 
        EN_PWR_CH1_RF1_ON;
    }
    else if ((LITERA_param.freq >= 4500000) && (LITERA_param.freq <= 6000000))
    {
        SW1_set = SWRF3;
        SW2_set = SWRF3;
        EN_PWR_CH1_RF2_ON;
        if ((LITERA_param.freq >= 4500000) && (LITERA_param.freq < 4800000))
        {
            F_DDS = fADF1_CH1_F3 - F_DDS;
            ADF_freq = fADF1_CH1_F3; 
            
        }
        else if ((LITERA_param.freq >= 4800000) && (LITERA_param.freq < 5100000))
        {
            F_DDS = fADF1_CH1_F4 - F_DDS;
            ADF_freq = fADF1_CH1_F4;         
        }
        else if ((LITERA_param.freq >= 5100000) && (LITERA_param.freq < 5400000))
        {
            F_DDS = fADF1_CH1_F5 - F_DDS;
            ADF_freq = fADF1_CH1_F5;          
        }
        else if ((LITERA_param.freq >= 5400000) && (LITERA_param.freq < 5700000))
        {
            F_DDS = fADF1_CH1_F6 - F_DDS;
            ADF_freq = fADF1_CH1_F6;           
        }
        else if ((LITERA_param.freq >= 5700000) && (LITERA_param.freq <= 6000000))
        {
            F_DDS = fADF1_CH1_F7 - F_DDS;
            ADF_freq = fADF1_CH1_F7;    
            PWR_CH1_AMPL_OFF;
        }
        else
        {
            return MCHP_CMD_FAILURE;
        }
    }
    
    Init_ADF1App(ADF_freq); 
    former9914_init(CS_DDS4);
    //-------------------------------------
    
    ad9914_select_dds_4();
    delay_us (10);    
    ad9914_spi_3wire_osk_mode();
    ad9914_unselect_dds_4();       
    
    //-------------------------------------
    
    if (LITERA_param .mod == 4) 
    {     
        deviation = LITERA_param.dev;
        lfm_speed = LITERA_param.manip;
        
        set9914_mode_lfm(CS_DDS4,F_DDS, deviation, lfm_speed, ADF_freq);                                    
    }else
    {
        set9914_mode_freq(CS_DDS4,F_DDS, ADF_freq);
    
    }
    //-------------------------------------
    
    set9914_mode_amp(CS_DDS4,1280);//<---------proverit
    //-------------------------------------    
    
   hmc1122_set_gain_amp1(63);   
   SW1DA2_RF1_L2(SW1_set);//signal
   SW2DA7_RF1_L2(SW2_set); //signal    
    
   genFlgOnCH1_2500_6000 = 1;
   
//   
//    ADC_Res = fun_Read7995(RF_CH1, AD7995_ADDR); 
//    
//    status =  MCHP_SUCCESS;
//    
//    if (ADC_Res < 500)
//    {
//        status =  MCHP_FAILURE;
//    }
//    
    
    return MCHP_SUCCESS;//MCHP_SUCCESS;
}

//****************Status ustroystva***********************
uint8_t exeCMD4(void) 
{
    uint8_t  status;     
 //   uint8_t  bittest;
    
    unsigned int  len_DT;
    unsigned char inf_Dt[10];     
    uint16_t  ADC_Res;     
    
    status = 0;  
    error_flg_Dt = 0x00; // sbros flaga oshibki
   // bittest = 0;
	//---------------------------------------
    if ( addrL == addrF4 )
    { 
        if (genFlgOnCH1_2500_6000)
        {
            ADC_Res = fun_Read7995(RF_CH1,AD7995_ADDR); // 2500-6000
            if (ADC_Res <= ADC_POROGCH1_2)
            {
                error_flg_Dt |= (1<<5); 
            }          
        }
        else
        {
            error_flg_Dt |= (1<<5);         
        }
        
        if (genFlgOnCH2_500_2500)
        { 
            ADC_Res = fun_Read7995(RF_CH2,AD7995_ADDR); // Ch2    500- 2500      
            if (ADC_Res <= ADC_POROGCH2_2)
            {
                error_flg_Dt |= (1<<6); 
            }      
        }
        else
        {
                error_flg_Dt |= (1<<6); 
        }

	}
	else
	{
		//-------------POWER------------------
        if((genFlgOnCH1_L2)||(genFlgOnCH2_L1))
        {        
            ADC_Res = fun_Read7995(RF_CH1,AD7995_ADDR); // L2 //
            if (ADC_Res <= ADC_POROGCH1_1_L2)
            {
                error_flg_Dt |= (1<<5); 
            }   
        }
        else
        {
            error_flg_Dt |= (1<<5); 
        
        }        
        //-----------------------------------------
        if(genFlgOnCH2)
        {         
            ADC_Res = fun_Read7995(RF_CH2,AD7995_ADDR); // L1
            if (ADC_Res <= ADC_POROGCH3_1_L1)
            {
                error_flg_Dt |= (1<<6); 
            } 
         }
        else
        {
            error_flg_Dt |= (1<<6); 
        }                     
        //--------------------------------------------
        if(genFlgOnCH3_100_500)
        {
            ADC_Res = fun_Read7995(RF_CH3,AD7995_ADDR); // 100-500 // Ch2    500- 2500      
            if (ADC_Res <= ADC_POROGCH2_1)
            {
                error_flg_Dt |= (1<<7); 
            }              
        }
        else
        {
            error_flg_Dt |= (1<<7); 
        }		
	}

	//---------------------------------------
	len_DT = 1;//                      
	error_flg_Dt = error_flg_Dt&0xFF;
	inf_Dt [0] = (unsigned char )error_flg_Dt;  //   

	TransmitDtUrt (0x04,inf_Dt,len_DT);     //
	return status;
}

//*****************************************************************************
// izluchenie OFF
//*****************************************************************************
uint8_t exeCMD24(uint8_t Cmdx) 
{ // 
    
    uint8_t status;     
    unsigned int len_DT;
    uint8_t inf_Dt[10];        
   
    status = 0;
    genFlgOnCH2 = 0;

    if(Cmdx == offCMD_CH2_100500)
    {
        genFlgOnCH3_100_500 = 0;
       // adl5240_set_gain_amp3(0);
        ad9914_deinit(CS_DDS3);  
        
       // SWtest100500(SWALLTEST);  
      //  DeInit_LMX2572(syntLMX3);
        EN_PWR_CH3_OFF;        
        DeInit_LMX2572(syntLMX3);
    }
    else if (Cmdx == offCMD_CH2_5002500)
    {
        genFlgOnCH2_500_2500 = 0;
        ad9914_deinit(CS_DDS5);
       // SWtest5002500(SWALLTEST);
        EN_PWR_CH2_OFF;      
        EN_PWR_CH2_IF1_OFF;
        EN_PWR_CH2_IF2_OFF;    
        
    } 
    else if (Cmdx == offCMD_CH1_25006000)
    {
        genFlgOnCH1_2500_6000 = 0;
      //  ad9910_deinit_1();
        //SWtest25006000(SWALLTEST);
        ad9914_deinit(CS_DDS4);
        EN_PWR_CH1_OFF;      
        //-----------------------------
        PWR_CH1_AMPL_OFF;
        EN_PWR_CH1_RF1_OFF;
        EN_PWR_CH1_RF2_OFF;
        EN_PWR_CH1_RF3_OFF;
        //-----------------------------
//        EN_PWR_CH2_IF1_OFF;
//        EN_PWR_CH2_IF2_OFF;         
    
    }
    else if (Cmdx == offCMD_CH1_GNSS)
   {
        genFlgOnCH2_L1 = 0;
        genFlgOnCH1_L2 = 0;
        adl5240_set_gain_amp2(0);  // L1 
        adl5240_set_gain_amp1(0);  // L2  
       // SWtestGNSS(SWALLTEST);
        
//        
        SetGPSL1 = 0; SetGLL1 = 0; 
        SetGPSL2 = 0; SetGLL2 = 0;
  //      DDS1Mod_XC_0;      
   //     GNSS_OFF_XC;
// 
//        _PWD_DDS1_1;

        EN_PWR_CH1_OFF;

        Stop_AD9959();
       DeInit_LMX2572(syntLMX1);
       DeInit_LMX2572(syntLMX2);            

         

   }  
    else if (Cmdx == offCMD_All)
    {
        genFlgOnCH2  = 0;
        genFlgOnCH2_L1 = 0; // L1
        genFlgOnCH1_L2 = 0; // L2 
        genFlgOnCH3_100_500 = 0; // 100-500 // 500-2500
        genFlgOnCH2_500_2500 = 0;
        genFlgOnCH1_2500_6000 = 0; // 2500-6000
        
        SetGPSL1 = 0; SetGLL1 = 0; 
        SetGPSL2 = 0; SetGLL2 = 0;  
        
//        DDS1Mod_XC_0;        

        if ((addrL == addrF1)||(addrL ==addrF3))
        {     
 
        //    adl5240_set_gain_amp2(0);  // L1 
        //    adl5240_set_gain_amp1(0);  // L2   
       //     adl5240_set_gain_amp3(0);
            Stop_AD9959();
            ad9914_deinit(CS_DDS3);  
        //    SWtestGNSS(SWALLTEST);
        //    SWtest100500(SWALLTEST);
            EN_PWR_CH3_OFF;                        
        }
        else
        {
        //    hmc1122_set_gain_amp1(0);
         //   hmc1122_set_gain_amp2(0);  
         //   ad9910_deinit_1();
        //    ad9910_deinit_2();
            
            ad9914_deinit(CS_DDS4);      
            ad9914_deinit(CS_DDS5);  
            EN_PWR_CH2_OFF;      
            EN_PWR_CH2_IF1_OFF;
            EN_PWR_CH2_IF2_OFF; 
            PWR_CH1_AMPL_OFF;
        }

            EN_PWR_CH1_OFF;
            EN_PWR_CH2_OFF;            
    //        _RF0_OFF;    
    }     
    
   //---------------- Send DATA ----------------       
    // 
    len_DT = 1;// 
    inf_Dt [0] = 0;  //         
    TransmitDtUrt (0x24,inf_Dt,len_DT);    
    return status;
}

//*******************Set_RF************************
uint8_t exeCMD28(void) 
{
    uint8_t status;

    unsigned int len_DT;
    uint8_t inf_Dt[10]; // dlya otveta

    status = MCHP_SUCCESS;
    
    if (addrL == addrF1)
    {
        if((LITERA_param.freq >= 100000) && (LITERA_param.freq <= 500000))
        {           
            status = setRF_100_500();
        }
    }
    else if ( addrL == addrF4)
    {           
        if ((LITERA_param.freq >= 500000) && (LITERA_param.freq < 2500000))
        {
            status = setRF_500_2500();
        }           
        else 
        if((LITERA_param.freq >= 2500000) && (LITERA_param.freq <= 6000000))
        { 
            status = setRF_2500_6000();    
        } 
        else
        {
            status = MCHP_CMD_FAILURE;
        }
    }

//---------------- Send DATA ----------------       
    if (LITERA_cmd_dt.cntSrc == 0)
    {
           
           len_DT = 1;//            
           inf_Dt [0] = status;  // 

           
           TransmitDtUrt (0x28,inf_Dt,len_DT); // 

    }
     return 0;    
}
//**********************test GNSS*****************************
uint8_t exeCMD30 (void) //  test GNSS
{
	 uint8_t  Status;
  unsigned char inf_Dt[10];
  unsigned int len_DT;

    Status = 0x00;  
//  error_flg_Dt = 0xFF;

    if ( addrL == addrF1)
    {
    
        DDS1Mod_XC_0;

        if (NAVIG_dt.GPS_L2)
        {
             Set_param_AD9959_GPS_L2();        
        }
        if (NAVIG_dt.GPS_L1)
        {
             Set_param_AD9959_GPS_L1();   
        }
        if (NAVIG_dt.GL_L2)
        {
           Set_param_AD9959_Glonass_L2();     
        }    
        if (NAVIG_dt.GL_L1)
        {
          Set_param_AD9959_Glonass_L1();    
        }

    }
    else
    {
        
        Status = MCHP_RUN_ERROR;  // error vypolneniya
    
    }
//    //---------------- Send DATA ----------------       
  // 
    len_DT = 1;//                       
    
    inf_Dt [0] = Status;  // 

    TransmitDtUrt (0x30,inf_Dt,len_DT); // 
        
    return Status;
	
}

//**********************test SW*****************************
uint8_t exeCMD31 (uint8_t Cmdx, uint8_t rfCh) // 
{
  uint8_t  Status;
  uint8_t inf_Dt[10];
  unsigned int len_DT;
  uint32_t tmp_rfCh;
// 
 
    if ( addrL == addrF1)
    { 

	}	
    else if ( addrL == addrF4)
    {
		 
        switch ( Cmdx ) {
        case 0:
            SW1DA2_RF1_L2 (rfCh);
          break;
        case 1:
            SW2DA7_RF1_L2 (rfCh);
          break;
        case 2:
            SW1DA4_RF2_L2 (rfCh);
          break;
        case 3:
            SW2DA25_RF2_L2 (rfCh);
          break;          
  
        case 10:    
           tmp_rfCh  = (uint32_t)rfCh;
           tmp_rfCh = tmp_rfCh*16;

            set9914_mode_amp(CS_DDS5,tmp_rfCh);
          break;          
        /*...*/
        default:

          break;
        }		 
	}	
	 else
    {
        Status = MCHP_RUN_ERROR;
    }
	
   //---------------- Send DATA ----------------       
  //   
    len_DT = 1;//                         
    inf_Dt [0] = Status;  // 

    TransmitDtUrt (0x31,inf_Dt,len_DT); //
        
    return Status;	
	
	
}
//************************************************
//===================== GNSS ===========================
    uint8_t exeCMD34 (void)
{   uint8_t   Status;
    uint16_t  error_flg_Dt;
    uint8_t  inf_Dt[10];
    unsigned int len_DT;
//    uint64_t   F_DDS1;  
//    unsigned int ADC_Res;
//    uint8_t   gain_gnss;
 //   uint8_t   GainL1;
//    uint8_t   GainL2;
//    uint8_t   bittest;
//    int cntr;
    Status = 0x00;  
        //-----------------------------------------------------
    DDS1Mod_XC_0;
    SetGPSL1 = 0; SetGLL1 = 0; 
    SetGPSL2 = 0; SetGLL2 = 0; 
    _PWD_DDS1_1;
    _PWD_DDS2_1;
    
//    getGainK_gnss(&GainL1,&GainL2);
    
   if ((addrL == addrF1)||(addrL ==addrF3))
   {          
     //   SWtestGNSS(SWALLTEST);   
          
        EN_PWR_CH1_ON;        
        delay_us(100);  
        
        adl5240_set_gain_amp1(0);  // L2    

        Status = Init_AD9959();
        
        if (Status == MCHP_FAILURE)                         
       {
            Status = MCHP_DDS_ERROR;
       }        

            Init_LMX2572 (syntLMX2);     //DA13
            LMX2572_Frequency_setting_MHz(syntLMX2,Fget1_GPS_GL_L1L2);
            LMX2572_Power_A(syntLMX2, 60);
            LMX2572_switch_RFoutA(syntLMX2,1);
//            LMX2572_Power_B(syntLMX2, 60);    
//            LMX2572_switch_RFoutB(syntLMX2,1);    
//            LMX2572_switch_RFoutB_multiply_A( syntLMX2, 1);   
            
        //---------------test--LMX1--------------------
//            cntr = 0; 
 //           bittest = 0;
//            while ((!bittest)&&(cntr<=100))
//            {
//                bittest = BITREAD(XC_LD_DATA(),0);
//                delay_ms (10);
//                cntr ++;
//            }          
//            if (cntr >= 100)
//            {
//               Status = MCHP_LMX_ERROR;
//            }              
        //---------------------------------------------            
////********************L2****************L2*********************        
        if (NAVIG_dt.GPS_L2||NAVIG_dt.GL_L2)
        {
            
            genFlgOnCH1_L2 = 1;            
//            _EN_PWR_CH1_ON;             

         //----------------------L2------------------------
            //   Set_param_AD9959_Glonass_L1();
            if (NAVIG_dt.GPS_L2)
            {
                Set_param_AD9959_GPS_L2();
                SetGPSL2 = 1;// for plis
             // =========  test=====================         
               
              //  F_DDS1 = F0DDS_GPS_L2; // ??   GPS
               // Set_param_AD9959_1RCH (F_DDS1); 
                 
             // ==============================                 
            }
            if (NAVIG_dt.GL_L2)
            {    
               Set_param_AD9959_Glonass_L2();
               SetGLL2 = 1;
              // =========  test=====================                
              //  F_DDS1 = F0DDS_GL_L2; // ??    Glonass
              //  Set_param_AD9959_0RCH (F_DDS1);  
             // ==============================                 
            }  
            //------------------------------------------------------------
//            Init_LMX2572 (syntLMX2);
//            LMX2572_Frequency_setting_MHz(syntLMX2,Fget2_GPS_GL_L2);
//           // LMX2572_Frequency_setting_MHz(syntLMX2,Fget_GPS_GL_L2_test);            
//            LMX2572_Power_A(syntLMX2, 22);    
//            LMX2572_switch_RFoutA(syntLMX2,1); 
            
            //------------------LMX2---------------------------------
//            cntr = 0; 
 //           bittest = 0;
//            while ((!bittest)&&(cntr<=100))
//            {
//                bittest = BITREAD(XC_LD_DATA(),1);
//                delay_ms (10);
//                cntr ++;
//            }          
//            if (cntr >= 100)
//            {
//               Status = MCHP_LMX_ERROR;
//            }                     
            //------------------------------------------------------------
//            gain_gnss = GainL2 - NAVIG_dt.GNSS_GAIN_L2*(GainL2/4);       //60/4=15 
            
            adl5240_set_gain_amp1(30);  // L2  
            
        //    SWtestGNSS(SWRFL2);// sw to Rf
//            adl5240_set_gain_amp1(40); 
            
        //    SW1DA16(1);              
        //    delay_ms(2);           
            error_flg_Dt = 0;//****************
        }
////**************L1***********L1*******L1*************************
        if (NAVIG_dt.GPS_L1||NAVIG_dt.GL_L1)
        {
            error_flg_Dt = 0;
            genFlgOnCH2_L1 = 1;
             EN_PWR_CH1_ON;
             delay_us(10);
             
             adl5240_set_gain_amp2(0);  // L1 
//           // _RF0_ON; // ? DA11 // ????? 2    
       //----------------------L1----------------------------------------
            if (NAVIG_dt.GPS_L1)
            {    
                Set_param_AD9959_GPS_L1();
                SetGPSL1 = 1;
            // =========  test=====================
             //   F_DDS1 = F0DDS_GPS_L1; // 120  GPS
             //   Set_param_AD9959_2RCH (F_DDS1);    
            // =====================================                
            }            
            if (NAVIG_dt.GL_L1)
            {   
               Set_param_AD9959_Glonass_L1();
               SetGLL1 = 1; // for plis
             // =========  test=====================               
              //  F_DDS1 = F0DDS_GL_L1; //  120 Glonass
              //  Set_param_AD9959_3RCH (F_DDS1);                 
            // =====================================                 
            }
           // _RF0_ON; //  DA12  
            
//            gain_gnss = GainL1 - NAVIG_dt.GNSS_GAIN_L1*(GainL1/4);
            adl5240_set_gain_amp2(30);  // L1 
            
//             adl5240_set_gain_amp2(40); 
            SW1DA18_RF1_L1(1);
        //     SWtestGNSS(SWRFL1);
//            genFlgOnCH3_L1 = 1;
//            ADC_Res = fun_Read7995(RF_CH3,AD7995_ADDR);     
            error_flg_Dt = 0;//****************
        }    
        
    //    Init_XC_GPSGL();
    //    DDS1Mod_XC_1;
    //    GNSS_ON_XC;
   }
  //  
        len_DT = 1;//              
        error_flg_Dt = error_flg_Dt&0xFF;
        inf_Dt [0] = (uint8_t )error_flg_Dt;  //

        TransmitDtUrt (0x34,inf_Dt,len_DT);        

    return Status;
 //   return 0;    
}
//--------------SetPARAM--------------------------
uint8_t exeCMD40(uint8_t status)
{
    unsigned int len_DT;
    uint8_t inf_Dt[10];
  //----------------------------------------------

  //----------------------------------------------  
    len_DT = 1;//                 

    inf_Dt [0] = (uint8_t )status;  // 

    TransmitDtUrt (0x40,inf_Dt,len_DT);   

 return status;
}
//--------------SavePARAM--------------------------
uint8_t exeCMD42(void)
{  
    unsigned int len_DT;
    uint8_t inf_Dt[10];
    uint8_t Status;
  //----------------------------------------------
     
     Status = SaveAllEEParam();
     error_flg_Dt = 0x00;
     if (Status != MCHP_SUCCESS)
     {
        error_flg_Dt = 0xFF;
     }
 //------------------------------------------------   
    len_DT = 1;//                 

    inf_Dt [0] = Status;  // 

    TransmitDtUrt (0x42,inf_Dt,len_DT);   
    
 return Status;
}
//--------------getPARAM--------------------------
uint8_t exeCMD41(uint8_t *IDparamP, uint8_t DtCnt)
{
uint8_t Status;

  Status = 0;
  uint16_t i,j,Nparam;
  uint16_t len_DT;
  uint8_t IDparam1;
  uint8_t inf_Dt[cntDtEEall];  
  
    for (i=0; i < cntParamEE; i++) //dt
    {
      inf_Dt[i] = 0;            
    }     

    j=0;
    Nparam = 0;
    
    if (DtCnt > (256/cntInfDtEE)) // 28 cntDtEE- maz cnt partams
    {        
            //------------------------------------------------    
       len_DT = 1;//        
       inf_Dt [0] = MCHP_CMD_FAILURE;  // 
       TransmitDtUrt (0x41,inf_Dt,len_DT);             
       return MCHP_CMD_FAILURE;
    }    
    
    while (Nparam < DtCnt)
    {               
      IDparam1 = IDparamP[Nparam];        // i = 0 -> ID   
      if (IDparam1 > cntIDDtEE) // cntDtEE- maz cnt partams
      {        
              //------------------------------------------------    
         len_DT = 1;//        
         inf_Dt [0] = MCHP_CMD_FAILURE;  // 
         TransmitDtUrt (0x41,inf_Dt,len_DT);             
         return MCHP_CMD_FAILURE;
      }

      inf_Dt[j] = IDparam1;
      j++;
      for (i=1; i < cntInfDtEE; i++) //dt
      { 
        inf_Dt[j] = setParamEEprom[IDparam1][i]; // load data params   // KK propuskaem  
        j++;
      }         
      inf_Dt[j] = setParamEEprom[IDparam1][cntInfDtEE+1]; // status         
    //  IDparam1++;
      j++;   
      Nparam++;
    }     
      len_DT = j;//


       TransmitDtUrt (0x41,inf_Dt,len_DT);  
 return Status;
}
// ======================send version============================
uint8_t exeCMD43(void)
{
  uint8_t Status;

  unsigned int len_DT;
  uint8_t inf_Dt[10];  
  
    Status = 0;
    len_DT = 2;//             
    inf_Dt [0] = (uint8_t )(versionPgm & 0xFF);  // 
    inf_Dt [1] = (uint8_t )((versionPgm >> 8) & 0xFF);      

    TransmitDtUrt (0x43,inf_Dt,len_DT);   
    
 return Status;
}

//-----------------------------------------------
uint8_t AnlRxDtUrt(void)
{ unsigned int i,j;
  uint16_t len_DT/*,shagDt*/;
  uint8_t inf_Dt[10];
//  unsigned char /*lit_K_num*/;
  uint8_t GPGL;
  uint32_t freq3,freq2,freq1;
  
//  uint32_t freq;  
  
  uint8_t Status; 
  uint8_t IDparam/*,IDparam1,IDparam2 */; 
  uint8_t IDparamN[cntIDDtEE];
  //unsigned int  sumEE;

  Status = MCHP_SUCCESS;
  if (LITERA_cmd_dt.addr != addrL)// 
  {
        return Status; // 
  }
  //----------------------------------------
   	if (LITERA_cmd_dt.err_CRC) //  CRC
	{ 
        Status = MCHP_CRC_ERROR;
        len_DT = 1;//  
        inf_Dt [0] = Status; // error                         
        TransmitDtUrt (LITERA_cmd_dt.cmd,inf_Dt,len_DT);
        return Status; 
    }  
//----------------exeCMD4----status------------------  
  	if (LITERA_cmd_dt.cmd == 0x04)// 
	{
       Status = exeCMD4();        
	}
//----------------exeCMD4---vykl izluchenia-----------  
    else if (LITERA_cmd_dt.cmd == 0x24) //
    {  
       Status = exeCMD24(data_RX_Urt[3]);          
    }
//------exeCMD28--------------vkl frch----------------      
    else if (LITERA_cmd_dt.cmd == 0x28) 
    {
       LITERA_cmd_dt.cntSrc = data_RX_Urt[3]; // cnt istochnikov
        j = 4;
        
       while (LITERA_cmd_dt.cntSrc > 0 )
       {    
            freq1 = data_RX_Urt[j]; j++;
            freq2 = data_RX_Urt[j]; j++;
            freq3 = data_RX_Urt[j]; j++;    

            LITERA_param .freq = 0;    
            LITERA_param .freq = ((freq3 <<16)|(freq2 <<8)|(freq1)); // kHz
            LITERA_param .mod = data_RX_Urt[j]; 
            j++;      
            
            if (LITERA_param .mod == 4)
            {
                if (data_RX_Urt[j]&0x80)
                {
                   data_RX_Urt[j]&=0x7F; // set in MHz
                   LITERA_param .dev = ((uint32_t) data_RX_Urt[j])*1000;    // result v  kHz      
                }    
                else
                {
                   LITERA_param .dev =  data_RX_Urt[j];        // kHz
                }            
            
            }
            else if (LITERA_param .mod == 5)
            {         
                LITERA_param .dev =  ((uint32_t) data_RX_Urt[j])*1000; //MHz
            }
            else
            {
                LITERA_param .dev = 0;
            }

            j++;               
            if (LITERA_param .mod == 4||LITERA_param .mod == 5||LITERA_param .mod == 2) 
            {
               LITERA_param .manip = data_RX_Urt[j];  // kHz
            }else
            {
               LITERA_param .manip = data_RX_Urt[j];
            }
            j++;           
            LITERA_param .timerad = data_RX_Urt[j]; 
            j++;       
            LITERA_cmd_dt.cntSrc--;
            Status = exeCMD28(); // vyzov comandy
       }
    }
    //------exeCMD30---- test GNSS modulation off
    else if (LITERA_cmd_dt.cmd == 0x30)
    {
         Status = exeCMD30();  
    }
     //------exeCMD31---- test switch--------
    else if (LITERA_cmd_dt.cmd == 0x31)
    { 
         Status = exeCMD31(data_RX_Urt[3],data_RX_Urt[4]);  
    }    
    //------------- exeCMD34---  GPS_GLONASS
    else if (LITERA_cmd_dt.cmd == 0x34) 
    { //

         GPGL = data_RX_Urt[3];

         NAVIG_dt.GNSS_GAIN_L1 = (GPGL>>4)&0x03;
         NAVIG_dt.GNSS_GAIN_L2 = (GPGL>>6)&0x03;        
         if (GPGL&0x01)
         {NAVIG_dt.GPS_L1 = 1; }
         else
         { NAVIG_dt.GPS_L1 = 0;}

          if (GPGL&0x02)
         { NAVIG_dt.GPS_L2 = 1;}
          else
         { NAVIG_dt.GPS_L2 = 0;}   

         if (GPGL&0x04)
         { NAVIG_dt.GL_L1 = 1; }
         else
         { NAVIG_dt.GL_L1 = 0; }   

          if (GPGL&0x08)
         { NAVIG_dt.GL_L2 = 1; }
          else
         {  NAVIG_dt.GL_L2 = 0;}      

         Status = exeCMD34();
    }    
 //------------- exeCMD40--- SetPARAM - izmenenie param usileniya 
    else if (LITERA_cmd_dt.cmd == 0x40) 
    {//cnt_Dt
      len_DT = 0;
      i = 0;
      j = 3;
     // sumEE = 0;//
      
        while (len_DT < LITERA_cmd_dt.cntDt)
        {     
              IDparam = data_RX_Urt[j];

              if (IDparam > cntIDDtEE )
              {
                  Status = exeCMD40(MCHP_CMD_FAILURE);
                  return MCHP_FAILURE;
              }

              setParamEEprom [IDparam][0] = IDparam; 
              for (i = 1; i < cntInfDtEE; i++)
              {
                  setParamEEprom [IDparam][i] = data_RX_Urt[j+i];         
              }
              setParamEEprom [IDparam][cntInfDtEE+1] = 0;// reset status
              j += cntInfDtEE;

              len_DT = len_DT + cntInfDtEE;
        }

        Status = exeCMD40(MCHP_SUCCESS); // 

  }  
  //------------- exeCMD41--- getPARAM -------------
    else if (LITERA_cmd_dt.cmd == 0x41) //  
    {
        i = 0;
        while(i < LITERA_cmd_dt.cntDt)
        {// cntDt <28   255/9

            IDparamN[i] = data_RX_Urt[3+i]; 
            i++;
        }

        Status = exeCMD41(IDparamN,LITERA_cmd_dt.cntDt);      
    }      
    else if (LITERA_cmd_dt.cmd == 0x42) //save params
    {
     // 
        Status = exeCMD42();

    }    
    else if (LITERA_cmd_dt.cmd == 0x43) //
    {
        Status = exeCMD43();
    }    
   else
   {  
        Status = 0xFF; 
        len_DT = 1;//   
        inf_Dt [0] = Status;  //     

        TransmitDtUrt (LITERA_cmd_dt.cmd,inf_Dt,len_DT);      
   }    

 return Status;
}


