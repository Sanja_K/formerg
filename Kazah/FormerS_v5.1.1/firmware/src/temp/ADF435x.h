/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef XC_AD435x_H
#define	XC_AD435x_H

#include <xc.h> // include processor files - each processor file is guarded. 
#include <stdint.h>

  typedef enum {
    RFAP   = 1,     // RFA+
    RFAM   = 2,     // RFA-
    RFBP   = 3,     // RFB+
    RFBM   = 4,     // RFB-            
            
  } CH_SEL_ADF435x;

  typedef enum {
    RFCH4   = 1,     // RFA+
    RFCH3   = 2,     // RFA-
    RFCH2   = 3,     // RFB+
    RFCH1   = 4,     // RFB-            
            
  } CH_SEL_ADF435x_2;

  typedef enum {
    RF_Z15   = 1,     // RFA+
    RF_Z14    = 2,     // RFA-
    RF_Z13    = 3,     // RFB+
    RF_Z12    = 4,     // RFB-            
            
  } CH_SEL_ADF4351;

  typedef enum {
    RF_Z24   = 1,     // RFA+
    RF_Z23    = 2,     // RFA-
    RF_Z22    = 3,     // RFB+
    RF_Z21    = 4,     // RFB-            
            
  } CH_SEL_ADF4355;  
  
    // Два выхода
typedef enum ADF_RFOUT_typedef{
    enum_ADF_RFOUT_A    = 1,
    enum_ADF_RFOUT_B    = 2,
    enum_ADF_RFOUT_both = 3
} enum_ADF_RFOUT;   
         /* 0 - THREE-STATE OUTPUT
         * 1 - DVdd
         * 2 - SDGnd
         * 3 - R DIVIDER OUTPUT
         * 4 - N DIVIDER OUTPUT
         * 5 - ANALOG LOCKDETECT
         * 6 - DIGITAL LOCKDETECT
         * 7 - RESERVED
         */

//  MUX_OUT_typeDef
typedef enum ADF_MUX_OUT_typeDef{
    enum_ADF_MUX_OUT_threeStateOutput  = 0,
    enum_ADF_MUX_OUT_D_VDD             = 1,
    enum_ADF_MUX_OUT_D_GND             = 2,  
    enum_ADF_MUX_OUT_R_dividerOutput   = 3,
    enum_ADF_MUX_OUT_N_dividerOutput   = 4,
    enum_ADF_MUX_OUT_analogLockDetect  = 5,
    enum_ADF_MUX_OUT_digitalLockDetect = 6 // +
} enum_ADF_MUX_OUT_typeDef;
//  lockDetectFunction_typeDef
 typedef enum ADF_lockDetectFunction_typeDef{
    enum_ADF_lockDetectFunction_low = 0,
    enum_ADF_lockDetectFunction_digitalLockDetect = 1,//+
    enum_ADF_lockDetectFunction_analogLockDetect = 2,
    enum_ADF_lockDetectFunction_high = 3
} enum_ADF_lockDetectFunction_typeDef;   
//  outputPower typedef
 typedef enum ADF_outputPower_typeDef{
    enum_ADF_outputPower_m4dBm    = 0, // -4 dBm
    enum_ADF_outputPower_m1dBm    = 1, // -1 dBm
    enum_ADF_outputPower_2dBm     = 2, // +2 dBm
    enum_ADF_outputPower_5dBm     = 3, // +5 dBm
    enum_ADF_outputPower_disabled = 4  // выходной канал отключен
} enum_ADF_outputPower_typeDef;   


//char Init_ADFApp(uint64_t freq_MHz, int ADF4351_LE);
int Init_ADFApp(uint64_t freq_MHz);
void Init_ADF1App (uint64_t freq_MHz);
void Set_freq_ADF1App (uint64_t AD4351_Freq_Hz);
void Set_RFoff_ADF1App (void);
//void Init_ADF2App (uint64_t freq_MHz);
//void Set_freq_ADF2App (uint64_t AD4351_Freq_Hz);
//void Set_RFoff_ADF2App (void);


#endif    