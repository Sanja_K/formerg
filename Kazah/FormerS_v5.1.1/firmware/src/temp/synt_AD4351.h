/******************************************************************************/
/*  Level #define Macros                                                */
/******************************************************************************/
#include <xc.h> 
#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#include <stdlib.h>        /* For true/false definition */
// 

#ifndef XC_ADF4351_H
#define XC_ADF4351_H

   
  struct  ADF4351_initDef {
    //-----
    bool mode_INT_nFRAC ;// = false;
    unsigned long int reference_freq_Hz ;//= 0;
	unsigned long int phaseDet_freq_Hz ;// = 0;
    unsigned long int freq_modulusValue;// CHSPACE;//freq_modulusValue= REFIN/fRES;
    enum_ADF_MUX_OUT_typeDef      enum_ADF_MUX_OUT;// = enum_MUX_OUT_D_GND;    
	enum_ADF_lockDetectFunction_typeDef  enum_ADF_lockDetectFunction;// = enum_lockDetectFunction_digitalLockDetect;    
    enum_ADF_outputPower_typeDef  enum_ADF_outputPower_RFOUT_A;// = enum_outputPower_5dBm;
	enum_ADF_outputPower_typeDef  enum_ADF_outputPower_RFOUT_B;// = enum_outputPower_5dBm;    
   
  };


  
//rf_Synt_ADF4351_initDef  InitData;


#define ADF4351_v_o_DATA_0 LATBbits.LATB14 = 0;
#define ADF4351_v_o_DATA_1 LATBbits.LATB14 = 1; 
  
#define ADF4351_v_o_CLK_0 LATBbits.LATB15 = 0;
#define ADF4351_v_o_CLK_1 LATBbits.LATB15 = 1; 
  
//--------------------------------------------------  

#define ADF4351_LE1  1
//#define ADF4351_LE2  2  
  
#define ADF4351_v_o_LE1_0 LATBbits.LATB11 = 0; 
#define ADF4351_v_o_LE1_1 LATBbits.LATB11 = 1;   
  
// #define ADF4351_v_o_LE2_0 LATBbits.LATB11 = 0;
// #define ADF4351_v_o_LE2_1 LATBbits.LATB11 = 1;  
//----------------------------------------------------  
//#define ADF4351_v_o_RFOUT1_nEN LATDbits.LATD4 = 0;
//#define ADF4351_v_o_RFOUT1_EN  LATDbits.LATD4 = 1;  
//  
// #define ADF4351_v_o_RFOUT2_nEN  LATBbits.LATB10 = 0;
// #define ADF4351_v_o_RFOUT2_EN   LATBbits.LATB10 = 1;

#define ADF4351_v_o_CE_0 LATAbits.LATA2 = 0;//test
#define ADF4351_v_o_CE_1 LATAbits.LATA2 = 1;   
  
#define ADF4351_v_i_LD  PORTGbits.RG2 //lockDetect  
/* #define ADF4351_v_i_LD2 PORTAbits.RA4 //lockDetect  */   
  
//#define ADF4351_v_i_MUX_OUT PORTBbits.RB8// MUX_OUT  


  
//     void  v_o_CLK(  bool x ) = 0;   // Serial Clock Input. The data is latched into the 32-bit shift register on the rising edge of the CLK line.
void  ADF4351_v_o_DATA( uint32_t ena );   // Serial Data Input. The serial data is loaded MSB first. The 3 LSBs identify the register address
  //   void  v_o_LE(   bool x ) = 0;   // Load Enable Input. When LE goes high the data stored in the shift register is loaded into the appropriate latches
  //   void  v_o_CE(   bool x )     {}; // Chip Enable. A logic-low powers the part down and the charge pump becomes high impedance.
     //void  v_o_RFOUT_EN( bool ena ); // RF Output Enable. A logic-low disables the RF outputs

char  ADF4351_check_lockDetect( void ); // Lock Detect Output. Logic-high when locked, and logic-low when unlocked. See register description for more details (Table 9).
char ADF4351_check_MUX_OUT( void ) ;


void  ADF4351_v_writeReg( uint32_t reg );
     

char ADF4351_initialize(struct ADF4351_initDef Init);
char ADF4351_disable( void );
     
 
char  ADF4351_setFreq( uint64_t freq_Hz );


    // 0=A, 1=B
char  ADF4351_setPowerLevel( enum_ADF_RFOUT RFOUT, enum_ADF_outputPower_typeDef lev );
char  ADF4351_PWUP( void );
char  ADF4351_PWDWN( void );
char  ADF4351_setCH(unsigned char CH);
char  ADF4351_update_REG();

struct ADF4351_Reg_typeDef {
	//  Register 0 (Address: 000, Default:  HEX)
	union {
		uint32_t   register0;
		struct {
			unsigned  ADDR_0      :3;  // Register address bits - Устанавливается при загрузке
            unsigned  FRAC        :12; //FRACTIONAL VALUE (FRAC) 0 - 4095
			unsigned  INT         :16; // 0 .... 65535 Integer Division Value. Sets integer part (N-divider) of the feedback divider factor. All integer values from 16 to 65,535 are allowed for integer mode. Integer values from 19 to 4,091 are allowed for fractional mode
			unsigned  RES1        :1;  // RESERVED 0
		} __attribute__((packed))Reg0;
	};
	//  Register 1 (Address: 001, Default:  HEX)
	union {
		uint32_t   register1;
		struct {
			unsigned   ADDR_1      :3;  // Register address bits - Устанавливается при загрузке
			unsigned   MOD         :12; // INTERPOLATOR MODULUS (MOD) 2 - 4095
 			unsigned   PHASE       :12; // PHASE VALUE (PHASE) 0 - 4095; 1-(RECOMMENDED)
 			unsigned   PRESC       :1; //  PRESCALER 4/5 8/9            
			unsigned   RES1        :4; // RESERVED 0000
		} __attribute__((packed))Reg1;
	};
	// Register 2 (Address: 010, Default:  HEX)
	union {
		uint32_t   register2;
		struct {
			unsigned   ADDR_2     :3;  // Register address bits - Устанавливается при загрузке
			unsigned   CNTRES     :1;  // COUNTER RESET 0 = DISABLED; 1 = ENABLED;
            unsigned   CPTS       :1;//CP THREE-STATE 0 = DISABLED; 1 = ENABLED;
            unsigned   PWRDN      :1;//POWER DOWN 0 = DISABLED; 1 = ENABLED;
            unsigned   PD         :1;//PD POLARITY 0 = NEGATIVE; 1 = POSITIVE;            
            unsigned   LDP         :1;//LDP 0 = 10ns; 1 = 6ns;    
            unsigned   LDF         :1;//LDF 0 = FRAC-N; 1 = INT-N;  
            unsigned  CPCR        :4;// CHARGE PUMP CURRENT  0 - 15 (0.31- 5mA)
            unsigned  DBF         :1; //DOUBLE BUFF 0 = DISABLED; 1 = ENABLED;
            unsigned  REFCNT        :10; //R DIVIDER (R) 1-1023
            unsigned  REFDIV      :1; //REFERENCE DIVIDE BY 2     0 = DISABLED; 1 = ENABLED;      
            unsigned  REFDB       :1; //REFERENCE DOUBLER      0 = DISABLED; 1 = ENABLED;            
            unsigned   MUXOUT     :3;  // OUTPUT
             /* 0 - THREE-STATE OUTPUT
             * 1 - DVdd
             * 2 - DGnd
             * 3 - R DIVIDER OUTPUT
             * 4 - N DIVIDER OUTPUT
             * 5 - ANALOG LOCKDETECT
             * 6 - DIGITAL LOCKDETECT
             * 7 - RESERVED
             */
            unsigned   LNMOD        :2;  // LOW NOISE MODE      0 - LOWNOISEMODE 3-LOWSPURMODE
            unsigned   RES1        :1; // RESERVED 0000
		} __attribute__((packed))Reg2;
	};
	// Register 3 (Address: 011, Default:  HEX)
	union {
		uint32_t   register3;
		struct {
			unsigned   ADDR_3       :3;  // Register address bits - Устанавливается при загрузке
			unsigned   CLKD         :12; // CLOCK DIVIDER VALUE 0 - 4095
			unsigned   CLKDM        :2; // CLOCK DIVIDER MODE  0-CLOCK DIVIDER OFF; 1-FAST-LOCK ENABLE; 2-RESYNC ENABLE; 
            unsigned   RES1         :1; // RESERVED
            unsigned   CSR          :1; // CYCLE SLIP REDUCTION  0 = DISABLED; 1 = ENABLED; 
            unsigned   RES2         :13; // RESERVED           
		} __attribute__((packed))Reg3;
	};
	// Table 8. Register 4 (Address: 100, Default:  HEX)
	union {
		uint32_t   register4;
		struct {
			unsigned   ADDR_4    :3;  // Register address bits - Устанавливается при загрузке
			unsigned   RFAPWR    :2;  // OUTPUT POWER 0 = –4dBm; 1 = –1dBm; 2 = +2dBm; 3 = +5dBm
            unsigned   RFAEN     :1;  // RF 0 = DISABLED; 1 = ENABLED
            unsigned   RFBPWR :2;  // AUXRF OUTPUT POWER 0 = –4dBm; 1 = –1dBm; 2 = +2dBm; 3 = +5dBm
            unsigned   RFBEN    :1;  // AUXILIARY OUT 0 = DISABLED; 1 = ENABLED
            unsigned   AOS    :1;  // AUX OUTPUT SELECT 0 = DISABLED; 1 = ENABLED  
            unsigned   MTLD    :1;  // MUTE TILL LOCK DETECT  0 = DISABLED; 1 = ENABLED         
            unsigned   VCOPWR    :1;  // VCO POWER-DOWN   0 = POWERED UP; 1 = POWERED DOWN
            
            unsigned   BNDCLKDIV    :8;  //BAND SELECT CLOCK DIVIDER (R) 1 - 255
            unsigned   RFDIV    :3;//RF DIVIDER SELECT RF DIVIDER SELECT   ÷1; ÷2; ÷4; ÷8; ÷16; ÷32; ÷64
            unsigned   FBSEL    :1;             //FEEDBACK SELECT 0 = DIVIDED; 1 = FUNDAMENTAL
            unsigned   RES1         :8; // RESERVED 
		} __attribute__((packed))Reg4;
	};
	//  Register 5 (Address: 101, Default:  HEX)
 	union {
		uint32_t   register5;
		struct {
			unsigned   ADDR_5   :3;  // Register address bits
            unsigned   RES1     :16;  // RESERVED   
             unsigned   RES2    :2;  // RESERVED  11  
             unsigned   RES3    :1;  // RESERVED  0               
             unsigned   LDPM    :2;  //LOCK DETECT PIN OPERATION 0- LOW; 1-DIGITAL LOCK DETECT; 2-LOW; 3-HIGH
             unsigned   RES4    :8;  // RESERVED               
		} __attribute__((packed))Reg5;
	};   
};

#endif    