/*
 * File:   timers.c
 * Author: Alexandr
 *
 * Created on September 20, 2017, 4:53 PM
 */


#include "xc.h"
#include "sys_cfg.h"
#include "user.h"
#include "executeCMD.h"
#include "channel.h"

char swtest;

//--------QPC6044--------------------
#define SW2RF1_V1 LATDbits.LATD3
#define SW2RF1_V2 LATDbits.LATD12
//--------------------------------------
void SW1DA2(char sw)//1
{
    if (sw==1)
    {
        SW1RF1_V1 = 1;  //   RFC => RF1 
        SW1RF1_V2 = 1; //   
    }
    else     
    if (sw==2)//
    {
        SW1RF1_V1 = 0;  //   RFC => RF2 
        SW1RF1_V2 = 0; //    
    }
    else  if (sw==3)//
    {
        SW1RF1_V1 = 1;  //   RFC => RF3 
        SW1RF1_V2 = 0; //    
    }
    else  if (sw==4)//
    {
        SW1RF1_V1 = 0;  //   RFC => RF4 
        SW1RF1_V2 = 1; //   
    } 
}

//--------QPC6044--------------------
#define SW8RF1_V1 LATAbits.LATA9
#define SW8RF1_V2 LATAbits.LATA10
//--------------------------------------
void SW2DA7(char sw)//2
{
    if (sw==1)
    {
        SW2RF1_V1 = 1;  //   RFC => RF1 
        SW2RF1_V2 = 1; //   
    }
    else     
    if (sw==2)//
    {
        SW2RF1_V1 = 0;  //   RFC => RF2 
        SW2RF1_V2 = 0; //    
    }
    else  if (sw==3)//
    {
        SW2RF1_V1 = 1;  //   RFC => RF3 
        SW2RF1_V2 = 0; //    
    }
    else  if (sw==4)//
    {
        SW2RF1_V1 = 0;  //   RFC => RF4 
        SW2RF1_V2 = 1; //   
    } 
}

//--------QPC6044--------------------
#define SW3RF1_V1 LATBbits.LATB8
#define SW3RF1_V2 LATBbits.LATB9
//--------------------------------------
void SW3DA4(char sw)//3
{
    if (sw==1)
    {
        SW3RF1_V1 = 1;  //   RFC => RF1 
        SW3RF1_V2 = 1; //   
    }
    else     
    if (sw==2)//
    {
        SW3RF1_V1 = 0;  //   RFC => RF2 
        SW3RF1_V2 = 0; //    
    }
    else  if (sw==3)//
    {
        SW3RF1_V1 = 1;  //   RFC => RF3 
        SW3RF1_V2 = 0; //    
    }
    else  if (sw==4)//
    {
        SW3RF1_V1 = 0;  //   RFC => RF4 
        SW3RF1_V2 = 1; //   
    } 
}

//--------QPC6044--------------------
#define SW4RF1_V1 LATFbits.LATF12
#define SW4RF1_V2 LATFbits.LATF13
//--------------------------------------
void SW4DA25(char sw)//4
{
    if (sw==1)
    {
        SW2RF1_V1 = 1;  //   RFC => RF1 
        SW2RF1_V2 = 1; //   
    }
    else     
    if (sw==2)//
    {
        SW2RF1_V1 = 0;  //   RFC => RF2 
        SW2RF1_V2 = 0; //    
    }
    else  if (sw==3)//
    {
        SW2RF1_V1 = 1;  //   RFC => RF3 
        SW2RF1_V2 = 0; //    
    }
    else  if (sw==4)//
    {
        SW2RF1_V1 = 0;  //   RFC => RF4 
        SW2RF1_V2 = 1; //   
    } 
}

