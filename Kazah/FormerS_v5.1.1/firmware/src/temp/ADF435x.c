//#include "system.h"
#include "ADF435x.h"
//#include "synt_AD4350.h"
//#include "synt_AD4351.h"
#include "synt_AD4351.h"

//#include "modulator.h"
#include "common.h"


void Init_ADF1App (uint64_t freq_MHz)
{
    Init_ADFApp(freq_MHz); 
    //ADF4351_v_o_RFOUT1_EN;            
}

void Set_freq_ADF1App (uint64_t AD4351_Freq_Hz)
{
    ADF4351_setFreq(AD4351_Freq_Hz);   
    ADF4351_update_REG(); 
    //ADF4351_v_o_RFOUT1_EN;                 
}
void Set_RFoff_ADF1App (void)
{

    ADF4351_PWDWN (); 
//    ADF4351_v_o_RFOUT1_nEN;    
    ADF4351_update_REG();     
  
}

int Init_ADFApp(uint64_t freq_MHz)
{
 //------4350----- uint64_t  AD4350_Freq_Hz = 150*MHz;// 137.5 MHz to 4400 MHz
  uint64_t  AD4351_Freq_Hz = freq_MHz;//40*MHz;// 35 MHz to 4400 MHz  
//  uint64_t  AD4351_Freq_Hz = 160*MHz;//160*MHz; // 54 MHz to 6800 MHz
    
    unsigned long int CHSPACE = 200000; // 200 ���

    int error_flg = 0;
    /* TODO Initialize User Ports/Peripherals/Project here */
    /* Setup analog functionality and port direction */
    delay_ms (10);
    /* Initialize peripherals */

   //--------------------------------------------------------
    struct ADF4351_initDef ADF4351_1_Init = {false, 0,0,4095,enum_ADF_MUX_OUT_digitalLockDetect,enum_ADF_lockDetectFunction_digitalLockDetect,enum_ADF_outputPower_2dBm,enum_ADF_outputPower_disabled };
   
    ADF4351_1_Init.reference_freq_Hz = 25*MHz;
    ADF4351_1_Init.phaseDet_freq_Hz  =  5*MHz;
    
   ADF4351_1_Init.freq_modulusValue = ADF4351_1_Init.phaseDet_freq_Hz/CHSPACE; 
  //  ADF4351_1_Init.enum_ADF_outputPower_RFOUT_A =  enum_ADF_outputPower_m1dBm;
  //  ADF4351_1_Init.enum_ADF_outputPower_RFOUT_B =  enum_ADF_outputPower_m1dBm;
   ADF4351_1_Init.enum_ADF_outputPower_RFOUT_A =  enum_ADF_outputPower_2dBm;
   ADF4351_1_Init.enum_ADF_outputPower_RFOUT_B =  enum_ADF_outputPower_disabled;  
   ADF4351_1_Init.enum_ADF_MUX_OUT =  enum_ADF_MUX_OUT_R_dividerOutput;//enum_ADF_MUX_OUT_R_dividerOutput;// enum_ADF_MUX_OUT_digitalLockDetect; 

           
    error_flg = ADF4351_initialize(ADF4351_1_Init);
 
    error_flg = ADF4351_setFreq(AD4351_Freq_Hz);
    ADF4351_PWUP ();
    delay_ms(1);
    //  ADF4351_PWDWN ();
    ADF4351_update_REG();   
  
   delay_ms(10);      
   
//-----------------------------------------------------------

   return error_flg;
}


