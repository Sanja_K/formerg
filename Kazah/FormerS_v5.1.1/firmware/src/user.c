    /*
 * File:   user.c
 * Author: Alexandr Kazeka
 *
 * Created on 12 11, 2020, 4:20 PM
 */

#include <xc.h>         /* XC8 General Include File */

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#include "user.h"
#include "system_cfg.h"
#include "definitions.h" 
#include "executeCMD.h"
#include "I2C.h"
#include "eeprom.h"
#include "ad7995.h"
#include "ReceiveCMD.h"
#include "eepromParam.h"

static bool volatile bToggleLED = false; 
           

APP_DATA_def app_task;

#define CntDtEESend   150     
 
uint8_t InitApp(void)
{ //  uint8_t eeprom_data;
//    int i;
//    uint8_t bufferTx[256];
//    uint8_t bufferRx[256];   
	AddrInit();
    I2CInit();
    app_task.state = app_task_idle;   
    cntrTMR2 = 0;
    //Init_XC(); 
    //eeprom_init();  
	//off_all_pwr();
   //

//    for (i=0;i<CntDtEESend;i++)
//    {
//        bufferTx[i]= i;
//    }
    
//    eeprom_write_array(0x0000,bufferTx, CntDtEESend);
//    eeprom_read_array(0x0000, bufferRx, CntDtEESend);
    
    loadAllEEParam();
    InitReceiveDtCMD();    
    

    return 1;
}

//void Send_UART_485P(unsigned char *TX, unsigned int len) // 
//{
//    unsigned int i;
//
//    AD485Tx_ON;
//    for(i=0;i<len;i++)
//    {
//        U2TXREG = TX[i];
//        while (!U2STAbits.TRMT == 1);
//    }
//    //PORTCbits.RC13 = 0;
//    PORTFbits.RF12 = 0;
////    asm("Exit_TransmitP:");                
//
//}



void UserTaskApp (void)
{ uint8_t status;
//  uint16_t eedata;
  
    switch (app_task.state) {
		case app_task_idle:   //
            wdt_clr();

            
            if (cntrTMR2 < 200)
            {
                LED1_ON;
            }
            else 
            {
                LED1_OFF;
            }
            
//        eedata = fun_Read7995(RF_CH1, AD7995_ADDR);
//        
//        if (eedata == myTxData[2])
//        {
//           LED1_ON;
//             
//        }        
            
            
        break; 
		case app_task_error:   //
            
            LED1_ON;
            delay_ms(300);
            LED1_OFF;
            delay_ms(200);  
            wdt_clr();
            delay_ms(200);              
            app_task.state = app_task_idle;
        break;         
        
		case app_task_exeCMD:   //
            wdt_clr();
            status = AnlRxDtUrt();
            if (status != MCHP_SUCCESS)
            {
                app_task.state = app_task_error;  
            }
            else
            {
                app_task.state = app_task_idle;           
            }
        break;            

        
            app_task.state = app_task_idle;
        default:
        break;        
 
    }
}

    