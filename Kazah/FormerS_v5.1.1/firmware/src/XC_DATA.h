/* 
 * File:   XC_DATA.h
 * Author: Alexandr
 *
 * Created on 25 октября 2017 г., 10:07
 */

#ifndef XC_DATA_H
#define	XC_DATA_H

#ifdef	__cplusplus
extern "C" {
#endif

#define XC_v_o_DATA_0 LATCbits.LATC1 = 0;//+
#define XC_v_o_DATA_1 LATCbits.LATC1 = 1; 
  
#define XC_v_o_CLK_0 LATCbits.LATC2 = 0;//+
#define XC_v_o_CLK_1 LATCbits.LATC2 = 1; 
  
#define XC1_v_o_LE_0 (LATCbits.LATC3 = 0)//XC1+
#define XC1_v_o_LE_1 (LATCbits.LATC3 = 1)

#define XC2_v_o_LE_0 (LATCbits.LATC4 = 0)//XC2+
#define XC2_v_o_LE_1 (LATCbits.LATC4 = 1)
    
#define GNSS_ON_XC LATGbits.LATG12 = 1;// on/ off+
#define GNSS_OFF_XC LATGbits.LATG12 = 0;     
    //---------------------------------------------------------------------
 //------modulation-----------------   
#define DDS1Mod_XC_0 LATGbits.LATG13 = 0;// mod off+
#define DDS1Mod_XC_1 LATGbits.LATG13 = 1; // mod on    +
    
//------------------------------------------------------------------------

#define data_reg_XC_status (PORTE&0x0FF);
    
#define XC_strob_status_1 LATAbits.LATA7 = 1;
#define XC_strob_status_0 LATAbits.LATA7 = 0;
    
#define XC_RW_status_1 LATAbits.LATA6 = 1;
#define XC_RW_status_0 LATAbits.LATA6 = 0;  
    
#define XC_ADR_status_1 LATGbits.LATG14 = 1;
#define XC_ADR_status_0 LATGbits.LATG14 = 0;    

extern uint8_t SetGPSL1, SetGLL1, SetGPSL2, SetGLL2;
//-------------------------------------------------------------------------    
/*  
#define DDS1P3_XC_0 LATEbits.LATE4 = 0;
#define DDS1P3_XC_1 LATEbits.LATE4 = 1; 
*/  

/*
#define DDS1P4_XC_0 LATEbits.LATE3 = 0;
#define DDS1P4_XC_1 LATEbits.LATE3 = 1; 


#define DDS2P5_XC_0 LATEbits.LATE2 = 0;
#define DDS2P5_XC_1 LATEbits.LATE2 = 1;
*/    

void Init_XC(void);    
 uint8_t Init_XC_freq (unsigned char DDSx, uint64_t F_man);   
 uint8_t Init_XC_ph (unsigned char DDSx, uint64_t F_man);
 void Init_XC_GPSGL (void);
 uint8_t  XC_LD_DATA( void);
    
#ifdef	__cplusplus
}
#endif

#endif	/* XC_DATA_H */

