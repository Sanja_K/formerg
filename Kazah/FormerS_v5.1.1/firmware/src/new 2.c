byte EE241_ReadByte(EE241_Address addr, byte *data)
{
  uint8_t res;
#if EE241_DEVICE_ID!=EE241_DEVICE_ID_8
  uint8_t addr16[2];                    /* big endian address on I2C bus needs to be 16bit */
 
  addr16[0] = (uint8_t)(addr>>8); /* 16 bit address must be in big endian format */
  addr16[1] = (uint8_t)(addr&0xff);
#endif
  res = GI2C1_SelectSlave(EE241_DEVICE_ADDR(addr));
  if (res != ERR_OK) {
    (void)GI2C1_UnselectSlave();
    return res;
  }
#if EE241_DEVICE_ID==EE241_DEVICE_ID_8
  res = GI2C1_WriteBlock(&addr, 1, GI2C1_DO_NOT_SEND_STOP); /* send 8bit address */
#else /* use 16bit address */
  res = GI2C1_WriteBlock(&addr16, 2, GI2C1_DO_NOT_SEND_STOP); /* send 16bit address */
#endif
  if (res != ERR_OK) {
    (void)GI2C1_UnselectSlave();
    return res;
  }
  res = GI2C1_ReadBlock(data, 1, GI2C1_SEND_STOP); /* read data byte from bus */
  if (res != ERR_OK) {
    (void)GI2C1_UnselectSlave();
    return res;
  }
  return GI2C1_UnselectSlave();
}