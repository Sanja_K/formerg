/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef XC_channel_H
#define	XC_channel_H

#include <xc.h> // include processor files - each processor file is guarded.  
       
//SKY13588-460LF

#define SWRF1    1
#define SWDA5XW3 1
#define SWRF2    2
#define SWRF3    3
#define SWDA3XW2 3
#define SWRF4    4
void SW1DA18_RF1_L1(char sw);
void SW1DA25_RF2_L1(char sw);
void SW1DA29_RF3_L1(char sw);
void SW1DA2_RF1_L2(char sw);
void SW2DA7_RF1_L2(char sw);
void SW1DA4_RF2_L2(char sw);
void SW2DA25_RF2_L2(char sw);

#endif    //XC_channel_H