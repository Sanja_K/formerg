#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default_PIC32.mk)" "nbproject/Makefile-local-default_PIC32.mk"
include nbproject/Makefile-local-default_PIC32.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default_PIC32
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/FormerS.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/FormerS.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=../src/config/default_PIC32/peripheral/clk/plib_clk.c ../src/config/default_PIC32/peripheral/evic/plib_evic.c ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c ../src/config/default_PIC32/peripheral/tmr/plib_tmr4.c ../src/config/default_PIC32/peripheral/tmr/plib_tmr2.c ../src/config/default_PIC32/peripheral/uart/plib_uart2.c ../src/config/default_PIC32/stdio/xc32_monitor.c ../src/config/default_PIC32/initialization.c ../src/config/default_PIC32/interrupts.c ../src/config/default_PIC32/exceptions.c ../src/main.c ../src/user.c ../src/system_cfg.c ../src/ADF435x.c ../src/DDS_AD9959.c ../src/LMX2572.c ../src/ad9914.c ../src/channel.c ../src/executeCMD.c ../src/former_signal.c ../src/softwareSPI.c ../src/synt_AD4351.c ../src/synt_AD9959.c ../src/ad7995.c ../src/XC_DATA.c ../src/eepromParam.c ../src/hmc1122.c ../src/adl5240.c ../src/I2C.c ../src/eeprom.c ../src/ReceiveCMD.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1471492474/plib_clk.o ${OBJECTDIR}/_ext/1628442791/plib_evic.o ${OBJECTDIR}/_ext/1628496619/plib_gpio.o ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o ${OBJECTDIR}/_ext/1628899562/plib_uart2.o ${OBJECTDIR}/_ext/279352370/xc32_monitor.o ${OBJECTDIR}/_ext/1177533048/initialization.o ${OBJECTDIR}/_ext/1177533048/interrupts.o ${OBJECTDIR}/_ext/1177533048/exceptions.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1360937237/user.o ${OBJECTDIR}/_ext/1360937237/system_cfg.o ${OBJECTDIR}/_ext/1360937237/ADF435x.o ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o ${OBJECTDIR}/_ext/1360937237/LMX2572.o ${OBJECTDIR}/_ext/1360937237/ad9914.o ${OBJECTDIR}/_ext/1360937237/channel.o ${OBJECTDIR}/_ext/1360937237/executeCMD.o ${OBJECTDIR}/_ext/1360937237/former_signal.o ${OBJECTDIR}/_ext/1360937237/softwareSPI.o ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o ${OBJECTDIR}/_ext/1360937237/ad7995.o ${OBJECTDIR}/_ext/1360937237/XC_DATA.o ${OBJECTDIR}/_ext/1360937237/eepromParam.o ${OBJECTDIR}/_ext/1360937237/hmc1122.o ${OBJECTDIR}/_ext/1360937237/adl5240.o ${OBJECTDIR}/_ext/1360937237/I2C.o ${OBJECTDIR}/_ext/1360937237/eeprom.o ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1471492474/plib_clk.o.d ${OBJECTDIR}/_ext/1628442791/plib_evic.o.d ${OBJECTDIR}/_ext/1628496619/plib_gpio.o.d ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o.d ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o.d ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o.d ${OBJECTDIR}/_ext/1628899562/plib_uart2.o.d ${OBJECTDIR}/_ext/279352370/xc32_monitor.o.d ${OBJECTDIR}/_ext/1177533048/initialization.o.d ${OBJECTDIR}/_ext/1177533048/interrupts.o.d ${OBJECTDIR}/_ext/1177533048/exceptions.o.d ${OBJECTDIR}/_ext/1360937237/main.o.d ${OBJECTDIR}/_ext/1360937237/user.o.d ${OBJECTDIR}/_ext/1360937237/system_cfg.o.d ${OBJECTDIR}/_ext/1360937237/ADF435x.o.d ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o.d ${OBJECTDIR}/_ext/1360937237/LMX2572.o.d ${OBJECTDIR}/_ext/1360937237/ad9914.o.d ${OBJECTDIR}/_ext/1360937237/channel.o.d ${OBJECTDIR}/_ext/1360937237/executeCMD.o.d ${OBJECTDIR}/_ext/1360937237/former_signal.o.d ${OBJECTDIR}/_ext/1360937237/softwareSPI.o.d ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o.d ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o.d ${OBJECTDIR}/_ext/1360937237/ad7995.o.d ${OBJECTDIR}/_ext/1360937237/XC_DATA.o.d ${OBJECTDIR}/_ext/1360937237/eepromParam.o.d ${OBJECTDIR}/_ext/1360937237/hmc1122.o.d ${OBJECTDIR}/_ext/1360937237/adl5240.o.d ${OBJECTDIR}/_ext/1360937237/I2C.o.d ${OBJECTDIR}/_ext/1360937237/eeprom.o.d ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1471492474/plib_clk.o ${OBJECTDIR}/_ext/1628442791/plib_evic.o ${OBJECTDIR}/_ext/1628496619/plib_gpio.o ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o ${OBJECTDIR}/_ext/1628899562/plib_uart2.o ${OBJECTDIR}/_ext/279352370/xc32_monitor.o ${OBJECTDIR}/_ext/1177533048/initialization.o ${OBJECTDIR}/_ext/1177533048/interrupts.o ${OBJECTDIR}/_ext/1177533048/exceptions.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1360937237/user.o ${OBJECTDIR}/_ext/1360937237/system_cfg.o ${OBJECTDIR}/_ext/1360937237/ADF435x.o ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o ${OBJECTDIR}/_ext/1360937237/LMX2572.o ${OBJECTDIR}/_ext/1360937237/ad9914.o ${OBJECTDIR}/_ext/1360937237/channel.o ${OBJECTDIR}/_ext/1360937237/executeCMD.o ${OBJECTDIR}/_ext/1360937237/former_signal.o ${OBJECTDIR}/_ext/1360937237/softwareSPI.o ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o ${OBJECTDIR}/_ext/1360937237/ad7995.o ${OBJECTDIR}/_ext/1360937237/XC_DATA.o ${OBJECTDIR}/_ext/1360937237/eepromParam.o ${OBJECTDIR}/_ext/1360937237/hmc1122.o ${OBJECTDIR}/_ext/1360937237/adl5240.o ${OBJECTDIR}/_ext/1360937237/I2C.o ${OBJECTDIR}/_ext/1360937237/eeprom.o ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o

# Source Files
SOURCEFILES=../src/config/default_PIC32/peripheral/clk/plib_clk.c ../src/config/default_PIC32/peripheral/evic/plib_evic.c ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c ../src/config/default_PIC32/peripheral/tmr/plib_tmr4.c ../src/config/default_PIC32/peripheral/tmr/plib_tmr2.c ../src/config/default_PIC32/peripheral/uart/plib_uart2.c ../src/config/default_PIC32/stdio/xc32_monitor.c ../src/config/default_PIC32/initialization.c ../src/config/default_PIC32/interrupts.c ../src/config/default_PIC32/exceptions.c ../src/main.c ../src/user.c ../src/system_cfg.c ../src/ADF435x.c ../src/DDS_AD9959.c ../src/LMX2572.c ../src/ad9914.c ../src/channel.c ../src/executeCMD.c ../src/former_signal.c ../src/softwareSPI.c ../src/synt_AD4351.c ../src/synt_AD9959.c ../src/ad7995.c ../src/XC_DATA.c ../src/eepromParam.c ../src/hmc1122.c ../src/adl5240.c ../src/I2C.c ../src/eeprom.c ../src/ReceiveCMD.c



CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default_PIC32.mk dist/${CND_CONF}/${IMAGE_TYPE}/FormerS.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX795F512L
MP_LINKER_FILE_OPTION=,--script="..\src\config\default_PIC32\p32MX795F512L.ld"
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1471492474/plib_clk.o: ../src/config/default_PIC32/peripheral/clk/plib_clk.c  .generated_files/46ad1d124e4405a644dd899c8ea97a9e4ad668cb.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1471492474" 
	@${RM} ${OBJECTDIR}/_ext/1471492474/plib_clk.o.d 
	@${RM} ${OBJECTDIR}/_ext/1471492474/plib_clk.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1471492474/plib_clk.o.d" -o ${OBJECTDIR}/_ext/1471492474/plib_clk.o ../src/config/default_PIC32/peripheral/clk/plib_clk.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1628442791/plib_evic.o: ../src/config/default_PIC32/peripheral/evic/plib_evic.c  .generated_files/20af2d23b514d769dea6108ece398eb29036bae6.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1628442791" 
	@${RM} ${OBJECTDIR}/_ext/1628442791/plib_evic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1628442791/plib_evic.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1628442791/plib_evic.o.d" -o ${OBJECTDIR}/_ext/1628442791/plib_evic.o ../src/config/default_PIC32/peripheral/evic/plib_evic.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1628496619/plib_gpio.o: ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c  .generated_files/4bfcaa9a5e6ce8dd34a6d5411ff14751cced0b22.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1628496619" 
	@${RM} ${OBJECTDIR}/_ext/1628496619/plib_gpio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1628496619/plib_gpio.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1628496619/plib_gpio.o.d" -o ${OBJECTDIR}/_ext/1628496619/plib_gpio.o ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o: ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c  .generated_files/62b2b2a242cd83504e349a9f8a3cfb4b93c3a709.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/997135251" 
	@${RM} ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o.d 
	@${RM} ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o.d" -o ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1471476099/plib_tmr4.o: ../src/config/default_PIC32/peripheral/tmr/plib_tmr4.c  .generated_files/e96b362b2168196d959c5cb7b04d7bced28182e0.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1471476099" 
	@${RM} ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o.d 
	@${RM} ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1471476099/plib_tmr4.o.d" -o ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o ../src/config/default_PIC32/peripheral/tmr/plib_tmr4.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1471476099/plib_tmr2.o: ../src/config/default_PIC32/peripheral/tmr/plib_tmr2.c  .generated_files/1f9a217098e73e38f8744971ba58021c4ad38877.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1471476099" 
	@${RM} ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o.d 
	@${RM} ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1471476099/plib_tmr2.o.d" -o ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o ../src/config/default_PIC32/peripheral/tmr/plib_tmr2.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1628899562/plib_uart2.o: ../src/config/default_PIC32/peripheral/uart/plib_uart2.c  .generated_files/a8107617a53c09df94bf871951ee8c7695dcd6ec.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1628899562" 
	@${RM} ${OBJECTDIR}/_ext/1628899562/plib_uart2.o.d 
	@${RM} ${OBJECTDIR}/_ext/1628899562/plib_uart2.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1628899562/plib_uart2.o.d" -o ${OBJECTDIR}/_ext/1628899562/plib_uart2.o ../src/config/default_PIC32/peripheral/uart/plib_uart2.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/279352370/xc32_monitor.o: ../src/config/default_PIC32/stdio/xc32_monitor.c  .generated_files/48a20d9dbf732184925a9a1f5806ea5114adc6c8.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/279352370" 
	@${RM} ${OBJECTDIR}/_ext/279352370/xc32_monitor.o.d 
	@${RM} ${OBJECTDIR}/_ext/279352370/xc32_monitor.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/279352370/xc32_monitor.o.d" -o ${OBJECTDIR}/_ext/279352370/xc32_monitor.o ../src/config/default_PIC32/stdio/xc32_monitor.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1177533048/initialization.o: ../src/config/default_PIC32/initialization.c  .generated_files/60bc15e3def3dacd3dd65fe387c62115ff699896.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/initialization.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/initialization.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1177533048/initialization.o.d" -o ${OBJECTDIR}/_ext/1177533048/initialization.o ../src/config/default_PIC32/initialization.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1177533048/interrupts.o: ../src/config/default_PIC32/interrupts.c  .generated_files/477032b9212bd74b30aa5e49bbf11c7e195ba532.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/interrupts.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1177533048/interrupts.o.d" -o ${OBJECTDIR}/_ext/1177533048/interrupts.o ../src/config/default_PIC32/interrupts.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1177533048/exceptions.o: ../src/config/default_PIC32/exceptions.c  .generated_files/324b5f50f3baac82ca90b18abc863b2e09f7bf6d.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1177533048/exceptions.o.d" -o ${OBJECTDIR}/_ext/1177533048/exceptions.o ../src/config/default_PIC32/exceptions.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  .generated_files/af8e099f50dced35130d96700df5c9e6905377a0.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/user.o: ../src/user.c  .generated_files/d5a8893291f4fb9ffab5f27ec2e18d77ef7ca700.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/user.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/user.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/user.o.d" -o ${OBJECTDIR}/_ext/1360937237/user.o ../src/user.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/system_cfg.o: ../src/system_cfg.c  .generated_files/344d4fdf018419cc8bfb7f885806efd33877b932.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/system_cfg.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/system_cfg.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/system_cfg.o.d" -o ${OBJECTDIR}/_ext/1360937237/system_cfg.o ../src/system_cfg.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/ADF435x.o: ../src/ADF435x.c  .generated_files/86d27bc939adbb79783dd19a4e7ad10700838c98.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ADF435x.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ADF435x.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ADF435x.o.d" -o ${OBJECTDIR}/_ext/1360937237/ADF435x.o ../src/ADF435x.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o: ../src/DDS_AD9959.c  .generated_files/3075ae0cc34ffdf35e2b05f2f373f3969631292d.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o.d" -o ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o ../src/DDS_AD9959.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/LMX2572.o: ../src/LMX2572.c  .generated_files/382d0ef62971d865fdd3220847157ccac07ea77f.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/LMX2572.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/LMX2572.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/LMX2572.o.d" -o ${OBJECTDIR}/_ext/1360937237/LMX2572.o ../src/LMX2572.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/ad9914.o: ../src/ad9914.c  .generated_files/9cb84c1bc186d39d5c49131831c66e303e7d44b7.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad9914.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad9914.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ad9914.o.d" -o ${OBJECTDIR}/_ext/1360937237/ad9914.o ../src/ad9914.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/channel.o: ../src/channel.c  .generated_files/2ba20bc0e4379b4b7c56e5cd6e41e33f1d909642.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/channel.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/channel.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/channel.o.d" -o ${OBJECTDIR}/_ext/1360937237/channel.o ../src/channel.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/executeCMD.o: ../src/executeCMD.c  .generated_files/35e9a3e061e24ce793f96361c8272d1d478daa16.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/executeCMD.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/executeCMD.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/executeCMD.o.d" -o ${OBJECTDIR}/_ext/1360937237/executeCMD.o ../src/executeCMD.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/former_signal.o: ../src/former_signal.c  .generated_files/8d8fb743e5ee934fa9e730523b49f2c71b5366a3.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/former_signal.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/former_signal.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/former_signal.o.d" -o ${OBJECTDIR}/_ext/1360937237/former_signal.o ../src/former_signal.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/softwareSPI.o: ../src/softwareSPI.c  .generated_files/c09d1ddf4a9bb577294b01f582bc013589529c3a.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/softwareSPI.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/softwareSPI.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/softwareSPI.o.d" -o ${OBJECTDIR}/_ext/1360937237/softwareSPI.o ../src/softwareSPI.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/synt_AD4351.o: ../src/synt_AD4351.c  .generated_files/3f3830bec9642804834f506bbbaa9809bfea0216.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/synt_AD4351.o.d" -o ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o ../src/synt_AD4351.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/synt_AD9959.o: ../src/synt_AD9959.c  .generated_files/96bf93814dd2f218c4a64027370e0f6d1741c6f1.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/synt_AD9959.o.d" -o ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o ../src/synt_AD9959.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/ad7995.o: ../src/ad7995.c  .generated_files/7a6079aba5dc4ecbd6eb3df91c97902e7ef6bc73.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad7995.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad7995.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ad7995.o.d" -o ${OBJECTDIR}/_ext/1360937237/ad7995.o ../src/ad7995.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/XC_DATA.o: ../src/XC_DATA.c  .generated_files/bb12f7f3809ece2fbe5fce833425d02b921879bb.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/XC_DATA.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/XC_DATA.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/XC_DATA.o.d" -o ${OBJECTDIR}/_ext/1360937237/XC_DATA.o ../src/XC_DATA.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/eepromParam.o: ../src/eepromParam.c  .generated_files/f0b944a461553b31304e3b881f04410585ee2a12.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eepromParam.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eepromParam.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/eepromParam.o.d" -o ${OBJECTDIR}/_ext/1360937237/eepromParam.o ../src/eepromParam.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/hmc1122.o: ../src/hmc1122.c  .generated_files/ebc561c9ccf43f9ea3ea923d8d7341d0dbf94328.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/hmc1122.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/hmc1122.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/hmc1122.o.d" -o ${OBJECTDIR}/_ext/1360937237/hmc1122.o ../src/hmc1122.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/adl5240.o: ../src/adl5240.c  .generated_files/9d44dd0de4a2fbdcf568e98069584d3ad9c937be.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/adl5240.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/adl5240.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/adl5240.o.d" -o ${OBJECTDIR}/_ext/1360937237/adl5240.o ../src/adl5240.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/I2C.o: ../src/I2C.c  .generated_files/1c6a54e7c2f5e37d48a8f0fcd85f0a1ba1282e74.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/I2C.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/I2C.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/I2C.o.d" -o ${OBJECTDIR}/_ext/1360937237/I2C.o ../src/I2C.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/eeprom.o: ../src/eeprom.c  .generated_files/9ca41b19b88504124952ece293fb9e41ce5895bb.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eeprom.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eeprom.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/eeprom.o.d" -o ${OBJECTDIR}/_ext/1360937237/eeprom.o ../src/eeprom.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o: ../src/ReceiveCMD.c  .generated_files/16f7d76cb7915919f1e3a5be21726920e50122c.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_ICD4=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o.d" -o ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o ../src/ReceiveCMD.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
else
${OBJECTDIR}/_ext/1471492474/plib_clk.o: ../src/config/default_PIC32/peripheral/clk/plib_clk.c  .generated_files/ac65962206e47074272da6f45f2d9d696616bbfb.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1471492474" 
	@${RM} ${OBJECTDIR}/_ext/1471492474/plib_clk.o.d 
	@${RM} ${OBJECTDIR}/_ext/1471492474/plib_clk.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1471492474/plib_clk.o.d" -o ${OBJECTDIR}/_ext/1471492474/plib_clk.o ../src/config/default_PIC32/peripheral/clk/plib_clk.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1628442791/plib_evic.o: ../src/config/default_PIC32/peripheral/evic/plib_evic.c  .generated_files/84b888305881584bc0e492443bbe022bc01e27f3.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1628442791" 
	@${RM} ${OBJECTDIR}/_ext/1628442791/plib_evic.o.d 
	@${RM} ${OBJECTDIR}/_ext/1628442791/plib_evic.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1628442791/plib_evic.o.d" -o ${OBJECTDIR}/_ext/1628442791/plib_evic.o ../src/config/default_PIC32/peripheral/evic/plib_evic.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1628496619/plib_gpio.o: ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c  .generated_files/34496d477801d442c5187a9ea60f2d271e8e1e21.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1628496619" 
	@${RM} ${OBJECTDIR}/_ext/1628496619/plib_gpio.o.d 
	@${RM} ${OBJECTDIR}/_ext/1628496619/plib_gpio.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1628496619/plib_gpio.o.d" -o ${OBJECTDIR}/_ext/1628496619/plib_gpio.o ../src/config/default_PIC32/peripheral/gpio/plib_gpio.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o: ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c  .generated_files/61c85f34a2f3ab67d9fc624b9bb64bcb4855d330.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/997135251" 
	@${RM} ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o.d 
	@${RM} ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o.d" -o ${OBJECTDIR}/_ext/997135251/plib_i2c1_master.o ../src/config/default_PIC32/peripheral/i2c/master/plib_i2c1_master.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1471476099/plib_tmr4.o: ../src/config/default_PIC32/peripheral/tmr/plib_tmr4.c  .generated_files/2d090393c7cd28d47ca9a8935c50120085f74b18.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1471476099" 
	@${RM} ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o.d 
	@${RM} ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1471476099/plib_tmr4.o.d" -o ${OBJECTDIR}/_ext/1471476099/plib_tmr4.o ../src/config/default_PIC32/peripheral/tmr/plib_tmr4.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1471476099/plib_tmr2.o: ../src/config/default_PIC32/peripheral/tmr/plib_tmr2.c  .generated_files/fdf37efd530267c71b486d134a878ede3210318.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1471476099" 
	@${RM} ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o.d 
	@${RM} ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1471476099/plib_tmr2.o.d" -o ${OBJECTDIR}/_ext/1471476099/plib_tmr2.o ../src/config/default_PIC32/peripheral/tmr/plib_tmr2.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1628899562/plib_uart2.o: ../src/config/default_PIC32/peripheral/uart/plib_uart2.c  .generated_files/eeb30031e16d598338a2d5cbc053820135badfed.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1628899562" 
	@${RM} ${OBJECTDIR}/_ext/1628899562/plib_uart2.o.d 
	@${RM} ${OBJECTDIR}/_ext/1628899562/plib_uart2.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1628899562/plib_uart2.o.d" -o ${OBJECTDIR}/_ext/1628899562/plib_uart2.o ../src/config/default_PIC32/peripheral/uart/plib_uart2.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/279352370/xc32_monitor.o: ../src/config/default_PIC32/stdio/xc32_monitor.c  .generated_files/36b6178ede54564bd84fc50d86190b7775133535.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/279352370" 
	@${RM} ${OBJECTDIR}/_ext/279352370/xc32_monitor.o.d 
	@${RM} ${OBJECTDIR}/_ext/279352370/xc32_monitor.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/279352370/xc32_monitor.o.d" -o ${OBJECTDIR}/_ext/279352370/xc32_monitor.o ../src/config/default_PIC32/stdio/xc32_monitor.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1177533048/initialization.o: ../src/config/default_PIC32/initialization.c  .generated_files/aa010db5e87898ccc32a3d8b278f4010edb75fe0.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/initialization.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/initialization.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1177533048/initialization.o.d" -o ${OBJECTDIR}/_ext/1177533048/initialization.o ../src/config/default_PIC32/initialization.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1177533048/interrupts.o: ../src/config/default_PIC32/interrupts.c  .generated_files/3e60310e51c4528e9ff9ffe8eff813d9bc073e8d.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/interrupts.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/interrupts.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1177533048/interrupts.o.d" -o ${OBJECTDIR}/_ext/1177533048/interrupts.o ../src/config/default_PIC32/interrupts.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1177533048/exceptions.o: ../src/config/default_PIC32/exceptions.c  .generated_files/b23280be43b4d95f9caa0c1e1320735eb41ca018.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1177533048" 
	@${RM} ${OBJECTDIR}/_ext/1177533048/exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1177533048/exceptions.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1177533048/exceptions.o.d" -o ${OBJECTDIR}/_ext/1177533048/exceptions.o ../src/config/default_PIC32/exceptions.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  .generated_files/3d26154725a5fba09498cf7e9e73921105a9a221.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/user.o: ../src/user.c  .generated_files/35ba1d49a13fe8b0746c9bd00be745ddeb5f9d31.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/user.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/user.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/user.o.d" -o ${OBJECTDIR}/_ext/1360937237/user.o ../src/user.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/system_cfg.o: ../src/system_cfg.c  .generated_files/3a69b14cf1166dd3073271fead0a43e6d6f45bf1.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/system_cfg.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/system_cfg.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/system_cfg.o.d" -o ${OBJECTDIR}/_ext/1360937237/system_cfg.o ../src/system_cfg.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/ADF435x.o: ../src/ADF435x.c  .generated_files/f52d947dc31ed142059a21396d40efd2996f8ddd.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ADF435x.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ADF435x.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ADF435x.o.d" -o ${OBJECTDIR}/_ext/1360937237/ADF435x.o ../src/ADF435x.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o: ../src/DDS_AD9959.c  .generated_files/a227dca04ba00fd8b13d5318a6cabd68cb160947.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o.d" -o ${OBJECTDIR}/_ext/1360937237/DDS_AD9959.o ../src/DDS_AD9959.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/LMX2572.o: ../src/LMX2572.c  .generated_files/2010736a53b67156ce12e65e5bbf6dc319fd81f9.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/LMX2572.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/LMX2572.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/LMX2572.o.d" -o ${OBJECTDIR}/_ext/1360937237/LMX2572.o ../src/LMX2572.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/ad9914.o: ../src/ad9914.c  .generated_files/d9e9c82136473146bd23a681527fed5a7b676980.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad9914.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad9914.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ad9914.o.d" -o ${OBJECTDIR}/_ext/1360937237/ad9914.o ../src/ad9914.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/channel.o: ../src/channel.c  .generated_files/fd43b2ec65b858932bfe96faf56db5c8766c20ec.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/channel.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/channel.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/channel.o.d" -o ${OBJECTDIR}/_ext/1360937237/channel.o ../src/channel.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/executeCMD.o: ../src/executeCMD.c  .generated_files/9183273fa5729b3787275aa8879fabf9b12ec6b3.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/executeCMD.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/executeCMD.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/executeCMD.o.d" -o ${OBJECTDIR}/_ext/1360937237/executeCMD.o ../src/executeCMD.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/former_signal.o: ../src/former_signal.c  .generated_files/8893f9f0e78f813f06fe4cb86a6b6b7b7da99f87.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/former_signal.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/former_signal.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/former_signal.o.d" -o ${OBJECTDIR}/_ext/1360937237/former_signal.o ../src/former_signal.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/softwareSPI.o: ../src/softwareSPI.c  .generated_files/529320b2b966100b1ea5bda4548edd9398bbc8a7.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/softwareSPI.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/softwareSPI.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/softwareSPI.o.d" -o ${OBJECTDIR}/_ext/1360937237/softwareSPI.o ../src/softwareSPI.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/synt_AD4351.o: ../src/synt_AD4351.c  .generated_files/33f0dc127883919440e4f4573d9daf7714787b1c.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/synt_AD4351.o.d" -o ${OBJECTDIR}/_ext/1360937237/synt_AD4351.o ../src/synt_AD4351.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/synt_AD9959.o: ../src/synt_AD9959.c  .generated_files/d90446baf3d0755c504feb10263fd183e7b00e1e.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/synt_AD9959.o.d" -o ${OBJECTDIR}/_ext/1360937237/synt_AD9959.o ../src/synt_AD9959.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/ad7995.o: ../src/ad7995.c  .generated_files/281f5bdee04b60cf79895e3e46e70d0575a35d90.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad7995.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ad7995.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ad7995.o.d" -o ${OBJECTDIR}/_ext/1360937237/ad7995.o ../src/ad7995.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/XC_DATA.o: ../src/XC_DATA.c  .generated_files/8981162148e9b6552ca31ee67e566b8892618cb6.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/XC_DATA.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/XC_DATA.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/XC_DATA.o.d" -o ${OBJECTDIR}/_ext/1360937237/XC_DATA.o ../src/XC_DATA.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/eepromParam.o: ../src/eepromParam.c  .generated_files/f68c88ddec0cb067193e286ff6a539dfd5b17683.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eepromParam.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eepromParam.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/eepromParam.o.d" -o ${OBJECTDIR}/_ext/1360937237/eepromParam.o ../src/eepromParam.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/hmc1122.o: ../src/hmc1122.c  .generated_files/82b514bc280fead6bbada4af36b8e737918b2b17.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/hmc1122.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/hmc1122.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/hmc1122.o.d" -o ${OBJECTDIR}/_ext/1360937237/hmc1122.o ../src/hmc1122.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/adl5240.o: ../src/adl5240.c  .generated_files/8f01edf8f61c0a1d6a92ce6513457cd0d9647e55.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/adl5240.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/adl5240.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/adl5240.o.d" -o ${OBJECTDIR}/_ext/1360937237/adl5240.o ../src/adl5240.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/I2C.o: ../src/I2C.c  .generated_files/f49fd7d5bc61c44de3dd9042f15041cb5618aa93.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/I2C.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/I2C.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/I2C.o.d" -o ${OBJECTDIR}/_ext/1360937237/I2C.o ../src/I2C.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/eeprom.o: ../src/eeprom.c  .generated_files/13758ae2c36d3d52eebbb4e6e1ea4d40d2894040.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eeprom.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/eeprom.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/eeprom.o.d" -o ${OBJECTDIR}/_ext/1360937237/eeprom.o ../src/eeprom.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o: ../src/ReceiveCMD.c  .generated_files/f48f70294dcce35e52e524785548a51587110844.flag .generated_files/ac1ed135c7382d812c39decf86f1e721fa63d510.flag
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o 
	${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -I"../src" -I"../src/config/default_PIC32" -I"../src/packs/PIC32MX795F512L_DFP" -Werror -Wall -MP -MMD -MF "${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o.d" -o ${OBJECTDIR}/_ext/1360937237/ReceiveCMD.o ../src/ReceiveCMD.c    -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -mdfp="${DFP_DIR}"  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/FormerS.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    ../src/config/default_PIC32/p32MX795F512L.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g -mdebugger -D__MPLAB_DEBUGGER_ICD4=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/FormerS.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)   -mreserve=data@0x0:0x1FC -mreserve=boot@0x1FC02000:0x1FC02FEF -mreserve=boot@0x1FC02000:0x1FC024FF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=__MPLAB_DEBUGGER_ICD4=1,--defsym=_min_heap_size=512,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -mdfp="${DFP_DIR}"
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/FormerS.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   ../src/config/default_PIC32/p32MX795F512L.ld
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/FormerS.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default_PIC32=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=512,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -mdfp="${DFP_DIR}"
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/FormerS.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default_PIC32
	${RM} -r dist/default_PIC32

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
