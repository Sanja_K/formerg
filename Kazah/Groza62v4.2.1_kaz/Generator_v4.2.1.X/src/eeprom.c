#include "eeprom.h"
#include "system_cfg.h"
//#define select_eeprom()         {}
//
//#define unselect_eeprom()       {}
//

void select_eeprom(void)
{
    delay_us(10);
    EEPROM_CS = 0;
    delay_us(10);
}
void unselect_eeprom(void)
{
    delay_us(10);
    EEPROM_CS = 1;
    delay_us(10);
}
void spi1_init(void)
{
    SPI1CON1bits.PPRE = 0b00;               //set baudrate = FCY / (4 * 2)
    SPI1CON1bits.SPRE = 0b000;
    SPI1CON1bits.MSTEN = 1;                 //master mode
    SPI1CON1bits.CKE = 1;
    SPI1CON1bits.CKP = 0;
    SPI1CON1bits.MODE16 = 0;                //communication is byte-wide
    SPI1STATbits.SPIEN = 1;                 //enable spi
}

unsigned char spi1_transmit(unsigned char byte)
{
    SPI1BUF = byte;
    while(SPI1STATbits.SPIRBF == 0);
    
    return SPI1BUF;
}

void spi1_write_byte(unsigned char byte)
{
    spi1_transmit(byte);
}

unsigned char spi1_read_byte(void)
{
    return spi1_transmit(0x00);
}

static void spi_write_byte(unsigned char byte)
{
    spi1_write_byte(byte);
}

static unsigned char spi_read_byte(void)
{
    return spi1_read_byte();
}

static void eeprom_write_command(unsigned char cmd)
{
    spi_write_byte(cmd);
}

static void eeprom_write_addr(unsigned short addr)
{
    addr &= (EE_SIZE - 1);
    spi_write_byte(addr >> 8);
    spi_write_byte(addr);
}

static unsigned char eeprom_read_status(void)
{
    unsigned char status;
    
    select_eeprom();
    eeprom_write_command(EE_RDSR);
    status = spi_read_byte();
    unselect_eeprom();
    
    return status;
}

void eeprom_init(void)
{
    unselect_eeprom();
    EEPROM_CS_TRIS = 0;
    delay_us(1);
}

/* Function write byte array of "size" bytes from start address "addr". 
 * Because up to EE_PAGE_SIZE bytes of data can be sent to the device 
 * before a write cycle, the only restriction is that all of the bytes must
 * reside in the same page. That's why function check start address and at
 * first iteration write EE_PAGE_SIZE - (curr_addr % EE_PAGE_SIZE) bytes 
 * to alignment data at the page boundary. Function allow to write data from 
 * 0 to EE_SIZE addresses, so you can't write more data than EE_SIZE */
void eeprom_write_array(unsigned short addr, unsigned char *buf, int size)
{
    unsigned short curr_addr;
    unsigned int curr_size;
    
    size = size > EE_SIZE ? EE_SIZE : size;
    curr_size = size > EE_PAGE_SIZE ? EE_PAGE_SIZE : size;
    curr_addr = addr;
    
    if((curr_addr % EE_PAGE_SIZE) != 0)
    {
        if((curr_size + (curr_addr % EE_PAGE_SIZE)) > EE_PAGE_SIZE)
        {
            curr_size = EE_PAGE_SIZE - (curr_addr % EE_PAGE_SIZE);
        }
    }
    
    do
    {
        unsigned int i;
        
        select_eeprom();
        eeprom_write_command(EE_WREN);
        unselect_eeprom();
        select_eeprom();
        eeprom_write_command(EE_WRITE);
        delay_us(10);
        eeprom_write_addr(curr_addr);
        delay_us(1);
        for(i = 0; i < curr_size; i++)
        {   delay_us(1);
            spi_write_byte(*buf++);        
        }

        unselect_eeprom();

        while(eeprom_read_status() & 0x01);
        
        size -= curr_size;
        curr_addr += curr_size;
        curr_size = size > EE_PAGE_SIZE ? EE_PAGE_SIZE : size;
    } while(size > 0 && curr_addr < EE_SIZE);
}

void eeprom_write_byte(unsigned short addr, unsigned char byte)
{
    eeprom_write_array(addr, &byte, 1);
}

/* Function read byte array from start address "addr" to "addr + size"
 * address. If "addr + size" > EE_SIZE, reading stop, when we reach end
 * address to prevent rolling address counter to 0 address. */
void eeprom_read_array(unsigned short addr, unsigned char *buf, int size)
{
    unsigned int i;
    if((addr + size) > EE_SIZE)
        size = EE_SIZE - addr;
    
    select_eeprom();
    eeprom_write_command(EE_READ);
     delay_us(1);
    eeprom_write_addr(addr);
    delay_us(1);
    for(i = 0; i < size; i++)
    {    delay_us(1);
        buf[i] = spi_read_byte();
    }
    unselect_eeprom();
}

unsigned char eeprom_read_byte(unsigned short addr)
{
    unsigned char data;
    eeprom_read_array(addr, &data, 1);
    return data;
}
