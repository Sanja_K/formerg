#include <xc.h>
#include "system_cfg.h"        /* System funct/params, like osc/peripheral config */
#include "synt_AD9959.h"        /* System funct/params, like osc/peripheral config */

uint8_t _IOUpdate_FAST;
uint8_t _CS_FAST;
  
uint8_t last_channels = 0xFF;
 
   // 1 - lock, 0 - not, -1 - не поддерживается


static void  v_o_DATA( uint8_t ena )
{
    if(ena)
        v_o_DATA_1;
    else
        v_o_DATA_0;   
}    

static void  SPIp_write( uint8_t reg )
{ unsigned int i=0;

    _SCLK_0;
    delay_us(1);
    for( i=0; i<8; i++ ) 
    {
        v_o_DATA(reg&0x80); // делаем маску на старший бит
        delay_us( 1 );
        _SCLK_1;
        reg = reg << 1;
        delay_us( 1 );
        _SCLK_0;
    }
    delay_us(1);
    
} 
 
static uint8_t  SPIp_read( void )
{ uint8_t i=0;
    uint8_t value;

    _SCLK_0;
    value = 0;
    TRISGbits.TRISG8 = 1;//
    
    delay_us(1);

    for( i=0; i<8; i++ ) 
    {
        value = (value<<1);
        delay_us( 1 );
        _SCLK_1;
        if (v_i_DATA)
         value |= 0x01; // 
        else
         value &= 0xFE;//     
        delay_us( 1 );
        _SCLK_0;
    }   
     TRISGbits.TRISG8 = 0;//
    // 
    return value;
} 

static void  AD9959_write_byte(ad9959_registers REG, uint8_t value )
{ 
    //_CS_DDS1_0;
    delay_us( 1 );
    SPIp_write(REG);
    SPIp_write(value);
   // _CS_DDS1_1;    
    delay_us( 1 );
      
} 

static void AD9959_write(ad9959_registers REG, uint8_t *buffer, uint8_t len)
{ uint8_t i;

//    _CS_DDS1_0;
    delay_us( 2 );    
    SPIp_write(REG);
    delay_us( 10 );
    for (i=0; i< len; i++)
    {
        SPIp_write(buffer[i]);
    }
//    _CS_DDS1_1;    
    delay_us(10);
}
//
static uint8_t AD9959_read_byte(ad9959_registers REG)
{
    uint8_t value = 0;

//    _CS_DDS1_0;
    SPIp_write(READ | REG);   
    delay_us( 10 );
    value = SPIp_read();
//    _CS_DDS1_1;
    return value;
}

static void AD9959_read(ad9959_registers REG, uint8_t *buffer, uint8_t len)
{ uint8_t i;
 //   _CS_DDS1_0;
    SPIp_write(READ | REG);
     delay_us( 10 );   
    for (i=0; i< len; i++)
    {
        buffer[i] = SPIp_read();
    }    
//    _CS_DDS1_1;

}

int AD9959_get_VCO(void)
{
  int status;
  uint32_t DtVCO;
  uint8_t buffer[3];
  
    status = 0;
     _CS_DDS1_0 ;
    AD9959_read(FR1,buffer,3);
     _CS_DDS1_1 ;
//    while (1)
//    {    
//      _CS_DDS1_0 ; 
//    AD9959_read(FR1,buffer,3);
//    _CS_DDS1_1;
//    
//    }
    
    DtVCO = 0;
    DtVCO += (unsigned long)buffer[2] << 0;    
    DtVCO += (unsigned long)buffer[1] << 8;
    DtVCO += (unsigned long)buffer[0] << 16;

    if (DtVCO == 0xD00000)
    {
        status = MCHP_SUCCESS;
    }
    else
    {
        status = MCHP_FAILURE;
    }

    
  return status;
}

uint8_t AD9959_begin(void)
{
  uint8_t status;
  uint8_t buffer[3];

  status = 0;
  _CS_DDS1_1 ; 
  _IO_Update_0;  
  _PWD_DDS1_1;
  delay_us(10);    
  _PWD_DDS1_0;  
  
  AD9959_reset();

 // AD9959_write_byte(CSR, 0xF2);

  buffer[2] = 0x00;
  buffer[1] = 0x00;  
  buffer[0] = 0xD0;
  
  _CS_DDS1_0;  // 

  AD9959_write(FR1,buffer,3);
  AD9959_IOUpdate();
  _CS_DDS1_1;  
   delay_us(100);
  status = AD9959_get_VCO(); // 
  
  return status;
}
 
 void AD9959_IOUpdate(void)
{   
    delay_us( 1 );  
    _IO_Update_1;
    delay_us( 40 );    
    _IO_Update_0;
    delay_us( 40 );    
}

void AD9959_reset(void)
{
    
  _reset_DDS1_1;
  delay_us( 10 );
  
  _reset_DDS1_0;  
  delay_us( 100 );  
}

//void AD9959_set_PLL( unsigned char value)// 500/25 = 0
//{unsigned char buffer[3];
///*If the value is 4 or 20 (decimal) or between 4 and 20, the PLL is enabled and the value sets the
//multiplication factor. If the value is outside of 4 and 20 (decimal), the PLL is disabled.*/
//    if (value < 4 || value > 20)
//    {
//      value = 1;
//    }
//    value = (value << 2) & 0b01111100;
//    _CS_DDS1_0;  
//    AD9959_read(FR1,buffer,3);
//    _CS_DDS1_1;
//    delay_us( 1 ); 
//    _CS_DDS1_0;  
//    buffer[0] = (buffer[0] & 0b10000011) | value;
//    AD9959_write(FR1,buffer,3);
//    delay_us( 1 );
//    _CS_DDS1_1; 
//}
//
//void AD9959_set_VCO( bool enable)
//{unsigned char buffer[3];
//
//    _CS_DDS1_0 ; 
//
//    AD9959_read(FR1,buffer,3); //the 0 -low range (system clock below 160 MHz) (default).
//
//    _CS_DDS1_1;   
//    
//    if (enable)                 //1 = the high range (system clock above 255 MHz).
//    {
//      buffer[0] = (buffer[0] | 0B10000000);
//    }
//    else
//    {
//      buffer[0] = (buffer[0] & 0B01111111);
//    }
//    
//    _CS_DDS1_0 ;
//    AD9959_write(FR1,buffer,3);
//    _CS_DDS1_1;         
//}

uint8_t setChannels(uint8_t chan)
{   
    uint8_t status;
    
    uint8_t CSR_val;
    uint8_t bufferCH;
    status = 0;
    if (last_channels != chan)
    {    
      CSR_val = AD9959_read_byte(CSR);
      CSR_val = chan | (0x0F & CSR_val);
      AD9959_write_byte(CSR,chan | (0x0F & CSR_val));
      
      bufferCH = AD9959_read_byte(CSR);
      if(bufferCH != CSR_val)
      {
        status = 1;
        return status;
      } 
    }
    last_channels = chan;
    return status;
}

static uint32_t FTW_calc(uint64_t frequency)
{
    uint32_t freq_int;

    freq_int = (uint32_t)(4294967296*frequency/_clock);
       
    return freq_int;
}

void AD9959_set_Frequency( uint8_t CH, uint64_t frequency)
{
    uint32_t freq_int;
    uint8_t buffer[4];   
    freq_int = FTW_calc(frequency);

    buffer[3] = (uint8_t) freq_int;
    buffer[2] = (uint8_t) (freq_int >> 8);
    buffer[1] = (uint8_t) (freq_int >> 16);
    buffer[0] = (uint8_t) (freq_int >> 24);

    _CS_DDS1_0;    
    setChannels (CH);
    _CS_DDS1_1; 
    delay_us(1);
     AD9959_IOUpdate();   
    _CS_DDS1_0 ;  
    AD9959_write(CTW0,buffer,4);
    AD9959_IOUpdate();
     _CS_DDS1_1;
     
}

uint64_t AD9959_get_Frequency( uint8_t CH)
{
    uint8_t buffer[4];

    uint64_t freq_int = 0;

     _CS_DDS1_0 ;     
    setChannels (CH); // выбор канала
    _CS_DDS1_1; 
    delay_us(1);

     _CS_DDS1_0;      
    AD9959_read(CTW0,buffer,4);
    _CS_DDS1_1; 
    
    freq_int += (uint32_t)buffer[3] << 0;
    freq_int += (uint32_t)buffer[2] << 8;
    freq_int += (uint32_t)buffer[1] << 16;
    freq_int += (uint32_t)buffer[0] << 24;
    return _clock/4294967296.0*(uint64_t)freq_int;
}

static uint32_t PhTW_calc(double phase)
{
    uint32_t phase_int;

    phase_int = (unsigned int)(45.51111111*phase);
       
    return phase_int;
}  

void setChannels00(void)
{    unsigned char buffer[3];
    uint64_t phase_int = 0;
    
    phase_int = 0x3000;
    
    buffer[3] = (uint8_t) phase_int;
    buffer[2] = (uint8_t) (phase_int >> 8);
    buffer[1] = (uint8_t) (phase_int >> 16);
    buffer[0] = (uint8_t) (phase_int >> 24);
    
        _CS_DDS1_0;
    setChannels (CH0);
    AD9959_write(CPW0,buffer,4);
        _CS_DDS1_1; 
}
void AD9959_set_DAC( uint8_t CH)
{    unsigned char buffer[3];
//     unsigned long int CFR_val;
    
    _CS_DDS1_0;       
    setChannels (CH); // выбор канала
    _CS_DDS1_1;
    delay_us(1);        

    buffer[2] = 0x00;
    buffer[1] = 0x01;  
    buffer[0] = 0x00;
  
    _CS_DDS1_0 ;  // инициализация DDS

    AD9959_write(CFR,buffer,3);  

    AD9959_IOUpdate();
  
  _CS_DDS1_1;   
   /*   
  DDSx ? _CS_DDS1_0 : _CS_DDS2_0;  // инициализация DDS  
  AD9959_read(CFR,buffer,3);  
  _CS_DDS1_1; _CS_DDS2_1;  
    CFR_val = 0;
    CFR_val += (unsigned long)buffer[2] << 0;    
    CFR_val += (unsigned long)buffer[1] << 8;
    CFR_val += (unsigned long)buffer[0] << 16;
  delay_us (10); 
  _CS_DDS1_1; _CS_DDS2_1;    
 */


}


void AD9959_set_Phase( uint8_t CH, uint16_t phase)
{
    // 45.51~ is 2^14/360.0
    int status;
    uint16_t phase_int;
    uint8_t buffer[2];

    status = 0;
    phase_int = (uint16_t) PhTW_calc(phase);

    buffer[1] = (uint8_t) phase_int;
    buffer[0] = (uint8_t) (phase_int >> 8);

     _CS_DDS1_0 ;       
    setChannels (CH); // выбор канала
    _CS_DDS1_1; 
    AD9959_IOUpdate();  
     _CS_DDS1_0 ;        
    AD9959_write(CPW0,buffer,2);
    AD9959_IOUpdate();
    _CS_DDS1_1;    
}

double AD9959_get_Phase(unsigned char CH)
{
    unsigned char buffer[2];
    unsigned int phase_int;

    _CS_DDS1_0 ;       
    setChannels (CH); // выбор канала
    _CS_DDS1_1;
    
    delay_us(1);    
    _CS_DDS1_0 ; 
    AD9959_read(CPW0,buffer,2);
    _CS_DDS1_1; 
    phase_int = 0;
    phase_int += (unsigned int)buffer[1] << 0;
    phase_int += (unsigned int)buffer[0] << 8;
    return (double)phase_int/45.51111111;
}


unsigned int ATW_calc(double amplitude)
{
    unsigned int amp_int;

    amp_int = (unsigned int)(1023.0*amplitude);

    amp_int = amp_int & 0x03FF;
       
    return amp_int;
}  

void AD9959_set_Amplitude(unsigned char CH, double amplitude)
{
    unsigned int amp_int;
    unsigned char buffer[3];

    amp_int = ATW_calc(amplitude);

    buffer[2] = (unsigned char) amp_int;
    buffer[1] = (unsigned char) (amp_int >> 8);
    buffer[0] = 0;

    buffer[1] = buffer[1] | 0x10;

    _CS_DDS1_0 ;       
    setChannels (CH); // выбор канала
    _CS_DDS1_1;
    delay_us(1);    
     _CS_DDS1_0 ; 
    AD9959_write(ACR,buffer,3);
    AD9959_IOUpdate();
    _CS_DDS1_1;     
}

double AD9959_get_Amplitude(unsigned char CH)
{
    unsigned char buffer[3];
    unsigned int amp_int = 0;

    _CS_DDS1_0;       
    setChannels (CH); // выбор канала
    _CS_DDS1_1; 
    delay_us(1);    
    _CS_DDS1_0; 
    AD9959_read(ACR,buffer,3);
    _CS_DDS1_1;
    amp_int += (unsigned int)buffer[2] << 0;
    amp_int += (unsigned int)buffer[1] << 8;
    amp_int = amp_int & 0x03FF;
    return (double)amp_int/1023.0;
}
 
 
 void AD9959_sweep_Frequency(unsigned char CH, uint64_t frequency, bool follow )
  {
    uint32_t freq_int;
    uint32_t CFR_int;    
    uint8_t buffer[4];
    uint8_t bufferChR[3];    //FOR CHANNEL REGISTERS
    
    
    CFR_int = ( FrequencyModulation | SweepEnable | DACFullScale | /*MatchPipeDelay | */(follow ? 0 : SweepNoDwell));
    bufferChR[2] = (uint8_t) CFR_int;
    bufferChR[1] = (uint8_t) (CFR_int >> 8);
    bufferChR[0] = (uint8_t) (CFR_int >> 16);    

    
    
    freq_int = FTW_calc(frequency);     
    
    buffer[3] = (uint8_t) freq_int;
    buffer[2] = (uint8_t) (freq_int >> 8);
    buffer[1] = (uint8_t) (freq_int >> 16);
    buffer[0] = (uint8_t) (freq_int >> 24);
    

    _CS_DDS1_0;       
    setChannels (CH); // выбор канала
    _CS_DDS1_1;  
    AD9959_IOUpdate(); 
    // Set up for frequency sweep    
     _CS_DDS1_0 ; 
    AD9959_write(CFR, bufferChR,3);
    _CS_DDS1_1; 
    AD9959_IOUpdate();    

     _CS_DDS1_0 ;        
    // Write the frequency delta into the sweep destination register
    AD9959_write(CTW1,buffer,4);
    AD9959_IOUpdate();
    _CS_DDS1_1; 
  }
 

 void AD9959_sweep_Phase(uint8_t CH, double phase, bool follow)          // Target phase (180 degrees)
  {
    uint8_t buffer[4];     
    uint32_t CFR_int;       
    uint32_t Phase_int;
    uint8_t bufferChR[3];    
    
    Phase_int = PhTW_calc(phase);
    
    
    Phase_int = Phase_int << 18;
    
    
    buffer[3] = (uint8_t) Phase_int;
    buffer[2] = (uint8_t) (Phase_int >> 8);
    buffer[1] = (uint8_t) (Phase_int >> 16);
    buffer[0] = (uint8_t) (Phase_int >> 24);   
    
     _CS_DDS1_0 ;       
    setChannels (CH); // выбор канала
    _CS_DDS1_1;
    delay_us(1);    
    _CS_DDS1_0 ; 
    // Set up for phase sweep
    
    CFR_int = ( PhaseModulation | DACFullScale | /*MatchPipeDelay | */(follow ? 0 : SweepNoDwell));
    bufferChR[2] = (unsigned char) CFR_int;
    bufferChR[1] = (unsigned char) (CFR_int >> 8);
    bufferChR[0] = (unsigned char) (CFR_int >> 16);    
    
    
    AD9959_write(CFR, bufferChR,3);
    _CS_DDS1_1;
    delay_us(1);     
    _CS_DDS1_0  ;  
    // Write the phase into the sweep destination register, MSB aligned
    AD9959_write(CTW1, buffer,4);
    AD9959_IOUpdate();
    _CS_DDS1_1;   
  } 
 
 void AD9959_sweep_Amplitude(unsigned char CH, uint16_t amplitude, bool follow)  // Target amplitude (half)
 {
     /* 
    unsigned char buffer[4];     
    
    uint32_t Amp_int;       
 
    
    Amp_int = (unsigned char )((uint32_t)amplitude) * (0x1<<(32-10));
    buffer[3] = (unsigned char) Amp_int;
    buffer[2] = (unsigned char) (Amp_int >> 8);
    buffer[1] = (unsigned char) (Amp_int >> 16);
    buffer[0] = (unsigned char) (Amp_int >> 24);       
     
    DDSx ? _CS_DDS1_0 : _CS_DDS2_0;       
    setChannels (CH); // выбор канала
    _CS_DDS1_1; _CS_DDS2_1; 
    delay_us(1);    
    DDSx ? _CS_DDS1_0 : _CS_DDS2_0; 
    // Set up for amplitude sweep
    AD9959_write_byte(
      CFR,
      AmplitudeModulation |
      SweepEnable |
      DACFullScale |
      MatchPipeDelay |
      (follow ? 0 : SweepNoDwell)
    );
    _CS_DDS1_1; _CS_DDS2_1;
    delay_us(1);    
    DDSx ? _CS_DDS1_0 : _CS_DDS2_0;    
    // Write the amplitude into the sweep destination register, MSB aligned
    AD9959_write(CTW1,buffer,4 );
    AD9959_IOUpdate();
    _CS_DDS1_1; _CS_DDS2_1;    
    * */
  }
 //// Sweep up at increments of 100 each 1us, and down 1000 each 2us:
 //sweepRates(MyAD9959::Channel0|MyAD9959::Channel1, 100, 125, 1000, 250);

 
  void AD9959_sweep_Rates(uint8_t CH, uint64_t dF1, uint8_t up_rate, uint64_t dF2, uint8_t down_rate)
  { 
    uint8_t bufferI[4];
    uint8_t bufferD[4];
    uint8_t bufferR[2];    
    
    uint32_t increment; 
    uint32_t decrement;     
    
    increment = FTW_calc(dF1);   
    decrement = FTW_calc(dF2);       

    bufferI[3] = (uint8_t) increment;
    bufferI[2] = (uint8_t) (increment >> 8);
    bufferI[1] = (uint8_t) (increment >> 16);
    bufferI[0] = (uint8_t) (increment >> 24);    
    
    bufferD[3] = (uint8_t) decrement;
    bufferD[2] = (uint8_t) (decrement >> 8);
    bufferD[1] = (uint8_t) (decrement >> 16);
    bufferD[0] = (uint8_t) (decrement >> 24);      
    
    bufferR [1] = (uint8_t) up_rate;   
    bufferR [0] = (uint8_t) down_rate;       
    
    _CS_DDS1_0 ;       
    setChannels (CH); // выбор канала
    _CS_DDS1_1; 
    AD9959_IOUpdate();    
    
     _CS_DDS1_0 ;   
    AD9959_write(RDW, bufferI,4);     //     increment                 // Rising Sweep Delta Word
    _CS_DDS1_1;  
    AD9959_IOUpdate();  
    
    _CS_DDS1_0; 
    AD9959_write(FDW, bufferD,4);      //    decrement
    // Falling Sweep Delta Word
    _CS_DDS1_1; 
    AD9959_IOUpdate();    
     _CS_DDS1_0 ; 
    AD9959_write(LSR, bufferR,2);      // Linear Sweep Ramp Rate
    AD9959_IOUpdate();
     _CS_DDS1_1; 
    delay_us(1);      
    
  }
  
//   void LFM (void)
//   {
//    AD9959_set_Frequency(CH1,(90*MHz));       
//    
//    AD9959_sweep_Frequency(CH1,(110*MHz),1);
//    
//    AD9959_sweep_Rates(CH1,1600,1,1600,1);       
//   
//   }