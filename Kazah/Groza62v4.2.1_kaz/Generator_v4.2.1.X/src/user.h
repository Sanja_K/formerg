/******************************************************************************/
/* User Level #define Macros                                                  */
/******************************************************************************/

/* TODO Application specific user parameters used in user.c may go here */

/******************************************************************************/
/* User Function Prototypes                                                   */
/******************************************************************************/
#ifndef XC_user_H
#define	XC_user_H

#include "system_cfg.h"

/* TODO User level functions prototypes (i.e. InitApp) go here */

uint8_t InitApp(void);         /* I/O and Peripheral Initialization */
void UserTaskApp (void);
void ReceiveDtUrt(unsigned char bBf_Rx);
void RsNewRxDtUrt(void);
void ExecutCMD(void);
void TransmitDtUrt (int CMD, unsigned char *infDt, int lenInfDt);



#endif  