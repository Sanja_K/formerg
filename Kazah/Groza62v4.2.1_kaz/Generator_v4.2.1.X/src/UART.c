/*
 * File:   UART.c
 * Author: Alexandr
 *
 * Created on September 20, 2017, 4:20 PM
 */


#include "xc.h"
#include "UART.h" 
#include "system_cfg.h" 
#include "timers.h" 
#include "user.h" 

//unsigned char   __attribute__((far)) BUF_UART[20];

extern unsigned int cntTMR;

void __attribute__ ((interrupt, no_auto_psv)) _U2RXInterrupt(void) 
{
    unsigned char buff;
    if(U2STAbits.FERR)
        RsNewRxDtUrt();
        
    if(U2STAbits.OERR)        
    {
        RsNewRxDtUrt();
        U2STAbits.OERR = 0;
    }
    cntTMR = 0;
    IEC1bits.T4IE = 1;  // разрешаем прерывание по таймеру      
    
    if (U2STAbits.URXDA == 1)// добавить while для вычитки
    {
        buff  = U2RXREG;
        ReceiveDtUrt(buff); // отправляем байт для анализа
    }

    TMR4 = 0;
    IFS1bits.T4IF = 0;
  
	IFS1bits.U2RXIF = 0;
}


void Init_UART_485(void)
{

        TRISFbits.TRISF4 = 0;
        TRISFbits.TRISF5 = 1;
        //TRISCbits.TRISC13 = 0;
        TRISFbits.TRISF12 = 0;        
        U2MODEbits.UARTEN = 0;	// Bit15 TX, RX DISABLED, ENABLE at end of func       
        U2MODEbits.STSEL = 0; // 1-stop bit
        U2MODEbits.PDSEL = 0; // No Parity, 8-data bits
        U2MODEbits.ABAUD = 0; // Auto-Baud Disabled
        U2MODEbits.BRGH = 0; // Low Speed mode
        U2BRG = BRGVAL; // BAUD Rate Setting
        U2STAbits.UTXISEL0 = 0; // Interrupt after one Tx character is transmitted
        U2STAbits.UTXISEL1 = 0;
        IEC1bits.U2RXIE = 1; // разрешить прерывание
        //IPC7bits.U2RXIP = 7 // highest priority interrupt)
        U2MODEbits.UARTEN = 1; // Enable UART
        U2STAbits.UTXEN = 1; // Enable UART Tx
        
        AD485Rx_ON;

}


void Send_UART_485P(unsigned char *TX, unsigned int len)
{
    unsigned int i;

    AD485Tx_ON;
    for(i=0;i<len;i++)
    {
        U2TXREG = TX[i];
        while (!U2STAbits.TRMT == 1);
    }
    //PORTCbits.RC13 = 0;
  //  PORTFbits.RF12 = 0;
//    asm("Exit_TransmitP:");                
        AD485Rx_ON;
}
void Send_UART_485(unsigned char TX)
{

    AD485Tx_ON;
    U2TXREG = TX;
    while (!U2STAbits.TRMT == 1);
     {Nop();}
//    asm("Exit_Transmit:");        
        
        AD485Rx_ON;
}
