#ifndef _AD9914_H    /* Guard against multiple inclusion */
#define _AD9914_H

#include <xc.h>
#include <stdint.h>

#define REG_BYTE        1
#define REG_WORD        2
#define REG_DWORD       4

#define READ_MASK       0x80

#define FREQ_290        0x1536202E
#define FREQ_400        0x1D41D41D
#define FREQ_512        0x369D0369
#define FREQ_700        0x33333333
#define FREQ_860        0x3EE721A5
#define FREQ_1000       0x49249249
#define FREQ_1215       0x58DE5AB2 

#define ADDAC_ON          0x00010008
#define ADDAC_OFF         0x00010048
#define ADSPI_3WIRE       0x0001000A
#define PROF_MODE       0x00800900
#define ADLFM_MODE        0x008E2900
#define DAC_CAL_EN      0x01052120
#define DAC_CAL_DIS     0x00052120

/********************************* VARIABLES **********************************/
struct ad9914
{
    unsigned char CFR1;
    unsigned char CFR2;
    unsigned char CFR3;
    unsigned char CFR4;
    unsigned char DRAMP_LO;
    unsigned char DRAMP_HI;
    unsigned char DRAMP_RIS;
    unsigned char DRAMP_FAL;
    unsigned char DRAMP_RATE;
    unsigned char PROF_F[8];
    unsigned char PROF_PA[8];
};

extern const struct ad9914 ad9914_reg;

/********************************* FUNCTIONS **********************************/
/* 
 * IO_UPDATE    LATD1
 * DDS_EXT_PD   LATD2
 * M_RESET      LATD11
 * CS_DDS       LATD9 
 */


#define SCLK_0              LATGbits.LATG6 = 0;  
#define SCLK_1              LATGbits.LATG6 = 1;
#define v_o_DATA_0           LATGbits.LATG8 = 0; //SDO2
#define v_o_DATA_1           LATGbits.LATG8 = 1;
  
#define v_i_DATA             PORTGbits.RG7   /// SDI2


//#define ad9914_select_dds_1()     LATDbits.LATD4 = 0;
//#define ad9914_unselect_dds_1()   LATDbits.LATD4 = 1;

#define ad9914_select_dds_2()     LATDbits.LATD11 = 0;
#define ad9914_unselect_dds_2()   LATDbits.LATD11 = 1;

//#define ad9914_reset_1()          {LATDbits.LATD6 = 1; LATDbits.LATD6 = 0;}   //DDS1 master reset

#define ad9914_reset_2()          {LATDbits.LATD10 = 1; LATDbits.LATD10 = 0;}   //DDS1 

//#define ad9914_on()             LATDbits.LATD4 = 0//DDS1
//#define ad9914_off()            LATDbits.LATD4 = 1//DDS1

#define ad9914_io_update_set()  LATDbits.LATD7 = 1;
#define ad9914_io_update_clr()  LATDbits.LATD7 = 0;

//#define ad9914_pow_on_1()         LATDbits.LATD7 = 0;//LATCbits.LATC2 = 1
//#define ad9914_pow_off_1()        LATDbits.LATD7 = 1;//LATCbits.LATC2 = 0

#define ad9914_pow_on_2()         LATDbits.LATD9 = 0;//LATCbits.LATC2 = 1
#define ad9914_pow_off_2()        LATDbits.LATD9 = 1;//LATCbits.LATC2 = 0


/* 
 * Reg's read/write functions
 */

void ad9914_write_reg(uint8_t reg, void *buf, uint32_t reg_size);
void ad9914_read_reg(uint8_t reg, void *buf, uint32_t reg_size);

/* 
 * Control functions 
 */
uint8_t ad9914_spi_3wire_mode(void);
void ad9914_init_dac_cal(void);
void ad9914_set_profile_mode(void);
void ad9914_set_lfm_mode(void);
void ad9914_set_freq(uint8_t reg_addr, uint32_t freq_curr);
void ad9914_set_phase(uint8_t reg_addr, uint32_t phase_curr);
void ad9914_set_dds_st1(void);
void ad9914_dac_on(void);
void ad9914_dac_off(void);

#endif /* _AD9914_H */

