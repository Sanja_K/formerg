/*
 * File:   I2C.c
 * Author: Alexandr
 *
 * Created on 27 сентября 2017 г., 14:08
 */


#include "xc.h"
#include "I2C.h"
#include "system_cfg.h" 

void I2CInit(void)
{
    ODCGbits.ODCG2=1;
	ODCGbits.ODCG3=1;
    
    I2C1BRG = I2C_BRG;
    /* ACKEN enabled; STREN disabled; GCEN disabled; SMEN disabled; 
     * DISSLW enabled; I2CSIDL disabled; ACKDT Sends ACK; 
     * SCLREL Holds; RSEN disabled; IPMIEN disabled; A10M 7 Bit;
     *  PEN enabled; RCEN enabled; SEN disabled; I2CEN enabled */ 
    I2C1CON = 0x801C;
    // P disabled; S disabled; I2COV disabled; IWCOL disabled; 
    I2C1STAT = 0x0000;
}

void StartI2C(void)
{
     I2C1CONbits.SEN = 1;   /* initiate Start on SDA and SCL pins */
}

void RestartI2C(void)
{ 
    I2C1CONbits.RSEN = 1;   /* initiate restart on SDA and SCL pins */
}

void StopI2C(void)
{
     I2C1CONbits.PEN = 1;   /* initiate Stop on SDA and SCL pins */
}

void AckI2C(void)
{
    I2C1CONbits.ACKDT = 0;
    I2C1CONbits.ACKEN = 1;
}

void NotAckI2C(void)
{
    I2C1CONbits.ACKDT = 1;
    I2C1CONbits.ACKEN = 1;
}

void IdleI2C(void)
{
    /* Wait until I2C Bus is Inactive */
    while(I2C1CONbits.SEN || I2C1CONbits.RSEN || I2C1CONbits.PEN || I2C1CONbits.RCEN ||
          I2C1CONbits.ACKEN || I2C1STATbits.TRSTAT);  
}

void WriteI2C(unsigned char data)
{
    I2C1TRN = data;
    while(I2C1STATbits.TBF);
}

unsigned char ReadI2C(void)
{
    I2C1CONbits.RCEN = 1;
    while(I2C1CONbits.RCEN);
    I2C1STATbits.I2COV = 0;
    return(I2C1RCV);
}
