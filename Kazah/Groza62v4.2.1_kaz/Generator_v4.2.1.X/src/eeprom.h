#ifndef _EEPROM_H    /* Guard against multiple inclusion */
#define _EEPROM_H

#include <xc.h>

#define EEPROM_CS           LATBbits.LATB2
#define EEPROM_CS_TRIS      TRISBbits.TRISB2  


#define EE_READ             0b00000011
#define EE_WRITE            0b00000010
#define EE_WRDI             0b00000100
#define EE_WREN             0b00000110
#define EE_RDSR             0b00000101
#define EE_WRSR             0b00000001

/* sizes in bytes*/
#define EE_PAGE_SIZE        16
#define EE_SIZE             1024

void eeprom_init(void);

void eeprom_write_byte(unsigned short addr, unsigned char byte);
void eeprom_write_array(unsigned short addr, unsigned char *byte, int size);

unsigned char eeprom_read_byte(unsigned short addr);
void eeprom_read_array(unsigned short addr, unsigned char *buf, int size);

void spi1_init(void);
unsigned char spi1_transmit(unsigned char byte);
void spi1_write_byte(unsigned char byte);
unsigned char spi1_read_byte(void);

#endif /* _EEPROM_H */

