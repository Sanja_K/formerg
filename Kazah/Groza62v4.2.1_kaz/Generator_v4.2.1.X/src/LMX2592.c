#include <xc.h>
#include <stdbool.h>
#include <math.h>
#include <p33FJ128GP710.h>
//#include "Declaration.h"
#include "softwareSPI.h"
#include "LMX2592.h"

struct Synthesizers LMX2592;
//==============================================================================
/*
0 - CSB1 - B11 - LE_ADF1 - LMX2572 - DA3
1 - CSB2 - B10 - LE_ADF2 - LMX2592 - DA16
2 - CSB3 - B13 - LE_ADF3 - LMX2572 - DA19
3 - CSB4 - B12 - LE_ADF4 - LMX2592 - DA34
*/
//==============================================================================
void Init_LMX2592 ( unsigned char synthesizer )
{ // 1800 ���
    RegWrite(synthesizer, 0x40, 0x0077);
    RegWrite(synthesizer, 0x3E, 0x0000);
    RegWrite(synthesizer, 0x3D, 0x0001);
    RegWrite(synthesizer, 0x3B, 0x0000);
    RegWrite(synthesizer, 0x30, 0x03FC);
    LMX2592.R47 = 0x00CF;
    RegWrite(synthesizer, 0x2F, LMX2592.R47);
    LMX2592.R46 = 0x0FE3;
    RegWrite(synthesizer, 0x2E, LMX2592.R46);
    LMX2592.R45 = 0x0000;
    RegWrite(synthesizer, 0x2D, LMX2592.R45);
    LMX2592.R44 = 0x0000;
    RegWrite(synthesizer, 0x2C, LMX2592.R44);
    RegWrite(synthesizer, 0x2B, 0x0000);
    RegWrite(synthesizer, 0x2A, 0x0000);
    RegWrite(synthesizer, 0x29, 0x03E8);
    RegWrite(synthesizer, 0x28, 0x0000);
    RegWrite(synthesizer, 0x27, 0x8204);
    LMX2592.R38 = 0x021C;
    RegWrite(synthesizer, 0x26, LMX2592.R38);
    LMX2592.R37 = 0x4000;
    RegWrite(synthesizer, 0x25, LMX2592.R37);
    LMX2592.R36 = 0x0011;
    RegWrite(synthesizer, 0x24, LMX2592.R36); 
    LMX2592.R35 = 0x021F;
    RegWrite(synthesizer, 0x23, LMX2592.R35);
    RegWrite(synthesizer, 0x22, 0xC3EA);
    RegWrite(synthesizer, 0x21, 0x2A0A);
    RegWrite(synthesizer, 0x20, 0x210A);
    LMX2592.R31 = 0x0601;
    RegWrite(synthesizer, 0x1F, LMX2592.R31);
    LMX2592.R30 = 0x0034;
    RegWrite(synthesizer, 0x1E, LMX2592.R30);
    RegWrite(synthesizer, 0x1D, 0x0084);
    RegWrite(synthesizer, 0x1C, 0x2924);
    RegWrite(synthesizer, 0x19, 0x0000);
    RegWrite(synthesizer, 0x18, 0x0509);
    RegWrite(synthesizer, 0x17, 0x8842);
    RegWrite(synthesizer, 0x16, 0x2300);
    RegWrite(synthesizer, 0x14, 0x012C);
    RegWrite(synthesizer, 0x13, 0x0965);
//    RegWrite(synthesizer, 0x12, 0x0064);
    RegWrite(synthesizer, 0x0E, 0x018C);
    RegWrite(synthesizer, 0x0D, 0x4000);
    // ��� ������� �������� ������� Fosc = 100��� Fpd =10���
    RegWrite(synthesizer, 0x0C, 0x7001); // ��������������� ������� �������� �� �����. ����. ��. ������� (11:0)
    RegWrite(synthesizer, 0x0B, 0x0058); // ��������������� ������� �������� ����� �����. ����. ��. ������� (11:4)
    RegWrite(synthesizer, 0x0A, 0x10D8);//0x10F8); // ��������������� ��������� ������� ������� (10:7)
    RegWrite(synthesizer, 0x09, 0x0B02);//0x0004); // ��������� ������� (12���)
    LMX2592.Fpd = 10;
    RegWrite(synthesizer, 0x08, 0x1084);
    LMX2592.R7 = 0x28B2;
    RegWrite(synthesizer, 0x07, LMX2592.R7);
    RegWrite(synthesizer, 0x04, 0x1943);
    RegWrite(synthesizer, 0x02, 0x0500);
    LMX2592.R1 = 0x0808;
    RegWrite(synthesizer, 0x01, LMX2592.R1);
    LMX2592.R0 = 0x221C;
    RegWrite(synthesizer, 0x00, LMX2592.R0);        
}
//==============================================================================
//==============================================================================
// CalibAmpl = 1 - �������������� ��������: ��� ���� �������� ���������� = 1660 ��� 
// CalibAmpl = 0 - ��� ���������� ��������: ��� ���� �������� ���������� = 600 ��� 
void LMX2592_Frequency_setting_MHz( unsigned char synthesizer, unsigned short int RFout, bool CalibAmpl )
{
    unsigned char total_div = 0;
    unsigned long int PLL_N=0;
    unsigned long int PLL_N_PRESCALAR=2;    
    unsigned long int VCO_DOUBLE=1;
    unsigned long int PLL_DEM = 1000;
    unsigned long int PLL_NUM = 0;
    
    unsigned short int PLL_NUM_H = 0;
    unsigned short int PLL_NUM_L = 0;    
    
    double PLL_NUM_double = 0;    
    double Fvco_double = 0;
    double Fpd_double = 0;
    double PLL_N_double = 0;
    double PLL_DEM_double = 0;
    
//    Channel_Devider_buffer = 6400/RFout;
//    Channel_Devider_buffer = 8000/RFout;    
            
    if ( (RFout>=20) && (RFout<28) ) 
        {//Outpu MUX = CHDIV
         LMX2592.R31 = LMX2592.R31 | 0x0200;
         LMX2592.R36 = LMX2592.R36 | 0x0400; 
         LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
         // Channel Divider MUX
         total_div = 192;
         LMX2592.R35 = 0x119F; 
         LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
         LMX2592.R36 = LMX2592.R36 | 0x0048;
         // VCO Double = X1
         VCO_DOUBLE=1;
         LMX2592.R30 = LMX2592.R30 & 0xFFFE;
         // Prescalar = 2
         PLL_N_PRESCALAR = 2;
         LMX2592.R37 = LMX2592.R37 & 0xEFFF;          
        }
    else    
        {
        if ( (RFout>=28) && (RFout<37) ) 
            {//Outpu MUX = CHDIV
             LMX2592.R31 = LMX2592.R31 | 0x0200;
             LMX2592.R36 = LMX2592.R36 | 0x0400; 
             LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
             // Channel Divider MUX
             total_div = 128;
             LMX2592.R35 = 0x119B; 
             LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
             LMX2592.R36 = LMX2592.R36 | 0x0048;
             // VCO Double = X1
             VCO_DOUBLE=1;
             LMX2592.R30 = LMX2592.R30 & 0xFFFE;
             // Prescalar = 2
             PLL_N_PRESCALAR = 2;
             LMX2592.R30 = LMX2592.R30 & 0xEFFF;              
            }
        else    
            {
            if ( (RFout>=37) && (RFout<56) ) 
                {//Outpu MUX = CHDIV
                 LMX2592.R31 = LMX2592.R31 | 0x0200;
                 LMX2592.R36 = LMX2592.R36 | 0x0400; 
                 LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
                 // Channel Divider MUX
                 total_div = 96;
                 LMX2592.R35 = 0x119B; 
                 LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
                 LMX2592.R36 = LMX2592.R36 | 0x0044;
                 // VCO Double = X1
                 VCO_DOUBLE=1;
                 LMX2592.R30 = LMX2592.R30 & 0xFFFE;
                 // Prescalar = 2
                 PLL_N_PRESCALAR = 2;
                 LMX2592.R37 = LMX2592.R37 & 0xEFFF;                  
                }
            else    
                {
                if ( (RFout>=56) && (RFout<74) ) 
                    {//Outpu MUX = CHDIV
                     LMX2592.R31 = LMX2592.R31 | 0x0200;
                     LMX2592.R36 = LMX2592.R36 | 0x0400; 
                     LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
                     // Channel Divider MUX
                     total_div = 64;
                     LMX2592.R35 = 0x119B; 
                     LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
                     LMX2592.R36 = LMX2592.R36 | 0x0042;
                     // VCO Double = X1
                     VCO_DOUBLE=1;
                     LMX2592.R30 = LMX2592.R30 & 0xFFFE;
                     // Prescalar = 2
                     PLL_N_PRESCALAR = 2;
                     LMX2592.R37 = LMX2592.R37 & 0xEFFF;                       
                    }
                else    
                    {
                    if ( (RFout>=74) && (RFout<99) ) 
                        {//Outpu MUX = CHDIV
                         LMX2592.R31 = LMX2592.R31 | 0x0200;
                         LMX2592.R36 = LMX2592.R36 | 0x0400; 
                         LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
                         // Channel Divider MUX
                         total_div = 48;
                         LMX2592.R35 = 0x119F; 
                         LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
                         LMX2592.R36 = LMX2592.R36 | 0x0041;
                         // VCO Double = X1
                         VCO_DOUBLE=1;
                         LMX2592.R30 = LMX2592.R30 & 0xFFFE;
                         // Prescalar = 2
                         PLL_N_PRESCALAR = 2;
                         LMX2592.R37 = LMX2592.R37 & 0xEFFF;                           
                        }
                    else    
                        {
                        if ( (RFout>=99) && (RFout<111) ) 
                            {//Outpu MUX = CHDIV
                             LMX2592.R31 = LMX2592.R31 | 0x0200;
                             LMX2592.R36 = LMX2592.R36 | 0x0400; 
                             LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
                             // Channel Divider MUX
                             total_div = 36;
                             LMX2592.R35 = 0x099F; 
                             LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
                             LMX2592.R36 = LMX2592.R36 | 0x0041;
                             // VCO Double = X1
                             VCO_DOUBLE=1;
                             LMX2592.R30 = LMX2592.R30 & 0xFFFE;
                             // Prescalar = 2
                             PLL_N_PRESCALAR = 2;
                             LMX2592.R37 = LMX2592.R37 & 0xEFFF;                               
                            }
                        else    
                            {
                            if ( (RFout>=111) && (RFout<148) ) 
                                {//Outpu MUX = CHDIV
                                 LMX2592.R31 = LMX2592.R31 | 0x0200;
                                 LMX2592.R36 = LMX2592.R36 | 0x0400; 
                                 LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
                                 // Channel Divider MUX
                                 total_div = 32;
                                 LMX2592.R35 = 0x119B; 
                                 LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
                                 LMX2592.R36 = LMX2592.R36 | 0x0041;
                                 // VCO Double = X1
                                 VCO_DOUBLE=1;
                                 LMX2592.R30 = LMX2592.R30 & 0xFFFE;
                                 // Prescalar = 2
                                 PLL_N_PRESCALAR = 2;
                                 LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                  
                                }
                            else    
                                {
                                if ( (RFout>=148) && (RFout<222) ) 
                                    {//Outpu MUX = CHDIV
                                     LMX2592.R31 = LMX2592.R31 | 0x0200;
                                     LMX2592.R36 = LMX2592.R36 | 0x0400; 
                                     LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
                                     // Channel Divider MUX
                                     total_div = 24;
                                     LMX2592.R35 = 0x109F; 
                                     LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
                                     LMX2592.R36 = LMX2592.R36 | 0x0021;
                                     // VCO Double = X1
                                     VCO_DOUBLE=1;
                                     LMX2592.R30 = LMX2592.R30 & 0xFFFE;
                                     // Prescalar = 2
                                     PLL_N_PRESCALAR = 2;
                                     LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                       
                                    }
                                else    
                                    {
                                    if ( (RFout>=222) && (RFout<296) ) 
                                        {//Outpu MUX = CHDIV
                                         LMX2592.R31 = LMX2592.R31 | 0x0200;
                                         LMX2592.R36 = LMX2592.R36 | 0x0400; 
                                         LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
                                         // Channel Divider MUX
                                         total_div = 16;
                                         LMX2592.R35 = 0x109B; 
                                         LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
                                         LMX2592.R36 = LMX2592.R36 | 0x0021;
                                         // VCO Double = X1
                                         VCO_DOUBLE=1;
                                         LMX2592.R30 = LMX2592.R30 & 0xFFFE;
                                         // Prescalar = 2
                                         PLL_N_PRESCALAR = 2;
                                         LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                           
                                        }
                                    else    
                                        {
                                      if ( (RFout>=296) && (RFout<444) ) 
                                            {//Outpu MUX = CHDIV
                                             LMX2592.R31 = LMX2592.R31 | 0x0200;
                                             LMX2592.R36 = LMX2592.R36 | 0x0400; 
                                             LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
                                             // Channel Divider MUX
                                             total_div = 12;
                                             LMX2592.R35 = 0x089B; 
                                             LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
                                             LMX2592.R36 = LMX2592.R36 | 0x0021;
                                             // VCO Double = X1
                                             VCO_DOUBLE=1;
                                             LMX2592.R30 = LMX2592.R30 & 0xFFFE;
                                             // Prescalar = 2
                                             PLL_N_PRESCALAR = 2;
                                             LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                              
                                            }
                                        else    
                                            {
                                            if ( (RFout>=444) && (RFout<592) ) 
                                                {//Outpu MUX = CHDIV
                                                 LMX2592.R31 = LMX2592.R31 | 0x0200;
                                                 LMX2592.R36 = LMX2592.R36 | 0x0400; 
                                                 LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
                                                 // Channel Divider MUX
                                                 total_div = 8;
                                                 LMX2592.R35 = 0x049B; 
                                                 LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
                                                 LMX2592.R36 = LMX2592.R36 | 0x0021;
                                                 // VCO Double = X1
                                                 VCO_DOUBLE=1;
                                                 LMX2592.R30 = LMX2592.R30 & 0xFFFE;
                                                 // Prescalar = 2
                                                 PLL_N_PRESCALAR = 2;
                                                 LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                                   
                                                }
                                            else    
                                                {
                                                if ( (RFout>=592) && (RFout<888) ) 
                                                    {//Outpu MUX = CHDIV
                                                     LMX2592.R31 = LMX2592.R31 | 0x0200;
                                                     LMX2592.R36 = LMX2592.R36 | 0x0400; 
                                                     LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
                                                     // Channel Divider MUX
                                                     total_div = 6;
                                                     LMX2592.R35 = 0x029F; 
                                                     LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
                                                     LMX2592.R36 = LMX2592.R36 | 0x0021;
                                                     // VCO Double = X1
                                                     VCO_DOUBLE=1;
                                                     LMX2592.R30 = LMX2592.R30 & 0xFFFE;
                                                     // Prescalar = 2
                                                     PLL_N_PRESCALAR = 2;
                                                     LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                                      
                                                    }
                                                else    
                                                    {
                                                    if ( (RFout>=888) && (RFout<1184) ) 
                                                        {//Outpu MUX = CHDIV
                                                         LMX2592.R31 = LMX2592.R31 | 0x0200;
                                                         LMX2592.R36 = LMX2592.R36 | 0x0400; 
                                                         LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
                                                         // Channel Divider MUX
                                                         total_div = 4;
                                                         LMX2592.R35 = 0x029B; 
                                                         LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
                                                         LMX2592.R36 = LMX2592.R36 | 0x0021;
                                                         // VCO Double = X1
                                                         VCO_DOUBLE=1;
                                                         LMX2592.R30 = LMX2592.R30 & 0xFFFE;
                                                         // Prescalar = 2
                                                         PLL_N_PRESCALAR = 2;
                                                         LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                                          
                                                        }
                                                    else    
                                                        {
                                                        if ( (RFout>=1184) && (RFout<1775) ) 
                                                            {//Outpu MUX = CHDIV
                                                             LMX2592.R31 = LMX2592.R31 | 0x0200;
                                                             LMX2592.R36 = LMX2592.R36 | 0x0400; 
                                                             LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
                                                             // Channel Divider MUX
                                                             total_div = 3;
                                                             LMX2592.R35 = 0x021F; 
                                                             LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
                                                             LMX2592.R36 = LMX2592.R36 | 0x0011;
                                                             // VCO Double = X1
                                                             VCO_DOUBLE=1;
                                                             LMX2592.R30 = LMX2592.R30 & 0xFFFE;
                                                             // Prescalar = 2
                                                             PLL_N_PRESCALAR = 2;
                                                             LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                                             
                                                            }
                                                        else    
                                                            {
                                                            if ( (RFout>=1775) && (RFout<3550) ) 
                                                                {//Outpu MUX = CHDIV
                                                                 LMX2592.R31 = LMX2592.R31 | 0x0200;
                                                                 LMX2592.R36 = LMX2592.R36 | 0x0400; 
                                                                 LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
                                                                 // Channel Divider MUX
                                                                 total_div = 2;
                                                                 LMX2592.R35 = 0x021B; 
                                                                 LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
                                                                 LMX2592.R36 = LMX2592.R36 | 0x0011;
                                                                 // VCO Double = X1
                                                                 VCO_DOUBLE=1;
                                                                 LMX2592.R30 = LMX2592.R30 & 0xFFFE;
                                                                 // Prescalar = 2
                                                                 PLL_N_PRESCALAR = 2;
                                                                 LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                                                 
                                                                }
                                                            else    
                                                                {
                                                                if ( (RFout>=3550) && (RFout<=7100) ) 
                                                                    {//Outpu MUX = VCO
                                                                     LMX2592.R31 = LMX2592.R31 & 0xFDFF;
                                                                     LMX2592.R36 = LMX2592.R36 & 0xFBFF; 
                                                                     LMX2592.R47 = LMX2592.R47 | 0x0800;
                                                                     // VCO Double = X1
                                                                     VCO_DOUBLE=1;
                                                                     LMX2592.R30 = LMX2592.R30 & 0xFFFE;
                                                                     // Prescalar = 2
                                                                     PLL_N_PRESCALAR = 2;
                                                                     LMX2592.R37 = LMX2592.R37 & 0xEFFF;
                                                                     total_div = 1;
                                                                    }
                                                                else    
                                                                    {//Outpu MUX = VCO
                                                                     LMX2592.R31 = LMX2592.R31 & 0xFDFF;
                                                                     LMX2592.R36 = LMX2592.R36 & 0xFBFF; 
                                                                     LMX2592.R47 = LMX2592.R47 | 0x0800;
                                                                     // VCO Double = X2
                                                                     VCO_DOUBLE=2;
                                                                     LMX2592.R30 = LMX2592.R30 | 0x0001;
                                                                     // Prescalar = 4
                                                                     PLL_N_PRESCALAR = 4;
                                                                     LMX2592.R37 = LMX2592.R37 | 0x1000;
                                                                     total_div = 1;
                                                                    }                                                                
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                } 
            }            
        }

    Fvco_double = (RFout * total_div);// / VCO_DOUBLE;
    
    PLL_N = Fvco_double/(LMX2592.Fpd*PLL_N_PRESCALAR);
    
    Fpd_double = LMX2592.Fpd;
    PLL_N_double = PLL_N;
    PLL_DEM_double = PLL_DEM;
    PLL_NUM_double = (Fvco_double/(Fpd_double*PLL_N_PRESCALAR) - PLL_N_double)*PLL_DEM_double;
    
    PLL_NUM = PLL_NUM_double;
    
    PLL_N = PLL_N<<1;
    PLL_N = PLL_N & 0x1FFE;
    LMX2592.R38 = PLL_N;
   
    PLL_NUM_L = PLL_NUM;
    LMX2592.R45 = PLL_NUM_L;
    PLL_NUM = PLL_NUM>>16;
    PLL_NUM_H = PLL_NUM;
    LMX2592.R44 = PLL_NUM_H;
    
    if ( CalibAmpl == 0 )
    {
        LMX2592.R0 = LMX2592.R0 & 0xFFEF; // fast  // ��������� ���������� ��������� � ACAL_EN = 0 
        RegWrite(synthesizer, 0x13, 0x0AF5);// fast  // ������� ������� ��������� � ������� VCO_IDAC (��������, 350)   
    }
    else
    {
        LMX2592.R0 = LMX2592.R0 | 0x0010;// �������� ���������� ��������� � ACAL_EN = 1 
    }    

    RegWrite(synthesizer, 0x40, 0x0377);// fast  // ��� ������� ���������� ������������� �������� FCAL_FAST = 1 � ACAL_FAST = 1
//    RegWrite(synthesizer, 0x04, 0x0543);// fast  // �������� ����������� �������� ����, ������������� ��������� ACAL_CMP_DLY (5 � ���� �������)     
//    RegWrite(synthesizer, 0x14, 0x015E);// fast  // ACAL_VCO_IDAC_STRT ��������� ������������ �������, � ����� ��������� ��� �������� �� ����� ���������� ���������    
    RegWrite(synthesizer, 0x2F, LMX2592.R47);
    RegWrite(synthesizer, 0x2D, LMX2592.R45);//
    RegWrite(synthesizer, 0x2C, LMX2592.R44);//
    RegWrite(synthesizer, 0x26, LMX2592.R38);//
    RegWrite(synthesizer, 0x25, LMX2592.R37);
    RegWrite(synthesizer, 0x24, LMX2592.R36);    
    RegWrite(synthesizer, 0x23, LMX2592.R35);
    RegWrite(synthesizer, 0x1F, LMX2592.R31);
    RegWrite(synthesizer, 0x1E, LMX2592.R30);
   
    Nop();
    Nop();
    Nop();    
    
    RegWrite(synthesizer, 0x00, LMX2592.R0); // ��� ������� ���������� VCO
}
//==============================================================================

//
////==============================================================================
//void LMX2592_Frequency_setting_MHz( unsigned char synthesizer, unsigned short int RFout )
//{
//    unsigned char total_div = 0;
//    unsigned long int PLL_N=0;
//    unsigned long int PLL_N_PRESCALAR=2;    
//    unsigned long int VCO_DOUBLE=1;
//    unsigned long int PLL_DEM = 1000;
//    unsigned long int PLL_NUM = 0;
//    
//    unsigned short int PLL_NUM_H = 0;
//    unsigned short int PLL_NUM_L = 0;    
//    
//    double PLL_NUM_double = 0;    
//    double Fvco_double = 0;
//    double Fpd_double = 0;
//    double PLL_N_double = 0;
//    double PLL_DEM_double = 0;
//    
////    Channel_Devider_buffer = 6400/RFout;
////    Channel_Devider_buffer = 8000/RFout;    
//            
//    if ( (RFout>=20) && (RFout<28) ) 
//        {//Outpu MUX = CHDIV
//         LMX2592.R31 = LMX2592.R31 | 0x0200;
//         LMX2592.R36 = LMX2592.R36 | 0x0400; 
//         LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
//         // Channel Divider MUX
//         total_div = 192;
//         LMX2592.R35 = 0x119F; 
//         LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
//         LMX2592.R36 = LMX2592.R36 | 0x0048;
//         // VCO Double = X1
//         VCO_DOUBLE=1;
//         LMX2592.R30 = LMX2592.R30 & 0xFFFE;
//         // Prescalar = 2
//         PLL_N_PRESCALAR = 2;
//         LMX2592.R37 = LMX2592.R37 & 0xEFFF;          
//        }
//    else    
//        {
//        if ( (RFout>=28) && (RFout<37) ) 
//            {//Outpu MUX = CHDIV
//             LMX2592.R31 = LMX2592.R31 | 0x0200;
//             LMX2592.R36 = LMX2592.R36 | 0x0400; 
//             LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
//             // Channel Divider MUX
//             total_div = 128;
//             LMX2592.R35 = 0x119B; 
//             LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
//             LMX2592.R36 = LMX2592.R36 | 0x0048;
//             // VCO Double = X1
//             VCO_DOUBLE=1;
//             LMX2592.R30 = LMX2592.R30 & 0xFFFE;
//             // Prescalar = 2
//             PLL_N_PRESCALAR = 2;
//             LMX2592.R30 = LMX2592.R30 & 0xEFFF;              
//            }
//        else    
//            {
//            if ( (RFout>=37) && (RFout<56) ) 
//                {//Outpu MUX = CHDIV
//                 LMX2592.R31 = LMX2592.R31 | 0x0200;
//                 LMX2592.R36 = LMX2592.R36 | 0x0400; 
//                 LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
//                 // Channel Divider MUX
//                 total_div = 96;
//                 LMX2592.R35 = 0x119B; 
//                 LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
//                 LMX2592.R36 = LMX2592.R36 | 0x0044;
//                 // VCO Double = X1
//                 VCO_DOUBLE=1;
//                 LMX2592.R30 = LMX2592.R30 & 0xFFFE;
//                 // Prescalar = 2
//                 PLL_N_PRESCALAR = 2;
//                 LMX2592.R37 = LMX2592.R37 & 0xEFFF;                  
//                }
//            else    
//                {
//                if ( (RFout>=56) && (RFout<74) ) 
//                    {//Outpu MUX = CHDIV
//                     LMX2592.R31 = LMX2592.R31 | 0x0200;
//                     LMX2592.R36 = LMX2592.R36 | 0x0400; 
//                     LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
//                     // Channel Divider MUX
//                     total_div = 64;
//                     LMX2592.R35 = 0x119B; 
//                     LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
//                     LMX2592.R36 = LMX2592.R36 | 0x0042;
//                     // VCO Double = X1
//                     VCO_DOUBLE=1;
//                     LMX2592.R30 = LMX2592.R30 & 0xFFFE;
//                     // Prescalar = 2
//                     PLL_N_PRESCALAR = 2;
//                     LMX2592.R37 = LMX2592.R37 & 0xEFFF;                       
//                    }
//                else    
//                    {
//                    if ( (RFout>=74) && (RFout<99) ) 
//                        {//Outpu MUX = CHDIV
//                         LMX2592.R31 = LMX2592.R31 | 0x0200;
//                         LMX2592.R36 = LMX2592.R36 | 0x0400; 
//                         LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
//                         // Channel Divider MUX
//                         total_div = 48;
//                         LMX2592.R35 = 0x119F; 
//                         LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
//                         LMX2592.R36 = LMX2592.R36 | 0x0041;
//                         // VCO Double = X1
//                         VCO_DOUBLE=1;
//                         LMX2592.R30 = LMX2592.R30 & 0xFFFE;
//                         // Prescalar = 2
//                         PLL_N_PRESCALAR = 2;
//                         LMX2592.R37 = LMX2592.R37 & 0xEFFF;                           
//                        }
//                    else    
//                        {
//                        if ( (RFout>=99) && (RFout<111) ) 
//                            {//Outpu MUX = CHDIV
//                             LMX2592.R31 = LMX2592.R31 | 0x0200;
//                             LMX2592.R36 = LMX2592.R36 | 0x0400; 
//                             LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
//                             // Channel Divider MUX
//                             total_div = 36;
//                             LMX2592.R35 = 0x099F; 
//                             LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
//                             LMX2592.R36 = LMX2592.R36 | 0x0041;
//                             // VCO Double = X1
//                             VCO_DOUBLE=1;
//                             LMX2592.R30 = LMX2592.R30 & 0xFFFE;
//                             // Prescalar = 2
//                             PLL_N_PRESCALAR = 2;
//                             LMX2592.R37 = LMX2592.R37 & 0xEFFF;                               
//                            }
//                        else    
//                            {
//                            if ( (RFout>=111) && (RFout<148) ) 
//                                {//Outpu MUX = CHDIV
//                                 LMX2592.R31 = LMX2592.R31 | 0x0200;
//                                 LMX2592.R36 = LMX2592.R36 | 0x0400; 
//                                 LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
//                                 // Channel Divider MUX
//                                 total_div = 32;
//                                 LMX2592.R35 = 0x119B; 
//                                 LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
//                                 LMX2592.R36 = LMX2592.R36 | 0x0041;
//                                 // VCO Double = X1
//                                 VCO_DOUBLE=1;
//                                 LMX2592.R30 = LMX2592.R30 & 0xFFFE;
//                                 // Prescalar = 2
//                                 PLL_N_PRESCALAR = 2;
//                                 LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                  
//                                }
//                            else    
//                                {
//                                if ( (RFout>=148) && (RFout<222) ) 
//                                    {//Outpu MUX = CHDIV
//                                     LMX2592.R31 = LMX2592.R31 | 0x0200;
//                                     LMX2592.R36 = LMX2592.R36 | 0x0400; 
//                                     LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
//                                     // Channel Divider MUX
//                                     total_div = 24;
//                                     LMX2592.R35 = 0x109F; 
//                                     LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
//                                     LMX2592.R36 = LMX2592.R36 | 0x0021;
//                                     // VCO Double = X1
//                                     VCO_DOUBLE=1;
//                                     LMX2592.R30 = LMX2592.R30 & 0xFFFE;
//                                     // Prescalar = 2
//                                     PLL_N_PRESCALAR = 2;
//                                     LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                       
//                                    }
//                                else    
//                                    {
//                                    if ( (RFout>=222) && (RFout<296) ) 
//                                        {//Outpu MUX = CHDIV
//                                         LMX2592.R31 = LMX2592.R31 | 0x0200;
//                                         LMX2592.R36 = LMX2592.R36 | 0x0400; 
//                                         LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
//                                         // Channel Divider MUX
//                                         total_div = 16;
//                                         LMX2592.R35 = 0x109B; 
//                                         LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
//                                         LMX2592.R36 = LMX2592.R36 | 0x0021;
//                                         // VCO Double = X1
//                                         VCO_DOUBLE=1;
//                                         LMX2592.R30 = LMX2592.R30 & 0xFFFE;
//                                         // Prescalar = 2
//                                         PLL_N_PRESCALAR = 2;
//                                         LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                           
//                                        }
//                                    else    
//                                        {
//                                      if ( (RFout>=296) && (RFout<444) ) 
//                                            {//Outpu MUX = CHDIV
//                                             LMX2592.R31 = LMX2592.R31 | 0x0200;
//                                             LMX2592.R36 = LMX2592.R36 | 0x0400; 
//                                             LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
//                                             // Channel Divider MUX
//                                             total_div = 12;
//                                             LMX2592.R35 = 0x089B; 
//                                             LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
//                                             LMX2592.R36 = LMX2592.R36 | 0x0021;
//                                             // VCO Double = X1
//                                             VCO_DOUBLE=1;
//                                             LMX2592.R30 = LMX2592.R30 & 0xFFFE;
//                                             // Prescalar = 2
//                                             PLL_N_PRESCALAR = 2;
//                                             LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                              
//                                            }
//                                        else    
//                                            {
//                                            if ( (RFout>=444) && (RFout<592) ) 
//                                                {//Outpu MUX = CHDIV
//                                                 LMX2592.R31 = LMX2592.R31 | 0x0200;
//                                                 LMX2592.R36 = LMX2592.R36 | 0x0400; 
//                                                 LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
//                                                 // Channel Divider MUX
//                                                 total_div = 8;
//                                                 LMX2592.R35 = 0x049B; 
//                                                 LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
//                                                 LMX2592.R36 = LMX2592.R36 | 0x0021;
//                                                 // VCO Double = X1
//                                                 VCO_DOUBLE=1;
//                                                 LMX2592.R30 = LMX2592.R30 & 0xFFFE;
//                                                 // Prescalar = 2
//                                                 PLL_N_PRESCALAR = 2;
//                                                 LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                                   
//                                                }
//                                            else    
//                                                {
//                                                if ( (RFout>=592) && (RFout<888) ) 
//                                                    {//Outpu MUX = CHDIV
//                                                     LMX2592.R31 = LMX2592.R31 | 0x0200;
//                                                     LMX2592.R36 = LMX2592.R36 | 0x0400; 
//                                                     LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
//                                                     // Channel Divider MUX
//                                                     total_div = 6;
//                                                     LMX2592.R35 = 0x029F; 
//                                                     LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
//                                                     LMX2592.R36 = LMX2592.R36 | 0x0021;
//                                                     // VCO Double = X1
//                                                     VCO_DOUBLE=1;
//                                                     LMX2592.R30 = LMX2592.R30 & 0xFFFE;
//                                                     // Prescalar = 2
//                                                     PLL_N_PRESCALAR = 2;
//                                                     LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                                      
//                                                    }
//                                                else    
//                                                    {
//                                                    if ( (RFout>=888) && (RFout<1184) ) 
//                                                        {//Outpu MUX = CHDIV
//                                                         LMX2592.R31 = LMX2592.R31 | 0x0200;
//                                                         LMX2592.R36 = LMX2592.R36 | 0x0400; 
//                                                         LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
//                                                         // Channel Divider MUX
//                                                         total_div = 4;
//                                                         LMX2592.R35 = 0x029B; 
//                                                         LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
//                                                         LMX2592.R36 = LMX2592.R36 | 0x0021;
//                                                         // VCO Double = X1
//                                                         VCO_DOUBLE=1;
//                                                         LMX2592.R30 = LMX2592.R30 & 0xFFFE;
//                                                         // Prescalar = 2
//                                                         PLL_N_PRESCALAR = 2;
//                                                         LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                                          
//                                                        }
//                                                    else    
//                                                        {
//                                                        if ( (RFout>=1184) && (RFout<1775) ) 
//                                                            {//Outpu MUX = CHDIV
//                                                             LMX2592.R31 = LMX2592.R31 | 0x0200;
//                                                             LMX2592.R36 = LMX2592.R36 | 0x0400; 
//                                                             LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
//                                                             // Channel Divider MUX
//                                                             total_div = 3;
//                                                             LMX2592.R35 = 0x021F; 
//                                                             LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
//                                                             LMX2592.R36 = LMX2592.R36 | 0x0011;
//                                                             // VCO Double = X1
//                                                             VCO_DOUBLE=1;
//                                                             LMX2592.R30 = LMX2592.R30 & 0xFFFE;
//                                                             // Prescalar = 2
//                                                             PLL_N_PRESCALAR = 2;
//                                                             LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                                             
//                                                            }
//                                                        else    
//                                                            {
//                                                            if ( (RFout>=1775) && (RFout<3550) ) 
//                                                                {//Outpu MUX = CHDIV
//                                                                 LMX2592.R31 = LMX2592.R31 | 0x0200;
//                                                                 LMX2592.R36 = LMX2592.R36 | 0x0400; 
//                                                                 LMX2592.R47 = LMX2592.R47 & 0xF7FF; 
//                                                                 // Channel Divider MUX
//                                                                 total_div = 2;
//                                                                 LMX2592.R35 = 0x021B; 
//                                                                 LMX2592.R36 = LMX2592.R36 & 0x0C00; // �������� ���� ��������� Channel Divider  
//                                                                 LMX2592.R36 = LMX2592.R36 | 0x0011;
//                                                                 // VCO Double = X1
//                                                                 VCO_DOUBLE=1;
//                                                                 LMX2592.R30 = LMX2592.R30 & 0xFFFE;
//                                                                 // Prescalar = 2
//                                                                 PLL_N_PRESCALAR = 2;
//                                                                 LMX2592.R37 = LMX2592.R37 & 0xEFFF;                                                                 
//                                                                }
//                                                            else    
//                                                                {
//                                                                if ( (RFout>=3550) && (RFout<=7100) ) 
//                                                                    {//Outpu MUX = VCO
//                                                                     LMX2592.R31 = LMX2592.R31 & 0xFDFF;
//                                                                     LMX2592.R36 = LMX2592.R36 & 0xFBFF; 
//                                                                     LMX2592.R47 = LMX2592.R47 | 0x0800;
//                                                                     // VCO Double = X1
//                                                                     VCO_DOUBLE=1;
//                                                                     LMX2592.R30 = LMX2592.R30 & 0xFFFE;
//                                                                     // Prescalar = 2
//                                                                     PLL_N_PRESCALAR = 2;
//                                                                     LMX2592.R37 = LMX2592.R37 & 0xEFFF;
//                                                                     total_div = 1;
//                                                                    }
//                                                                else    
//                                                                    {//Outpu MUX = VCO
//                                                                     LMX2592.R31 = LMX2592.R31 & 0xFDFF;
//                                                                     LMX2592.R36 = LMX2592.R36 & 0xFBFF; 
//                                                                     LMX2592.R47 = LMX2592.R47 | 0x0800;
//                                                                     // VCO Double = X2
//                                                                     VCO_DOUBLE=2;
//                                                                     LMX2592.R30 = LMX2592.R30 | 0x0001;
//                                                                     // Prescalar = 4
//                                                                     PLL_N_PRESCALAR = 4;
//                                                                     LMX2592.R37 = LMX2592.R37 | 0x1000;
//                                                                     total_div = 1;
//                                                                    }                                                                
//                                                                }
//                                                            }
//                                                        }
//                                                    }
//                                                }
//                                            }
//                                        }
//                                    }
//                                }
//                            }
//                        }
//                    }
//                } 
//            }            
//        }
//
//    Fvco_double = (RFout * total_div);// / VCO_DOUBLE;
//    
//    PLL_N = Fvco_double/(LMX2592.Fpd*PLL_N_PRESCALAR);
//    
//    Fpd_double = LMX2592.Fpd;
//    PLL_N_double = PLL_N;
//    PLL_DEM_double = PLL_DEM;
//    PLL_NUM_double = (Fvco_double/(Fpd_double*PLL_N_PRESCALAR) - PLL_N_double)*PLL_DEM_double;
//    
//    PLL_NUM = PLL_NUM_double;
//    
//    PLL_N = PLL_N<<1;
//    PLL_N = PLL_N & 0x1FFE;
//    LMX2592.R38 = PLL_N;
//   
//    PLL_NUM_L = PLL_NUM;
//    LMX2592.R45 = PLL_NUM_L;
//    PLL_NUM = PLL_NUM>>16;
//    PLL_NUM_H = PLL_NUM;
//    LMX2592.R44 = PLL_NUM_H;
//    
//    RegWrite(synthesizer, 0x2F, LMX2592.R47);
//    RegWrite(synthesizer, 0x2D, LMX2592.R45);
//    RegWrite(synthesizer, 0x2C, LMX2592.R44);
//    RegWrite(synthesizer, 0x26, LMX2592.R38);
//    RegWrite(synthesizer, 0x25, LMX2592.R37);
//    RegWrite(synthesizer, 0x24, LMX2592.R36);    
//    RegWrite(synthesizer, 0x23, LMX2592.R35);
//    RegWrite(synthesizer, 0x1F, LMX2592.R31);
//    RegWrite(synthesizer, 0x1E, LMX2592.R30);
//   
//    Nop();
//    Nop();
//    Nop();    
//    
//    RegWrite(synthesizer, 0x00, LMX2592.R0); // ��� ������� ���������� VCO
//}
//==============================================================================
// ���/���� RFoutB
// N = 1 - ���
// N = 0 - ����
void LMX2592_switch_RFoutB( unsigned char synthesizer, char N)  
{
    if ( N == 1 ) 
    {
//        LMX2592.R36 = LMX2592.R36 | 0x0800;
//        RegWrite(synthesizer, 0x24, LMX2592.R36);
        LMX2592.R46 = LMX2592.R46 & 0xFF7F;
        RegWrite(synthesizer, 0x2E, LMX2592.R46);
    }
    else 
    {
//        LMX2592.R36 = LMX2592.R36 & 0xF7FF;
//        RegWrite(synthesizer, 0x24, LMX2592.R36);
        LMX2592.R46 = LMX2592.R46 | 0x0080;
        RegWrite(synthesizer, 0x2E, LMX2592.R46);   
    }
//    Nop();
//    Nop();
//    Nop();     
}
//==============================================================================
// ���/���� RFoutA
// N = 1 - ���
// N = 0 - ����
void LMX2592_switch_RFoutA( unsigned char synthesizer, char N)  
{
    if ( N == 1 ) 
    {
//        LMX2592.R36 = LMX2592.R36 | 0x0400;
//        RegWrite(synthesizer, 0x24, LMX2592.R36);
        LMX2592.R46 = LMX2592.R46 & 0xFFBF;
        RegWrite(synthesizer, 0x2E, LMX2592.R46);        
    }
    else 
    {
//        LMX2592.R36 = LMX2592.R36 & 0xFBFF;
//        RegWrite(synthesizer, 0x24, LMX2592.R36);
        LMX2592.R46 = LMX2592.R46 | 0x0040;
        RegWrite(synthesizer, 0x2E, LMX2592.R46);   
    }
//    Nop();
//    Nop();
//    Nop();     
}
//==============================================================================
// power �� 0 �� 63 
void LMX2592_Power_A( unsigned char synthesizer, unsigned char power)
{
   unsigned short int REG_power;
   
    if ( power > 63 ) power = 63;   
   
   REG_power = power;
   REG_power = REG_power << 8;
   REG_power = REG_power & 0x3F00;
   LMX2592.R46 = LMX2592.R46 & 0xC0FF;
   LMX2592.R46 = LMX2592.R46 | REG_power;
    
    RegWrite(synthesizer, 0x2E, LMX2592.R46);    
//    Nop();
//    Nop();
//    Nop();     
}
//==============================================================================
// power �� 0 �� 63 
void LMX2592_Power_B( unsigned char synthesizer, unsigned char power)
{  
    if ( power > 63 ) power = 63;
    LMX2592.R47 = LMX2592.R47 & 0xFFC0;    
    LMX2592.R47 = LMX2592.R47 | power;   
    
    RegWrite(synthesizer, 0x2D, LMX2592.R47);
//    Nop();
//    Nop();
//    Nop();  
}
//==============================================================================
//==============================================================================
//  frequency - ��� ������� ������� ������������ � ������� LMX2592_Frequency_setting_MHz
/*
void LMX2592_Power_frequency_A( unsigned char synthesizer, unsigned short int frequency)
{
   unsigned short int REG_power;
   signed long int count_power;
   
   if ( synthesizer == 1 )
   { 
       count_power = frequency;
       if ( count_power < 5100) count_power = 0;
       else {if ( (count_power >= 5100) && (count_power < 5300) ) count_power = ( -32900 + 7*count_power ) / 1400;
            else { if ( (count_power >= 5300) && (count_power <= 5900) ) count_power = 5;   
                 else {if ( (count_power > 5900) && (count_power < 6500) ) count_power = ( -32900 + 7*count_power ) / 1400; 
                      else { if ( (count_power >= 6500) && (count_power < 7110) ) count_power = ( 44400 - 6*count_power ) / 600;   
                             else {if ( count_power >= 7110) count_power = 31;        
                                  }
                           }
                      }
                 }
            }
   }
    if ( count_power > 31 ) count_power = 31;   
    Nop();
    Nop();
    Nop();   
   REG_power = count_power;
   REG_power = REG_power << 8;
   REG_power = REG_power & 0x3F00;
   LMX2592.R46 = LMX2592.R46 & 0xC0FF;
   LMX2592.R46 = LMX2592.R46 | REG_power;
    
    RegWrite(synthesizer, 0x2E, LMX2592.R46);
}
*/
////==============================================================================
////  frequency - ��� ������� ������� ������������ � ������� LMX2592_Frequency_setting_MHz
//void LMX2592_Power_frequency_A( unsigned char synthesizer, unsigned short int frequency)
//{
//   unsigned short int REG_power;
//   signed long int count_power;
//   
//   if ( synthesizer == 1 )
//   { 
//       count_power = frequency;
//// ������ ������� - ����       
////       if ( count_power < 5100) count_power = 0;
////       else {if ( (count_power >= 5100) && (count_power < 5300) ) count_power = ( -32900 + 7*count_power ) / 1400;
////            else { if ( (count_power >= 5300) && (count_power <= 5900) ) count_power = 5;   
////                 else {if ( (count_power > 5900) && (count_power < 6500) ) count_power = ( -32900 + 7*count_power ) / 1400; 
////                      else { if ( (count_power >= 6500) && (count_power < 7110) ) count_power = ( 44400 - 6*count_power ) / 600;   
////                             else {if ( count_power >= 7110) count_power = 31;        
////                                  }
////                           }
////                      }
////                 }
////            }
//// ������, ������ �� ����������       
//       if ( count_power < 5200) count_power = 0;
//       else {if ( (count_power >= 5200) && (count_power < 6500) ) count_power = ( -131800 + 26*count_power ) / 1200;
//            else { if (count_power >= 65000) count_power = 31;   
//                 }
//            }       
//   }
//    if ( count_power > 31 ) count_power = 31;   
//    Nop();
//    Nop();
//    Nop();   
//   REG_power = count_power;
//   REG_power = REG_power << 8;
//   REG_power = REG_power & 0x3F00;
//   LMX2592.R46 = LMX2592.R46 & 0xC0FF;
//   LMX2592.R46 = LMX2592.R46 | REG_power;
//    
//    RegWrite(synthesizer, 0x2E, LMX2592.R46);
//}
////==============================================================================
//==============================================================================
//  frequency - ��� ������� ������� ������������ � ������� LMX2592_Frequency_setting_MHz
void LMX2592_Power_frequency_A( unsigned char synthesizer, unsigned short int frequency)
{
   unsigned short int REG_power;
   static signed long int count_power;
   
   if ( synthesizer == 1 )
   {        
    if ( (frequency >= 4400) && (frequency < 7000) )
        {
        count_power = 0.00385*frequency - 17;
        }
    else 
        {
        if ( (frequency >= 7000) && (frequency < 8000) )  
            count_power = 31;
        }
   }
    if ( count_power > 31 ) count_power = 31;   
    Nop();
    Nop();
    Nop();   
   REG_power = count_power;
   REG_power = REG_power << 8;
   REG_power = REG_power & 0x3F00;
   LMX2592.R46 = LMX2592.R46 & 0xC0FF;
   LMX2592.R46 = LMX2592.R46 | REG_power;
    
    RegWrite(synthesizer, 0x2E, LMX2592.R46);
}
//==============================================================================
//==============================================================================
