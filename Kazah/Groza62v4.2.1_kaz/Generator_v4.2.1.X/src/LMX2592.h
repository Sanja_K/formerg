/* Microchip Technology Inc. and its subsidiaries.  You may use this software 
 * and any derivatives exclusively with Microchip products. 
 * 
 * THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS".  NO WARRANTIES, WHETHER 
 * EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED 
 * WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A 
 * PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION 
 * WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
 *
 * IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
 * INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
 * WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS 
 * BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE 
 * FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS 
 * IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF 
 * ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.
 *
 * MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE 
 * TERMS. 
 */

/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef LMX2592_H
#define	LMX2592_H

#include <xc.h> // include processor files - each processor file is guarded.  

//
//    typedef enum {
//        syntLMX1  = 0,
//        syntLMX2,
//        syntLMX3,  
//        syntLMX4   
//    } enum_LMX2592_typeDef;

    

void Init_LMX2592 ( unsigned char synthesizer );
//void LMX2592_Frequency_setting_MHz( unsigned char synthesizer, unsigned short int RFout);
void LMX2592_Frequency_setting_MHz( unsigned char synthesizer, unsigned short int RFout, bool CalibAmpl);
void LMX2592_Power_frequency_A( unsigned char synthesizer, unsigned short int frequency);
void LMX2592_switch_RFoutA( unsigned char synthesizer, char N);
void LMX2592_switch_RFoutB( unsigned char synthesizer, char N);
void LMX2592_switch_RFoutB_multiply_A( unsigned char synthesizer, char N); 
void LMX2592_Power_A( unsigned char synthesizer, unsigned char power);
void LMX2592_Power_B( unsigned char synthesizer, unsigned char power);
#endif	/* XC_HEADER_TEMPLATE_H */

