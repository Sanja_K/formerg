//#include <math.h>
#include <p33FJ128GP710.h>
//#include "Declaration.h"
#include "softwareSPI.h"

void Init_LMX_Port (void)
{
    AD1PCFGL = 0xFFFF;
    AD1PCFGH = 0xFFFF;   
    AD2PCFGL = 0xFFFF;
    
// spi LMX2572    
    _TRISB15 = 0;   //SCK
    _TRISB14 = 0;   //MOSI

    _TRISB11 = 0;   //CSB1
    _TRISB10 = 0;   //CSB2
    _TRISB13 = 0;   //CSB3
    _TRISB12 = 0;   //CSB4

    CSB1 = 1;
    CSB2 = 1;
    CSB3 = 1;
    CSB4 = 1;
// power 3.3VCO1, 3.3VCO2
    _TRISD1 = 0;   //EN_PWR_CH1
//    EN_PWR_CH1 = 1;
// power 3.3VCO3, 3.3VCO4
    _TRISD14 = 0;   //EN_PWR_CH2
//    EN_PWR_CH2 = 1;  
}
