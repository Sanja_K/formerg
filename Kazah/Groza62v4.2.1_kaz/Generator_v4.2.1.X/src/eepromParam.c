/*
 * File:   eepromParam.c
 * Author: User
 *
 * Created on 31 ���� 2019 �., 11:04
 */


#include "xc.h"
#include "eeprom.h"
#include "eepromParam.h"
#include "system_cfg.h"

#define addreepromshift     16
// setaddreeprom

// (1) id (3) fn (3) fv (1)kn (1)kv (1)KK mod255  all 10byte

// odin dly raboty 
//uint8_t setparameeprom [28][cntPgEE]; //[ id ][]// Data//kk//      status = 1 Ok/0 nOK
//vtoroy dly zagruzki
uint8_t setParamEEprom [cntDtEE][cntPgEE]; //setParamEEprom[ id ][ ]/_____/id// Data//kk//status (= 1 Ok/0 nOK)//


uint8_t Save1EEParam(uint8_t IDdata, uint8_t *buf)
{
    
    uint8_t Status;
    unsigned int i;
    static uint8_t pageSetParam[cntPgEE];
    static uint8_t pageGetParam[cntPgEE];    
    unsigned short addrParam;
    unsigned int  sumEE;

    addrParam = IDdata*addreepromshift;   
    
    for (i=0; i < cntPgEE; i++) //dt �������
    {
        pageSetParam [i] = 0;            
    }       
    sumEE = 0;// ����������� ���     
    pageSetParam [0] = IDdata;
    sumEE = CalcCRC(sumEE, IDdata); 
    for (i = 1; i < cntInfDtEE ;i++ )
    {        
        pageSetParam [i] = buf[i]; 
        sumEE = CalcCRC(sumEE, pageSetParam [i]);         
    }          

    pageSetParam [cntInfDtEE] = sumEE;
    
//----------����� � ������------------------  
     
    eeprom_write_array(addrParam,pageSetParam,cntPgEE);    
    delay_ms (10);
//----------������� ������-------------------------     
    eeprom_read_array(addrParam,pageGetParam,cntPgEE);      
    
    sumEE = 0;// ����������� ���
    for (i = 0; i < cntInfDtEE ;i++ )
    {        
     sumEE = CalcCRC(sumEE, pageGetParam [i]);    
    }    
    
    if (sumEE != pageGetParam[cntInfDtEE])
    {
        Status = XST_FAILURE;
    }
    else
    {
        Status = XST_SUCCESS;
    }
    return Status;
}

uint8_t SaveAllEEParam(void)
{
    uint8_t Status;
    unsigned int i,j;
    static uint8_t pageSetParam[cntPgEE];
 
    for (j=0; j < cntDtEE; j++) //dt
    {    
        for (i=0; i < cntInfDtEE; i++) //dt
        {
            pageSetParam [i] = setParamEEprom [j][i];            
        }  
        
       Status = Save1EEParam(j, pageSetParam);
       if (Status == XST_SUCCESS)
       {
           setParamEEprom [j][cntInfDtEE+1] = 1;
       }
       else
       {
           setParamEEprom [j][cntInfDtEE+1] = 0;
       }
        
    }
    return Status;
}
void loadAllEEParam(void)
{
    unsigned int i,j;
    static uint8_t pageGetParam[cntPgEE]; 
//    unsigned char page1GetParam[cntdtEE];   
    unsigned short addr;
    
    unsigned int  sumEE;

    for (j=0; j < cntDtEE; j++) // id 
    {
        for (i=0; i < cntPgEE; i++) //dt
        {
            setParamEEprom[j][i]= 0;            
        }        
    }     
    sumEE = 0;// ����������� ���   
    addr = 0;   
    for (j=0; j < cntDtEE; j++) // id 
    {        
        eeprom_read_array(addr,pageGetParam,cntPgEE); 
        addr = addr + cntPgEE;  
        sumEE = 0;        
        for (i=0; i < cntInfDtEE; i++) //dt
        {
            sumEE = CalcCRC(sumEE, pageGetParam [i]);             
            setParamEEprom[j][i] = pageGetParam[i];   //chtenie dannyh parametrov i zapis v bufer            
        }                          
        setParamEEprom[j][cntInfDtEE] = pageGetParam[cntInfDtEE];
        // proveryem summu KK
        if (sumEE == pageGetParam[cntInfDtEE])
        {
            setParamEEprom[j][cntInfDtEE+1] = 1;        // set status OK / nOK
        }
        else
        {
            setParamEEprom[j][cntInfDtEE+1] = 0;         
        }   
    }    
}

