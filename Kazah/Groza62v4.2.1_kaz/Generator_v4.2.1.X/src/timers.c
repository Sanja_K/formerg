/*
 * File:   timers.c
 * Author: Alexandr
 *
 * Created on September 20, 2017, 4:53 PM
 */


#include "xc.h"
#include "system_cfg.h"
#include "timers.h"
#include "user.h"
#include "executeCMD.h"


unsigned int cntTMR;

unsigned int cntTMR21;// таймер 1 источника
unsigned int cntTMR22;// таймер 2 источника




void __attribute__((interrupt,no_auto_psv)) _T2Interrupt(void)
///void __attribute__((section("ADR_T4INT"),address(ADR_T4INT),__interrupt__, no_auto_psv)) _T4Interrupt(void)
{ // прерывание каждые 0,5мс

    if (cntTMR21>0)
        cntTMR21--;   // источник 1       
    
    if (cntTMR22>0)
         cntTMR22--;    // источник 2    
        
    if (cntTMR21 == 0)
	{          
        if (cntTMR22==0)
          IEC0bits.T2IE = 0;
            // излучение выкл первого источника
      //   PowerOffForm(1);
    }
    
    if (cntTMR22 == 0)
    {
         if (cntTMR21==0)
             IEC0bits.T2IE = 0; // выкл прерывания если первый таймер не активен 
       //  PowerOffForm(2);
     
    }

       // __builtin_btg((unsigned int *)&LATG, 15);
    TMR2 = 0;
    IFS0bits.T2IF = 0; // 
    //PORTCbits.RC14 = !PORTCbits.RC14;
}

void __attribute__((interrupt,no_auto_psv)) _T4Interrupt(void)
///void __attribute__((section("ADR_T4INT"),address(ADR_T4INT),__interrupt__, no_auto_psv)) _T4Interrupt(void)
{
    //IEC1bits.T4IE = 0;
    cntTMR++;
    if (cntTMR > 20)
	{
       RsNewRxDtUrt();//     
       IEC1bits.T4IE = 0;//
       
 
//        LATGbits.LATG15 = 1;
//        delay_ms(100);    
//        LATGbits.LATG15 = 0;       

    }
    TMR4 = 0;
    IFS1bits.T4IF = 0; // 
    //PORTCbits.RC14 = !PORTCbits.RC14;
}


void  InitTimer4(void)
{
    TMR4 = 0;
    PR4 = 0x0480;
    cntTMR = 0;// 
    IPC6bits.T4IP = 5;
    T4CON = 0b1000000000110000;
    IFS1bits.T4IF = 0;
    IEC1bits.T4IE = 0;

}

void  InitTimer2(void)
{
    TMR2 = 0;
    PR2 = 0x0138;
    cntTMR21 = 0;// 
    cntTMR22 = 0;//    
    IPC1bits.T2IP = 5;
    T2CON = 0b1000000000100000;
    IFS0bits.T2IF = 0;
    IEC0bits.T2IE = 0;

}
//=====