/*
 * File:   UART.c
 * Author: Alexandr Kazeka
 *
 * Created on September 20, 2017, 4:20 PM
 */

#include <xc.h>         /* XC8 General Include File */

#include <stdint.h>         /* For uint8_t definition */
#include <stdbool.h>        /* For true/false definition */
#include "user.h"
#include "system_cfg.h"
#include "I2C.h"
#include "DDS_AD9959.h"
#include "adl5240.h"
#include "hmc1122.h"
#include "UART.h"
#include "timers.h"
#include "executeCMD.h"
#include <math.h>
#include "XC_DATA.h"
#include "eeprom.h"
#include "eepromParam.h"
#include "test.h"
/******************************************************************************/
/* User Data                                                             */
/******************************************************************************/

extern unsigned char   gainN1; // ������ �������� ������������ ��� ������ � ������ �����
extern unsigned char   gainN2; // ������ �������� ������������ ��� ����� ������
extern unsigned char   gainN3; // ����������� ��� ������������� 3
extern unsigned char   gainN4; // ����������� ��� ������������� 3
extern unsigned char   gainN5; // ����������� ��� ������������� 3
extern int   gainK1; // ����������� ��� ������������� 1
extern int   gainK2; // ����������� ��� ������������� 2
extern int   gainK3; // ����������� ��� ������������� 2
extern int   gainK4; // ����������� ��� ������������� 4
extern int   gainK5; // ����������� ��� ������������� 4
//---------------UART DATA-------------------
unsigned char   addr_Dt;
unsigned char   cmd_Dt;
volatile unsigned char   cnt_Dt;
unsigned char   data_Urt[255];
unsigned char   crc_Urt;
unsigned char   err_CRC;

uint32_t FastLFM;
uint32_t FastDevLFM;
uint32_t FastLFM2;
uint32_t FastDevLFM2;

enum RxState_def { // ����� ������
	rx_addr,rx_inf, rx_crc
}RxUrtState; 


//-----------------------------

unsigned int        ki_Urt;
unsigned int        sumUrt;
extern unsigned int cntTMR;
extern unsigned int cntTMR22;
extern unsigned int cntTMR21;

extern unsigned char addrL, addrO;

unsigned char       flgURTok;


unsigned char Diapazon;
//-----------------------------------
/******************************************************************************/
/* User Functions                                                             */
/******************************************************************************/
/* <Initialize variables in user.h and insert code for user algorithms.> */
extern  LITERA_param_def LITERA_param;

uint8_t InitApp(void)
{  // char error_flg = 0;
//    uint8_t pageSetParam[cntPgEE];
//    uint8_t pageGetParam[cntPgEE];    
////    exeCMD34 ();
//    int icnt;
    
    InitTimer2();
    InitTimer4();    
    
    Init_XC(); 
    delay_ms (100);
    FastLFM = 0;
    FastLFM2 = 0;    
    off_all_pwr();
   //test();
//   adl5240_set_gain_amp(1);//   
 //   hmc1122_set_gain(1);
//    delay_ms (1);        
//    //-----------------------------------------------
//    LITERA_param.freq = 3500000; // kHz
//    LITERA_param .dev =  50000; // kHz
//    LITERA_param .manip =  100;     // kHz
//    exeCMD28() ;
//-----eeprom_init--------   
    spi1_init();
    
    eeprom_init();        
//    
//    eeprom_read_array(0,pageGetParam,cntPgEE); 
//        
//    delay_ms(20);
//    //---------------------
//      
//        for (icnt = 0; icnt < 16; icnt++) //dt �������
//        {
//            pageSetParam [icnt] = icnt;            
//        }       
//    delay_ms(20);
//        eeprom_write_array(0,pageSetParam,16);
//        
//    delay_ms(20);          
//        for (icnt=0; icnt < 16; icnt++) //dt �������
//        {
//            pageGetParam [icnt] = 0;            
//        }        
//    
//    eeprom_read_array(0,pageGetParam,16); 
//
//    delay_ms(20);
//        for (icnt=0; icnt < 16; icnt++) //dt �������
//        {
//            pageSetParam [icnt] = 0;            
//        }     
//    
//    delay_ms(20);     
//
//    eeprom_write_array(1,pageSetParam,16);
        
    //    return pageSetParam[7];
    //----------------------
    
    loadAllEEParam();

    
    delay_ms (1);

    //--------------------------
//    DDS2P6_XC_0;
 //   DDS1P3_XC_0;   
    //-----------------------------------   
    
 //   error_flg = Init_AD9959 (DDS1);
 //   _PWD_DDS1_1;
 //   error_flg = Init_AD9959 (DDS2);  
 //   _PWD_DDS2_1;

    
    //--------------------------------
    
    I2CInit();
    RsNewRxDtUrt(); // ����� ��� ������ ������  
    
    delay_ms (1);        
   //   setTest();
 
    //---------------------------------
   //     F_DDS = 100*MHz;
   //     Set_param_AD9959_test3 (F_DDS);
    //---------------------------------
    Init_UART_485();
    return 0;
}


void RsNewRxDtUrt(void)
{ uint8_t buff;
	sumUrt = 0;
	RxUrtState = rx_addr;
    cntTMR = 0;
	ki_Urt = 0;
    flgURTok = 0;
    TMR4 = 0;
    IFS1bits.T4IF = 0;
    IEC1bits.T4IE = 0;// ������ ���, ������ �� �����, ���� ����� ����
    while (U2STAbits.URXDA == 1)
    {
        buff  = U2RXREG;        
    }    
    IFS1bits.U2RXIF = 0;  
}
// ����� ����� �� UART
void ReceiveDtUrt(unsigned char bBf_Rx)
{
	flgURTok = 0;
    
    switch (RxUrtState) {
		case rx_addr:   //
            
         data_Urt [ki_Urt] = bBf_Rx;
         sumUrt = CalcCRC(sumUrt, bBf_Rx);         
         ki_Urt++;     

          if (ki_Urt == 3)
          { addr_Dt = data_Urt[0]; 
            cmd_Dt = data_Urt[1]; 
            cnt_Dt = data_Urt[2];
            if (cnt_Dt==0)
                RxUrtState = rx_crc; 
            else
                RxUrtState = rx_inf;//������� ����� ������� � ������ �������            
          }  
		break;
		case rx_inf:          
			data_Urt[ki_Urt] = bBf_Rx;
			sumUrt = CalcCRC(sumUrt, bBf_Rx);
            ki_Urt++;            
			if (ki_Urt == cnt_Dt+3) 
			{
				RxUrtState = rx_crc; // ��������� �� ����� crc
			}

        break;   
		case rx_crc:
            data_Urt[ki_Urt] = bBf_Rx;      
            ki_Urt++;
            err_CRC = 1;         
 			if (sumUrt == bBf_Rx) // ���� ������� CRC
			{ err_CRC = 0;}

				flgURTok = 1; // ��� ��������� ��� ����������
                AnlRxDtUrt(); // ������ �������� ������                
                RsNewRxDtUrt();
                      
        break;           
    }
}

// ������� ����� �� ����� �������, �������������� ������, ����� ��� ���������
void TransmitDtUrt (int CMD, unsigned char *infDt, int lenInfDt)
{
     int i, len_DT;
     
      //---------------- Send DATA ----------------       
        len_DT = 0;
        data_Urt [0] = addrO; // ����� ������
        len_DT++;
        
        data_Urt [1] = CMD; // �������
        len_DT++;
        
        data_Urt [2] = lenInfDt; // ����� ������
        len_DT++; 
        
        for (i = 0; i< lenInfDt; i++)
        {
           data_Urt [len_DT] = infDt [i];
           len_DT++;
        }
        //len_DT += lenInfDt;// ��������� ���������� ������
     //-------------��������� �� ------  

        sumUrt = 0;// ����������� ���
        for (i = 0; i < len_DT ;i++ )
        {        
         sumUrt = CalcCRC(sumUrt, data_Urt [i]);    
        }
        
        data_Urt [len_DT] = sumUrt;
        len_DT++;
        Send_UART_485P(data_Urt,len_DT);
}


void UserTaskApp (void)
{


    if (FastLFM == 2)
    {
        exeFastLfm_500_2500();    
    }
    else if (FastLFM2 == 2)
    {
         exeFastLfm_2500_6000();       
    }
    else
    {
        LED1_ON;
        ClrWdt();
        delay_ms(40);
        ClrWdt();    
        LED1_OFF;
        delay_ms(200);
        ClrWdt();   
        delay_ms(200);    
        ClrWdt();    
        delay_ms(200);
        ClrWdt();   
        delay_ms(200);    
        ClrWdt();      
        delay_ms(200);
        ClrWdt();   
        delay_ms(200);    
        ClrWdt();      
    }
    
    ClrWdt();
   
    
    if(U2STAbits.OERR)        
    {
        RsNewRxDtUrt();
        U2STAbits.OERR = 0;
    } 


}

