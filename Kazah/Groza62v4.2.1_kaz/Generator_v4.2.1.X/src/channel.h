/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef XC_channel_H
#define	XC_channel_H

#include <xc.h> // include processor files - each processor file is guarded.  
       
#define DA15RF1     1
#define DA15RF2     2

#define DA16RF1     1
#define DA16RF2     2

#define SW1DA18RF1     1
#define SW1DA18RF2     2
#define SW1DA18RF3     3
#define SW1DA18RF4     4

#define SW1DA19RF1     1
#define SW1DA19RF2     2

#define SW1DA29RF1     1
#define SW1DA29RF2     2

#define SW1DA30RF1     1
#define SW1DA30RF2     2
#define SW1DA30RF3     3
#define SW1DA30RF4     4

#define SW1DA34RF1     1 //test
#define SW1DA34RF2     2

#define SW1DA40RF1     1 //test
#define SW1DA40RF2     2

#define SW2DA3RF1     1
#define SW2DA3RF2     2
#define SW2DA3RF3     3
#define SW2DA3RF4     4

#define SW2DA11RF1     1
#define SW2DA11RF2     2
#define SW2DA11RF3     3
#define SW2DA11RF4     4

#define SW2DA14RF1     1
#define SW2DA14RF2     2

#define SW2DA21RF1     1
#define SW2DA21RF2     2
#define SW2DA21RF3     3
#define SW2DA21RF4     4

#define SW2DA29RF1     1 //test
#define SW2DA29RF2     2
#define SW2DA29RF3     3
#define SW2DA29RF4     4

#define SW2DA32RF1     1 
#define SW2DA32RF2     2


#define SWALLTEST     0 
#define SWRF1         1 
#define SWRFOUT       1 
#define SWRFL2        1 
#define SWRF2         2
#define SWRFL1        2
#define SWRF3         3

void initChannelPorts1(void);
void initChannelPorts2(void);

void SW1DA15(char sw);
void SW1DA16(char sw);
void SW1DA18(char sw);
void SW1DA19(char sw);
void SW1DA29(char sw);
void SW1DA30(char sw);
void SW1DA34(char sw);
void SW1DA40(char sw);
//-------------------------
void SW2DA3(char sw) ;
void SW2DA11(char sw);
void SW2DA14(char sw);
void SW2DA21(char sw);
void SW2DA29(char sw);
void SW2DA32(char sw);
void SWtestGNSS(char sw);

void SWtest100500(char sw);
void SWtest5002500(char sw);
void SWtest25006000(char sw);
#endif    //XC_channel_H