
#include <xc.h>
#include "system_cfg.h"
#include "softwareSPI.h"
#include "LMX2572.h"

struct Synthesizers LMX2572;
uint16_t Fmin[7] = {0, 3200, 3650, 4200, 4650, 5200, 5750};
uint16_t Fmax[7] = {0, 3650, 4200, 4650, 5200, 5750, 6400};
uint16_t Cmin[7] = {0, 131, 143, 135, 136, 133, 151};
uint16_t Cmax[7] = {0, 19, 25, 34, 25, 20, 27};
uint16_t Amin[7] = {0, 138, 162, 126, 195, 190, 256};
uint16_t Amax[7] = {0, 137, 142, 114, 172, 163, 204};

void DeInit_LMX2572 (  unsigned char synthesizer )
{
    LMX2572.R0 = 0x211E;
    RegWrite(synthesizer, 0x00, LMX2572.R0);  
    
}

//==============================================================================
/*
0 - CSB1 - B11 - LE_ADF1 - LMX2572 - DA3
1 - CSB2 - B10 - LE_ADF2 - LMX2592 - DA16
2 - CSB3 - B13 - LE_ADF3 - LMX2572 - DA19
3 - CSB4 - B12 - LE_ADF4 - LMX2592 - DA34
*/
//==============================================================================
void Init_LMX2572 (  unsigned char synthesizer )
{
    RegWrite(synthesizer, 0x7D, 0x2288);    
    RegWrite(synthesizer, 0x7C, 0x0000);
    RegWrite(synthesizer, 0x7B, 0x0000);
    RegWrite(synthesizer, 0x7A, 0x0000);
    RegWrite(synthesizer, 0x79, 0x0000);
    RegWrite(synthesizer, 0x78, 0x0000);
    RegWrite(synthesizer, 0x77, 0x0000);
    RegWrite(synthesizer, 0x76, 0x0000);
    RegWrite(synthesizer, 0x75, 0x0000);
    RegWrite(synthesizer, 0x74, 0x0000);
    RegWrite(synthesizer, 0x73, 0x0000);
    RegWrite(synthesizer, 0x72, 0x7802);
    RegWrite(synthesizer, 0x71, 0x0000);      
    RegWrite(synthesizer, 0x70, 0x0000);
    RegWrite(synthesizer, 0x6F, 0x0000);
    RegWrite(synthesizer, 0x6E, 0x0000);
    RegWrite(synthesizer, 0x6D, 0x0000);
    RegWrite(synthesizer, 0x6C, 0x0000);
    RegWrite(synthesizer, 0x6B, 0x0000);
    RegWrite(synthesizer, 0x6A, 0x0007);
    RegWrite(synthesizer, 0x69, 0x4440);    
    RegWrite(synthesizer, 0x68, 0x0000);
    RegWrite(synthesizer, 0x67, 0x0000);
    RegWrite(synthesizer, 0x66, 0x0000);
    RegWrite(synthesizer, 0x65, 0x0000);
    RegWrite(synthesizer, 0x64, 0x0000);
    RegWrite(synthesizer, 0x63, 0x0000);
    RegWrite(synthesizer, 0x62, 0x0000);
    RegWrite(synthesizer, 0x61, 0x0000);
    RegWrite(synthesizer, 0x60, 0x0000);
    RegWrite(synthesizer, 0x5F, 0x0000);
    RegWrite(synthesizer, 0x5E, 0x0000);
    RegWrite(synthesizer, 0x5D, 0x0000);
    RegWrite(synthesizer, 0x5C, 0x0000);
    RegWrite(synthesizer, 0x5B, 0x0000);
    RegWrite(synthesizer, 0x5A, 0x0000);
    RegWrite(synthesizer, 0x59, 0x0000);
    RegWrite(synthesizer, 0x58, 0x0000);
    RegWrite(synthesizer, 0x57, 0x0000);
    RegWrite(synthesizer, 0x56, 0x0000);
    RegWrite(synthesizer, 0x55, 0x0000);
    RegWrite(synthesizer, 0x54, 0x0000);
    RegWrite(synthesizer, 0x53, 0x0000);
    RegWrite(synthesizer, 0x52, 0x0000);
    RegWrite(synthesizer, 0x51, 0x0000);
    RegWrite(synthesizer, 0x50, 0x0000);
    RegWrite(synthesizer, 0x4F, 0x0000);
    RegWrite(synthesizer, 0x4E, 0x0001);    
    RegWrite(synthesizer, 0x4D, 0x0000);
    RegWrite(synthesizer, 0x4C, 0x000C);
    RegWrite(synthesizer, 0x4B, 0x0800);
//    RegWrite(0x4B, 0x08C0); // 1410
    RegWrite(synthesizer, 0x4A, 0x0000);
    RegWrite(synthesizer, 0x49, 0x003F);
    RegWrite(synthesizer, 0x48, 0x0001);
    RegWrite(synthesizer, 0x47, 0x0081);
    RegWrite(synthesizer, 0x46, 0xC350);    
    RegWrite(synthesizer, 0x45, 0x0000);
    RegWrite(synthesizer, 0x44, 0x03E8);
    RegWrite(synthesizer, 0x43, 0x0000);
    RegWrite(synthesizer, 0x42, 0x01F4);
    RegWrite(synthesizer, 0x41, 0x0000);
    RegWrite(synthesizer, 0x40, 0x1388);
    RegWrite(synthesizer, 0x3F, 0x0000);
    RegWrite(synthesizer, 0x3E, 0x00AF);
    RegWrite(synthesizer, 0x3D, 0x00A8);
    RegWrite(synthesizer, 0x3C, 0x03E8);
    RegWrite(synthesizer, 0x3B, 0x0001);
    RegWrite(synthesizer, 0x3A, 0x9001);
    RegWrite(synthesizer, 0x39, 0x0020);
    RegWrite(synthesizer, 0x38, 0x0000);
    RegWrite(synthesizer, 0x37, 0x0000);
    RegWrite(synthesizer, 0x36, 0x0000);
    RegWrite(synthesizer, 0x35, 0x0000);
    RegWrite(synthesizer, 0x34, 0x0421);
    RegWrite(synthesizer, 0x33, 0x0080);
    RegWrite(synthesizer, 0x32, 0x0080);
    RegWrite(synthesizer, 0x31, 0x4180);
    RegWrite(synthesizer, 0x30, 0x03E0);
    RegWrite(synthesizer, 0x2F, 0x0300);
    LMX2572.R46 = 0x07F0;    
    RegWrite(synthesizer, 0x2E, LMX2572.R46);
    LMX2572.R45 = 0xC61F;// 0xC0DF
    RegWrite(synthesizer, 0x2D, LMX2572.R45);
    LMX2572.R44 = 0x1FE3;
    RegWrite(synthesizer, 0x2C, LMX2572.R44);
    RegWrite(synthesizer, 0x2B, 0x0000);
    RegWrite(synthesizer, 0x2A, 0x0000);
    RegWrite(synthesizer, 0x29, 0x0000);
    RegWrite(synthesizer, 0x28, 0x0000);
    RegWrite(synthesizer, 0x27, 0x03E8);
    RegWrite(synthesizer, 0x26, 0x0000);
    RegWrite(synthesizer, 0x25, 0x0205);
    RegWrite(synthesizer, 0x24, 0x0190);
//    RegWrite(0x24, 0x0468);  // 1410  
    RegWrite(synthesizer, 0x23, 0x0004);
    RegWrite(synthesizer, 0x22, 0x0010);
    RegWrite(synthesizer, 0x21, 0x1E01);
    RegWrite(synthesizer, 0x20, 0x05BF);
    RegWrite(synthesizer, 0x1F, 0xC3E6);
    RegWrite(synthesizer, 0x1E, 0x18A6);
    RegWrite(synthesizer, 0x1D, 0x0000);
    RegWrite(synthesizer, 0x1C, 0x0488);
    RegWrite(synthesizer, 0x1B, 0x0002);
    RegWrite(synthesizer, 0x1A, 0x0808);
    RegWrite(synthesizer, 0x19, 0x0624);
    RegWrite(synthesizer, 0x18, 0x071A);
    RegWrite(synthesizer, 0x17, 0x007C);
    RegWrite(synthesizer, 0x16, 0x0001);
    RegWrite(synthesizer, 0x15, 0x0409);
    RegWrite(synthesizer, 0x14, 0x4848);
    RegWrite(synthesizer, 0x13, 0x27B7);
    RegWrite(synthesizer, 0x12, 0x0064);
    RegWrite(synthesizer, 0x11, 0x0096);
    RegWrite(synthesizer, 0x10, 0x0080);
    RegWrite(synthesizer, 0x0F, 0x060E);
    RegWrite(synthesizer, 0x0E, 0x1820);
    RegWrite(synthesizer, 0x0D, 0x4000);
    // ��� ������� �������� ������� Fosc = 100��� Fpd =10���
    RegWrite(synthesizer, 0x0C, 0x5001); // ��������������� ������� �������� �� �����. ����. ��. ������� (11:0)
    RegWrite(synthesizer, 0x0B, 0xB058); // ��������������� ������� �������� ����� �����. ����. ��. ������� (11:4)
    RegWrite(synthesizer, 0x0A, 0x10F8);//0x10F8); // ��������������� ��������� ������� ������� (10:7)
    RegWrite(synthesizer, 0x09, 0x1004);//0x0004); // ��������� ������� (12���)
    LMX2572.Fpd = 10;
    RegWrite(synthesizer, 0x08, 0x2000);
    LMX2572.R7 = 0x00B2;//0x40B2
    RegWrite(synthesizer, 0x07, LMX2572.R7);
    RegWrite(synthesizer, 0x06, 0xC802);
    RegWrite(synthesizer, 0x05, 0x30C8);
    RegWrite(synthesizer, 0x04, 0x0A43);
    RegWrite(synthesizer, 0x03, 0x0782);
    RegWrite(synthesizer, 0x02, 0x0500);
    LMX2572.R1 = 0x0808;
    RegWrite(synthesizer, 0x01, LMX2572.R1);
    LMX2572.R0 = 0x211C;
    RegWrite(synthesizer, 0x00, LMX2572.R0);            
}
//==============================================================================
void LMX2572_Frequency_setting_MHz( unsigned char synthesizer, unsigned short int RFout)
{
    unsigned char Channel_Devider=0;
    unsigned short int Channel_Devider_buffer=0;
    unsigned short int Fvco=0;
    unsigned long int PLL_N=0;
    unsigned long int PLL_DEM = 1000;
    unsigned long int PLL_NUM = 0;
    
    unsigned short int PLL_N_H=0;
    unsigned short int PLL_N_L=0;    
    unsigned short int Channel_Devider_REG = 0;
    unsigned short int PLL_NUM_H = 0;
    unsigned short int PLL_NUM_L = 0;    
    
    double PLL_NUM_double = 0;    
    double Fvco_double = 0;
    double Fpd_double = 0;
    double PLL_N_double = 0;
    double PLL_DEM_double = 0;

    
    Channel_Devider_buffer = 6400/RFout;   
            
    if (Channel_Devider_buffer<2)  
    {Channel_Devider_buffer = 1; LMX2572.R45 = LMX2572.R45 | 0x0800; Channel_Devider = 0;}      // ?????
    else    
    { 
        if ( (Channel_Devider_buffer>=2) && (Channel_Devider_buffer<4)) 
        {Channel_Devider_buffer = 2; LMX2572.R45 = LMX2572.R45 & 0xE7FF; Channel_Devider = 0;
    }  
              else  {   if ( (Channel_Devider_buffer>=4) && (Channel_Devider_buffer<8)) {Channel_Devider_buffer = 4; LMX2572.R45 = LMX2572.R45 & 0xE7FF; Channel_Devider = 1;}
                        else    {if ( (Channel_Devider_buffer>=8) && (Channel_Devider_buffer<16)) {Channel_Devider_buffer = 8; LMX2572.R45 = LMX2572.R45 & 0xE7FF; Channel_Devider = 3;}
                                else    {if ( (Channel_Devider_buffer>=16) && (Channel_Devider_buffer<32)) {Channel_Devider_buffer = 16; LMX2572.R45 = LMX2572.R45 & 0xE7FF; Channel_Devider = 5;}
                                        else    {if ( (Channel_Devider_buffer>=32) && (Channel_Devider_buffer<64)) {Channel_Devider_buffer = 32; LMX2572.R45 = LMX2572.R45 & 0xE7FF; Channel_Devider = 7;}
                                                else    {if ( (Channel_Devider_buffer>=64) && (Channel_Devider_buffer<128)) {Channel_Devider_buffer = 64; LMX2572.R45 = LMX2572.R45 & 0xE7FF; Channel_Devider = 9;}
                                                        else    {if ( (Channel_Devider_buffer>=128) && (Channel_Devider_buffer<256)) {Channel_Devider_buffer = 128; LMX2572.R45 = LMX2572.R45 & 0xE7FF; Channel_Devider = 12;}
                                                                                                                                                else    {Channel_Devider_buffer = 256; LMX2572.R45 = LMX2572.R45 & 0xE7FF; Channel_Devider = 14;}
                                                                }

                                                        }

                                                }

                                        }

                                }

                    }

            }

    Fvco = Channel_Devider_buffer * RFout;
    
    PLL_N = Fvco/LMX2572.Fpd;
    
    Fvco_double = Fvco;
    Fpd_double = LMX2572.Fpd;
    PLL_N_double = PLL_N;
    PLL_DEM_double = PLL_DEM;
    PLL_NUM_double = (Fvco_double/Fpd_double - PLL_N_double)*PLL_DEM_double;
    
    PLL_NUM = PLL_NUM_double;
    
    Nop();
    Nop();
    Nop();
    
    Channel_Devider_REG = Channel_Devider;
    Channel_Devider_REG = (Channel_Devider_REG<<6);
    Channel_Devider_REG = Channel_Devider_REG & 0x07C0;
    Channel_Devider_REG = Channel_Devider_REG | 0x0800;
    
    PLL_N_L = PLL_N;
    PLL_N = PLL_N>>16;
    PLL_N_H = PLL_N_H & 0x0007;
    
    PLL_NUM_L = PLL_NUM;
    PLL_NUM = PLL_NUM>>16;
    PLL_NUM_H = PLL_NUM;
    
    Nop();
    Nop();
    Nop();
    
    RegWrite(synthesizer, 0x4B, Channel_Devider_REG);     // LMX2572.R75 Channel Devider 10:6
    RegWrite(synthesizer, 0x22, PLL_N_H);       // R34 PLL_N    2:0    
    RegWrite(synthesizer, 0x24, PLL_N_L);       // R36 PLL_N    15:0
    RegWrite(synthesizer, 0x2A, PLL_NUM_H);     // R42 PLL_NUM    15:0    
    RegWrite(synthesizer, 0x2B, PLL_NUM_L);     // R43 PLL_NUM    15:0
    RegWrite(synthesizer, 0x2D, LMX2572.R45);   // LMX2572.R45 VCO ��� Channel Devider
    RegWrite(synthesizer, 0x00, LMX2572.R0);    // ��� ������� ���������� VCO
}
//==============================================================================
// ���/���� RFoutB
// N = 1 - ���
// N = 0 - ����
void LMX2572_switch_RFoutB( unsigned char synthesizer, char N)  
{
    if ( N == 1 ) 
    {
        LMX2572.R44 = LMX2572.R44 & 0xFF7F;
    RegWrite(synthesizer, 0x2C, LMX2572.R44);
    }
    else 
    {
        LMX2572.R44 = LMX2572.R44 | 0x0080;
    RegWrite(synthesizer, 0x2C, LMX2572.R44);   
    }
//    Nop();
//    Nop();
//    Nop();     
}
//==============================================================================
// ���/���� RFoutA
// N = 1 - ���
// N = 0 - ����
void LMX2572_switch_RFoutA( unsigned char synthesizer, char N)  
{
    if ( N == 1 ) 
    {
        LMX2572.R44 = LMX2572.R44 & 0xFFBF;
    RegWrite(synthesizer, 0x2C, LMX2572.R44);
    }
    else 
    {
        LMX2572.R44 = LMX2572.R44 | 0x0040;
    RegWrite(synthesizer, 0x2C, (LMX2572.R44 | 0x0040) );    
    }
//    Nop();
//    Nop();
//    Nop();     
}
//==============================================================================
//  frequency - ��� ������� ������� ������������ � ������� LMX2572_Frequency_setting_MHz

//void LMX2572_Power_frequency_A( unsigned char synthesizer, unsigned short int frequency)
//{
//   unsigned short int REG_power;
//   signed long int count_power;
//   
//   if ( synthesizer == 3 )
//   { 
//       count_power = frequency;
//       if ( count_power <= 4750)
//       count_power = ( -3700 + 6 * count_power ) / 700;
//       else count_power = ( -54900 + 18 * count_power ) / 700;
//   }
/*   if ( synthesizer == 3 )
   { 
       count_power = frequency;
       if ( count_power <= 4750)
       count_power = ( count_power / cof72.k_1 ) + cof72.b_1;         // k = 116; b = -5; 
       else count_power = ( count_power / cof72.k_2 ) + cof72.b_2;  // k = 39; b = -78; 
   }  */ 
//    if ( count_power > 63 ) count_power = 63;   
//    Nop();
//    Nop();
//    Nop();   
//   REG_power = count_power;
//   REG_power = REG_power << 8;
//   REG_power = REG_power & 0x3F00;
//   LMX2572.R44 = LMX2572.R44 & 0xC0FF;
//   LMX2572.R44 = LMX2572.R44 | REG_power;
//    
//    RegWrite(synthesizer, 0x2C, LMX2572.R44);
//}

////==============================================================================
////  frequency - ��� ������� ������� ������������ � ������� LMX2572_Frequency_setting_MHz
//void LMX2572_Power_frequency_A( unsigned char synthesizer, unsigned short int frequency)
//{
//   unsigned short int REG_power;
//   signed long int count_power;
//   
//   if ( synthesizer == 3 )
//   { 
//       count_power = 63;
////       count_power = frequency;
////       if ( count_power <= 4750)
////       count_power = ( -3700 + 6 * count_power ) / 700;
////       else count_power = ( -54900 + 18 * count_power ) / 700;
//   }
////   if ( synthesizer == 3 )
////   { 
////       count_power = frequency;
////       if ( count_power <= 4750)
////       count_power = ( count_power / cof72.k_1 ) + cof72.b_1;         // k = 116; b = -5; 
////       else count_power = ( count_power / cof72.k_2 ) + cof72.b_2;  // k = 39; b = -78; 
////   }   
//    if ( count_power > 63 ) count_power = 63;   
//    Nop();
//    Nop();
//    Nop();   
//   REG_power = count_power;
//   REG_power = REG_power << 8;
//   REG_power = REG_power & 0x3F00;
//   LMX2572.R44 = LMX2572.R44 & 0xC0FF;
//   LMX2572.R44 = LMX2572.R44 | REG_power;
//    
//    RegWrite(synthesizer, 0x2C, LMX2572.R44);
//}
////==============================================================================
//==============================================================================
//  frequency - ��� ������� ������� ������������ � ������� LMX2572_Frequency_setting_MHz
void LMX2572_Power_frequency_A( unsigned char synthesizer, unsigned short int frequency)
{
   unsigned short int REG_power;
   static signed long int count_power;
   
   if ( synthesizer == 3 )
   { 
       count_power = (0.0165*frequency) - 44;
   }
    if ( count_power > 63 ) count_power = 63;   
   
   REG_power = count_power;
   REG_power = REG_power << 8;
   REG_power = REG_power & 0x3F00;
   LMX2572.R44 = LMX2572.R44 & 0xC0FF;
   LMX2572.R44 = LMX2572.R44 | REG_power;
    
    RegWrite(synthesizer, 0x2C, LMX2572.R44);
}
//==============================================================================
//==============================================================================
// power �� 0 �� 63 
void LMX2572_Power_A( unsigned char synthesizer, unsigned char power)
{
   unsigned short int REG_power;
   
    if ( power > 63 ) power = 63;   
   
   REG_power = power;
   REG_power = REG_power << 8;
   REG_power = REG_power & 0x3F00;
   LMX2572.R44 = LMX2572.R44 & 0xC0FF;
   LMX2572.R44 = LMX2572.R44 | REG_power;
    
    RegWrite(synthesizer, 0x2C, LMX2572.R44);
}
//==============================================================================
// power �� 0 �� 63 
void LMX2572_Power_B( unsigned char synthesizer, unsigned char power)
{  
    if ( power > 63 ) power = 63;
    LMX2572.R45 = LMX2572.R45 & 0xFFC0;    
    LMX2572.R45 = LMX2572.R45 | power;   
    
    RegWrite(synthesizer, 0x2D, LMX2572.R45);
}
//==============================================================================
// �������� ������� ������ � �� 2 � ������ � 
// N = 1 - ���
// N = 0 - ����
void LMX2572_switch_RFoutB_multiply_A( unsigned char synthesizer, char N)  
{
    if ( N == 1 ) 
    {
        LMX2572.R46 = LMX2572.R46 | 0x0001;
    RegWrite(synthesizer, 0x2E, LMX2572.R46);
    }
    else 
    {
        LMX2572.R46 = LMX2572.R46 & 0xFFFE;
    RegWrite(synthesizer, 0x2E, LMX2572.R46 );    
    }
//    Nop();
//    Nop();
//    Nop();     
}
//==============================================================================
//==============================================================================
void LMX2572_Frequency_setting_Hz_ForsedCalibration( uint8_t synthesizer, uint64_t RFout)
{
//_LATB2 = 1;    
    
    uint8_t Channel_Devider=0;
    uint64_t Channel_Devider_buffer=0;
    uint64_t Fvco=0;
    uint32_t PLL_N=0;
    uint32_t PLL_DEM = 0xFFFFFFFF;
    uint32_t PLL_NUM = 0;
    
    uint16_t PLL_N_H=0;
    uint16_t PLL_N_L=0;    
    uint16_t Channel_Devider_REG = 0;
    uint16_t PLL_NUM_H = 0;
    uint16_t PLL_NUM_L = 0;    
    uint16_t PLL_DEM_H = 0;
    uint16_t PLL_DEM_L = 0;     
    
    long double PLL_NUM_double = 0;    
    long double Fvco_double = 0;
    long double Fpd_double = 0;
    long double PLL_N_double = 0;
    long double PLL_DEM_double = 0; 
    
    long double VCO_CAPCTRL_STRT_double = 0;
    uint16_t VCO_CAPCTRL_STRT = 0;
    long double VCO_DACISET_STRT_double = 0;     
    uint16_t VCO_DACISET_STRT = 0;        
    
    Channel_Devider_buffer = 6400000000/RFout;   
            
    if (Channel_Devider_buffer<2)  {Channel_Devider_buffer = 1; LMX2572.R45 = LMX2572.R45 | 0x0800; LMX2572.R46 = LMX2572.R46 | 0x0001;Channel_Devider = 0;}      // ?????
    else    { if ( (Channel_Devider_buffer>=2) && (Channel_Devider_buffer<4)) {Channel_Devider_buffer = 2; LMX2572.R45 = LMX2572.R45 & 0xE7FF; LMX2572.R46 = LMX2572.R46 & 0xFFFE; Channel_Devider = 0;}  
              else  {   if ( (Channel_Devider_buffer>=4) && (Channel_Devider_buffer<8)) {Channel_Devider_buffer = 4; LMX2572.R45 = LMX2572.R45 & 0xE7FF; LMX2572.R46 = LMX2572.R46 & 0xFFFE; Channel_Devider = 1;}
                        else    {if ( (Channel_Devider_buffer>=8) && (Channel_Devider_buffer<16)) {Channel_Devider_buffer = 8; LMX2572.R45 = LMX2572.R45 & 0xE7FF; LMX2572.R46 = LMX2572.R46 & 0xFFFE; Channel_Devider = 3;}
                                else    {if ( (Channel_Devider_buffer>=16) && (Channel_Devider_buffer<32)) {Channel_Devider_buffer = 16; LMX2572.R45 = LMX2572.R45 & 0xE7FF; LMX2572.R46 = LMX2572.R46 & 0xFFFE; Channel_Devider = 5;}
                                        else    {if ( (Channel_Devider_buffer>=32) && (Channel_Devider_buffer<64)) {Channel_Devider_buffer = 32; LMX2572.R45 = LMX2572.R45 & 0xE7FF; LMX2572.R46 = LMX2572.R46 & 0xFFFE; Channel_Devider = 7;}
                                                else    {if ( (Channel_Devider_buffer>=64) && (Channel_Devider_buffer<128)) {Channel_Devider_buffer = 64; LMX2572.R45 = LMX2572.R45 & 0xE7FF; LMX2572.R46 = LMX2572.R46 & 0xFFFE; Channel_Devider = 9;}
                                                        else    {if ( (Channel_Devider_buffer>=128) && (Channel_Devider_buffer<256)) {Channel_Devider_buffer = 128; LMX2572.R45 = LMX2572.R45 & 0xE7FF; LMX2572.R46 = LMX2572.R46 & 0xFFFE; Channel_Devider = 12;}
                                                                                                                                                else    {Channel_Devider_buffer = 256; LMX2572.R45 = LMX2572.R45 & 0xE7FF; LMX2572.R46 = LMX2572.R46 & 0xFFFE; Channel_Devider = 14;}
                                                                }

                                                        }

                                                }

                                        }

                                }

                    }

            }

    Fvco = Channel_Devider_buffer * RFout;
    
    PLL_N = Fvco/(LMX2572.Fpd*MHz);
    
    Fvco_double = Fvco;
    Fpd_double = LMX2572.Fpd*MHz;
    PLL_N_double = PLL_N;
    PLL_DEM_double = PLL_DEM;    
    PLL_NUM_double = (Fvco_double/Fpd_double - PLL_N_double)*PLL_DEM_double;    
    
    PLL_NUM = (uint32_t)PLL_NUM_double;
    
    Channel_Devider_REG = Channel_Devider;
    Channel_Devider_REG = (Channel_Devider_REG<<6);
    Channel_Devider_REG = Channel_Devider_REG & 0x07C0;
    Channel_Devider_REG = Channel_Devider_REG | 0x0800;
    
    PLL_N_L = PLL_N;
    PLL_N = PLL_N>>16;
    PLL_N_H = PLL_N_H & 0x0007;
    
    PLL_NUM_L = PLL_NUM;
    PLL_NUM = PLL_NUM>>16;
    PLL_NUM_H = PLL_NUM;
    
    PLL_DEM_L = PLL_DEM;
    PLL_DEM = PLL_DEM>>16;
    PLL_DEM_H = PLL_DEM;    
    
        // ��������� VCO_SEL 
    if ( ( Fvco >= 3200000000LL ) && ( Fvco < 3650000000LL ) ) // VCO1
        {
        LMX2572.R20 = LMX2572.R20 & 0xC7FF;
        LMX2572.R20 = LMX2572.R20 | 0x0800;
        LMX2572.R20 = LMX2572.R20 | 0x0400; // ���������� VCO ������������ ����, ��������� � VCO_SEL.
        VCO_CAPCTRL_STRT_double = Cmin[1] - ( Fvco/1000000 - Fmin[1] )*( Cmin[1] - Cmax[1] )/( Fmax[1] - Fmin[1] );
        VCO_DACISET_STRT_double = Amin[1] - ( Fvco/1000000 - Fmin[1] )*( Amin[1] - Amax[1] )/( Fmax[1] - Fmin[1] );        
        }    
    if ( ( Fvco >= 3650000000LL ) && ( Fvco < 4200000000LL ) ) // VCO2
        {
        LMX2572.R20 = LMX2572.R20 & 0xC7FF;
        LMX2572.R20 = LMX2572.R20 | 0x1000;
        LMX2572.R20 = LMX2572.R20 | 0x0400; // ���������� VCO ������������ ����, ��������� � VCO_SEL.
        VCO_CAPCTRL_STRT_double = Cmin[2] - ( Fvco/1000000 - Fmin[2] )*( Cmin[2] - Cmax[2] )/( Fmax[2] - Fmin[2] ); 
        VCO_DACISET_STRT_double = Amin[2] - ( Fvco/1000000 - Fmin[2] )*( Amin[2] - Amax[2] )/( Fmax[2] - Fmin[2] );           
        } 
    if ( ( Fvco >= 4200000000LL ) && ( Fvco < 4650000000LL ) ) // VCO3
        {
        LMX2572.R20 = LMX2572.R20 & 0xC7FF;
        LMX2572.R20 = LMX2572.R20 | 0x1800;
        LMX2572.R20 = LMX2572.R20 | 0x0400; // ���������� VCO ������������ ����, ��������� � VCO_SEL.
        VCO_CAPCTRL_STRT_double = Cmin[3] - ( Fvco/1000000 - Fmin[3] )*( Cmin[3] - Cmax[3] )/( Fmax[3] - Fmin[3] );     
        VCO_DACISET_STRT_double = Amin[3] - ( Fvco/1000000 - Fmin[3] )*( Amin[3] - Amax[3] )/( Fmax[3] - Fmin[3] );           
        }
    if ( ( Fvco >= 4650000000LL ) && ( Fvco < 5200000000LL ) ) // VCO4
        {
        LMX2572.R20 = LMX2572.R20 & 0xC7FF;
        LMX2572.R20 = LMX2572.R20 | 0x2000;
        LMX2572.R20 = LMX2572.R20 | 0x0400; // ���������� VCO ������������ ����, ��������� � VCO_SEL.
        VCO_CAPCTRL_STRT_double = Cmin[4] - ( Fvco/1000000 - Fmin[4] )*( Cmin[4] - Cmax[4] )/( Fmax[4] - Fmin[4] );        
        VCO_DACISET_STRT_double = Amin[4] - ( Fvco/1000000 - Fmin[4] )*( Amin[4] - Amax[4] )/( Fmax[4] - Fmin[4] );           
        } 
    if ( ( Fvco >= 5200000000LL ) && ( Fvco < 5750000000LL ) ) // VCO5
        {
        LMX2572.R20 = LMX2572.R20 & 0xC7FF;
        LMX2572.R20 = LMX2572.R20 | 0x2800;
        LMX2572.R20 = LMX2572.R20 | 0x0400; // ���������� VCO ������������ ����, ��������� � VCO_SEL.
        VCO_CAPCTRL_STRT_double = Cmin[5] - ( Fvco/1000000 - Fmin[5] )*( Cmin[5] - Cmax[5] )/( Fmax[5] - Fmin[5] ); 
        VCO_DACISET_STRT_double = Amin[5] - ( Fvco/1000000 - Fmin[5] )*( Amin[5] - Amax[5] )/( Fmax[5] - Fmin[5] );           
        }   
    if ( ( Fvco >= 5750000000LL ) && ( Fvco < 6400000000LL ) ) // VCO6
        {
        LMX2572.R20 = LMX2572.R20 & 0xC7FF;
        LMX2572.R20 = LMX2572.R20 | 0x3000;
        LMX2572.R20 = LMX2572.R20 | 0x0400; // ���������� VCO ������������ ����, ��������� � VCO_SEL.
        VCO_CAPCTRL_STRT_double = Cmin[6] - ( Fvco/1000000 - Fmin[6] )*( Cmin[6] - Cmax[6] )/( Fmax[6] - Fmin[6] ); 
        VCO_DACISET_STRT_double = Amin[7] - ( Fvco/1000000 - Fmin[6] )*( Amin[6] - Amax[6] )/( Fmax[6] - Fmin[6] );            
        }    
    VCO_CAPCTRL_STRT = VCO_CAPCTRL_STRT_double;   
    VCO_CAPCTRL_STRT = VCO_CAPCTRL_STRT << 1;
    VCO_CAPCTRL_STRT = VCO_CAPCTRL_STRT & 0x01FE;
    LMX2572.R78 = LMX2572.R78 & 0xFE01;
    LMX2572.R78 = LMX2572.R78 | VCO_CAPCTRL_STRT;
    
    VCO_DACISET_STRT = VCO_DACISET_STRT_double;
    VCO_DACISET_STRT = VCO_DACISET_STRT & 0x01FF;
    LMX2572.R17 = LMX2572.R17 & 0xFE00;
    LMX2572.R17 = LMX2572.R17 | VCO_DACISET_STRT;
   
    LMX2572.R78 = LMX2572.R78 | 0x0200;         // QUICK_RECAL_EN ���������� ���������� � �������� ��������        
       
    RegWrite(synthesizer, 0x4B, Channel_Devider_REG);     // LMX2572.R75 Channel Devider 10:6
    RegWrite(synthesizer, 0x22, PLL_N_H);       // R34 PLL_N    2:0    
    RegWrite(synthesizer, 0x24, PLL_N_L);       // R36 PLL_N    15:0
    RegWrite(synthesizer, 0x26, PLL_DEM_H);     // R42 PLL_DEM    15:0    
    RegWrite(synthesizer, 0x27, PLL_DEM_L);     // R43 PLL_DEM    15:0     
    RegWrite(synthesizer, 0x2A, PLL_NUM_H);     // R42 PLL_NUM    15:0    
    RegWrite(synthesizer, 0x2B, PLL_NUM_L);     // R43 PLL_NUM    15:0   
    RegWrite(synthesizer, 0x2D, LMX2572.R45);   // LMX2572.R45 VCO ��� Channel Devider FOR ChannelA
    RegWrite(synthesizer, 0x2E, LMX2572.R46);   // LMX2572.R46 VCO ��� Channel Devider FOR ChannelB   
    
    RegWrite(synthesizer, 0x14, LMX2572.R20);   // VCO_SEL 
    RegWrite(synthesizer, 0x4E, LMX2572.R78);   // VCO_CAPCTRL_STRT    
    RegWrite(synthesizer, 0x11, LMX2572.R17);   // VCO_DACISET_STRT           
    
    RegWrite(synthesizer, 0x00, LMX2572.R0);    // ��� ������� ���������� VCO
    
//    _LATB2 = 0;    
}
//==============================================================================
//==============================================================================
void LMX2572_Frequency_setting_MHz_ForsedCalibration( uint8_t synthesizer, uint16_t RFout)
{
    uint8_t Channel_Devider=0;
    uint16_t Channel_Devider_buffer=0;
    uint16_t Fvco=0;
    uint32_t PLL_N=0;
    uint32_t PLL_DEM = 1000;
    uint32_t PLL_NUM = 0;
    
    uint16_t PLL_N_H=0;
    uint16_t PLL_N_L=0;    
    uint16_t Channel_Devider_REG = 0;
    uint16_t PLL_NUM_H = 0;
    uint16_t PLL_NUM_L = 0;    
    
    double PLL_NUM_double = 0;    
    double Fvco_double = 0;
    double Fpd_double = 0;
    double PLL_N_double = 0;
    double PLL_DEM_double = 0;
    
    long double VCO_CAPCTRL_STRT_double = 0;
    uint16_t VCO_CAPCTRL_STRT = 0;
    long double VCO_DACISET_STRT_double = 0;     
    uint16_t VCO_DACISET_STRT = 0;
    
//    _LATB2 = 1;

    Channel_Devider_buffer = 6400/RFout;   
            
    if (Channel_Devider_buffer<2)  {Channel_Devider_buffer = 1; LMX2572.R45 = LMX2572.R45 | 0x0800; LMX2572.R46 = LMX2572.R46 | 0x0001;Channel_Devider = 0;}      // ?????
    else    { if ( (Channel_Devider_buffer>=2) && (Channel_Devider_buffer<4)) {Channel_Devider_buffer = 2; LMX2572.R45 = LMX2572.R45 & 0xE7FF; LMX2572.R46 = LMX2572.R46 & 0xFFFE; Channel_Devider = 0;}  
              else  {   if ( (Channel_Devider_buffer>=4) && (Channel_Devider_buffer<8)) {Channel_Devider_buffer = 4; LMX2572.R45 = LMX2572.R45 & 0xE7FF; LMX2572.R46 = LMX2572.R46 & 0xFFFE; Channel_Devider = 1;}
                        else    {if ( (Channel_Devider_buffer>=8) && (Channel_Devider_buffer<16)) {Channel_Devider_buffer = 8; LMX2572.R45 = LMX2572.R45 & 0xE7FF; LMX2572.R46 = LMX2572.R46 & 0xFFFE; Channel_Devider = 3;}
                                else    {if ( (Channel_Devider_buffer>=16) && (Channel_Devider_buffer<32)) {Channel_Devider_buffer = 16; LMX2572.R45 = LMX2572.R45 & 0xE7FF; LMX2572.R46 = LMX2572.R46 & 0xFFFE; Channel_Devider = 5;}
                                        else    {if ( (Channel_Devider_buffer>=32) && (Channel_Devider_buffer<64)) {Channel_Devider_buffer = 32; LMX2572.R45 = LMX2572.R45 & 0xE7FF; LMX2572.R46 = LMX2572.R46 & 0xFFFE; Channel_Devider = 7;}
                                                else    {if ( (Channel_Devider_buffer>=64) && (Channel_Devider_buffer<128)) {Channel_Devider_buffer = 64; LMX2572.R45 = LMX2572.R45 & 0xE7FF; LMX2572.R46 = LMX2572.R46 & 0xFFFE; Channel_Devider = 9;}
                                                        else    {if ( (Channel_Devider_buffer>=128) && (Channel_Devider_buffer<256)) {Channel_Devider_buffer = 128; LMX2572.R45 = LMX2572.R45 & 0xE7FF; LMX2572.R46 = LMX2572.R46 & 0xFFFE; Channel_Devider = 12;}
                                                                                                                                                else    {Channel_Devider_buffer = 256; LMX2572.R45 = LMX2572.R45 & 0xE7FF; LMX2572.R46 = LMX2572.R46 & 0xFFFE; Channel_Devider = 14;}
                                                                }

                                                        }

                                                }

                                        }

                                }

                    }

            }

    Fvco = Channel_Devider_buffer * RFout;
      
    PLL_N = Fvco/LMX2572.Fpd;
    
    Fvco_double = Fvco;
    Fpd_double = LMX2572.Fpd;
    PLL_N_double = PLL_N;
    PLL_DEM_double = PLL_DEM;
    PLL_NUM_double = (Fvco_double/Fpd_double - PLL_N_double)*PLL_DEM_double;
    
    PLL_NUM = PLL_NUM_double;
    
    Channel_Devider_REG = Channel_Devider;
    Channel_Devider_REG = (Channel_Devider_REG<<6);
    Channel_Devider_REG = Channel_Devider_REG & 0x07C0;
    Channel_Devider_REG = Channel_Devider_REG | 0x0800;
    
    PLL_N_L = PLL_N;
    PLL_N = PLL_N>>16;
    PLL_N_H = PLL_N_H & 0x0007;
    
    PLL_NUM_L = PLL_NUM;
    PLL_NUM = PLL_NUM>>16;
    PLL_NUM_H = PLL_NUM;
    
    // ��������� VCO_SEL 
    if ( ( Fvco >= 3200 ) && ( Fvco < 3650) ) // VCO1
        {
        LMX2572.R20 = LMX2572.R20 & 0xC7FF;
        LMX2572.R20 = LMX2572.R20 | 0x0800;
        LMX2572.R20 = LMX2572.R20 | 0x0400; // ���������� VCO ������������ ����, ��������� � VCO_SEL.
        VCO_CAPCTRL_STRT_double = Cmin[1] - ( Fvco - Fmin[1] )*( Cmin[1] - Cmax[1] )/( Fmax[1] - Fmin[1] );
        VCO_DACISET_STRT_double = Amin[1] - ( Fvco - Fmin[1] )*( Amin[1] - Amax[1] )/( Fmax[1] - Fmin[1] );        
        }    
    if ( ( Fvco >= 3650 ) && ( Fvco < 4200) ) // VCO2
        {
        LMX2572.R20 = LMX2572.R20 & 0xC7FF;
        LMX2572.R20 = LMX2572.R20 | 0x1000;
        LMX2572.R20 = LMX2572.R20 | 0x0400; // ���������� VCO ������������ ����, ��������� � VCO_SEL.
        VCO_CAPCTRL_STRT_double = Cmin[2] - ( Fvco - Fmin[2] )*( Cmin[2] - Cmax[2] )/( Fmax[2] - Fmin[2] ); 
        VCO_DACISET_STRT_double = Amin[2] - ( Fvco - Fmin[2] )*( Amin[2] - Amax[2] )/( Fmax[2] - Fmin[2] );           
        } 
    if ( ( Fvco >= 4200 ) && ( Fvco < 4650) ) // VCO3
        {
        LMX2572.R20 = LMX2572.R20 & 0xC7FF;
        LMX2572.R20 = LMX2572.R20 | 0x1800;
        LMX2572.R20 = LMX2572.R20 | 0x0400; // ���������� VCO ������������ ����, ��������� � VCO_SEL.
        VCO_CAPCTRL_STRT_double = Cmin[3] - ( Fvco - Fmin[3] )*( Cmin[3] - Cmax[3] )/( Fmax[3] - Fmin[3] );     
        VCO_DACISET_STRT_double = Amin[3] - ( Fvco - Fmin[3] )*( Amin[3] - Amax[3] )/( Fmax[3] - Fmin[3] );           
        }
    if ( ( Fvco >= 4650 ) && ( Fvco < 5200) ) // VCO4
        {
        LMX2572.R20 = LMX2572.R20 & 0xC7FF;
        LMX2572.R20 = LMX2572.R20 | 0x2000;
        LMX2572.R20 = LMX2572.R20 | 0x0400; // ���������� VCO ������������ ����, ��������� � VCO_SEL.
        VCO_CAPCTRL_STRT_double = Cmin[4] - ( Fvco - Fmin[4] )*( Cmin[4] - Cmax[4] )/( Fmax[4] - Fmin[4] );        
        VCO_DACISET_STRT_double = Amin[4] - ( Fvco - Fmin[4] )*( Amin[4] - Amax[4] )/( Fmax[4] - Fmin[4] );           
        } 
    if ( ( Fvco >= 5200 ) && ( Fvco < 5750) ) // VCO5
        {
        LMX2572.R20 = LMX2572.R20 & 0xC7FF;
        LMX2572.R20 = LMX2572.R20 | 0x2800;
        LMX2572.R20 = LMX2572.R20 | 0x0400; // ���������� VCO ������������ ����, ��������� � VCO_SEL.
        VCO_CAPCTRL_STRT_double = Cmin[5] - ( Fvco - Fmin[5] )*( Cmin[5] - Cmax[5] )/( Fmax[5] - Fmin[5] ); 
        VCO_DACISET_STRT_double = Amin[5] - ( Fvco - Fmin[5] )*( Amin[5] - Amax[5] )/( Fmax[5] - Fmin[5] );           
        }   
    if ( ( Fvco >= 5750 ) && ( Fvco < 6400) ) // VCO6
        {
        LMX2572.R20 = LMX2572.R20 & 0xC7FF;
        LMX2572.R20 = LMX2572.R20 | 0x3000;
        LMX2572.R20 = LMX2572.R20 | 0x0400; // ���������� VCO ������������ ����, ��������� � VCO_SEL.
        VCO_CAPCTRL_STRT_double = Cmin[6] - ( Fvco - Fmin[6] )*( Cmin[6] - Cmax[6] )/( Fmax[6] - Fmin[6] );   
        VCO_DACISET_STRT_double = Amin[6] - ( Fvco - Fmin[6] )*( Amin[6] - Amax[6] )/( Fmax[6] - Fmin[6] );           
        }    
    VCO_CAPCTRL_STRT = VCO_CAPCTRL_STRT_double;   
    VCO_CAPCTRL_STRT = VCO_CAPCTRL_STRT << 1;
    VCO_CAPCTRL_STRT = VCO_CAPCTRL_STRT & 0x01FE;
    LMX2572.R78 = LMX2572.R78 & 0xFE01;
    LMX2572.R78 = LMX2572.R78 | VCO_CAPCTRL_STRT;
    
    VCO_DACISET_STRT = VCO_DACISET_STRT_double;
    VCO_DACISET_STRT = VCO_DACISET_STRT & 0x01FF;
    LMX2572.R17 = LMX2572.R17 & 0xFE00;
    LMX2572.R17 = LMX2572.R17 | VCO_DACISET_STRT;
     LED1_ON;
    RegWrite(synthesizer, 0x4B, Channel_Devider_REG);     // LMX2572.R75 Channel Devider 10:6
    RegWrite(synthesizer, 0x22, PLL_N_H);       // R34 PLL_N    2:0    
    RegWrite(synthesizer, 0x24, PLL_N_L);       // R36 PLL_N    15:0
    RegWrite(synthesizer, 0x2A, PLL_NUM_H);     // R42 PLL_NUM    15:0    
    RegWrite(synthesizer, 0x2B, PLL_NUM_L);     // R43 PLL_NUM    15:0
    RegWrite(synthesizer, 0x2D, LMX2572.R45);   // LMX2572.R45 VCO ��� Channel Devider FOR ChannelA
    RegWrite(synthesizer, 0x2E, LMX2572.R46);   // LMX2572.R46 VCO ��� Channel Devider FOR ChannelB   
    
    RegWrite(synthesizer, 0x14, LMX2572.R20);   // VCO_SEL 
    RegWrite(synthesizer, 0x4E, LMX2572.R78);   // VCO_CAPCTRL_STRT    
    RegWrite(synthesizer, 0x11, LMX2572.R17);   // VCO_DACISET_STRT
    /*
    RegWrite(synthesizer, 0x14, 0x4848);   // VCO_SEL      
    RegWrite(synthesizer, 0x4E, 0x0271);   // 0x0271
    RegWrite(synthesizer, 0x11, 0x0089);   //     
     */    
     LED1_OFF;   RegWrite(synthesizer, 0x00, LMX2572.R0);    // ��� ������� ���������� VCO

}
//==============================================================================

