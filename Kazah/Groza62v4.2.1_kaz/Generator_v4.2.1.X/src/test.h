/* 
 * File:   
 * Author: 
 * Comments:
 * Revision history: 
 */

// This is a guard condition so that contents of this file are not included
// more than once.  
#ifndef XC_test_H
#define	XC_test_H

#include <xc.h> // include processor files - each processor file is guarded.  
       
void test(void);
void test100_500(void);
void test500_2500(void);
void test2500_6000(void);
void testGNSS_init(void);

#endif    //XC_channel_H