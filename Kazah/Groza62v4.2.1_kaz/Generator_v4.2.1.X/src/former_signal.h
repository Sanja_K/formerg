#ifndef _FORMER_SIGNAL_H    /* Guard against multiple inclusion */
#define _FORMER_SIGNAL_H

/* Convert frequency from kHz to FTW */
#define KHZ_TO_FTW(freq)    (((uint64_t)(freq)) * 4294 + \
                            ((((uint64_t)(freq)) * 1981) >> 11))

/* Convert frequency from Hz to FTW */
#define HZ_TO_FTW(freq)     (((uint64_t)(freq)) * 4 + \
                            ((((uint64_t)(freq)) * 604) >> 11))

/* Convert frequency from kHz to Hz */
#define KHZ_TO_HZ(freq)     ((freq) * 1000)

/* 200 ns per one frequency */
#define DIGITAL_RAMP_RATE   0x00320032
#define LFM_POINT_TIME      200 //in ns
#define RAMP_RATE           0x00010001  //0x000F000F AD9914

uint8_t former_init_1(void);
uint8_t former_init_2(void);
uint8_t former_init_3(void);
void ad9910_deinit_1(void);
void ad9910_deinit_2(void);
void ad9914_deinit_3(void);
void repeat_init_former(void);

void set_mode_lfm_1(uint32_t freq, uint32_t dev, uint32_t speed);
void set_mode_lfm_2(uint32_t freq, uint32_t dev, uint32_t speed);
void set_mode_lfm_3(uint64_t freq, uint64_t dev, uint32_t speed);
void set_mode_freq_1(uint32_t freq);
void set_mode_freq_2(uint32_t freq);
void set_mode_freq_3(uint64_t freq);
#endif /* _FORMER_SIGNAL_H */

/* *****************************************************************************
 End of File
 */
